/**
 *	Connection oriented routing
 *	Copyright (C) 2007-2023 Michael Blizek
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License
 *	as published by the Free Software Foundation; either version 2
 *	of the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 */

#include "cor.h"

/**
 * neighbor congestion window:
 * increment by 4096 every round trip if more that 2/3 of cwin is used
 *
 * in case of packet loss decrease by 1/4:
 * - <= 1/8 immediately and
 * - <= 1/4 during the next round trip
 *
 * in case of multiple packet loss events, do not decrement more than once per
 * round trip
 *
 * when acks are received, do not free the entire window at once to avoid send
 * bursts
 */

void cor_nbcongwin_data_retransmitted(struct cor_neighbor *nb,
		__u64 bytes_sent)
{
	__u64 min_cwin = cor_mss_conndata(nb, 0) * 2 << NBCONGWIN_SHIFT;
	__u64 cwin;

	unsigned long iflags;

	spin_lock_irqsave(&nb->nbcongwin.lock, iflags);

#ifdef FIXED_NBCONGWIN

	atomic64_set(&nb->nbcongwin.cwin, FIXED_NBCONGWIN << NBCONGWIN_SHIFT);

#else

	cwin = atomic64_read(&nb->nbcongwin.cwin);

	/* printk(KERN_ERR "retrans %llu %llu\n", cwin >> NBCONGWIN_SHIFT,
			get_bufspace_used());
	print_conn_bufstats(nb); */

	BUG_ON(nb->nbcongwin.cwin_shrinkto > cwin);

	if (nb->nbcongwin.cwin_shrinkto == cwin) {
		cwin = max(min_cwin, cwin - cwin / 16);
		atomic64_set(&nb->nbcongwin.cwin, cwin);
	}

	nb->nbcongwin.cwin_shrinkto = max(min_cwin, cwin - cwin / 16);

#endif

	spin_unlock_irqrestore(&nb->nbcongwin.lock, iflags);
}

static void cor_nbcongwin_resume(struct cor_neighbor *nb_cwlocked)
{
	struct cor_dev *cd = nb_cwlocked->cd;

	__u64 data_intransit = atomic64_read(
			&nb_cwlocked->nbcongwin.data_intransit);
	__u64 cwin = atomic64_read(&nb_cwlocked->nbcongwin.cwin);

	if (data_intransit >= cwin >> NBCONGWIN_SHIFT)
		return;

	spin_lock(&cd->send_queue.qlock);
	if (nb_cwlocked->rb.in_queue == RB_INQUEUE_NBCONGWIN) {
		if (nb_cwlocked->conns_waiting.cnt == 0) {
			nb_cwlocked->rb.in_queue = RB_INQUEUE_FALSE;
		} else {
			_cor_dev_queue_enqueue(cd, &nb_cwlocked->rb, 0, ns_to_ktime(0),
					QOS_CALLER_NEIGHBOR, 1, 0);
		}
	}
	spin_unlock(&cd->send_queue.qlock);
}

static void cor_nbcongwin_sched_timer(struct cor_neighbor *nb_cwlocked)
{
	if (unlikely(cor_get_neigh_state(nb_cwlocked) == NEIGHBOR_STATE_KILLED))
		return;

	hrtimer_start(&nb_cwlocked->nbcongwin.acked_timer, ms_to_ktime(3),
			HRTIMER_MODE_REL);
}

static int cor_nbcongwin_update_intransit(struct cor_neighbor *nb_cwlocked,
		ktime_t now)
{
	__u64 bytes;

	__s64 time_passed = ktime_to_us(ktime_sub(now,
			nb_cwlocked->nbcongwin.acked_refresh_time));
	__s64 time_needed = ktime_to_us(ktime_sub(
			nb_cwlocked->nbcongwin.acked_timeout_time,
			nb_cwlocked->nbcongwin.acked_refresh_time));

	if (time_passed < 0 || time_needed <= 0 || time_passed >= time_needed) {
		bytes = nb_cwlocked->nbcongwin.data_acked;
		nb_cwlocked->nbcongwin.acked_timeout_time = now;
	} else if (unlikely(nb_cwlocked->nbcongwin.data_acked >= (1LL << 40))) {
		bytes = ((__u64) time_passed) * div64_u64(
				nb_cwlocked->nbcongwin.data_acked,
				(__u64) time_needed);
	} else {
		while (time_passed >= (1LL << 24)) {
			time_needed = time_needed >> 1;
			time_passed = time_passed >> 1;
		}
		bytes = div64_u64(nb_cwlocked->nbcongwin.data_acked *
				(__u64) time_passed, (__u64) time_needed);
	}

	if (bytes == 0)
		return 0;

	nb_cwlocked->nbcongwin.acked_refresh_time = now;
	nb_cwlocked->nbcongwin.data_acked -= bytes;

	/* printk(KERN_ERR "ack delayed %llu %llu\n",
			atomic64_read(&nb_cwlocked->nbcongwin.data_intransit),
			bytes); */

	cor_update_atomic_sum(&nb_cwlocked->nbcongwin.data_intransit, bytes, 0);

	return 1;
}

enum hrtimer_restart cor_nbcongwin_hrtimerfunc(struct hrtimer *nb_congwin_timer)
{
	struct cor_neighbor *nb = container_of(nb_congwin_timer,
				struct cor_neighbor,
				nbcongwin.acked_timer);
	unsigned long iflags;
	int resume;

	spin_lock_irqsave(&nb->nbcongwin.lock, iflags);

	resume = cor_nbcongwin_update_intransit(nb, ktime_get());

	if (nb->nbcongwin.data_acked > 0)
		cor_nbcongwin_sched_timer(nb);

	if (resume)
		cor_nbcongwin_resume(nb);

	spin_unlock_irqrestore(&nb->nbcongwin.lock, iflags);

	return HRTIMER_NORESTART;
}

static void cor_nbcongwin_update_cwin(struct cor_neighbor *nb_cwlocked,
		__u64 bytes_acked)
{
#ifdef FIXED_NBCONGWIN

	atomic64_set(&nb_cwlocked->nbcongwin.cwin,
			FIXED_NBCONGWIN << NBCONGWIN_SHIFT);

#else

	__u64 data_intransit = atomic64_read(
			&nb_cwlocked->nbcongwin.data_intransit);
	__u64 cwin = atomic64_read(&nb_cwlocked->nbcongwin.cwin);
	__u64 CWIN_MUL = (1 << NBCONGWIN_SHIFT);
	__u32 INCR_PER_RTT = 8192;

	__u64 cwin_tmp;
	__u64 incrby;

	if (nb_cwlocked->nbcongwin.cwin_shrinkto < cwin) {
		__u64 shrinkby = (bytes_acked << (NBCONGWIN_SHIFT - 2));

		BUILD_BUG_ON(NBCONGWIN_SHIFT < 2);

		if (unlikely(shrinkby > cwin))
			cwin = 0;
		else
			cwin -= shrinkby;

		if (cwin < nb_cwlocked->nbcongwin.cwin_shrinkto)
			cwin = nb_cwlocked->nbcongwin.cwin_shrinkto;
	}


	if (cwin * 2 > data_intransit * CWIN_MUL * 3)
		goto out;

	cwin_tmp = max(cwin, bytes_acked << NBCONGWIN_SHIFT);

	if (unlikely(bytes_acked >= U64_MAX / INCR_PER_RTT / CWIN_MUL))
		incrby = div64_u64(bytes_acked * INCR_PER_RTT,
				cwin_tmp / CWIN_MUL / CWIN_MUL);
	else if (unlikely(bytes_acked >=
			U64_MAX / INCR_PER_RTT / CWIN_MUL / CWIN_MUL))
		incrby = div64_u64(bytes_acked * INCR_PER_RTT * CWIN_MUL,
				cwin_tmp / CWIN_MUL);
	else
		incrby = div64_u64(bytes_acked * INCR_PER_RTT * CWIN_MUL *
				CWIN_MUL, cwin_tmp);

	BUG_ON(incrby > INCR_PER_RTT * CWIN_MUL);

	if (unlikely(cwin + incrby < cwin))
		cwin = U64_MAX;
	else
		cwin += incrby;

	if (unlikely(nb_cwlocked->nbcongwin.cwin_shrinkto + incrby <
			nb_cwlocked->nbcongwin.cwin_shrinkto))
		nb_cwlocked->nbcongwin.cwin_shrinkto = U64_MAX;
	else
		nb_cwlocked->nbcongwin.cwin_shrinkto += incrby;

out:
	atomic64_set(&nb_cwlocked->nbcongwin.cwin, cwin);
#endif
}

void cor_nbcongwin_data_acked(struct cor_neighbor *nb, __u64 bytes_acked)
{
	unsigned long iflags;
	ktime_t now;
	__u64 delay_us = atomic_read(&nb->max_remote_ackconn_delay_us);
	__u64 ack_now;
	__u64 ack_delayed;

	if (delay_us == 0) {
		ack_now = bytes_acked;
	} else {
		__u32 mss = cor_mss_conndata(nb, 1);
		ack_now = mss + bytes_acked / 8;
		if (ack_now > bytes_acked)
			ack_now = bytes_acked;
	}
	ack_delayed = bytes_acked - ack_now;

	spin_lock_irqsave(&nb->nbcongwin.lock, iflags);

	now = ktime_get();

	cor_nbcongwin_update_intransit(nb, now);

	/* printk(KERN_ERR "ack now %llu %llu %llu\n",
			atomic64_read(&nb->nbcongwin.data_intransit),
			ack_now, ack_delayed); */

	cor_nbcongwin_update_cwin(nb, bytes_acked);

	cor_update_atomic_sum(&nb->nbcongwin.data_intransit, ack_now, 0);

	if (ack_delayed != 0) {
		nb->nbcongwin.acked_refresh_time = now;
		nb->nbcongwin.acked_timeout_time =
				ktime_add_us(now, delay_us);
		nb->nbcongwin.data_acked += ack_delayed;
		if (nb->nbcongwin.data_acked == ack_delayed)
			cor_nbcongwin_sched_timer(nb);
	}

	cor_nbcongwin_resume(nb);

	spin_unlock_irqrestore(&nb->nbcongwin.lock, iflags);
}

void cor_nbcongwin_data_sent(struct cor_neighbor *nb, __u32 bytes_sent)
{
	atomic64_add(bytes_sent, &nb->nbcongwin.data_intransit);
}

int cor_nbcongwin_send_allowed(struct cor_neighbor *nb)
{
	unsigned long iflags;
	int ret = 1;
	struct cor_dev *cd = nb->cd;
	int krefput_cd = 0;

#ifdef COR_NBCONGWIN
	if (atomic64_read(&nb->nbcongwin.data_intransit) <=
			atomic64_read(&nb->nbcongwin.cwin) >> NBCONGWIN_SHIFT)
		return 1;

	spin_lock_irqsave(&nb->nbcongwin.lock, iflags);

	if (atomic64_read(&nb->nbcongwin.data_intransit) <=
			atomic64_read(&nb->nbcongwin.cwin) >> NBCONGWIN_SHIFT)
		goto out_ok;

	ret = 0;

	spin_lock(&cd->send_queue.qlock);
	if (nb->rb.in_queue == RB_INQUEUE_FALSE) {
		nb->rb.in_queue = RB_INQUEUE_NBCONGWIN;
	} else if (nb->rb.in_queue == RB_INQUEUE_TRUE) {
		list_del(&nb->rb.lh);
		cor_nb_kref_put_bug(nb, "qos_queue_nb");
		nb->rb.in_queue = RB_INQUEUE_NBCONGWIN;
		BUG_ON(nb->conns_waiting.cnt > cd->send_queue.numconns);
		cd->send_queue.numconns -= nb->conns_waiting.cnt;
		BUG_ON(nb->conns_waiting.priority_sum > cd->send_queue.priority_sum);
		cd->send_queue.priority_sum -= nb->conns_waiting.priority_sum;

		if (list_empty(&cd->send_queue.neighbors_waiting) &&
				list_empty(&cd->send_queue.neighbors_waiting_nextpass)) {
			BUG_ON(cd->send_queue.numconns != 0);
			BUG_ON(cd->send_queue.priority_sum != 0);
		}

		krefput_cd = 1;

		cor_dev_queue_set_congstatus(cd);
	} else if (nb->rb.in_queue == RB_INQUEUE_NBCONGWIN) {
	} else {
		BUG();
	}
	spin_unlock(&cd->send_queue.qlock);

	if (krefput_cd != 0)
		kref_put(&cd->ref, cor_dev_free);

out_ok:
	spin_unlock_irqrestore(&nb->nbcongwin.lock, iflags);

	return ret;
#else
	return 1;
#endif
}
