/**
 *	Connection oriented routing
 *	Copyright (C) 2007-2021 Michael Blizek
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License
 *	as published by the Free Software Foundation; either version 2
 *	of the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 */

 #include "cor.h"

static DEFINE_SPINLOCK(cor_announce_snd_lock);
static LIST_HEAD(cor_announce_out_list);

static int ___cor_send_announce(struct sk_buff *skb, int *sent)
{
	int rc;
	struct cor_dev *cd = cor_dev_get(skb->dev);

	if (cd == 0) {
		kfree_skb(skb);
		*sent = 1;
		return NET_XMIT_SUCCESS;
	}

	rc = cor_dev_queue_xmit(skb, cd, QOS_CALLER_ANNOUNCE);
	kref_put(&cd->ref, cor_dev_free);
	if (rc != NET_XMIT_DROP)
		*sent = 1;

	return rc;
}

static int __cor_send_announce(struct cor_announce_data *ann, int *sent)
{
	__u32 len;
	__u32 offset = 0;

	char *msg = 0;

	struct sk_buff *skb;
	__u32 headroom;

	__u8 has_addr;
	__be64 addr;

	headroom = LL_RESERVED_SPACE(ann->dev) +
			ann->dev->needed_tailroom;

	spin_lock_bh(&cor_local_addr_lock);
	has_addr = cor_local_has_addr;
	addr = cor_local_addr;
	spin_unlock_bh(&cor_local_addr_lock);

	if (has_addr == 0)
		len = 1 + 4 + 8 + 4;
	else
		len = 1 + 4 + 8 + 4 + 8;

	skb = alloc_skb(headroom + len, GFP_ATOMIC);
	if (unlikely(skb == 0))
		return NET_XMIT_SUCCESS;

	skb->protocol = htons(ETH_P_COR);
	skb->dev = ann->dev;
	skb_reserve(skb, headroom);

	#warning net_device locking? (other places too)
	if (unlikely(dev_hard_header(skb, ann->dev, ETH_P_COR,
			ann->mac, ann->dev->dev_addr, skb->len) < 0))
		goto out_err;

	skb_reset_network_header(skb);

	msg = skb_put(skb, len);
	if (unlikely(msg == 0))
		goto out_err;

	msg[0] = PACKET_TYPE_ANNOUNCE;
	offset++;

	cor_put_be32(msg + offset, cor_local_addr_sessionid); /* sessionid */
	offset += 4;

	cor_put_u16(msg + offset, ANNCMD_VERSION); /* command */
	offset += 2;
	cor_put_u16(msg + offset, 4); /* command length */
	offset += 2;
	cor_put_u16(msg + offset, 0); /* version */
	offset += 2;
	cor_put_u16(msg + offset, 0); /* minversion */
	offset += 2;

	if (has_addr == 0) {
		cor_put_u16(msg + offset, ANNCMD_NOADDR); /* command */
		offset += 2;
		cor_put_u16(msg + offset, 0); /* command length */
		offset += 2;
	} else {
		cor_put_u16(msg + offset, ANNCMD_ADDR); /* command */
		offset += 2;
		cor_put_u16(msg + offset, 8); /* command length */
		offset += 2;
		cor_put_be64(msg + offset, addr); /* addr */
		offset += 8;
	}

	BUILD_BUG_ON(offset != len);

	return ___cor_send_announce(skb, sent);

	if (0) {
out_err:
		kfree_skb(skb);
		return NET_XMIT_SUCCESS;
	}
}

void cor_announce_data_free(struct kref *ref)
{
	struct cor_announce_data *ann = container_of(ref,
			struct cor_announce_data, ref);
	kfree(ann);
}

int _cor_send_announce(struct cor_announce_data *ann, int fromqos, int *sent)
{
	int reschedule = 0;
	int rc = 0;

	spin_lock_bh(&cor_announce_snd_lock);

	if (unlikely(ann->dev == 0)) {
		rc = NET_XMIT_SUCCESS;
		goto out;
	}

	if (cor_is_device_configurated(ann->dev) == 0)
		rc = NET_XMIT_SUCCESS;
	else if (fromqos == 0 &&
			cor_qos_fastsend_allowed_announce(ann->dev) == 0)
		rc = NET_XMIT_DROP;
	else
		rc = __cor_send_announce(ann, sent);

	if (rc != NET_XMIT_DROP && ann->type != ANNOUNCE_TYPE_BROADCAST) {
		ann->sndcnt++;
		reschedule = (ann->sndcnt < ANNOUNCE_SEND_UNICAST_MAXCNT ?
				1 : 0);

		if (reschedule == 0) {
			dev_put(ann->dev);
			ann->dev = 0;

			list_del(&ann->lh);
			kref_put(&ann->ref, cor_kreffree_bug);
		}
	} else {
		reschedule = 1;
	}

out:
	spin_unlock_bh(&cor_announce_snd_lock);

	if (unlikely(reschedule == 0)) {
		kref_put(&ann->ref, cor_announce_data_free);
	} else if (rc == NET_XMIT_DROP) {
		if (fromqos == 0) {
			struct cor_dev *cd = cor_dev_get(ann->dev);

			if (cd != 0) {
				cor_dev_queue_enqueue(cd, &ann->rb, 0,
						ns_to_ktime(0),
						QOS_CALLER_ANNOUNCE, 0);
				kref_put(&cd->ref, cor_dev_free);
			}
		}
	} else {
		schedule_delayed_work(&ann->announce_work, msecs_to_jiffies(
				ANNOUNCE_SEND_PACKETINTELVAL_MS));
	}

	if (rc != NET_XMIT_SUCCESS)
		return QOS_RESUME_CONG;

	return QOS_RESUME_DONE;
}

static void cor_send_announce(struct work_struct *work)
{
	struct cor_announce_data *ann = container_of(to_delayed_work(work),
			struct cor_announce_data, announce_work);
	int sent = 0;

	_cor_send_announce(ann, 0, &sent);
}

void cor_announce_send_start(struct net_device *dev, char *mac, int type)
{
	struct cor_announce_data *ann;

	ann = kmalloc(sizeof(struct cor_announce_data), GFP_KERNEL);

	if (unlikely(ann == 0)) {
		printk(KERN_ERR "cor cannot allocate memory for sending announces\n");
		return;
	}

	memset(ann, 0, sizeof(struct cor_announce_data));

	kref_init(&ann->ref);

	dev_hold(dev);
	ann->dev = dev;
	memcpy(ann->mac, mac, MAX_ADDR_LEN);
	ann->type = type;

	spin_lock_bh(&cor_announce_snd_lock);
	list_add_tail(&ann->lh, &cor_announce_out_list);
	spin_unlock_bh(&cor_announce_snd_lock);

	INIT_DELAYED_WORK(&ann->announce_work, cor_send_announce);
	kref_get(&ann->ref);
	schedule_delayed_work(&ann->announce_work, 1);
}

void cor_announce_send_stop(struct net_device *dev, char *mac, int type)
{
	struct list_head *lh;

	spin_lock_bh(&cor_announce_snd_lock);

	lh = cor_announce_out_list.next;
	while (lh != &cor_announce_out_list) {
		struct cor_announce_data *ann = container_of(lh,
				struct cor_announce_data, lh);

		lh = lh->next;


		if (dev != 0 && (ann->dev != dev || (
				type != ANNOUNCE_TYPE_BROADCAST && (
				ann->type != type ||
				memcmp(ann->mac, mac, MAX_ADDR_LEN) != 0))))
			continue;

		dev_put(ann->dev);
		ann->dev = 0;

		list_del(&ann->lh);
		kref_put(&ann->ref, cor_kreffree_bug);
	}

	spin_unlock_bh(&cor_announce_snd_lock);
}

MODULE_LICENSE("GPL");
