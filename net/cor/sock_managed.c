/**
 *	Connection oriented routing
 *	Copyright (C) 2007-2021 Michael Blizek
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License
 *	as published by the Free Software Foundation; either version 2
 *	of the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 */

#include <net/sock.h>
#include <linux/net.h>
#include <linux/uaccess.h>

#include <linux/crc32c.h>
#include <linux/err.h>
#include <linux/scatterlist.h>

#include "cor.h"


static DEFINE_SPINLOCK(cor_cookie_gen);

static DEFINE_SPINLOCK(cor_sock_cookie_lock);
static struct rb_root cor_sock_cookie_rb;

#warning todo which lock protects sk_err, sk_rcvtimeo and sk_sndtimeo ???


struct cor_sock *cor_get_sock_by_cookie(__be64 cookie)
{
	struct rb_node *n = 0;
	struct cor_sock *ret = 0;

	spin_lock_bh(&cor_sock_cookie_lock);

	n = cor_sock_cookie_rb.rb_node;

	while (likely(n != 0) && ret == 0) {
		struct cor_sock *cs = container_of(n, struct cor_sock,
				data.conn_managed.rbn);

		BUG_ON(cs->type != CS_TYPE_CONN_MANAGED);
		BUG_ON(cs->data.conn_managed.cookie == 0);

		if (cookie < cs->data.conn_managed.cookie)
			n = n->rb_left;
		else if (cookie > cs->data.conn_managed.cookie)
			n = n->rb_right;
		else
			ret = cs;
	}

	if (ret != 0)
		kref_get(&ret->ref);

	spin_unlock_bh(&cor_sock_cookie_lock);

	return ret;
}

static void cor_insert_sock_cookie(struct cor_sock *ins_l)
{
	struct rb_root *root;
	struct rb_node **p;
	struct rb_node *parent = 0;

	__u64 cookie = ins_l->data.conn_managed.cookie;

	BUG_ON(ins_l->type != CS_TYPE_CONN_MANAGED);
	BUG_ON(ins_l->data.conn_managed.cookie == 0);

	spin_lock_bh(&cor_sock_cookie_lock);

	root = &cor_sock_cookie_rb;
	p = &root->rb_node;

	while ((*p) != 0) {
		struct cor_sock *curr = container_of(*p,
				struct cor_sock, data.conn_managed.rbn);

		BUG_ON(curr->type != CS_TYPE_CONN_MANAGED);
		BUG_ON(curr->data.conn_managed.cookie == 0);

		parent = *p;
		if (unlikely(cookie == curr->data.conn_managed.cookie)) {
			BUG();
		} else if (cookie < curr->data.conn_managed.cookie) {
			p = &(*p)->rb_left;
		} else if (cookie > curr->data.conn_managed.cookie) {
			p = &(*p)->rb_right;
		}
	}

	kref_get(&ins_l->ref);
	rb_link_node(&ins_l->data.conn_managed.rbn, parent, p);
	rb_insert_color(&ins_l->data.conn_managed.rbn, root);

	spin_unlock_bh(&cor_sock_cookie_lock);
}

static int cor_alloc_corsock_cookie(struct cor_sock *cs_m_l)
{
	__be64 cookie;
	int i;

	BUG_ON(cs_m_l->type != CS_TYPE_CONN_MANAGED);
	BUG_ON(cs_m_l->data.conn_managed.cookie != 0);

	spin_lock_bh(&cor_cookie_gen);
	for (i = 0; i < 16; i++) {
		struct cor_sock *cs2 = 0;

		cookie = 0;
		get_random_bytes((char *) &cookie, sizeof(cookie));

		if (unlikely(cookie == 0))
			continue;

		cs2 = cor_get_sock_by_cookie(cookie);
		if (unlikely(cs2 != 0)) {
			kref_put(&cs2->ref, cor_free_sock);
			continue;
		}

		goto found;
	}
	spin_unlock_bh(&cor_cookie_gen);
	return 1;

found:
	cs_m_l->data.conn_managed.cookie = cookie;
	cor_insert_sock_cookie(cs_m_l);
	spin_unlock_bh(&cor_cookie_gen);
	return 0;
}

static void _cor_mngdsocket_shutdown(struct cor_sock *cs_m_l, int flags);

static int cor_mngdsocket_closefinished(struct cor_sock *cs_m)
{
	int rc = 0;

	mutex_lock(&cs_m->lock);
	BUG_ON(cs_m->type != CS_TYPE_CONN_MANAGED);

	if (cs_m->data.conn_managed.rcvd_eof != 0 &&
			cs_m->data.conn_managed.rcvd_rcvend != 0) {
		rc = 1;
	} else if (cs_m->data.conn_managed.src_sock == 0 ||
			cs_m->data.conn_managed.trgt_sock == 0 ||
			cs_m->data.conn_managed.is_reset != 0) {
		rc = 1;
	} else {
		spin_lock_bh(&cs_m->data.conn_managed.src_sock->rcv_lock);
		if (cs_m->data.conn_managed.src_sock->isreset != 0)
			rc = 1;
		spin_unlock_bh(&cs_m->data.conn_managed.src_sock->rcv_lock);
	}

	mutex_unlock(&cs_m->lock);

	return rc;
}

static void cor_mngdsocket_release_closewait(struct cor_sock *cs_m,
		long timeout)
{
	while (cor_mngdsocket_closefinished(cs_m) == 0) {
		long waitret;

		if (atomic_read(&cs_m->ready_to_read) != 0)
			msleep(10);

		waitret = wait_event_interruptible_timeout(
				*sk_sleep(&cs_m->sk),
				atomic_read(&cs_m->ready_to_read) != 0,
				timeout);

		if (waitret <= 0)
			break;
	}
}

static void cor_mngdsocket_release_mngd(struct cor_sock *cs_m)
{
	long timeout = 0;

	mutex_lock(&cs_m->lock);
	BUG_ON(cs_m->type != CS_TYPE_CONN_MANAGED);


	if (sock_flag(&cs_m->sk, SOCK_LINGER) &&
			!(current->flags & PF_EXITING))
		timeout = cs_m->sk.sk_lingertime;

	if (timeout > 0) {
		_cor_mngdsocket_shutdown(cs_m, SHUT_RDWR);
		mutex_unlock(&cs_m->lock);

		cor_mngdsocket_release_closewait(cs_m, timeout);

		mutex_lock(&cs_m->lock);
		BUG_ON(cs_m->type != CS_TYPE_CONN_MANAGED);
	}

	cs_m->isreleased = 1;

	if (cs_m->data.conn_managed.src_sock != 0 &&
			cs_m->data.conn_managed.trgt_sock != 0) {
		cor_reset_conn(cs_m->data.conn_managed.src_sock);

		cor_conn_kref_put_bug(cs_m->data.conn_managed.src_sock,
				"socket");
		cor_conn_kref_put(cs_m->data.conn_managed.trgt_sock,
				"socket");

		cs_m->data.conn_managed.src_sock = 0;
		cs_m->data.conn_managed.trgt_sock = 0;
	}
	mutex_unlock(&cs_m->lock);

	cor_usersock_release(cs_m);

	mutex_lock(&cs_m->lock);
	BUG_ON(cs_m->type != CS_TYPE_CONN_MANAGED);
	if (cs_m->data.conn_managed.cookie != 0) {
		spin_lock_bh(&cor_sock_cookie_lock);
		rb_erase(&cs_m->data.conn_managed.rbn, &cor_sock_cookie_rb);
		kref_put(&cs_m->ref, cor_kreffree_bug);
		spin_unlock_bh(&cor_sock_cookie_lock);
		cs_m->data.conn_managed.cookie = 0;
	}
	mutex_unlock(&cs_m->lock);
}

int cor_mngdsocket_release(struct socket *sock)
{
	struct cor_sock *cs = (struct cor_sock *) sock->sk;
	__u8 type;

	if (sock->sk == 0) {
		/* accept may return before newsock is initialised */
		return 0;
	}

	mutex_lock(&cs->lock);
	type = cs->type;
	if (type != CS_TYPE_CONN_MANAGED)
		cs->isreleased = 1;
	mutex_unlock(&cs->lock);

	if (type == CS_TYPE_UNCONNECTED) {
	} else if (type == CS_TYPE_LISTENER) {
		cor_close_port(cs);
	} else if (type == CS_TYPE_CONN_MANAGED) {
		cor_mngdsocket_release_mngd(cs);
	} else {
		BUG();
	}

	kref_put(&cs->ref, cor_free_sock);

	return 0;
}

int cor_mngdsocket_bind(struct socket *sock, struct sockaddr *saddr,
		int sockaddr_len)
{
	int rc = 0;
	struct cor_sock *cs = (struct cor_sock *) sock->sk;
	struct cor_sockaddr *addr = (struct cor_sockaddr *) saddr;

	if (unlikely(sockaddr_len < sizeof(struct cor_sockaddr)))
		return -EINVAL;

	if (unlikely(addr->sin_family != AF_COR))
		return -EINVAL;

	if (unlikely(be64_to_cpu(addr->addr) != 0))
		return -EINVAL;

	if (unlikely(be32_to_cpu(addr->port) == 0))
		return -EINVAL;

	mutex_lock(&cs->lock);
	if (unlikely(cs->type != CS_TYPE_UNCONNECTED))
		rc = -EINVAL;
	else
		rc = cor_open_port(cs, addr->port);
	mutex_unlock(&cs->lock);

	return rc;
}

static int cor_mngdsocket_init_conn_managed(struct cor_sock *cs_l,
		char *rcvbuf, char *sndbuf)
{
	BUG_ON(rcvbuf == 0);
	BUG_ON(sndbuf == 0);
	BUG_ON(cs_l->type != CS_TYPE_CONN_MANAGED);

	memset(&cs_l->data.conn_managed, 0, sizeof(cs_l->data.conn_managed));

	INIT_LIST_HEAD(&cs_l->data.conn_managed.rd_msgs);
	cs_l->data.conn_managed.rcv_buf = rcvbuf;
	cs_l->data.conn_managed.rcv_buf_state = RCV_BUF_STATE_INCOMPLETE;
	cs_l->data.conn_managed.snd_buf = sndbuf;
	cs_l->data.conn_managed.snd_segment_size = CONN_MNGD_MAX_SEGMENT_SIZE;

	return 0;
}

static int cor_mngdsocket_connect(struct socket *sock,
		struct sockaddr *saddr, int sockaddr_len, int flags)
{
	struct cor_sock *cs = (struct cor_sock *) sock->sk;
	struct cor_sockaddr *addr = (struct cor_sockaddr *) saddr;

	char *rcvbuf;
	char *sndbuf;

	int rc;

	if (unlikely(sockaddr_len < sizeof(struct cor_sockaddr)))
		return -EINVAL;

	if (unlikely(addr->sin_family != AF_COR))
		return -EINVAL;

	rcvbuf = kmalloc(CONN_MNGD_MAX_SEGMENT_SIZE, GFP_KERNEL);
	if (unlikely(rcvbuf == 0))
		return -ETIMEDOUT;

	sndbuf = kmalloc(CONN_MNGD_MAX_SEGMENT_SIZE, GFP_KERNEL);
	if (unlikely(sndbuf == 0)) {
		kfree(rcvbuf);
		return -ETIMEDOUT;
	}

	mutex_lock(&cs->lock);
	if (unlikely(cs->type != CS_TYPE_UNCONNECTED)) {
		mutex_unlock(&cs->lock);
		kfree(sndbuf);
		kfree(rcvbuf);
		return -EISCONN;
	}

	cs->type = CS_TYPE_CONN_MANAGED;
	rc = cor_mngdsocket_init_conn_managed(cs, rcvbuf, sndbuf);
	if (unlikely(rc != 0))
		goto err;

	rc = cor_alloc_corsock_cookie(cs);
	if (unlikely(rc != 0)) {
err:
		cs->type = CS_TYPE_UNCONNECTED;
		mutex_unlock(&cs->lock);
		kfree(sndbuf);
		kfree(rcvbuf);
		cs->data.conn_managed.rcv_buf = 0;
		cs->data.conn_managed.snd_buf = 0;
		return -ETIMEDOUT;
	}

	memcpy(&cs->data.conn_managed.remoteaddr, addr,
			sizeof(struct cor_sockaddr));

	cs->data.conn_managed.connect_state = CS_CONNECTSTATE_CONNECTING;

	mutex_unlock(&cs->lock);

	lock_sock(&cs->sk);
	sock->state = SS_CONNECTING;
	release_sock(&cs->sk);

	rc = cor_rdreq_connect(cs);

	if (unlikely(rc != -EINPROGRESS)) {
		mutex_lock(&cs->lock);
		cs->data.conn_managed.connect_state =
				CS_CONNECTSTATE_ERROR;
		mutex_unlock(&cs->lock);
		return rc;
	}

	if ((sock->file->f_flags & O_NONBLOCK) != 0)
		goto nonblock;

	while (1) {
		long waitret;

		mutex_lock(&cs->lock);
		if (cs->data.conn_managed.connect_state !=
				CS_CONNECTSTATE_CONNECTING) {
			mutex_unlock(&cs->lock);
			break;
		}

		atomic_set(&cs->ready_to_write, 0);

		mutex_unlock(&cs->lock);

		waitret = wait_event_interruptible_timeout(
				*sk_sleep(&cs->sk),
				atomic_read(&cs->ready_to_write) != 0,
				cs->sk.sk_sndtimeo);

		if (unlikely(waitret < 0))
			return sock_intr_errno(cs->sk.sk_sndtimeo);
		else if (unlikely(waitret == 0))
			return -ETIMEDOUT;
	}

nonblock:
	return sock_error(&cs->sk);
}

int cor_mngdsocket_accept(struct socket *sock, struct socket *newsock,
		int flags, bool kern)
{
	struct cor_sock *cs = (struct cor_sock *) sock->sk;

	char *rcvbuf;
	char *sndbuf;
	struct cor_conn *src_sock_o;
	struct cor_conn *trgt_sock_o;
	int rc;
	struct cor_sock *newcs;

	rcvbuf = kmalloc(CONN_MNGD_MAX_SEGMENT_SIZE, GFP_KERNEL);
	if (unlikely(rcvbuf == 0))
		return -ENOMEM;

	sndbuf = kmalloc(CONN_MNGD_MAX_SEGMENT_SIZE, GFP_KERNEL);
	if (unlikely(sndbuf == 0)) {
		kfree(rcvbuf);
		return -ENOMEM;
	}

	mutex_lock(&cs->lock);

	BUG_ON(cs->type != CS_TYPE_UNCONNECTED &&
			cs->type != CS_TYPE_LISTENER &&
			cs->type != CS_TYPE_CONN_MANAGED);

	if (unlikely(cs->type != CS_TYPE_LISTENER)) {
		mutex_unlock(&cs->lock);
		kfree(sndbuf);
		kfree(rcvbuf);
		return -EINVAL;
	}

	spin_lock_bh(&cor_bindnodes);
	if (unlikely(cs->data.listener.queue_maxlen <= 0)) {
		spin_unlock_bh(&cor_bindnodes);
		mutex_unlock(&cs->lock);
		kfree(sndbuf);
		kfree(rcvbuf);
		return -EINVAL;
	}

	while (list_empty(&cs->data.listener.conn_queue)) {
		atomic_set(&cs->ready_to_accept, 0);
		spin_unlock_bh(&cor_bindnodes);
		mutex_unlock(&cs->lock);

		if ((flags & O_NONBLOCK) != 0)
			return -EAGAIN;

		if (wait_event_interruptible(*sk_sleep(&cs->sk),
				atomic_read(&cs->ready_to_accept) != 0) !=
				0) {
			kfree(sndbuf);
			kfree(rcvbuf);
			return -ERESTARTSYS;
		}
		mutex_lock(&cs->lock);
		spin_lock_bh(&cor_bindnodes);
	}

	src_sock_o = container_of(cs->data.listener.conn_queue.next,
			struct cor_conn, src.sock.cl_list);

	/* cor_reset_conn(src_sock_o); */ //testing

	BUG_ON(src_sock_o->src.sock.in_cl_list == 0);
	list_del(&src_sock_o->src.sock.cl_list);
	src_sock_o->src.sock.in_cl_list = 0;

	cs->data.listener.queue_len--;

	spin_unlock_bh(&cor_bindnodes);
	mutex_unlock(&cs->lock);

	spin_lock_bh(&src_sock_o->rcv_lock);
	trgt_sock_o = cor_get_conn_reversedir(src_sock_o);
	spin_unlock_bh(&src_sock_o->rcv_lock);

	/* kern = 0 - ugly, bit af_unix does it too... */
	rc = _cor_createsock(sock_net(sock->sk), newsock,
			cs->sk.sk_protocol, 0, 0);

	if (unlikely(rc != 0)) {
		cor_reset_conn(src_sock_o);
		cor_conn_kref_put(src_sock_o, "conn_queue");
		kfree(sndbuf);
		kfree(rcvbuf);
		printk(KERN_ERR "cor: _cor_createsock() failed, connection resetted\n");
		return rc;
	}

	newcs = (struct cor_sock *) newsock->sk;

	if (unlikely(newcs == 0)) {
		cor_reset_conn(src_sock_o);
		cor_conn_kref_put(src_sock_o, "conn_queue");
		kfree(sndbuf);
		kfree(rcvbuf);
		printk(KERN_ERR "cor: newsock->sk is null, connection resetted\n");
		return -ENOMEM;
	}

	mutex_lock(&newcs->lock);
	spin_lock_bh(&trgt_sock_o->rcv_lock);
	spin_lock_bh(&src_sock_o->rcv_lock);

	BUG_ON(trgt_sock_o->is_client == 0);
	BUG_ON(src_sock_o->is_client != 0);

	BUG_ON(trgt_sock_o->targettype != TARGET_SOCK);
	BUG_ON(src_sock_o->sourcetype != SOURCE_SOCK);

	BUG_ON(newcs->type != CS_TYPE_UNCONNECTED);

	newcs->type = CS_TYPE_CONN_MANAGED;
	rc = cor_mngdsocket_init_conn_managed(newcs, rcvbuf, sndbuf);
	if (unlikely(rc != 0)) {
		cor_reset_conn(src_sock_o);
		cor_conn_kref_put(src_sock_o, "conn_queue");
		kfree(sndbuf);
		kfree(rcvbuf);
		printk(KERN_ERR "cor: cor_mngdsocket_init_conn_managed() failed, connection resetted\n");
		return -ENOMEM;
	}

	newcs->data.conn_managed.src_sock = src_sock_o;
	newcs->data.conn_managed.trgt_sock = trgt_sock_o;
	cor_conn_kref_get(src_sock_o, "socket");
	cor_conn_kref_get(trgt_sock_o, "socket");

	/* we will notice resetted conns when we try to use it */
	if (likely(src_sock_o->isreset == 0)) {
		src_sock_o->src.sock.ed->cs = newcs;
		trgt_sock_o->trgt.sock.cs = newcs;
		kref_get(&newcs->ref);
		kref_get(&newcs->ref);

		BUG_ON(newcs->data.conn_managed.rcv_buf == 0);
		src_sock_o->src.sock.socktype = SOCKTYPE_MANAGED;
		trgt_sock_o->trgt.sock.socktype = SOCKTYPE_MANAGED;
		trgt_sock_o->trgt.sock.rcv_buf_state =
				RCV_BUF_STATE_INCOMPLETE;
		trgt_sock_o->trgt.sock.rcv_buf =
				newcs->data.conn_managed.rcv_buf;
		trgt_sock_o->trgt.sock.rcvd = 0;

		BUG_ON(src_sock_o->src.sock.keepalive_intransit != 0);
		src_sock_o->src.sock.ed->jiffies_keepalive_lastact =
				jiffies - KEEPALIVE_INTERVAL_SECS * HZ + HZ;
		cor_keepalive_req_sched_timer(src_sock_o);
	}

	newcs->data.conn_managed.connect_state = CS_CONNECTSTATE_CONNECTED;

	spin_unlock_bh(&src_sock_o->rcv_lock);
	spin_unlock_bh(&trgt_sock_o->rcv_lock);
	mutex_unlock(&newcs->lock);

	newsock->ops = sock->ops;
	newsock->sk = (struct sock *) newcs;
	newsock->state = SS_CONNECTED;

	cor_conn_kref_put(src_sock_o, "conn_queue");

	return 0;
}

int cor_mngdsocket_listen(struct socket *sock, int len)
{
	struct cor_sock *cs = (struct cor_sock *) sock->sk;

	mutex_lock(&cs->lock);
	spin_lock_bh(&cor_bindnodes);

	BUG_ON(cs->type != CS_TYPE_UNCONNECTED &&
			cs->type != CS_TYPE_LISTENER &&
			cs->type != CS_TYPE_CONN_MANAGED);

	if (unlikely(cs->type != CS_TYPE_LISTENER)) {
		mutex_unlock(&cs->lock);
		return -EOPNOTSUPP;
	}

	if (len < 0)
		len = 0;

	cs->data.listener.queue_maxlen = len;

	spin_unlock_bh(&cor_bindnodes);
	mutex_unlock(&cs->lock);

	return 0;
}

static void _cor_mngdsocket_shutdown(struct cor_sock *cs_m_l, int flags)
{
	__u8 send_eof = 0;
	__u8 send_rcvend = 0;

	BUG_ON(cs_m_l->type != CS_TYPE_CONN_MANAGED);

	if (flags == SHUT_RD || flags == SHUT_RDWR) {
		if (cs_m_l->data.conn_managed.sent_rcvend == 0) {
			send_rcvend = 1;
			cs_m_l->data.conn_managed.sent_rcvend = 1;
		}

		cs_m_l->data.conn_managed.shutdown_rd = 1;
	}

	if (flags == SHUT_WR || flags == SHUT_RDWR) {
		if (cs_m_l->data.conn_managed.sent_eof == 0) {
			send_eof = 1;
			cs_m_l->data.conn_managed.sent_eof = 1;
		}
		cs_m_l->data.conn_managed.shutdown_wr = 1;

		cs_m_l->data.conn_managed.flush = 1;
	}

	if (send_eof != 0 || send_rcvend != 0)
		cor_mngdsocket_flushtoconn_ctrl(cs_m_l, send_eof, send_rcvend,
				0, 0);
}

int cor_mngdsocket_shutdown(struct socket *sock, int flags)
{
	struct cor_sock *cs = (struct cor_sock *) sock->sk;

	mutex_lock(&cs->lock);

	BUG_ON(cs->type != CS_TYPE_UNCONNECTED &&
			cs->type != CS_TYPE_LISTENER &&
			cs->type != CS_TYPE_CONN_MANAGED);

	if (unlikely(cs->type == CS_TYPE_UNCONNECTED)) {
		mutex_unlock(&cs->lock);
		return -ENOTCONN;
	} else if (unlikely(cs->type != CS_TYPE_CONN_MANAGED)) {
		mutex_unlock(&cs->lock);
		return -EBADF;
	}

	_cor_mngdsocket_shutdown(cs, flags);

	mutex_unlock(&cs->lock);

	return 0;
}

int cor_mngdsocket_ioctl(struct socket *sock, unsigned int cmd,
		unsigned long arg)
{
	return -ENOIOCTLCMD;
}

static int cor_mngdsocket_setsockopt_publishservice(struct socket *sock,
		char __user *optval, unsigned int optlen)
{
	struct cor_sock *cs = (struct cor_sock *) sock->sk;
	int publish;
	int notread;

	if (unlikely(optlen != 4))
		return -EINVAL;

	notread = copy_from_user(&publish, optval, 4);
	if (unlikely(notread != 0))
		return -EFAULT;

	if (publish != 0 && publish != 1)
		return -EINVAL;

	cor_set_publish_service(cs, (__u8) publish);

	return 0;
}

int cor_mngdsocket_setsockopt(struct socket *sock, int level,
		int optname, char __user *optval, unsigned int optlen)
{
	if (unlikely(level != SOL_COR))
		return -ENOPROTOOPT;

	if (optname == COR_PUBLISH_SERVICE) {
		return cor_mngdsocket_setsockopt_publishservice(sock, optval,
				optlen);
	} else if (optname == COR_TOS) {
		return cor_socket_setsockopt_tos(sock, optval, optlen);
	} else if (optname == COR_PRIORITY) {
		return cor_socket_setsockopt_priority(sock, optval, optlen);
	} else {
		return -ENOPROTOOPT;
	}
}

int cor_mngdsocket_getsockopt(struct socket *sock, int level,
		int optname, char __user *optval, int __user *optlen)
{
	return -ENOPROTOOPT;
}

void __cor_set_sock_connecterror(struct cor_sock *cs_m_l, int errorno)
{
	BUG_ON(cs_m_l->type != CS_TYPE_CONN_MANAGED);

	if (unlikely(unlikely(cs_m_l->isreleased != 0) ||
			unlikely(cs_m_l->data.conn_managed.connect_state !=
			CS_CONNECTSTATE_CONNECTING)))
		return;

	cs_m_l->data.conn_managed.connect_state = CS_CONNECTSTATE_ERROR;

	lock_sock(&cs_m_l->sk);
	xchg(&cs_m_l->sk.sk_err, errorno);
	release_sock(&cs_m_l->sk);

	atomic_set(&cs_m_l->ready_to_read, 1);
	atomic_set(&cs_m_l->ready_to_write, 1);
	atomic_set(&cs_m_l->ready_to_accept, 1);
	barrier();
	cs_m_l->sk.sk_state_change(&cs_m_l->sk);
}

void _cor_set_sock_connecterror(struct cor_sock *cs, int errorno)
{
	BUG_ON(errorno == 0);

	if (cs == 0)
		return;

	mutex_lock(&cs->lock);
	__cor_set_sock_connecterror(cs, errorno);
	mutex_unlock(&cs->lock);
}

void cor_mngdsocket_chksum(char *hdr, __u32 hdrlen,
		char *data, __u32 datalen,
		char *chksum, __u32 chksum_len)
{
	__u32 crc = 0;

	BUG_ON(chksum_len != 4);

	crc = crc32c(crc, hdr, hdrlen);
	crc = crc32c(crc, data, datalen);

	cor_put_u32(chksum, crc);
}

static int cor_mngdsocket_check_connected(struct cor_sock *cs_l)
{
	BUG_ON(cs_l->type != CS_TYPE_UNCONNECTED &&
			cs_l->type != CS_TYPE_LISTENER &&
			cs_l->type != CS_TYPE_CONN_MANAGED);
	if (unlikely(cs_l->type == CS_TYPE_UNCONNECTED)) {
		return -ENOTCONN;
	} else if (unlikely(cs_l->type != CS_TYPE_CONN_MANAGED)) {
		return -EBADF;
	} else if (unlikely(cs_l->data.conn_managed.connect_state !=
			CS_CONNECTSTATE_CONNECTED)) {
		return -ENOTCONN;
	}

	if (unlikely(cs_l->data.conn_managed.is_reset != 0))
		return -ECONNRESET;

	return 0;
}

static __u32 cor_get_segment_size(__u32 sndspeed_limited, __u8 is_highlatency,
		__u32 l4overhead)
{
	if (is_highlatency)
		return 4096;

	if (sndspeed_limited >= 4096 * 300)
		return 4096 - l4overhead;
	else if (sndspeed_limited >= 2048 * 200)
		return 2048 - l4overhead;
	else if (sndspeed_limited >= 1024 * 100)
		return 1024 - l4overhead;
	else if (sndspeed_limited >= 512 * 40)
		return 512 - l4overhead;
	else if (sndspeed_limited >= 256 * 10)
		return 256 - l4overhead;
	else
		return 128 - l4overhead;
}

static int __cor_mngdsocket_sendmsg(struct msghdr *msg, __u32 totallen,
		struct cor_sock *cs_m_l, __u32 sndspeed_limited)
{
	__u32 len = totallen;
	__u16 bufleft;
	size_t st_rc;

	if (cs_m_l->data.conn_managed.snd_data_len == 0)
		cs_m_l->data.conn_managed.snd_segment_size =
				cor_get_segment_size(sndspeed_limited,
				cs_m_l->is_highlatency, 6);

	BUG_ON(cs_m_l->data.conn_managed.snd_segment_size >
			CONN_MNGD_MAX_SEGMENT_SIZE);

	BUG_ON(totallen > (1024 * 1024 * 1024));

	BUG_ON(cs_m_l->type != CS_TYPE_CONN_MANAGED);
	BUG_ON(cs_m_l->data.conn_managed.send_in_progress != 0);

	BUG_ON(cs_m_l->data.conn_managed.snd_segment_size <=
			cs_m_l->data.conn_managed.snd_data_len);

	bufleft = cs_m_l->data.conn_managed.snd_segment_size -
			cs_m_l->data.conn_managed.snd_data_len;

	if (len > bufleft)
		len = bufleft;
	BUG_ON(len >= 65536);

	st_rc = copy_from_iter(cs_m_l->data.conn_managed.snd_buf +
			cs_m_l->data.conn_managed.snd_data_len, len,
			&msg->msg_iter);

	if (unlikely(st_rc != len))
		return -EFAULT;

	cs_m_l->data.conn_managed.snd_data_len += (__u16) len;

	return len;
}

static int _cor_mngdsocket_sendmsg(struct msghdr *msg, __u32 totallen,
		__u32 *iovidx, __u32 *iovread, struct cor_sock *cs,
		__u8 flush)
{
	int rc;
	struct cor_conn *src_sock;
	__u32 sndspeed_limited;

	mutex_lock(&cs->lock);

	rc = cor_mngdsocket_check_connected(cs);
	if (unlikely(rc != 0))
		goto out;

	if (unlikely(cs->data.conn_managed.shutdown_wr != 0)) {
		rc = -ECONNRESET;
		goto out;
	}

	BUG_ON(cs->data.conn_managed.snd_data_len >
			cs->data.conn_managed.snd_segment_size);

	if (unlikely(cs->data.conn_managed.send_in_progress != 0 ||
			cs->data.conn_managed.snd_data_len ==
			cs->data.conn_managed.snd_segment_size)) {
		cs->data.conn_managed.flush = 0;
		cor_mngdsocket_flushtoconn_data(cs);
		if (cs->data.conn_managed.send_in_progress != 0 ||
				cs->data.conn_managed.snd_data_len ==
				cs->data.conn_managed.snd_segment_size) {
			rc = -EAGAIN;
			goto out;
		}
	}

	src_sock = cs->data.conn_managed.src_sock;
	if (unlikely(src_sock == 0)) {
		rc = -ENOTCONN;
		goto out;
	}

	spin_lock_bh(&src_sock->rcv_lock);
	if (unlikely(src_sock->isreset != 0 ||
			cor_is_src_sock(src_sock, cs) == 0)) {
		rc = -ECONNRESET;
		spin_unlock_bh(&src_sock->rcv_lock);
		goto out;
	} else if (cor_sock_sndbufavailable(src_sock, 0) == 0) {
		rc = -EAGAIN;
		src_sock->flush = 0;
		atomic_set(&cs->ready_to_write, 0);
		spin_unlock_bh(&src_sock->rcv_lock);
		goto out;
	}

	sndspeed_limited = src_sock->src.sock.ed->snd_speed.speed_limited;

	spin_unlock_bh(&src_sock->rcv_lock);

	rc = __cor_mngdsocket_sendmsg(msg, totallen, cs, sndspeed_limited);

	cs->data.conn_managed.flush = flush;
	if (unlikely(likely(rc > 0) && unlikely(rc != totallen)))
		cs->data.conn_managed.flush = 0;

	if (flush != 0 || cs->data.conn_managed.snd_data_len ==
			cs->data.conn_managed.snd_segment_size) {
		cor_mngdsocket_flushtoconn_data(cs);
	}

out:
	mutex_unlock(&cs->lock);

	return rc;
}

int cor_mngdsocket_sendmsg(struct socket *sock, struct msghdr *msg,
		size_t total_len)
{
	struct cor_sock *cs = (struct cor_sock *) sock->sk;

	__u8 flush = ((msg->msg_flags & MSG_MORE) == 0) ? 1 : 0;
	int blocking = (msg->msg_flags & MSG_DONTWAIT) == 0;

	int rc = 0;
	int copied = 0;
	__u32 max = (1024 * 1024 * 1024);
	__u32 totallen;

	__u32 iovidx = 0;
	__u32 iovread = 0;

	totallen = total_len;
	if (unlikely(totallen > max || total_len > max)) {
		totallen = max;
		flush = 0;
	}

	while (rc >= 0 && copied < totallen) {
		rc = _cor_mngdsocket_sendmsg(msg, totallen - copied, &iovidx,
				&iovread, cs, flush);

		if (rc == -EAGAIN && blocking) {
			long waitret;

			waitret = wait_event_interruptible_timeout(
					*sk_sleep(&cs->sk),
					atomic_read(&cs->ready_to_write) != 0,
					cs->sk.sk_sndtimeo);

			if (unlikely(waitret < 0))
				rc = sock_intr_errno(cs->sk.sk_sndtimeo);
			else if (unlikely(waitret == 0))
				rc = -ETIMEDOUT;
			else
				continue;
		}

		if (rc > 0 || copied == 0)
			copied += rc;
		if (unlikely(rc == -EFAULT))
			copied = rc;

		BUG_ON(copied > 0 && ((__u32) copied > totallen));
	}

	BUG_ON(copied > 0 && unlikely((copied > total_len ||
			copied > totallen)));

	return copied;
}

static void __cor_mngdsocket_readfromconn(struct cor_sock *cs_m_l,
		struct cor_conn *trgt_sock_l,
		__u8 *send_eof, __u8 *send_rcvend,
		__u8 *keepalive_req_rcvd,
		__be32 *keepalive_req_cookie,
		__u8 *keepalive_resp_rcvd,
		__be32 *keepalive_resp_cookie)
{
	__u16 rcvbuf_consumed = 0;

	BUG_ON(trgt_sock_l->trgt.sock.rcv_buf_state != RCV_BUF_STATE_OK);

	if (likely((trgt_sock_l->trgt.sock.rcv_hdr_flags &
			CONN_MNGD_HASDATA) != 0)) {
		if (unlikely(trgt_sock_l->trgt.sock.rcv_data_len == 0 ||
				cs_m_l->data.conn_managed.shutdown_rd != 0))
			cs_m_l->data.conn_managed.rcv_data_len = 0;
		else
			cs_m_l->data.conn_managed.rcv_data_len =
					trgt_sock_l->trgt.sock.rcv_data_len;
		cs_m_l->data.conn_managed.rcv_buf_state = RCV_BUF_STATE_OK;
		return;
	}

	if (unlikely((trgt_sock_l->trgt.sock.rcv_hdr_flags &
			CONN_MNGD_EOF) != 0)) {
		if (cs_m_l->data.conn_managed.sent_rcvend == 0) {
			*send_rcvend = 1;
			cs_m_l->data.conn_managed.sent_rcvend = 1;
		}

		cs_m_l->data.conn_managed.rcvd_eof = 1;
	}

	if (unlikely((trgt_sock_l->trgt.sock.rcv_hdr_flags &
			CONN_MNGD_RCVEND) != 0)) {
		if (cs_m_l->data.conn_managed.sent_eof == 0) {
			*send_eof = 1;
			cs_m_l->data.conn_managed.sent_eof = 1;
		}

		cs_m_l->data.conn_managed.rcvd_rcvend = 1;
	}

	if (unlikely((trgt_sock_l->trgt.sock.rcv_hdr_flags &
			CONN_MNGD_KEEPALIVE_REQ) != 0)) {
		BUG_ON(rcvbuf_consumed + 4 >
				trgt_sock_l->trgt.sock.rcv_data_len);

		*keepalive_req_cookie = cor_parse_be32(
				trgt_sock_l->trgt.sock.rcv_buf +
				rcvbuf_consumed);
		rcvbuf_consumed += 4;

		*keepalive_req_rcvd = 1;
	}

	if (unlikely((trgt_sock_l->trgt.sock.rcv_hdr_flags &
			CONN_MNGD_KEEPALIVE_RESP) != 0)) {
		BUG_ON(rcvbuf_consumed + 4 >
				trgt_sock_l->trgt.sock.rcv_data_len);

		*keepalive_resp_cookie = cor_parse_be32(
				trgt_sock_l->trgt.sock.rcv_buf +
				rcvbuf_consumed);
		rcvbuf_consumed += 4;

		*keepalive_resp_rcvd = 1;
	}

	BUG_ON(rcvbuf_consumed != trgt_sock_l->trgt.sock.rcv_data_len);

	trgt_sock_l->trgt.sock.rcv_buf_state = RCV_BUF_STATE_INCOMPLETE;
	trgt_sock_l->trgt.sock.rcvd = 0;
}

static void _cor_mngdsocket_readfromconn(struct cor_sock *cs_m_l)
{
	__u8 do_wake_sender = 0;
	int reset_needed = 0;
	__u8 send_eof = 0;
	__u8 send_rcvend = 0;
	__u8 keepalive_req_rcvd = 0;
	__be32 keepalive_req_cookie = 0;
	__u8 keepalive_resp_rcvd = 0;
	__be32 keepalive_resp_cookie = 0;

	struct cor_conn *trgt_sock = cs_m_l->data.conn_managed.trgt_sock;

	spin_lock_bh(&trgt_sock->rcv_lock);

	if (unlikely(cor_is_trgt_sock(trgt_sock, cs_m_l) == 0))
		goto reset;

	cs_m_l->is_highlatency = trgt_sock->is_highlatency;

	if (unlikely(trgt_sock->isreset != 0))
		goto reset;

	BUG_ON(trgt_sock->trgt.sock.socktype != SOCKTYPE_MANAGED);
	BUG_ON(trgt_sock->trgt.sock.rcv_buf == 0);
	BUG_ON(trgt_sock->trgt.sock.rcv_buf !=
			cs_m_l->data.conn_managed.rcv_buf);

	if (cs_m_l->data.conn_managed.rcv_buf_state == RCV_BUF_STATE_OK) {
		cs_m_l->data.conn_managed.rcv_data_len = 0;
		cs_m_l->data.conn_managed.rcvbuf_consumed = 0;
		cs_m_l->data.conn_managed.rcv_buf_state =
				RCV_BUF_STATE_INCOMPLETE;

		trgt_sock->trgt.sock.rcv_buf_state =
				RCV_BUF_STATE_INCOMPLETE;
		trgt_sock->trgt.sock.rcvd = 0;
	}

	while (cs_m_l->data.conn_managed.rcv_buf_state ==
			RCV_BUF_STATE_INCOMPLETE) {
		cor_flush_sock_managed(trgt_sock, 1, &do_wake_sender);

		if (trgt_sock->trgt.sock.rcv_buf_state ==
				RCV_BUF_STATE_INCOMPLETE) {
			break;
		} else if (unlikely(trgt_sock->trgt.sock.rcv_buf_state ==
				RCV_BUF_STATE_RESET)) {
			goto reset;
		} else {
			BUG_ON(trgt_sock->trgt.sock.rcv_buf_state !=
					RCV_BUF_STATE_OK);

			__cor_mngdsocket_readfromconn(cs_m_l, trgt_sock,
					&send_eof, &send_rcvend,
					&keepalive_req_rcvd,
					&keepalive_req_cookie,
					&keepalive_resp_rcvd,
					&keepalive_resp_cookie);
		}
	}

	if (unlikely(cs_m_l->data.conn_managed.rcvd_eof != 0 &&
			cs_m_l->data.conn_managed.rcvd_rcvend != 0 &&
			trgt_sock->is_client == 0)) {
reset:
		reset_needed = 1;
	}

	spin_unlock_bh(&trgt_sock->rcv_lock);

	if (unlikely(reset_needed)) {
		cor_reset_conn(trgt_sock);

		cor_conn_kref_put_bug(cs_m_l->data.conn_managed.src_sock,
				"socket");
		cor_conn_kref_put(cs_m_l->data.conn_managed.trgt_sock,
				"socket");

		cs_m_l->data.conn_managed.src_sock = 0;
		cs_m_l->data.conn_managed.trgt_sock = 0;

		cs_m_l->data.conn_managed.is_reset = 1;
		cor_sk_data_ready(cs_m_l);
		cor_sk_write_space(cs_m_l);

		return;
	} else if (do_wake_sender != 0) {
		cor_wake_sender(trgt_sock);
	}

	if (unlikely(send_eof != 0 || send_rcvend != 0  ||
			keepalive_req_rcvd != 0)) {
		cor_mngdsocket_flushtoconn_ctrl(cs_m_l, send_eof,
				send_rcvend, keepalive_req_rcvd,
				keepalive_req_cookie);
	}
	if (unlikely(keepalive_resp_rcvd != 0))
		cor_keepalive_resp_rcvd(cs_m_l, keepalive_resp_cookie);
}

#define RC_RFC_OK 0
#define RC_RFC_INCOMPLETE 1
#define RC_RFC_EOF 2
#define RC_RFC_RESET 3
int cor_mngdsocket_readfromconn(struct cor_sock *cs_m_l)
{
	BUG_ON(cs_m_l->type != CS_TYPE_CONN_MANAGED);

	if (unlikely(cs_m_l->isreleased != 0))
		return RC_RFC_RESET;

	if (unlikely(cs_m_l->data.conn_managed.is_reset != 0))
		return RC_RFC_RESET;

	BUG_ON(cs_m_l->data.conn_managed.rcvbuf_consumed >
			cs_m_l->data.conn_managed.rcv_data_len);

	if (cs_m_l->data.conn_managed.rcvbuf_consumed <
			cs_m_l->data.conn_managed.rcv_data_len &&
			likely(cs_m_l->data.conn_managed.shutdown_rd == 0))
		return RC_RFC_OK;

	if (unlikely(cs_m_l->data.conn_managed.trgt_sock == 0))
		return RC_RFC_INCOMPLETE;

	_cor_mngdsocket_readfromconn(cs_m_l);

	if (unlikely(cs_m_l->data.conn_managed.is_reset != 0 ||
			cs_m_l->data.conn_managed.shutdown_rd != 0))
		return RC_RFC_RESET;

	if (unlikely(cs_m_l->data.conn_managed.shutdown_rd != 0))
		return RC_RFC_RESET;

	if (unlikely(cs_m_l->data.conn_managed.rcvd_eof != 0))
		return RC_RFC_EOF;

	if (cs_m_l->data.conn_managed.rcv_buf_state == RCV_BUF_STATE_INCOMPLETE)
		return RC_RFC_INCOMPLETE;

	return RC_RFC_OK;
}

void cor_mngdsocket_readfromconn_wq(struct work_struct *work)
{
	struct cor_sock *cs = container_of(work, struct cor_sock,
			readfromconn_work);
	int rc;
	__u8 data_ready = 0;

	mutex_lock(&cs->lock);

	atomic_set(&cs->readfromconn_work_scheduled, 0);
	barrier();

	if (unlikely(cs->isreleased != 0))
		goto out;

	BUG_ON(cs->type != CS_TYPE_CONN_MANAGED);

	rc = cor_mngdsocket_readfromconn(cs);

	if (rc == RCV_BUF_STATE_OK && cs->data.conn_managed.rcv_data_len > 0)
		data_ready = 1;

out:
	mutex_unlock(&cs->lock);

	if (data_ready != 0)
		cor_sk_data_ready(cs);

	kref_put(&cs->ref, cor_free_sock);
}

void cor_mngdsocket_readfromconn_fromatomic(struct cor_sock *cs)
{
	if (unlikely(cs == 0))
		return;

	if (atomic_xchg(&cs->readfromconn_work_scheduled, 1) == 0) {
		barrier();
		kref_get(&cs->ref);
		schedule_work(&cs->readfromconn_work);
	}
}

static int _cor_mngdsocket_recvmsg(struct msghdr *msg, __u32 totallen,
		struct cor_sock *cs, int firstrecv, int peek)
{
	int rc = 0;
	int rfc_rc;

	__u32 bufleft;

	__u32 len;

	size_t st_rc;

	mutex_lock(&cs->lock);

	rc = cor_mngdsocket_check_connected(cs);
	if (unlikely(rc != 0))
		goto out;

	rfc_rc = cor_mngdsocket_readfromconn(cs);
	if (unlikely(rfc_rc == RC_RFC_RESET)) {
		rc = -ECONNRESET;
		goto out;
	} else if (unlikely(rfc_rc == RC_RFC_EOF)) {
		rc = 0;
		if (firstrecv)
			cs->data.conn_managed.shutdown_rd = 1;
		goto out;
	} else if (rfc_rc == RC_RFC_INCOMPLETE) {
		rc = -EAGAIN;
		goto out;
	}

	BUG_ON(rfc_rc != RC_RFC_OK);

	BUG_ON(cs->data.conn_managed.rcv_data_len >= 65536);
	BUG_ON(cs->data.conn_managed.rcvbuf_consumed >=
			cs->data.conn_managed.rcv_data_len);
	bufleft = cs->data.conn_managed.rcv_data_len -
			cs->data.conn_managed.rcvbuf_consumed;

	BUG_ON(totallen > 1024 * 1024 * 1024);
	len = totallen;
	if (len > bufleft)
		len = bufleft;

	BUG_ON(len <= 0);
	BUG_ON(cs->data.conn_managed.rcv_buf == 0);

	st_rc = copy_to_iter(cs->data.conn_managed.rcv_buf +
			cs->data.conn_managed.rcvbuf_consumed, len,
			&msg->msg_iter);

	if (unlikely(st_rc != len)) {
		rc = -EFAULT;
		goto out;
	}

	if (likely(peek == 0))
		cs->data.conn_managed.rcvbuf_consumed += (__u16) len;

	rc = len;

out:
	mutex_unlock(&cs->lock);

	return rc;
}

int cor_mngdsocket_recvmsg(struct socket *sock, struct msghdr *msg,
		size_t total_len, int flags)
{
	struct cor_sock *cs = (struct cor_sock *) sock->sk;

	int blocking = (flags & MSG_DONTWAIT) == 0;
	int peek = (flags & MSG_PEEK) != 0;

	int rc = 0;
	int copied = 0;
	__u32 max = (1024 * 1024 * 1024);
	__u32 totallen;

	totallen = total_len;
	if (unlikely(totallen > max || total_len > max))
		totallen = max;
	if (unlikely(peek != 0) && totallen > 1)
		totallen = 1;


	while (copied < totallen) {
		rc = _cor_mngdsocket_recvmsg(msg, totallen - copied, cs,
				copied == 0, peek);

		if (rc == -EAGAIN && blocking && copied == 0) {
			long waitret;

			waitret = wait_event_interruptible_timeout(
					*sk_sleep(&cs->sk),
					atomic_read(&cs->ready_to_read) != 0,
					cs->sk.sk_rcvtimeo);

			if (unlikely(waitret < 0))
				rc = sock_intr_errno(cs->sk.sk_rcvtimeo);
			else if (unlikely(waitret == 0))
				rc = -ETIMEDOUT;
			else
				continue;
		}

		if (rc > 0 || copied == 0)
			copied += rc;
		if (unlikely(rc == -EFAULT))
			copied = rc;
		if (rc <= 0)
			break;

		BUG_ON(copied > 0 && ((__u32) copied > totallen));
	}

	BUG_ON(copied > 0 && unlikely((copied > total_len ||
			copied > totallen)));

	return copied;
}

static unsigned int cor_mngdsocket_poll(struct file *file, struct socket *sock,
		poll_table *wait)
{
	unsigned int mask = 0;

	struct cor_sock *cs = (struct cor_sock *) sock->sk;

	sock_poll_wait(file, sock, wait);

	mutex_lock(&cs->lock);

	if (cs->type == CS_TYPE_UNCONNECTED) {
		mask = U32_MAX;
	} else if (cs->type == CS_TYPE_LISTENER) {
		spin_lock_bh(&cor_bindnodes);
		if (unlikely(cs->data.listener.queue_maxlen <= 0))
			mask = U32_MAX;
		else if (list_empty(&cs->data.listener.conn_queue) == 0)
			mask |= (POLLIN | POLLRDNORM);
		spin_unlock_bh(&cor_bindnodes);
	} else if (cs->type == CS_TYPE_CONN_MANAGED) {
		struct cor_conn *src_sock = cs->data.conn_managed.src_sock;

		if (unlikely(unlikely(cs->data.conn_managed.is_reset != 0) ||
				unlikely(cs->data.conn_managed.connect_state ==
				CS_CONNECTSTATE_ERROR))) {
			mask = U32_MAX;
			goto out;
		}

		if (unlikely(cs->data.conn_managed.connect_state !=
				CS_CONNECTSTATE_CONNECTED))
			goto out;

		if (cor_mngdsocket_readfromconn(cs) != RC_RFC_INCOMPLETE)
			mask |= (POLLIN | POLLRDNORM);


		if (unlikely(src_sock == 0)) {
			mask = U32_MAX;
			goto out;
		}

		spin_lock_bh(&src_sock->rcv_lock);
		if (unlikely(src_sock->isreset != 0 ||
				cor_is_src_sock(src_sock, cs) == 0)) {
			mask = U32_MAX;
		} else if (cor_sock_sndbufavailable(src_sock, 1) != 0) {
			mask |= (POLLOUT | POLLWRNORM);
		}
		spin_unlock_bh(&src_sock->rcv_lock);
	} else {
		BUG();
	}

out:
	mutex_unlock(&cs->lock);

	return mask;
}

const struct proto_ops cor_mngd_proto_ops = {
		.family = PF_COR,
		.owner = THIS_MODULE,
		.release = cor_mngdsocket_release,
		.bind = cor_mngdsocket_bind,
		.connect = cor_mngdsocket_connect,
		.accept = cor_mngdsocket_accept,
		.listen = cor_mngdsocket_listen,
		.shutdown = cor_mngdsocket_shutdown,
		.ioctl = cor_mngdsocket_ioctl,
		.setsockopt = cor_mngdsocket_setsockopt,
		.getsockopt = cor_mngdsocket_getsockopt,
 #ifdef CONFIG_COMPAT
		.combat_ioctl = cor_mngdsocket_ioctl,
		.compat_setsockopt = cor_mngdsocket_setsockopt,
		.compat_getsockopt = cor_mngdsocket_getsockopt,
#endif
		.sendmsg = cor_mngdsocket_sendmsg,
		.recvmsg = cor_mngdsocket_recvmsg,
		.poll = cor_mngdsocket_poll,
		.socketpair = cor_socket_socketpair,
		.getname = cor_socket_getname,
		.mmap = cor_socket_mmap,

	/* sendpage, splice_read, are optional */
};

int cor_create_managed_sock(struct net *net, struct socket *sock, int protocol,
		int kern)
{
	int rc = _cor_createsock(net, sock, protocol, kern, 1);

	if (rc != 0)
		return rc;

	sock->ops = &cor_mngd_proto_ops;

	return 0;
}

/* static void __init test_chksum(void)
{
	char buf[5];
	buf[4] = 0;

	cor_mngdsocket_chksum("12", 2, "3456789", 7, &buf[0], 4);
	printk(KERN_ERR "test_chksum %hhx %hhx %hhx %hhx %hhx\n", buf[0],
			buf[1], buf[2], buf[3], buf[4]); // 83 92 6 e3
	cor_mngdsocket_chksum("123456789", 9, "", 0, &buf[0], 4);
	printk(KERN_ERR "test_chksum %hhx %hhx %hhx %hhx %hhx\n", buf[0],
			buf[1], buf[2], buf[3], buf[4]); // 83 92 6 e3
} */

int __init cor_sock_managed_init1(void)
{
	memset(&cor_sock_cookie_rb, 0, sizeof(cor_sock_cookie_rb));

	/* test_chksum(); */

	return 0;
}

MODULE_LICENSE("GPL");
