/**
 *	Connection oriented routing
 *	Copyright (C) 2007-2023 Michael Blizek
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License
 *	as published by the Free Software Foundation; either version 2
 *	of the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 */


#include "cor.h"


static struct cor_neighbor *cor_resume_neighbors_peeknextnb(
		struct cor_dev *cd, unsigned long *jiffies_nb_lastduration)
{
	unsigned long iflags;

	struct cor_neighbor *nb;

	spin_lock_irqsave(&cd->send_queue.qlock, iflags);

	if (list_empty(&cd->send_queue.neighbors_waiting)) {
		if (list_empty(&cd->send_queue.neighbors_waiting_nextpass)) {
			BUG_ON(cd->send_queue.numconns != 0);
			spin_unlock_irqrestore(&cd->send_queue.qlock, iflags);

			return 0;
		} else {
			unsigned long jiffies_tmp = jiffies;

			cor_swap_list_items(&cd->send_queue.neighbors_waiting,
					&cd->send_queue.neighbors_waiting_nextpass);

			WARN_ONCE(time_before(jiffies_tmp,
					cd->send_queue.jiffies_nb_pass_start),
					"cor_resume_neighbors_peeknextnb: jiffies after jiffies_nb_pass_start (this is only a performance issue)");

			cd->send_queue.jiffies_nb_lastduration = jiffies -
					cd->send_queue.jiffies_nb_pass_start;
			cd->send_queue.jiffies_nb_pass_start = jiffies_tmp;
		}
	}

	*jiffies_nb_lastduration = cd->send_queue.jiffies_nb_lastduration;


	BUG_ON(cd->send_queue.numconns == 0);
	BUG_ON(list_empty(&cd->send_queue.neighbors_waiting));

	nb = container_of(cd->send_queue.neighbors_waiting.next,
			struct cor_neighbor, rb.lh);

	BUG_ON(nb->rb.in_queue != RB_INQUEUE_TRUE);
	BUG_ON(nb->rb.lh.prev != &cd->send_queue.neighbors_waiting);
	BUG_ON((nb->rb.lh.next == &cd->send_queue.neighbors_waiting) &&
			(cd->send_queue.neighbors_waiting.prev != &nb->rb.lh));

	cor_nb_kref_get(nb, "stack");

	spin_unlock_irqrestore(&cd->send_queue.qlock, iflags);

	return nb;
}

static int cor_resume_neighbors(struct cor_dev *cd, int *sent)
{
	unsigned long iflags;
	int rc;

	unsigned long jiffies_nb_lastduration;
	struct cor_neighbor *nb = cor_resume_neighbors_peeknextnb(cd,
			&jiffies_nb_lastduration);

	if (nb == 0)
		return QOS_RESUME_DONE;

	atomic_set(&nb->cmsg_delay_conndata, 1);

	rc = cor_neigh_waitingsconns_resume(cd, nb, jiffies_nb_lastduration,
			sent);
	if (rc == QOS_RESUME_CONG) {
		cor_nb_kref_put(nb, "stack");
		return QOS_RESUME_CONG;
	}
	BUG_ON(rc != QOS_RESUME_DONE && rc != QOS_RESUME_NEXTNEIGHBOR);

	atomic_set(&nb->cmsg_delay_conndata, 0);
	spin_lock_bh(&nb->cmsg_lock);
	cor_schedule_controlmsg_timer(nb);
	spin_unlock_bh(&nb->cmsg_lock);

	#warning todo rename qos_*
	spin_lock_irqsave(&cd->send_queue.qlock, iflags);
	if (likely(nb->rb.in_queue == RB_INQUEUE_TRUE)) {
		if (nb->conns_waiting.cnt == 0) {
			BUG_ON(nb->conns_waiting.priority_sum != 0);
			nb->rb.in_queue = RB_INQUEUE_FALSE;
			list_del(&nb->rb.lh);
			cor_nb_kref_put_bug(nb, "qos_queue_nb");
		} else {
			BUG_ON(nb->conns_waiting.priority_sum >
					cd->send_queue.priority_sum);
			list_del(&nb->rb.lh);
			list_add_tail(&nb->rb.lh,
					&cd->send_queue.neighbors_waiting_nextpass);
		}
	}
	spin_unlock_irqrestore(&cd->send_queue.qlock, iflags);

	cor_nb_kref_put(nb, "stack");

	return QOS_RESUME_NEXTNEIGHBOR;
}

static int __cor_qos_resume(struct cor_dev *cd, int caller, int *sent)
{
	unsigned long iflags;
	int rc = QOS_RESUME_DONE;
	struct list_head *lh;

	spin_lock_irqsave(&cd->send_queue.qlock, iflags);

	if (caller == QOS_CALLER_KPACKET)
		lh = &cd->send_queue.kpackets_waiting;
	else if (caller == QOS_CALLER_CONN_RETRANS)
		lh = &cd->send_queue.conn_retrans_waiting;
	else if (caller == QOS_CALLER_ANNOUNCE)
		lh = &cd->send_queue.announce_waiting;
	else
		BUG();

	while (list_empty(lh) == 0) {
		struct cor_resume_block *rb = container_of(lh->next,
				struct cor_resume_block, lh);

		unsigned long cmsg_send_start_j;
		ktime_t cmsg_send_start_kt;

		BUG_ON(rb->in_queue != RB_INQUEUE_TRUE);
		rb->in_queue = RB_INQUEUE_FALSE;
		list_del(&rb->lh);

		if (caller == QOS_CALLER_KPACKET) {
			struct cor_neighbor *nb = container_of(rb,
					struct cor_neighbor, rb_kp);
			cmsg_send_start_j = nb->cmsg_send_start_j;
			cmsg_send_start_kt = nb->cmsg_send_start_kt;
		}

		spin_unlock_irqrestore(&cd->send_queue.qlock, iflags);
		if (caller == QOS_CALLER_KPACKET) {
			rc = cor_send_messages(container_of(rb,
					struct cor_neighbor, rb_kp),
					cmsg_send_start_j, cmsg_send_start_kt,
					sent);
		} else if (caller == QOS_CALLER_CONN_RETRANS) {
			rc = cor_send_retrans(container_of(rb,
					struct cor_neighbor, rb_cr), sent);
		} else if (caller == QOS_CALLER_ANNOUNCE) {
			rc = _cor_send_announce(container_of(rb,
					struct cor_announce_data, rb), 1, sent);
		} else {
			BUG();
		}
		spin_lock_irqsave(&cd->send_queue.qlock, iflags);

		if (rc != QOS_RESUME_DONE && caller == QOS_CALLER_KPACKET) {
			struct cor_neighbor *nb = container_of(rb,
					struct cor_neighbor, rb_kp);

			nb->cmsg_send_start_j = cmsg_send_start_j;
			nb->cmsg_send_start_kt = cmsg_send_start_kt;
		}

		if (rc != QOS_RESUME_DONE && rb->in_queue == RB_INQUEUE_FALSE) {
			rb->in_queue = RB_INQUEUE_TRUE;
			list_add(&rb->lh, lh);
			break;
		}

		if (caller == QOS_CALLER_KPACKET) {
			cor_nb_kref_put(container_of(rb, struct cor_neighbor,
					rb_kp), "qos_queue_kpacket");
		} else if (caller == QOS_CALLER_CONN_RETRANS) {
			cor_nb_kref_put(container_of(rb, struct cor_neighbor,
					rb_cr), "qos_queue_conn_retrans");
		} else if (caller == QOS_CALLER_ANNOUNCE) {
			kref_put(&container_of(rb, struct cor_announce_data,
					rb)->ref, cor_announce_data_free);
		} else {
			BUG();
		}

		kref_put(&cd->ref, cor_kreffree_bug);
	}

	spin_unlock_irqrestore(&cd->send_queue.qlock, iflags);

	return rc;
}

static int _cor_qos_resume(struct cor_dev *cd, int *sent)
{
	unsigned long iflags;
	int i = QOS_CALLER_KPACKET;
	int rc;

	spin_lock_irqsave(&cd->send_queue.qlock, iflags);

	while (1) {
		if (unlikely(cd->send_queue.is_destroyed == 1)) {
			rc = QOS_RESUME_EXIT;
			break;
		}

		if (i == QOS_CALLER_KPACKET &&
				list_empty(&cd->send_queue.kpackets_waiting)) {
			i = QOS_CALLER_CONN_RETRANS;
			continue;
		} else if (i == QOS_CALLER_CONN_RETRANS &&
				list_empty(&cd->send_queue.conn_retrans_waiting)) {
			i = QOS_CALLER_ANNOUNCE;
			continue;
		} else if (i == QOS_CALLER_ANNOUNCE &&
				list_empty(&cd->send_queue.announce_waiting)) {
			i = QOS_CALLER_NEIGHBOR;
			continue;
		} else if (i == QOS_CALLER_NEIGHBOR &&
				list_empty(&cd->send_queue.neighbors_waiting) &&
				list_empty(&cd->send_queue.neighbors_waiting_nextpass)) {
			rc = QOS_RESUME_DONE;
			break;
		}

		spin_unlock_irqrestore(&cd->send_queue.qlock, iflags);

		if (i == QOS_CALLER_NEIGHBOR)
			rc = cor_resume_neighbors(cd, sent);
		else
			rc = __cor_qos_resume(cd, i, sent);

		spin_lock_irqsave(&cd->send_queue.qlock, iflags);

		if (rc == QOS_RESUME_CONG)
			break;

		i = QOS_CALLER_KPACKET;
	}

	if (rc == QOS_RESUME_DONE) {
		BUG_ON(!list_empty(&cd->send_queue.kpackets_waiting));
		BUG_ON(!list_empty(&cd->send_queue.conn_retrans_waiting));
		BUG_ON(!list_empty(&cd->send_queue.announce_waiting));
		BUG_ON(!list_empty(&cd->send_queue.neighbors_waiting));
		BUG_ON(!list_empty(&cd->send_queue.neighbors_waiting_nextpass));

		atomic_set(&cd->send_queue.qos_resume_scheduled, 0);
	}

	cor_dev_queue_set_congstatus(cd);

	if (unlikely(cd->send_queue.is_destroyed == 1))
		rc = QOS_RESUME_EXIT;

	spin_unlock_irqrestore(&cd->send_queue.qlock, iflags);

	return rc;
}

int cor_qos_resume_threadfunc(void *data)
{
	struct cor_dev *cd = (struct cor_dev *) data;

	while (1) {
		int sent = 0;
		int rc;

		rc = _cor_qos_resume(cd, &sent);

		if (rc == QOS_RESUME_DONE) {
			wait_event(cd->send_queue.qos_resume_wq,
					atomic_read(
					&cd->send_queue.qos_resume_scheduled)
					!= 0);
		} else if (rc == QOS_RESUME_CONG) {
			unsigned long jiffies_tmp = jiffies;
			unsigned long delay_ms = 0;

			if (sent)
				cd->send_queue.jiffies_lastprogress = jiffies_tmp;
			delay_ms = (jiffies_to_msecs(jiffies_tmp -
					cd->send_queue.jiffies_lastprogress) + 8) / 4;
			if (delay_ms < 2)
				delay_ms = 2;
			else if (delay_ms > 20)
				delay_ms = 20;

			msleep(delay_ms);
		} else if (rc == QOS_RESUME_EXIT) {
			return 0;
		} else {
			BUG();
		}
	}
}

static inline void cor_dev_queue_schedule_resume(struct cor_dev *cd)
{
	if (atomic_cmpxchg(&cd->send_queue.qos_resume_scheduled, 0, 1) == 0) {
		barrier();
		wake_up(&cd->send_queue.qos_resume_wq);
	}
}

static void _cor_destroy_queue_kpackets(struct cor_dev *cd_qlocked)
{
	while (list_empty(&cd_qlocked->send_queue.kpackets_waiting) == 0) {
		struct list_head *curr =
				cd_qlocked->send_queue.kpackets_waiting.next;
		struct cor_resume_block *rb = container_of(curr,
				struct cor_resume_block, lh);
		BUG_ON(rb->in_queue != RB_INQUEUE_TRUE);
		rb->in_queue = RB_INQUEUE_FALSE;
		list_del(curr);

		cor_nb_kref_put(container_of(rb, struct cor_neighbor, rb_kp),
				"qos_queue_kpacket");
		kref_put(&cd_qlocked->ref, cor_kreffree_bug);
	}
}

static void _cor_destroy_queue_conn_retrans(struct cor_dev *cd_qlocked)
{
	while (list_empty(&cd_qlocked->send_queue.conn_retrans_waiting) == 0) {
		struct list_head *curr =
				cd_qlocked->send_queue.conn_retrans_waiting.next;
		struct cor_resume_block *rb = container_of(curr,
				struct cor_resume_block, lh);
		BUG_ON(rb->in_queue != RB_INQUEUE_TRUE);
		rb->in_queue = RB_INQUEUE_FALSE;
		list_del(curr);

		cor_nb_kref_put(container_of(rb, struct cor_neighbor, rb_cr),
				"qos_queue_conn_retrans");
		kref_put(&cd_qlocked->ref, cor_kreffree_bug);
	}
}

static void _cor_destroy_queue_announce(struct cor_dev *cd_qlocked)
{
	while (list_empty(&cd_qlocked->send_queue.announce_waiting) == 0) {
		struct list_head *curr =
				cd_qlocked->send_queue.announce_waiting.next;
		struct cor_resume_block *rb = container_of(curr,
				struct cor_resume_block, lh);
		BUG_ON(rb->in_queue != RB_INQUEUE_TRUE);
		rb->in_queue = RB_INQUEUE_FALSE;
		list_del(curr);

		kref_put(&container_of(rb, struct cor_announce_data, rb)->ref,
				cor_announce_data_free);
		kref_put(&cd_qlocked->ref, cor_kreffree_bug);
	}
}

static void _cor_destroy_queue_neighbor(struct cor_dev *cd_qlocked,
		struct list_head *lh)
{
	while (list_empty(lh) == 0) {
		struct list_head *curr = lh->next;
		struct cor_resume_block *rb = container_of(curr,
				struct cor_resume_block, lh);
		BUG_ON(rb->in_queue != RB_INQUEUE_TRUE);
		rb->in_queue = RB_INQUEUE_FALSE;
		list_del(curr);

		cor_nb_kref_put(container_of(rb, struct cor_neighbor, rb),
				"qos_queue_nb");
		kref_put(&cd_qlocked->ref, cor_kreffree_bug);
	}
}

void cor_dev_queue_destroy(struct cor_dev *cd)
{
	unsigned long iflags;

	spin_lock_irqsave(&cd->send_queue.qlock, iflags);
	cd->send_queue.is_destroyed = 1;
	_cor_destroy_queue_kpackets(cd);
	_cor_destroy_queue_conn_retrans(cd);
	_cor_destroy_queue_announce(cd);
	_cor_destroy_queue_neighbor(cd, &cd->send_queue.neighbors_waiting);
	_cor_destroy_queue_neighbor(cd,
			&cd->send_queue.neighbors_waiting_nextpass);

	spin_unlock_irqrestore(&cd->send_queue.qlock, iflags);

	cor_dev_queue_schedule_resume(cd);
}

int cor_dev_queue_init(struct cor_dev *cd)
{
	spin_lock_init(&cd->send_queue.qlock);

	atomic_set(&cd->send_queue.qos_resume_scheduled, 0);

	init_waitqueue_head(&cd->send_queue.qos_resume_wq);

	INIT_LIST_HEAD(&cd->send_queue.kpackets_waiting);
	INIT_LIST_HEAD(&cd->send_queue.conn_retrans_waiting);
	INIT_LIST_HEAD(&cd->send_queue.announce_waiting);
	INIT_LIST_HEAD(&cd->send_queue.neighbors_waiting);
	INIT_LIST_HEAD(&cd->send_queue.neighbors_waiting_nextpass);

	atomic_set(&cd->send_queue.cong_status, 0);

	cd->send_queue.qos_resume_thread = kthread_create(
			cor_qos_resume_threadfunc, cd, "cor_qos_resume");
	if (cd->send_queue.qos_resume_thread == 0) {
		printk(KERN_ERR "cor: unable to start qos_resume thread\n");

		return 1;
	}
	get_task_struct(cd->send_queue.qos_resume_thread);
	wake_up_process(cd->send_queue.qos_resume_thread);

	return 0;
}

void cor_dev_queue_set_congstatus(struct cor_dev *cd_qlocked)
{
	__u32 newstatus;

	if (time_before(cd_qlocked->send_queue.jiffies_lastdrop,
			jiffies - HZ / 50)) {
		newstatus = CONGSTATUS_NONE;
	} else if (list_empty(&cd_qlocked->send_queue.kpackets_waiting) == 0) {
		newstatus = CONGSTATUS_KPACKETS;
	} else if (list_empty(&cd_qlocked->send_queue.conn_retrans_waiting) == 0) {
		newstatus = CONGSTATUS_RETRANS;
	} else if (list_empty(&cd_qlocked->send_queue.announce_waiting) == 0) {
		newstatus = CONGSTATUS_ANNOUNCE;
	} else if (list_empty(&cd_qlocked->send_queue.neighbors_waiting) == 0 ||
			list_empty(&cd_qlocked->send_queue.neighbors_waiting_nextpass) == 0) {
		newstatus = CONGSTATUS_CONNDATA;
	} else {
		newstatus = CONGSTATUS_NONE;
	}

	atomic_set(&cd_qlocked->send_queue.cong_status, newstatus);
}

void cor_dev_queue_set_lastdrop(struct cor_dev *cd)
{
	unsigned long iflags;

	spin_lock_irqsave(&cd->send_queue.qlock, iflags);
	cd->send_queue.jiffies_lastdrop = jiffies;
	cor_dev_queue_set_congstatus(cd);
	spin_unlock_irqrestore(&cd->send_queue.qlock, iflags);
}

/**
 * if caller == QOS_CALLER_NEIGHBOR, nb->conns_waiting.lock must be held by
 * caller
 */
void _cor_dev_queue_enqueue(struct cor_dev *cd_qlocked,
		struct cor_resume_block *rb,
		unsigned long cmsg_send_start_j, ktime_t cmsg_send_start_kt,
		int caller, int from_nbcongwin_resume,
		int from_nbnotactive_resume)
{
	int queues_empty;

	if (rb->in_queue == RB_INQUEUE_TRUE) {
		BUG_ON(caller == QOS_CALLER_NEIGHBOR);

		if (caller == QOS_CALLER_KPACKET) {
			struct cor_neighbor *nb = container_of(rb,
					struct cor_neighbor, rb_kp);
			if (time_before(cmsg_send_start_j,
					nb->cmsg_send_start_j))
				nb->cmsg_send_start_j = cmsg_send_start_j;
			if (ktime_before(cmsg_send_start_kt,
					nb->cmsg_send_start_kt))
				nb->cmsg_send_start_kt = cmsg_send_start_kt;
		}
		return;
	} else if (rb->in_queue == RB_INQUEUE_NBCONGWIN &&
			from_nbcongwin_resume == 0) {
		return;
	} else if (rb->in_queue == RB_INQUEUE_NBNOTACTIVE) {
		return;
	}

	if (unlikely(cd_qlocked->send_queue.is_destroyed != 0))
		return;

	queues_empty = list_empty(&cd_qlocked->send_queue.kpackets_waiting) &&
			list_empty(&cd_qlocked->send_queue.conn_retrans_waiting) &&
			list_empty(&cd_qlocked->send_queue.announce_waiting) &&
			list_empty(&cd_qlocked->send_queue.neighbors_waiting) &&
			list_empty(&cd_qlocked->send_queue.neighbors_waiting_nextpass);

	BUG_ON(!queues_empty &&
			atomic_read(&cd_qlocked->send_queue.qos_resume_scheduled) == 0);

	if (caller == QOS_CALLER_KPACKET) {
		struct cor_neighbor *nb = container_of(rb, struct cor_neighbor,
				rb_kp);
		nb->cmsg_send_start_j = cmsg_send_start_j;
		nb->cmsg_send_start_kt = cmsg_send_start_kt;
		list_add_tail(&rb->lh, &cd_qlocked->send_queue.kpackets_waiting);
		cor_nb_kref_get(nb, "qos_queue_kpacket");
	} else if (caller == QOS_CALLER_CONN_RETRANS) {
		list_add_tail(&rb->lh, &cd_qlocked->send_queue.conn_retrans_waiting);
		cor_nb_kref_get(container_of(rb, struct cor_neighbor, rb_cr),
				"qos_queue_conn_retrans");
	} else if (caller == QOS_CALLER_ANNOUNCE) {
		list_add_tail(&rb->lh, &cd_qlocked->send_queue.announce_waiting);
		kref_get(&container_of(rb, struct cor_announce_data, rb)->ref);
	} else if (caller == QOS_CALLER_NEIGHBOR) {
		struct cor_neighbor *nb = container_of(rb, struct cor_neighbor,
				rb);
		if (unlikely(nb->conns_waiting.cnt == 0))
			return;

		list_add_tail(&rb->lh,
				&cd_qlocked->send_queue.neighbors_waiting_nextpass);
		cor_nb_kref_get(nb, "qos_queue_nb");
		cd_qlocked->send_queue.numconns += nb->conns_waiting.cnt;
		cd_qlocked->send_queue.priority_sum +=
				nb->conns_waiting.priority_sum;
		cd_qlocked->send_queue.jiffies_nb_lastduration = 0;
		cd_qlocked->send_queue.jiffies_nb_pass_start = jiffies;
	} else {
		BUG();
	}
	rb->in_queue = RB_INQUEUE_TRUE;
	kref_get(&cd_qlocked->ref);

	cor_dev_queue_schedule_resume(cd_qlocked);

	cor_dev_queue_set_congstatus(cd_qlocked);
}

void cor_dev_queue_enqueue(struct cor_dev *cd, struct cor_resume_block *rb,
		unsigned long cmsg_send_start_j, ktime_t cmsg_send_start_kt,
		int caller, int from_nbnotactive_resume)
{
	unsigned long iflags;

	spin_lock_irqsave(&cd->send_queue.qlock, iflags);
	_cor_dev_queue_enqueue(cd, rb, cmsg_send_start_j, cmsg_send_start_kt,
			caller, 0, from_nbnotactive_resume);
	spin_unlock_irqrestore(&cd->send_queue.qlock, iflags);
}
