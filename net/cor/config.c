/**
 *	Connection oriented routing
 *	Copyright (C) 2007-2021 Michael Blizek
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License
 *	as published by the Free Software Foundation; either version 2
 *	of the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 */

#include "cor.h"

static DEFINE_MUTEX(cor_config_lock);

DEFINE_SPINLOCK(cor_local_addr_lock);
__u8 cor_local_has_addr;
__be64 cor_local_addr;
__be32 cor_local_addr_sessionid;


static DEFINE_SPINLOCK(cor_interface_config_lock);
static struct cor_interface_config *cor_interface_config;
static __u32 cor_num_interfaces;
static int cor_all_interfaces;


int cor_is_device_configurated(struct net_device *dev)
{
	int ret = 0;

	unsigned long iflags;

	__u32 i;

	spin_lock_irqsave(&cor_interface_config_lock, iflags);

	if (cor_all_interfaces != 0) {
		ret = 1;
		goto out;
	}

	BUG_ON(cor_num_interfaces > 65536);
	for (i = 0; i < cor_num_interfaces; i++) {
		struct cor_interface_config *curr = &cor_interface_config[i];
		__u32 j;

		BUG_ON(curr->name == 0);

		for (j = 0;; j++) {
			if (j >= sizeof(dev->name))
				break;

			if (dev->name[j] == 0 && j == curr->name_len) {
				ret = 1;
				goto out;
			}

			if (dev->name[j] == 0 || j >= curr->name_len)
				break;

			if (dev->name[j] != curr->name[j])
				break;
		}
	}

out:
	spin_unlock_irqrestore(&cor_interface_config_lock, iflags);

	return ret;
}

void cor_set_interface_config(struct cor_interface_config *new_config,
		__u32 new_num_interfaces, int new_all_interfaces)
{
	unsigned long iflags;
	__u32 i;

	spin_lock_irqsave(&cor_interface_config_lock, iflags);

	BUG_ON(cor_num_interfaces > 65536);
	for (i = 0; i < cor_num_interfaces; i++) {
		struct cor_interface_config *curr = &cor_interface_config[i];

		BUG_ON(curr->name == 0);
		kfree(curr->name);
		curr->name = 0;
	}

	kfree(cor_interface_config);
	cor_interface_config = 0;
	cor_num_interfaces = 0;
	cor_all_interfaces = 0;


	cor_interface_config = new_config;
	cor_num_interfaces = new_num_interfaces;
	cor_all_interfaces = new_all_interfaces;

	spin_unlock_irqrestore(&cor_interface_config_lock, iflags);
}


void _cor_config_down(void)
{
	cor_dev_down();

	spin_lock_bh(&cor_local_addr_lock);
	cor_local_has_addr = 0;
	cor_local_addr = 0;
	spin_unlock_bh(&cor_local_addr_lock);

	cor_reset_neighbors(0);
	cor_reset_neighbors(0);
	cor_dev_destroy(0);

	cor_announce_send_stop(0, 0, ANNOUNCE_TYPE_BROADCAST);
}

void cor_config_down(void)
{
	mutex_lock(&cor_config_lock);
	_cor_config_down();
	mutex_unlock(&cor_config_lock);
}

int cor_config_up(__u8 has_addr, __be64 addr)
{
	int rc = 0;

	if (has_addr != 0 && be64_to_cpu(addr) == 0)
		return 1;

	mutex_lock(&cor_config_lock);

	_cor_config_down();

	spin_lock_bh(&cor_local_addr_lock);

	BUG_ON(cor_local_has_addr != 0);
	BUG_ON(cor_local_addr != 0);

	cor_local_has_addr = has_addr;
	cor_local_addr = addr;
	get_random_bytes((char *) &cor_local_addr_sessionid,
			sizeof(cor_local_addr_sessionid));

	spin_unlock_bh(&cor_local_addr_lock);

	if (cor_dev_up() != 0) {
		spin_lock_bh(&cor_local_addr_lock);
		cor_local_has_addr = 0;
		cor_local_addr = 0;
		spin_unlock_bh(&cor_local_addr_lock);
		rc = 1;
	}

	mutex_unlock(&cor_config_lock);

	return rc;
}

int cor_is_clientmode(void)
{
	int rc;

	spin_lock_bh(&cor_local_addr_lock);
	rc = (cor_local_has_addr == 0);
	spin_unlock_bh(&cor_local_addr_lock);
	return rc;
}

MODULE_LICENSE("GPL");
