/**
 *	Connection oriented routing
 *	Copyright (C) 2007-2021 Michael Blizek
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License
 *	as published by the Free Software Foundation; either version 2
 *	of the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 */

#include "cor.h"

#define MAX_SND_MSGLEN 4096
#define MAX_MSG_LEN 256

struct cor_rd_msg {
	struct list_head lh;

	struct list_head cs_lh;
	struct cor_sock *cs;

	__u32 type;
	union{
	} msg;
};

struct cor_rdsock {
	struct sock sk;

	atomic_t connected;

	__u8 versioninited;

	struct list_head socks;

	struct mutex sndbuf_lock;
	__u8 snd_cmdplen_read;
	__u32 param_read;
	char *cmdparams;

	atomic_t ready_to_read;
	struct list_head rcv_msgs; /* protected by rds_lock */

	struct mutex rcvbuf_lock;
	__u32 rcvbuflen;
	__u32 rcvbufoffset;

	struct{
		char snd_cmdplen_buf[8];
		char rcvbuf[MAX_MSG_LEN + 8];
	} user_copy;
};

static struct kmem_cache *cor_rdmsg_slab;

static DEFINE_MUTEX(cor_rds_lock);
static struct cor_rdsock *cor_crd;


int cor_rd_socket_release(struct socket *sock)
{
	mutex_lock(&cor_rds_lock);

	BUG_ON(((struct cor_rdsock *) sock->sk) != cor_crd);

	cor_config_down();

	cor_set_interface_config(0, 0, 0);

	while (list_empty(&cor_crd->rcv_msgs) == 0) {
		struct cor_rd_msg *rdm = container_of(cor_crd->rcv_msgs.next,
				struct cor_rd_msg, lh);

		list_del(&rdm->lh);
		if (rdm->cs != 0) {
			list_del(&rdm->cs_lh);
			kref_put(&rdm->cs->ref, cor_free_sock);
			rdm->cs = 0;
		}
		kmem_cache_free(cor_rdmsg_slab, rdm);
	}

	while (list_empty(&cor_crd->socks) == 0) {
		struct cor_sock *cs = container_of(cor_crd->socks.next,
				struct cor_sock, data.conn_managed.crd_lh);

		BUG_ON(cs->type != CS_TYPE_CONN_MANAGED);
		BUG_ON(cs->data.conn_managed.in_crd_list == 0);
		list_del(&cs->data.conn_managed.crd_lh);
		cs->data.conn_managed.in_crd_list = 0;
		_cor_set_sock_connecterror(cs, ENETUNREACH);
		kref_put(&cs->ref, cor_free_sock);
	}

	if (cor_crd->cmdparams != 0) {
		kfree(cor_crd->cmdparams);
		cor_crd->cmdparams = 0;
	}

	cor_crd = 0;

	mutex_unlock(&cor_rds_lock);

	sock_put(sock->sk);

	return 0;
}

int cor_rd_socket_bind(struct socket *sock, struct sockaddr *saddr,
		int sockaddr_len)
{
	return -EOPNOTSUPP;
}

int cor_rd_socket_connect(struct socket *sock, struct sockaddr *saddr,
		int sockaddr_len, int flags)
{
	struct cor_rdsock *crd = (struct cor_rdsock *) sock->sk;

	atomic_set(&crd->connected, 1);

	lock_sock(sock->sk);
	sock->state = SS_CONNECTED;
	release_sock(sock->sk);
	return 0;
}

int cor_rd_socket_accept(struct socket *sock, struct socket *newsock, int flags,
		bool kern)
{
	return -EOPNOTSUPP;
}

int cor_rd_socket_listen(struct socket *sock, int len)
{
	return -EOPNOTSUPP;
}

int cor_rd_socket_shutdown(struct socket *sock, int flags)
{
	return 0;
}

int cor_rd_ioctl(struct socket *sock, unsigned int cmd, unsigned long arg)
{
	return -ENOIOCTLCMD;
}

int cor_rd_setsockopt(struct socket *sock, int level,
		int optname, char __user *optval, unsigned int optlen)
{
	return -ENOPROTOOPT;
}

int cor_rd_getsockopt(struct socket *sock, int level,
		int optname, char __user *optval, int __user *optlen)
{
	return -ENOPROTOOPT;
}

static int cor_rd_parse_version(struct cor_rdsock *crd, __u32 cmd,
		char *param, __u32 paramlen)
{
	int rc = 0;
	__u32 version;

	mutex_lock(&cor_rds_lock);

	BUG_ON(crd == 0);

	if (paramlen != 4)
		goto err;

	version = cor_parse_u32(param);
	if (version != 0)
		goto err;

	if (crd->versioninited != 0)
		goto err;

	crd->versioninited = 1;

	if (0) {
err:
		rc = 1;
	}
	mutex_unlock(&cor_rds_lock);

	return rc;
}

/* interface_config_lock must be held */


static int _cor_rd_parse_up_interfaces(struct cor_rdsock *crd, char *param,
		__u32 paramlen, __u32 *offset)
{
	__u32 num_intf;
	__u32 i;
	struct cor_interface_config *newconfig = 0;

	if (unlikely(*offset + 4 > paramlen))
		return 1;

	num_intf = cor_parse_u32(param + *offset);
	*offset += 4;

	if (unlikely(num_intf > 65536))
		return 1;

	newconfig = kmalloc(num_intf * sizeof(struct cor_interface_config),
			GFP_KERNEL);
	if (unlikely(newconfig == 0))
		return 1;

	memset(newconfig, 0, num_intf * sizeof(struct cor_interface_config));

	for (i = 0; i < num_intf; i++) {
		struct cor_interface_config *newconfig_curr = &newconfig[i];

		if (unlikely(*offset + 4 > paramlen))
			goto out_err;

		newconfig_curr->name_len = cor_parse_u32(param + *offset);
		*offset += 4;

		if (unlikely(*offset + newconfig_curr->name_len < paramlen))
			goto out_err;

		newconfig_curr->name = kmalloc(newconfig_curr->name_len,
				GFP_KERNEL);
		if (unlikely(newconfig_curr->name == 0))
			goto out_err;

		memcpy(newconfig_curr->name, param + *offset,
				newconfig_curr->name_len);
		*offset += newconfig_curr->name_len;
	}

	cor_set_interface_config(newconfig, num_intf, 0);

	return 0;

out_err:
	while (i > 0) {
		struct cor_interface_config *newconfig_curr;

		i--;

		newconfig_curr = &newconfig[i];

		BUG_ON(newconfig_curr->name == 0);
		kfree(newconfig_curr->name);
		newconfig_curr->name = 0;
	}
	kfree(newconfig);
	return 1;
}

static int cor_rd_parse_up(struct cor_rdsock *crd, __u32 cmd,
		char *param, __u32 paramlen)
{
	__u32 offset = 0;

	__u64 flags;
	__u8 has_addr = 0;
	__be64 addr = 0;

	if (unlikely(paramlen < 8))
		return 1;

	flags = cor_parse_u64(param);
	offset += 8;

	if ((flags & CRD_UTK_UP_FLAGS_ADDR) != 0) {
		if (unlikely(paramlen - offset < 8))
			return 1;

		has_addr = 1;
		addr = cor_parse_be64(param + offset);
		offset += 8;
	}

	if ((flags & CRD_UTK_UP_FLAGS_INTERFACES) != 0) {
		if (_cor_rd_parse_up_interfaces(crd, param, paramlen, &offset)
				!= 0) {
			return 1;
		}
	} else {
		cor_set_interface_config(0, 0, 1);
	}

	if (cor_config_up(has_addr, addr) != 0)
		return 1;

	return 0;
}

static int cor_rd_parse_connecterror(struct cor_rdsock *crd, __u32 cmd,
		char *param, __u32 paramlen)
{
	__be64 cookie;
	__u32 error;
	int errorno;

	if (unlikely(paramlen < 12))
		return 1;

	cookie = cor_parse_be64(param);
	error = cor_parse_u32(param + 8);

	if (error == CRD_UTK_CONNECTERROR_ACCES)
		errorno = EACCES;
	else if (error == CRD_UTK_CONNECTERROR_NETUNREACH)
		errorno = ENETUNREACH;
	else if (error == CRD_UTK_CONNECTERROR_TIMEDOUT)
		errorno = ETIMEDOUT;
	else if (error == CRD_UTK_CONNECTERROR_REFUSED)
		errorno = ECONNREFUSED;
	else
		errorno = ENETUNREACH;

	cor_set_sock_connecterror(cookie, errorno);

	return 0;
}

static int cor_rd_parse(struct cor_rdsock *crd, __u32 cmd, char *param,
		__u32 paramlen)
{
	if (unlikely(unlikely(cmd != CRD_UTK_VERSION) &&
			unlikely(crd->versioninited == 0)))
		return 1;

	if (cmd == CRD_UTK_VERSION)
		return cor_rd_parse_version(crd, cmd, param, paramlen);
	else if (cmd == CRD_UTK_UP)
		return cor_rd_parse_up(crd, cmd, param, paramlen);
	else if (cmd == CRD_UTK_CONNECTERROR)
		return cor_rd_parse_connecterror(crd, cmd, param, paramlen);
	else
		return 1;
}

static int _cor_rd_sendmsg_hdr(struct cor_rdsock *crd, struct msghdr *msg,
		__u32 len)
{
	__u32 cpy;
	size_t st_rc;

	BUG_ON(len == 0);
	BUG_ON(len > (1024 * 1024 * 1024));

	BUG_ON(crd->snd_cmdplen_read > 8);
	cpy = (8 - crd->snd_cmdplen_read);
	if (unlikely(cpy > len))
		cpy = len;

	st_rc = copy_from_iter(crd->user_copy.snd_cmdplen_buf +
			crd->snd_cmdplen_read, cpy, &msg->msg_iter);

	if (unlikely(st_rc != cpy))
		return -EFAULT;

	crd->snd_cmdplen_read += cpy;

	return cpy;
}

static int _cor_rd_sendmsg_body(struct cor_rdsock *crd, struct msghdr *msg,
		__u32 len)
{
	__u32 cpy = 0;

	__u32 cmd;
	__u32 paramlen;

	BUG_ON(len == 0);
	BUG_ON(len > (1024 * 1024 * 1024));

	BUG_ON(crd->snd_cmdplen_read != 8);

	cmd = cor_parse_u32(crd->user_copy.snd_cmdplen_buf);
	paramlen = cor_parse_u32(crd->user_copy.snd_cmdplen_buf + 4);

	if (crd->cmdparams == 0 && paramlen != 0) {
		BUG_ON(crd->param_read != 0);
		if (unlikely(paramlen > MAX_SND_MSGLEN))
			return -ECONNRESET;

		crd->cmdparams = kmalloc(paramlen, GFP_KERNEL);
		if (unlikely(crd->cmdparams == 0))
			return -ENOMEM;
	}

	if (crd->param_read < paramlen) {
		size_t st_rc;

		cpy = (paramlen - crd->param_read);
		if (cpy > len)
			cpy = len;

		BUG_ON(crd->cmdparams == 0);

		st_rc = copy_from_iter(crd->cmdparams +
				crd->param_read, cpy, &msg->msg_iter);

		if (unlikely(st_rc != cpy))
			return -EFAULT;

		crd->param_read += cpy;
	}

	BUG_ON(crd->param_read > paramlen);

	if (crd->param_read == paramlen) {
		int rc = cor_rd_parse(crd, cmd, crd->cmdparams, paramlen);

		if (unlikely(rc != 0))
			return -ECONNRESET;

		memset(crd->user_copy.snd_cmdplen_buf, 0,
				sizeof(crd->snd_cmdplen_read));
		crd->snd_cmdplen_read = 0;
		crd->param_read = 0;
		kfree(crd->cmdparams);
		crd->cmdparams = 0;
	}

	return cpy;
}

static int _cor_rd_sendmsg(struct cor_rdsock *crd, struct msghdr *msg,
		__u32 len)
{
	if (crd->snd_cmdplen_read < 8)
		return _cor_rd_sendmsg_hdr(crd, msg, len);
	else
		return _cor_rd_sendmsg_body(crd, msg, len);
}

int cor_rd_sendmsg(struct socket *sock, struct msghdr *msg, size_t total_len)
{
	struct cor_rdsock *crd = (struct cor_rdsock *) sock->sk;

	int rc = 0;
	__u32 totalread = 0;
	__u32 currread = 0;

	__u32 len;

	if (unlikely(total_len > 1024 * 1024 * 1024))
		len = 1024 * 1024 * 1024;
	else
		len = (__u32) total_len;

	if (unlikely(atomic_read(&crd->connected) == 0))
		return -ENOTCONN;

	mutex_lock(&crd->sndbuf_lock);

	while (currread < len) {
		rc = _cor_rd_sendmsg(crd, msg, len - currread);
		if (unlikely(rc < 0))
			goto out;
		currread += rc;
		totalread += currread;
	}

out:
	mutex_unlock(&crd->sndbuf_lock);

	if (rc >= 0 && totalread != 0) {
		BUG_ON(totalread > (1024 * 1024 * 1024));
		rc = totalread;
	}

	return rc;
}

static void cor_fill_msgbuf_supportedversions(struct cor_rdsock *crd,
		struct cor_rd_msg *rdm)
{
	BUG_ON(rdm->cs != 0);

	BUG_ON(MAX_MSG_LEN < 16);

	cor_put_u32(crd->user_copy.rcvbuf, CRD_KTU_SUPPORTEDVERSIONS);
	cor_put_u32(crd->user_copy.rcvbuf + 4, 8); /* len */
	cor_put_u32(crd->user_copy.rcvbuf + 8, 0);
	cor_put_u32(crd->user_copy.rcvbuf + 12, 0);

	crd->rcvbuflen = 16;
}

static void cor_fill_msgbuf_connect(struct cor_rdsock *crd,
		struct cor_rd_msg *rdm)
{
	char *remoteaddr;
	__u32 remoteaddr_len;

	BUG_ON(rdm->cs == 0);
	mutex_lock(&rdm->cs->lock);
	BUG_ON(rdm->cs->type != CS_TYPE_CONN_MANAGED);

	remoteaddr = (char *) &rdm->cs->data.conn_managed.remoteaddr;
	remoteaddr_len = sizeof(struct cor_sockaddr);
	BUILD_BUG_ON(remoteaddr_len != 16);

	BUG_ON(MAX_MSG_LEN < (20 + remoteaddr_len));

	cor_put_u32(crd->user_copy.rcvbuf, CRD_KTU_CONNECT);
	cor_put_u32(crd->user_copy.rcvbuf + 4, 12 + remoteaddr_len);
	cor_put_be64(crd->user_copy.rcvbuf + 8,
			rdm->cs->data.conn_managed.cookie);
	memcpy(crd->user_copy.rcvbuf + 16, remoteaddr, remoteaddr_len);
	cor_put_u32(crd->user_copy.rcvbuf + 16 + remoteaddr_len,
			rdm->cs->is_highlatency ?
			COR_TOS_HIGH_LATENCY : COR_TOS_LOW_LATENCY);

	crd->rcvbuflen = 20 + remoteaddr_len;
	mutex_unlock(&rdm->cs->lock);
}

static void _cor_fill_msgbuf(struct cor_rdsock *crd, struct cor_rd_msg *rdm)
{
	if (rdm->type == CRD_KTU_SUPPORTEDVERSIONS) {
		cor_fill_msgbuf_supportedversions(crd, rdm);
	} else if (rdm->type == CRD_KTU_CONNECT) {
		cor_fill_msgbuf_connect(crd, rdm);
	} else {
		BUG();
	}
}

static int cor_fill_msgbuf(struct socket *sock, struct cor_rdsock *crd,
		int blocking)
{
	int rc = 0;
	struct cor_rd_msg *rdm = 0;

	while (1) {
		mutex_lock(&cor_rds_lock);
		if (list_empty(&crd->rcv_msgs) == 0)
			break;
		atomic_set(&crd->ready_to_read, 0);
		mutex_unlock(&cor_rds_lock);

		if (blocking == 0)
			return -EAGAIN;

		if (wait_event_interruptible(*sk_sleep(sock->sk),
				atomic_read(&crd->ready_to_read) != 0) != 0)
			return -ERESTARTSYS;
	}

	rdm = container_of(crd->rcv_msgs.next, struct cor_rd_msg, lh);
	list_del(&rdm->lh);

	if (rdm->cs != 0)
		list_del(&rdm->cs_lh);

	mutex_unlock(&cor_rds_lock);

	memset(crd->user_copy.rcvbuf, 0, sizeof(crd->user_copy.rcvbuf));
	crd->rcvbuflen = 0;
	crd->rcvbufoffset = 0;

	_cor_fill_msgbuf(crd, rdm);

	if (rdm->cs != 0) {
		kref_put(&rdm->cs->ref, cor_free_sock);
		rdm->cs = 0;
	}

	kmem_cache_free(cor_rdmsg_slab, rdm);

	return rc;
}

int cor_rd_recvmsg(struct socket *sock, struct msghdr *msg, size_t total_len,
		int flags)
{
	int copied = 0;
	int blocking = (flags & MSG_DONTWAIT) == 0;

	struct cor_rdsock *crd = (struct cor_rdsock *) sock->sk;

	__u32 totallen;

	if (unlikely(total_len > 1024 * 1024 * 1024))
		totallen = 1024 * 1024 * 1024;
	else
		totallen = (__u32) total_len;


	if (unlikely((flags & MSG_PEEK) != 0))
		return -EINVAL;

	if (unlikely(atomic_read(&crd->connected) == 0))
		return -ENOTCONN;

	mutex_lock(&crd->rcvbuf_lock);
	while (copied < totallen) {
		__u32 len = totallen - copied;
		size_t st_rc;

		if (crd->rcvbufoffset == crd->rcvbuflen) {
			int rc = cor_fill_msgbuf(sock, crd,
					blocking && copied == 0);
			if (rc != 0 && copied == 0)
				copied = rc;
			if (rc != 0)
				break;
		}

		BUG_ON(crd->rcvbufoffset > crd->rcvbuflen);

		if (len > (crd->rcvbuflen - crd->rcvbufoffset))
			len = crd->rcvbuflen - crd->rcvbufoffset;

		st_rc = copy_to_iter(crd->user_copy.rcvbuf + crd->rcvbufoffset,
				len, &msg->msg_iter);

		if (unlikely(st_rc != len)) {
			copied = -EFAULT;
			break;
		}

		copied += len;
		crd->rcvbufoffset += len;
	}
	mutex_unlock(&crd->rcvbuf_lock);

	BUG_ON(copied > 0 && unlikely((copied > total_len ||
			copied > totallen)));

	return copied;
}

static unsigned int cor_rd_poll(struct file *file, struct socket *sock,
		poll_table *wait)
{
	unsigned int mask = 0;

	struct cor_rdsock *crd = (struct cor_rdsock *) sock->sk;

	if (unlikely(atomic_read(&crd->connected) == 0))
		return 0;

	sock_poll_wait(file, sock, wait);

	mutex_lock(&crd->rcvbuf_lock);
	mutex_lock(&cor_rds_lock);

	if (crd->rcvbufoffset != crd->rcvbuflen ||
			(list_empty(&crd->rcv_msgs) == 0))
		mask |= (POLLIN | POLLRDNORM);

	mutex_unlock(&cor_rds_lock);
	mutex_unlock(&crd->rcvbuf_lock);

	mask |= (POLLOUT | POLLWRNORM);

	return mask;
}

struct proto cor_rd_proto = {
	.name = "cor_rd",
	.obj_size = sizeof(struct cor_rdsock),
	.useroffset = offsetof(struct cor_rdsock, user_copy),
	.usersize = sizeof(((struct cor_rdsock *) 0)->user_copy),
	.owner = THIS_MODULE,
};

const struct proto_ops cor_rd_proto_ops = {
		.family = PF_COR,
		.owner = THIS_MODULE,
		.release = cor_rd_socket_release,
		.bind = cor_rd_socket_bind,
		.connect = cor_rd_socket_connect,
		.accept = cor_rd_socket_accept,
		.listen = cor_rd_socket_listen,
		.shutdown = cor_rd_socket_shutdown,
		.ioctl = cor_rd_ioctl,
		.setsockopt = cor_rd_setsockopt,
		.getsockopt = cor_rd_getsockopt,
 #ifdef CONFIG_COMPAT
		.combat_ioctl = cor_rd_ioctl,
		.compat_setsockopt = cor_rd_setsockopt,
		.compat_getsockopt = cor_rd_getsockopt,
#endif
		.sendmsg = cor_rd_sendmsg,
		.recvmsg = cor_rd_recvmsg,
		.poll = cor_rd_poll,

		.socketpair = cor_socket_socketpair,
		.getname = cor_socket_getname,
		.mmap = cor_socket_mmap,

	/* sendpage, splice_read, are optional */
};

int cor_create_rdaemon_sock(struct net *net, struct socket *sock, int protocol,
		int kern)
{
	struct cor_rd_msg *rdm = 0;
	struct cor_rdsock *newcrd = 0;

	rdm = kmem_cache_alloc(cor_rdmsg_slab, GFP_KERNEL);
	if (unlikely(rdm == 0))
		return -ENOMEM;

	newcrd = (struct cor_rdsock *) sk_alloc(net, PF_COR, GFP_KERNEL,
			&cor_rd_proto, kern);
	if (unlikely(newcrd == 0)) {
		kmem_cache_free(cor_rdmsg_slab, rdm);
		return -ENOMEM;
	}

	sock_init_data(sock, (struct sock *) newcrd);
	newcrd->sk.sk_protocol = protocol;
	memset(((char *)newcrd) + sizeof(struct sock), 0,
			sizeof(struct cor_rdsock) - sizeof(struct sock));

	atomic_set(&newcrd->connected, 0);
	INIT_LIST_HEAD(&newcrd->socks);
	mutex_init(&newcrd->sndbuf_lock);
	mutex_init(&newcrd->rcvbuf_lock);
	atomic_set(&newcrd->ready_to_read, 0);
	INIT_LIST_HEAD(&newcrd->rcv_msgs);

	mutex_lock(&cor_rds_lock);
	if (cor_crd != 0) {
		sock_put((struct sock *) newcrd);
		mutex_unlock(&cor_rds_lock);
		kmem_cache_free(cor_rdmsg_slab, rdm);
		return -EACCES;
	}
	cor_crd = newcrd;

	memset(rdm, 0, sizeof(struct cor_rd_msg));
	rdm->type = CRD_KTU_SUPPORTEDVERSIONS;
	list_add_tail(&rdm->lh, &cor_crd->rcv_msgs);

	atomic_set(&newcrd->ready_to_read, 1);

	mutex_unlock(&cor_rds_lock);

	sock->state = SS_UNCONNECTED;
	sock->ops = &cor_rd_proto_ops;
	sock->sk = (struct sock *) cor_crd;

	return 0;
}

int cor_rdreq_connect(struct cor_sock *cs)
{
	int rc;

	struct cor_rd_msg *rdm = kmem_cache_alloc(cor_rdmsg_slab, GFP_KERNEL);

	mutex_lock(&cor_rds_lock);
	mutex_lock(&cs->lock);

	BUG_ON(cs->type != CS_TYPE_CONN_MANAGED);
	BUG_ON(cs->data.conn_managed.cookie == 0);

	if (unlikely(cor_crd == 0 || atomic_read(&cor_crd->connected) == 0 ||
			cor_crd->versioninited == 0)) {
		rc = -ENETUNREACH;
		goto out;
	}

	if (unlikely(rdm == 0)) {
		rc = -ETIMEDOUT;
		goto out;
	}

	memset(rdm, 0, sizeof(struct cor_rd_msg));

	kref_get(&cs->ref);
	list_add_tail(&rdm->cs_lh, &cs->data.conn_managed.rd_msgs);
	rdm->cs = cs;
	rdm->type = CRD_KTU_CONNECT;

	if (list_empty(&cor_crd->rcv_msgs)) {
		atomic_set(&cor_crd->ready_to_read, 1);
		barrier();
		cor_crd->sk.sk_data_ready(&cor_crd->sk);
	}
	list_add_tail(&rdm->lh, &cor_crd->rcv_msgs);
	kref_get(&cs->ref);

	kref_get(&cs->ref);
	list_add_tail(&cs->data.conn_managed.crd_lh, &cor_crd->socks);
	cs->data.conn_managed.in_crd_list = 1;

	rc = -EINPROGRESS;

out:
	mutex_unlock(&cs->lock);
	mutex_unlock(&cor_rds_lock);

	return rc;
}

void cor_usersock_release(struct cor_sock *cs)
{
	mutex_lock(&cor_rds_lock);
	mutex_lock(&cs->lock);

	if (cs->type != CS_TYPE_CONN_MANAGED)
		goto out;

	while (list_empty(&cs->data.conn_managed.rd_msgs) == 0) {
		struct cor_rd_msg *rdm = container_of(
				cs->data.conn_managed.rd_msgs.next,
				struct cor_rd_msg, cs_lh);

		list_del(&rdm->lh);
		BUG_ON(rdm->cs != cs);
		list_del(&rdm->cs_lh);
		kref_put(&cs->ref, cor_kreffree_bug);
		rdm->cs = 0;
		kmem_cache_free(cor_rdmsg_slab, rdm);
	}

	if (cs->data.conn_managed.in_crd_list != 0) {
		list_del(&cs->data.conn_managed.crd_lh);
		cs->data.conn_managed.in_crd_list = 0;
		kref_put(&cs->ref, cor_kreffree_bug);
	}

out:
	mutex_unlock(&cs->lock);
	mutex_unlock(&cor_rds_lock);
}

int __init cor_rd_init1(void)
{
	cor_rdmsg_slab = kmem_cache_create("cor_rdmsg",
			sizeof(struct cor_rd_msg), 8, 0, 0);
	if (unlikely(cor_rdmsg_slab == 0))
		return -ENOMEM;

	return 0;
}

int __init cor_rd_init2(void)
{
	return proto_register(&cor_rd_proto, 1);
}

void __exit cor_rd_exit1(void)
{
	proto_unregister(&cor_rd_proto);
}

void __exit cor_rd_exit2(void)
{
	kmem_cache_destroy(cor_rdmsg_slab);
	cor_rdmsg_slab = 0;
}

MODULE_LICENSE("GPL");
