/**
 *	Connection oriented routing
 *	Copyright (C) 2007-2021 Michael Blizek
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License
 *	as published by the Free Software Foundation; either version 2
 *	of the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 */

#include <asm/byteorder.h>

#include "cor.h"

/* not sent over the network - internal meaning only */
#define MSGTYPE_PONG 1
#define MSGTYPE_ACK 2
#define MSGTYPE_ACK_CONN 3
#define MSGTYPE_CONNECT 4
#define MSGTYPE_CONNECT_SUCCESS 5
#define MSGTYPE_RESET_CONN 6
#define MSGTYPE_CONNDATA 7
#define MSGTYPE_SET_MAX_CMSG_DELAY 8
#define MSGTYPE_SET_RCVMTU 9

#define MSGTYPE_PONG_TIMEENQUEUED 1
#define MSGTYPE_PONG_RESPDELAY 2

struct cor_control_msg_out {
	__u8 type;
	__u32 length;
	struct kref ref;
	struct cor_neighbor *nb;

	/* either queue or control_retrans_packet */
	struct list_head lh;

	unsigned long time_added;

	union{
		struct{
			__u32 cookie;
			__u8 type;

			ktime_t ping_rcvtime;
			ktime_t time_enqueued;
		} pong;

		struct{
			__u32 seqno;
			__u8 fast;
		} ack;

		struct{
			struct cor_conn *src_in;
			struct list_head conn_acks;
			__u32 conn_id;
			__u32 seqno;
			__u32 seqno_ooo;
			__u32 length;

			__u8 flags;

			__u8 bufsize_changerate;

			__u16 priority;
			__u8 priority_seqno;
			__u8 is_highlatency;

			__u8 queue;
		} ack_conn;

		struct{
			__u32 conn_id;
			__u32 seqno1;
			__u32 seqno2;
			struct cor_conn *src_in;
		} connect;

		struct{
			__u32 conn_id;
			struct cor_conn *src_in;
		} connect_success;

		struct{
			struct rb_node rbn;
			__u8 in_pending_conn_resets;
			__u32 conn_id;
		} reset_conn;

		struct{
			__u32 conn_id;
			__u32 seqno;
			__u32 datalen;
			__u8 windowused;
			__u8 flush;
			__u8 highlatency;
			char *data_orig;
			char *data;
			struct cor_conn_retrans *cr;
		} conn_data;

		struct{
			__u32 ack_fast_delay;
			__u32 ack_slow_delay;
			__u32 ackconn_lowlatency_delay;
			__u32 pong_delay;
		} set_max_cmsg_delay;

		struct{
			__u32 rcvmtu;
		} set_rcvmtu;
	} msg;
};

struct cor_control_retrans {
	struct kref ref;

	struct cor_neighbor *nb;
	__u32 seqno;

	unsigned long timeout;

	struct list_head msgs;

	struct rb_node rbn;
	struct list_head timeout_list;
};


static struct kmem_cache *cor_controlmsg_slab;
static struct kmem_cache *cor_controlretrans_slab;

static atomic_t cor_cmsg_othercnt = ATOMIC_INIT(0);

#define ADDCMSG_SRC_NEW 1
#define ADDCMSG_SRC_SPLITCONNDATA 2
#define ADDCMSG_SRC_READD 3
#define ADDCMSG_SRC_RETRANS 4

static void cor_enqueue_control_msg(struct cor_control_msg_out *msg, int src);

static void cor_try_merge_ackconns(struct cor_conn *src_in_l,
		struct cor_control_msg_out *cm);

static void cor_merge_or_enqueue_ackconn(struct cor_conn *src_in_l,
		struct cor_control_msg_out *cm, int src);

static struct cor_control_msg_out *_cor_alloc_control_msg(
		struct cor_neighbor *nb)
{
	struct cor_control_msg_out *cm;

	BUG_ON(nb == 0);

	cm = kmem_cache_alloc(cor_controlmsg_slab, GFP_ATOMIC);
	if (unlikely(cm == 0))
		return 0;
	memset(cm, 0, sizeof(struct cor_control_msg_out));
	kref_init(&cm->ref);
	cm->nb = nb;
	return cm;
}

static int cor_calc_limit(int limit, int priority)
{
	if (priority == ACM_PRIORITY_LOW)
		return (limit + 1) / 2;
	else if (priority == ACM_PRIORITY_MED)
		return (limit * 3 + 1) / 4;
	else if (priority == ACM_PRIORITY_HIGH)
		return limit;
	else
		BUG();
}

struct cor_control_msg_out *cor_alloc_control_msg(struct cor_neighbor *nb,
		int priority)
{
	struct cor_control_msg_out *cm = 0;

	long packets1;
	long packets2;

	BUG_ON(nb == 0);

	packets1 = atomic_inc_return(&nb->cmsg_othercnt);
	packets2 = atomic_inc_return(&cor_cmsg_othercnt);

	BUG_ON(packets1 <= 0);
	BUG_ON(packets2 <= 0);

	if (packets1 <= cor_calc_limit(GUARANTEED_CMSGS_PER_NEIGH, priority))
		goto alloc;

	if (unlikely(unlikely(packets1 > cor_calc_limit(MAX_CMSGS_PER_NEIGH,
			priority)) ||
			unlikely(packets2 > cor_calc_limit(MAX_CMSGS,
			priority))))
		goto full;

alloc:
	cm = _cor_alloc_control_msg(nb);
	if (unlikely(cm == 0)) {
full:

		/* printk(KERN_ERR "cor_alloc_control_msg failed %ld %ld\n",
				packets1, packets2); */
		atomic_dec(&nb->cmsg_othercnt);
		atomic_dec(&cor_cmsg_othercnt);
	}
	return cm;
}

static void cor_cmsg_kref_free(struct kref *ref)
{
	struct cor_control_msg_out *cm = container_of(ref,
			struct cor_control_msg_out, ref);
	kmem_cache_free(cor_controlmsg_slab, cm);
}

void cor_free_control_msg(struct cor_control_msg_out *cm)
{
	if (likely(cm->type != MSGTYPE_PONG)) {
		atomic_dec(&cm->nb->cmsg_othercnt);
		atomic_dec(&cor_cmsg_othercnt);
	}

	if (cm->type == MSGTYPE_ACK_CONN) {
		BUG_ON(cm->msg.ack_conn.src_in == 0);
		if ((cm->msg.ack_conn.flags & KP_ACK_CONN_FLAGS_PRIORITY) != 0) {
			struct cor_conn *trgt_out = cor_get_conn_reversedir(
					cm->msg.ack_conn.src_in);
			spin_lock_bh(&trgt_out->rcv_lock);
			BUG_ON(trgt_out->targettype != TARGET_OUT);
			if (trgt_out->trgt.out.priority_send_allowed == 0) {
				trgt_out->trgt.out.priority_send_allowed = 1;
				spin_unlock_bh(&trgt_out->rcv_lock);
				cor_conn_refresh_priority(trgt_out, 0);
			} else {
				spin_unlock_bh(&trgt_out->rcv_lock);
			}
		}
		cor_conn_kref_put(cm->msg.ack_conn.src_in,
				"cor_control_msg_out ack_conn");
		cm->msg.ack_conn.src_in = 0;
	} else if (cm->type == MSGTYPE_CONNECT) {
		BUG_ON(cm->msg.connect.src_in == 0);
		cor_conn_kref_put(cm->msg.connect.src_in,
				"cor_control_msg_out connect");
		cm->msg.connect.src_in = 0;
	} else if (cm->type == MSGTYPE_CONNECT_SUCCESS) {
		BUG_ON(cm->msg.connect_success.src_in == 0);
		cor_conn_kref_put(cm->msg.connect_success.src_in,
				"cor_control_msg_out connect_success");
		cm->msg.connect_success.src_in = 0;
	} else if (cm->type == MSGTYPE_RESET_CONN) {
		spin_lock_bh(&cm->nb->cmsg_lock);
		if (cm->msg.reset_conn.in_pending_conn_resets != 0) {
			rb_erase(&cm->msg.reset_conn.rbn,
					&cm->nb->pending_conn_resets_rb);
			cm->msg.reset_conn.in_pending_conn_resets = 0;

			kref_put(&cm->ref, cor_kreffree_bug);
		}
		spin_unlock_bh(&cm->nb->cmsg_lock);
	}

	kref_put(&cm->ref, cor_cmsg_kref_free);
}

static void cor_free_control_retrans(struct kref *ref)
{
	struct cor_control_retrans *cr = container_of(ref,
			struct cor_control_retrans, ref);

	while (list_empty(&cr->msgs) == 0) {
		struct cor_control_msg_out *cm = container_of(cr->msgs.next,
				struct cor_control_msg_out, lh);

		if (cm->type == MSGTYPE_PONG)
			atomic_dec(&cm->nb->cmsg_pongs_retrans_cnt);

		list_del(&cm->lh);
		cor_free_control_msg(cm);
	}

	kmem_cache_free(cor_controlretrans_slab, cr);
}

struct cor_control_retrans *cor_get_control_retrans(
		struct cor_neighbor *nb_retranslocked, __u32 seqno)
{
	struct rb_node *n = nb_retranslocked->kp_retransmits_rb.rb_node;

	while (likely(n != 0)) {
		struct cor_control_retrans *cr = container_of(n,
				struct cor_control_retrans, rbn);

		BUG_ON(cr->nb != nb_retranslocked);

		if (seqno < cr->seqno) {
			n = n->rb_left;
		} else if (seqno > cr->seqno) {
			n = n->rb_right;
		} else {
			kref_get(&cr->ref);
			return cr;
		}
	}

	return 0;
}

/* nb->retrans_lock must be held */
void cor_insert_control_retrans(struct cor_control_retrans *ins)
{
	struct cor_neighbor *nb = ins->nb;
	__u32 seqno = ins->seqno;

	struct rb_root *root;
	struct rb_node **p;
	struct rb_node *parent = 0;

	BUG_ON(nb == 0);

	root = &nb->kp_retransmits_rb;
	p = &root->rb_node;

	while ((*p) != 0) {
		struct cor_control_retrans *cr = container_of(*p,
				struct cor_control_retrans, rbn);

		BUG_ON(cr->nb != nb);

		parent = *p;
		if (unlikely(seqno == cr->seqno)) {
			BUG();
		} else if (seqno < cr->seqno) {
			p = &(*p)->rb_left;
		} else if (seqno > cr->seqno) {
			p = &(*p)->rb_right;
		}
	}

	kref_get(&ins->ref);
	rb_link_node(&ins->rbn, parent, p);
	rb_insert_color(&ins->rbn, root);
}

static void cor_remove_connack_oooflag_ifold(struct cor_conn *src_in_l,
		struct cor_control_msg_out *cm)
{
	if (cor_ooolen(cm->msg.ack_conn.flags) != 0 && cor_seqno_before_eq(
			cm->msg.ack_conn.seqno_ooo +
			cm->msg.ack_conn.length,
			src_in_l->src.in.next_seqno)) {
		cm->msg.ack_conn.length = 0;
		cm->msg.ack_conn.flags = (cm->msg.ack_conn.flags &
			(~KP_ACK_CONN_FLAGS_OOO));
	}
}

static int cor_ackconn_prepare_requeue(struct cor_conn *cn_l,
		struct cor_control_msg_out *cm)
{
	if (unlikely(unlikely(cn_l->sourcetype != SOURCE_IN) ||
			unlikely(cn_l->src.in.nb != cm->nb) ||
			unlikely(
			cor_get_connid_reverse(cn_l->src.in.conn_id) !=
			cm->msg.ack_conn.conn_id) ||
			unlikely(cn_l->isreset != 0)))
		return 0;

	cor_remove_connack_oooflag_ifold(cn_l, cm);

	if (cor_seqno_before(cm->msg.ack_conn.seqno, cn_l->src.in.next_seqno))
		cm->msg.ack_conn.flags = (cm->msg.ack_conn.flags &
				(~KP_ACK_CONN_FLAGS_SEQNO) &
				(~KP_ACK_CONN_FLAGS_WINDOW));

	if (cm->msg.ack_conn.flags == 0)
		return 0;

	cm->length = 5 + cor_ack_conn_len(cm->msg.ack_conn.flags);

	return 1;
}

static void cor_requeue_control_retrans(struct cor_control_retrans *cr)
{
	atomic_inc(&cr->nb->cmsg_bulk_readds);

	while (list_empty(&cr->msgs) == 0) {
		struct cor_control_msg_out *cm = container_of(cr->msgs.prev,
				struct cor_control_msg_out, lh);
		list_del(&cm->lh);

		BUG_ON(cm->nb != cr->nb);

		if (cm->type == MSGTYPE_ACK_CONN) {
			struct cor_conn *cn_l = cm->msg.ack_conn.src_in;

			spin_lock_bh(&cn_l->rcv_lock);
			if (unlikely(cor_ackconn_prepare_requeue(cn_l,
					cm) == 0)) {
				cor_free_control_msg(cm);
			} else {
				cor_merge_or_enqueue_ackconn(cn_l, cm,
						ADDCMSG_SRC_RETRANS);
			}

			spin_unlock_bh(&cn_l->rcv_lock);
		} else {
			if (cm->type == MSGTYPE_PONG)
				atomic_dec(&cm->nb->cmsg_pongs_retrans_cnt);
			cor_enqueue_control_msg(cm, ADDCMSG_SRC_RETRANS);
		}
	}

	atomic_dec(&cr->nb->cmsg_bulk_readds);

	spin_lock_bh(&cr->nb->cmsg_lock);
	cor_schedule_controlmsg_timer(cr->nb);
	spin_unlock_bh(&cr->nb->cmsg_lock);
}

static void _cor_empty_retrans_queue(struct cor_neighbor *nb_retranslocked,
		struct list_head *retrans_list)
{
	while (!list_empty(retrans_list)) {
		struct cor_control_retrans *cr = container_of(
				retrans_list->next, struct cor_control_retrans,
				timeout_list);

		BUG_ON(cr->nb != nb_retranslocked);

		list_del(&cr->timeout_list);
		rb_erase(&cr->rbn, &nb_retranslocked->kp_retransmits_rb);

		kref_put(&cr->ref, cor_kreffree_bug); /* rb */
		kref_put(&cr->ref, cor_free_control_retrans); /* list */
	}
}

static void cor_empty_retrans_queue(struct cor_neighbor *nb_retranslocked)
{
	_cor_empty_retrans_queue(nb_retranslocked,
			&nb_retranslocked->retrans_fast_list);
	_cor_empty_retrans_queue(nb_retranslocked,
			&nb_retranslocked->retrans_slow_list);
}

static unsigned long cor_get_retransmit_timeout(
		struct cor_neighbor *nb_retranslocked)
{
	struct cor_control_retrans *cr1 = 0;
	struct cor_control_retrans *cr2 = 0;
	struct cor_control_retrans *cr = 0;

	if (list_empty(&nb_retranslocked->retrans_fast_list) == 0) {
		cr1 = container_of(nb_retranslocked->retrans_fast_list.next,
				struct cor_control_retrans, timeout_list);
		BUG_ON(cr1->nb != nb_retranslocked);
	}

	if (list_empty(&nb_retranslocked->retrans_slow_list) == 0) {
		cr2 = container_of(nb_retranslocked->retrans_slow_list.next,
				struct cor_control_retrans, timeout_list);
		BUG_ON(cr2->nb != nb_retranslocked);
	}

	if (cr1 == 0)
		cr = cr2;
	else if (cr2 == 0)
		cr = cr1;
	else
		cr = (time_after(cr1->timeout, cr2->timeout) ? cr2 : cr1);

	BUG_ON(cr == 0);

	return cr->timeout;
}

void cor_retransmit_timerfunc(struct timer_list *retrans_timer)
{
	struct cor_neighbor *nb = container_of(retrans_timer,
			struct cor_neighbor, retrans_timer);
	int nbstate = cor_get_neigh_state(nb);
	unsigned long timeout;

	spin_lock_bh(&nb->retrans_lock);

	if (list_empty(&nb->retrans_fast_list) &&
			list_empty(&nb->retrans_slow_list)) {
		spin_unlock_bh(&nb->retrans_lock);
		cor_nb_kref_put(nb, "retransmit_timer");
		return;
	}

	if (unlikely(nbstate == NEIGHBOR_STATE_KILLED)) {
		cor_empty_retrans_queue(nb);
		spin_unlock_bh(&nb->retrans_lock);
		cor_nb_kref_put(nb, "retransmit_timer");
		return;
	}

	timeout = cor_get_retransmit_timeout(nb);

	if (time_after(timeout, jiffies)) {
		int rc = mod_timer(&nb->retrans_timer, timeout);

		spin_unlock_bh(&nb->retrans_lock);
		if (rc != 0)
			cor_nb_kref_put(nb, "retransmit_timer");
		return;
	}

	spin_unlock_bh(&nb->retrans_lock);

	spin_lock_bh(&nb->cmsg_lock);
	nb->add_retrans_needed = 1;
	cor_schedule_controlmsg_timer(nb);
	spin_unlock_bh(&nb->cmsg_lock);

	cor_nb_kref_put(nb, "retransmit_timer");
}

static void cor_schedule_retransmit(struct cor_control_retrans *cr,
		struct cor_neighbor *nb, int fastack)
{
	int first;

	cr->timeout = cor_calc_timeout(atomic_read(&nb->latency_retrans_us),
			atomic_read(&nb->latency_stddev_retrans_us),
			fastack ?
			atomic_read(&nb->max_remote_ack_fast_delay_us) :
			atomic_read(&nb->max_remote_ack_slow_delay_us));

	spin_lock_bh(&nb->retrans_lock);

	cor_insert_control_retrans(cr);
	if (fastack) {
		first = list_empty(&nb->retrans_fast_list);
		list_add_tail(&cr->timeout_list, &nb->retrans_fast_list);
	} else {
		first = list_empty(&nb->retrans_slow_list);
		list_add_tail(&cr->timeout_list, &nb->retrans_slow_list);
	}

	if (first) {
		if (mod_timer(&nb->retrans_timer,
				cor_get_retransmit_timeout(nb)) == 0) {
			cor_nb_kref_get(nb, "retransmit_timer");
		}
	}

	spin_unlock_bh(&nb->retrans_lock);
}

void cor_kern_ack_rcvd(struct cor_neighbor *nb, __u32 seqno)
{
	struct cor_control_retrans *cr = 0;

	spin_lock_bh(&nb->retrans_lock);

	cr = cor_get_control_retrans(nb, seqno);

	if (cr == 0) {
		/* printk(KERN_ERR "bogus/duplicate ack received %x\n", seqno); */
		goto out;
	}

	rb_erase(&cr->rbn, &nb->kp_retransmits_rb);

	BUG_ON(cr->nb != nb);

	list_del(&cr->timeout_list);

out:
	spin_unlock_bh(&nb->retrans_lock);

	if (cr != 0) {
		/* cor_get_control_retrans */
		kref_put(&cr->ref, cor_kreffree_bug);

		kref_put(&cr->ref, cor_kreffree_bug); /* rb_erase */
		kref_put(&cr->ref, cor_free_control_retrans); /* list */
	}
}

static __u8 cor_get_window(struct cor_conn *cn,
		struct cor_neighbor *expectedsender, __u32 expected_connid)
{
	__u8 window = 0;

	BUG_ON(expectedsender == 0);

	spin_lock_bh(&cn->rcv_lock);

	if (cor_is_conn_in(cn, expectedsender, expected_connid) == 0)
		goto out;

	window = cor_enc_log_64_11(cn->src.in.window_seqnolimit -
			cn->src.in.next_seqno);

	cn->src.in.window_seqnolimit_remote = cn->src.in.next_seqno +
			cor_dec_log_64_11(window);

out:
	spin_unlock_bh(&cn->rcv_lock);

	return window;
}

/* static void padding(struct sk_buff *skb, __u32 length)
{
	char *dst;
	if (length <= 0)
		return;
	dst = skb_put(skb, length);
	BUG_ON(dst == 0);
	memset(dst, KP_PADDING, length);
} */


static __u32 cor_add_init_session(struct sk_buff *skb, __be32 sessionid,
		__u32 spaceleft)
{
	char *dst;

	BUG_ON(KP_MISC_INIT_SESSION_CMDLEN != 5);

	if (unlikely(spaceleft < 5))
		return 0;

	dst = skb_put(skb, 5);
	BUG_ON(dst == 0);

	dst[0] = get_kp_code(KP_MISC, KP_MISC_INIT_SESSION);
	cor_put_be32(dst + 1, sessionid);

	return 5;
}

static __u32 cor_add_ack(struct sk_buff *skb, struct cor_control_retrans *cr,
		struct cor_control_msg_out *cm, __u32 spaceleft)
{
	char *dst;

	BUG_ON(cm->length != 5);

	if (unlikely(spaceleft < 5))
		return 0;

	dst = skb_put(skb, 5);
	BUG_ON(dst == 0);

	dst[0] = get_kp_code(KP_MISC, KP_MISC_ACK);
	cor_put_u32(dst + 1, cm->msg.ack.seqno);

	list_add_tail(&cm->lh, &cr->msgs);

	return 5;
}

static inline __u8 cor_add_ack_conn_get_delayremaining(
		struct cor_control_msg_out *cm, unsigned long cmsg_send_start_j)
{
	__u32 maxdelay_jiffies = msecs_to_jiffies(CMSG_MAXDELAY_ACKCONN_MS);
	unsigned long jiffies_timeout;

	jiffies_timeout = cm->time_added + maxdelay_jiffies;

	if (time_before_eq(cmsg_send_start_j, cm->time_added)) {
		return 255;
	} else if (time_after_eq(cmsg_send_start_j, jiffies_timeout)) {
		return 0;
	} else {
		__u64 delay_remaining = jiffies_timeout - cmsg_send_start_j;

		BUG_ON(delay_remaining > U32_MAX);
		BUG_ON(delay_remaining > maxdelay_jiffies);

		return (__u8) div64_u64(255 * delay_remaining +
				maxdelay_jiffies / 2,
				maxdelay_jiffies);
	}
}

static __u32 cor_add_ack_conn(struct sk_buff *skb,
		struct cor_control_retrans *cr, struct cor_control_msg_out *cm,
		__u32 spaceleft, unsigned long cmsg_send_start_j,
		int *ackneeded)
{
	char *dst;
	__u32 offset = 0;

	if (unlikely(spaceleft < cm->length))
		return 0;

	dst = skb_put(skb, cm->length);
	BUG_ON(dst == 0);

	dst[offset] = get_kp_code(KP_ACK_CONN, cm->msg.ack_conn.flags);
	offset++;
	cor_put_u32(dst + offset, cm->msg.ack_conn.conn_id);
	offset += 4;

	if (likely((cm->msg.ack_conn.flags & KP_ACK_CONN_FLAGS_SEQNO) != 0 ||
			cor_ooolen(cm->msg.ack_conn.flags) != 0)) {
		dst[offset] = cor_add_ack_conn_get_delayremaining(cm,
				cmsg_send_start_j);
		offset++;
	}

	if ((cm->msg.ack_conn.flags & KP_ACK_CONN_FLAGS_SEQNO) != 0) {
		cor_put_u32(dst + offset, cm->msg.ack_conn.seqno);
		offset += 4;

		if ((cm->msg.ack_conn.flags & KP_ACK_CONN_FLAGS_WINDOW) != 0) {
			BUG_ON(cm->msg.ack_conn.src_in == 0);
			dst[offset] = cor_get_window(
					cm->msg.ack_conn.src_in,
					cm->nb, cor_get_connid_reverse(
					cm->msg.ack_conn.conn_id));
			offset++;
			dst[offset] = cm->msg.ack_conn.bufsize_changerate;
			offset++;
		}
	}

	if (cor_ooolen(cm->msg.ack_conn.flags) != 0) {
		cor_put_u32(dst + offset, cm->msg.ack_conn.seqno_ooo);
		offset += 4;
		if (cor_ooolen(cm->msg.ack_conn.flags) == 1) {
			BUG_ON(cm->msg.ack_conn.length > 255);
			dst[offset] = cm->msg.ack_conn.length;
			offset += 1;
		} else if (cor_ooolen(cm->msg.ack_conn.flags) == 2) {
			BUG_ON(cm->msg.ack_conn.length <= 255);
			BUG_ON(cm->msg.ack_conn.length > 65535);
			cor_put_u16(dst + offset, cm->msg.ack_conn.length);
			offset += 2;
		} else if (cor_ooolen(cm->msg.ack_conn.flags) == 4) {
			BUG_ON(cm->msg.ack_conn.length <= 65535);
			cor_put_u32(dst + offset, cm->msg.ack_conn.length);
			offset += 4;
		} else {
			BUG();
		}
	}

	if (unlikely((cm->msg.ack_conn.flags &
			KP_ACK_CONN_FLAGS_PRIORITY) != 0)) {
		__u16 priority = (cm->msg.ack_conn.priority_seqno << 12) &
				cm->msg.ack_conn.priority;
		BUG_ON(cm->msg.ack_conn.priority_seqno > 15);
		BUG_ON(cm->msg.ack_conn.priority > 4095);

		cor_put_u16(dst + offset, priority);
		offset += 2;
		if (cm->msg.ack_conn.is_highlatency == 0)
			dst[offset] = 0;
		else
			dst[offset] = 1;
		offset++;
	}

	list_add_tail(&cm->lh, &cr->msgs);
	if (likely(cm->msg.ack_conn.flags & KP_ACK_CONN_FLAGS_SEQNO) != 0 ||
			cor_ooolen(cm->msg.ack_conn.flags) != 0) {
		*ackneeded = ACK_NEEDED_FAST;
	} else if (*ackneeded != ACK_NEEDED_FAST) {
		*ackneeded = ACK_NEEDED_SLOW;
	}

	BUG_ON(offset != cm->length);
	return offset;
}

static __u32 cor_add_ping(struct sk_buff *skb, __u32 cookie, __u32 spaceleft)
{
	char *dst;

	BUG_ON(KP_MISC_PING_CMDLEN != 5);

	if (unlikely(spaceleft < 5))
		return 0;

	dst = skb_put(skb, 5);
	BUG_ON(dst == 0);

	dst[0] = get_kp_code(KP_MISC, KP_MISC_PING);
	cor_put_u32(dst + 1, cookie);

	return 5;
}

static __u32 cor_calc_respdelay(ktime_t time_pong_enqueued, ktime_t time_end)
{
	if (unlikely(ktime_before(time_end, time_pong_enqueued))) {
		return 0;
	} else {
		__u64 respdelay = div_u64(ktime_to_ns(time_end) -
				ktime_to_ns(time_pong_enqueued) + 500,
				1000);

		if (unlikely(respdelay > U32_MAX))
			return U32_MAX;
		else
			return (__u32) respdelay;
	}
}

static __u32 cor_add_pong(struct sk_buff *skb, struct cor_control_retrans *cr,
		struct cor_control_msg_out *cm, __u32 spaceleft, int nbstate,
		ktime_t cmsg_send_start, int *ackneeded)
{
	__u32 respdelay_full;
	__u32 respdelay_netonly;
	char *dst;

	BUG_ON(cm->length != 13);

	if (unlikely(spaceleft < 13))
		return 0;

	respdelay_full = cor_calc_respdelay(cm->msg.pong.time_enqueued,
			cmsg_send_start);
	respdelay_netonly = cor_calc_respdelay(cm->msg.pong.ping_rcvtime,
			ktime_get());

	dst = skb_put(skb, 13);
	BUG_ON(dst == 0);

	dst[0] = get_kp_code(KP_MISC, KP_MISC_PONG);
	cor_put_u32(dst + 1, cm->msg.pong.cookie);
	cor_put_u32(dst + 5, (__u32) respdelay_full);
	cor_put_u32(dst + 9, (__u32) respdelay_netonly);

	list_add_tail(&cm->lh, &cr->msgs);
	if (likely(nbstate == NEIGHBOR_STATE_ACTIVE) &&
			*ackneeded != ACK_NEEDED_FAST)
		*ackneeded = ACK_NEEDED_SLOW;

	return 13;
}

static __u32 cor_add_connect(struct sk_buff *skb,
		struct cor_control_retrans *cr, struct cor_control_msg_out *cm,
		__u32 spaceleft, int *ackneeded)
{
	char *dst;
	struct cor_conn *src_in = cm->msg.connect.src_in;
	struct cor_conn *trgt_out = cor_get_conn_reversedir(src_in);
	__u16 priority;

	BUG_ON(cm->length != 17);

	if (unlikely(spaceleft < 17))
		return 0;

	dst = skb_put(skb, 17);
	BUG_ON(dst == 0);

	dst[0] = get_kp_code(KP_MISC, KP_MISC_CONNECT);
	cor_put_u32(dst + 1, cm->msg.connect.conn_id);
	cor_put_u32(dst + 5, cm->msg.connect.seqno1);
	cor_put_u32(dst + 9, cm->msg.connect.seqno2);
	BUG_ON(cm->msg.connect.src_in == 0);
	dst[13] = cor_get_window(cm->msg.connect.src_in, cm->nb,
			cor_get_connid_reverse(cm->msg.connect.conn_id));

	spin_lock_bh(&trgt_out->rcv_lock);
	BUG_ON(trgt_out->targettype != TARGET_OUT);

	priority = (trgt_out->trgt.out.priority_seqno << 12) &
			trgt_out->trgt.out.priority_last;
	BUG_ON(trgt_out->trgt.out.priority_seqno > 15);
	BUG_ON(trgt_out->trgt.out.priority_last > 4095);
	cor_put_u16(dst + 14, priority);

	if (trgt_out->is_highlatency == 0)
		dst[16] = 0;
	else
		dst[16] = 1;

	spin_unlock_bh(&trgt_out->rcv_lock);

	list_add_tail(&cm->lh, &cr->msgs);
	if (*ackneeded != ACK_NEEDED_FAST)
		*ackneeded = ACK_NEEDED_SLOW;

	return 17;
}

static __u32 cor_add_connect_success(struct sk_buff *skb,
		struct cor_control_retrans *cr, struct cor_control_msg_out *cm,
		__u32 spaceleft, int *ackneeded)
{
	char *dst;

	BUG_ON(cm->length != 6);

	if (unlikely(spaceleft < 6))
		return 0;

	dst = skb_put(skb, 6);
	BUG_ON(dst == 0);

	dst[0] = get_kp_code(KP_MISC, KP_MISC_CONNECT_SUCCESS);
	cor_put_u32(dst + 1, cm->msg.connect_success.conn_id);
	BUG_ON(cm->msg.connect_success.src_in == 0);
	dst[5] = cor_get_window(
			cm->msg.connect_success.src_in, cm->nb,
			cor_get_connid_reverse(
			cm->msg.connect_success.conn_id));

	list_add_tail(&cm->lh, &cr->msgs);
	if (*ackneeded != ACK_NEEDED_FAST)
		*ackneeded = ACK_NEEDED_SLOW;

	return 6;
}

static __u32 cor_add_reset_conn(struct sk_buff *skb,
		struct cor_control_retrans *cr, struct cor_control_msg_out *cm,
		__u32 spaceleft, int *ackneeded)
{
	char *dst;

	BUG_ON(cm->length != 5);

	if (unlikely(spaceleft < 5))
		return 0;

	dst = skb_put(skb, 5);
	BUG_ON(dst == 0);

	dst[0] = get_kp_code(KP_MISC, KP_MISC_RESET_CONN);
	cor_put_u32(dst + 1, cm->msg.reset_conn.conn_id);

	list_add_tail(&cm->lh, &cr->msgs);
	if (*ackneeded != ACK_NEEDED_FAST)
		*ackneeded = ACK_NEEDED_SLOW;

	return 5;
}

static __u32 cor_add_conndata(struct sk_buff *skb,
		struct cor_control_retrans *cr, struct cor_control_msg_out *cm,
		__u32 spaceleft, struct cor_control_msg_out **split_conndata,
		__u32 *sc_sendlen)
{
	char *dst;
	__u32 offset = 0;

	__u32 totallen = get_kp_conn_data_length(cm->msg.conn_data.datalen);
	__u32 putlen = totallen;
	__u32 dataputlen = cm->msg.conn_data.datalen;
	__u8 code_min = 0;

	BUILD_BUG_ON(KP_CONN_DATA_MAXLEN != 128 + 32767);
	BUG_ON(cm->msg.conn_data.datalen > KP_CONN_DATA_MAXLEN);

	BUG_ON(cm->length != totallen);

	BUG_ON(putlen > 1024 * 1024 * 1024);

	BUG_ON(split_conndata == 0);
	BUG_ON(*split_conndata != 0);
	BUG_ON(sc_sendlen == 0);
	BUG_ON(*sc_sendlen != 0);

	if (putlen > spaceleft) {
		if (spaceleft < get_kp_conn_data_length(1))
			return 0;

		BUG_ON(spaceleft < 11);

		if (spaceleft <= 127 + 10) {
			dataputlen = spaceleft - 10;
			putlen = spaceleft;
		} else if (spaceleft == 127 + 10 + 1) {
			dataputlen = spaceleft - 10 - 1;
			putlen = spaceleft - 1;
		} else {
			dataputlen = spaceleft - 11;
			putlen = spaceleft;
		}

		BUG_ON(putlen != get_kp_conn_data_length(dataputlen));
	}

	dst = skb_put(skb, putlen);
	BUG_ON(dst == 0);

	BUG_ON((cm->msg.conn_data.windowused &
			(~KP_CONN_DATA_FLAGS_WINDOWUSED)) != 0);
	code_min = 0;
	if (cm->msg.conn_data.flush != 0)
		code_min |= KP_CONN_DATA_FLAGS_FLUSH;
	code_min |= cm->msg.conn_data.windowused;

	dst[0] = get_kp_code(KP_CONN_DATA, code_min);
	offset++;
	cor_put_u32(dst + offset, cm->msg.conn_data.conn_id);
	offset += 4;
	cor_put_u32(dst + offset, cm->msg.conn_data.seqno);
	offset += 4;

	if (dataputlen < 128) {
		dst[offset] = (__u8) dataputlen;
		offset++;
	} else {
		__u8 high = (__u8) (128 + ((dataputlen - 128) / 256));
		__u8 low = (__u8) ((dataputlen - 128) % 256);

		BUG_ON(((dataputlen - 128) / 256) > 127);
		dst[offset] = high;
		dst[offset + 1] = low;
		offset += 2;
	}

	BUG_ON(offset > putlen);
	BUG_ON(putlen - offset != dataputlen);
	memcpy(dst + offset, cm->msg.conn_data.data, dataputlen);
	offset += dataputlen;

	if (cm->msg.conn_data.datalen == dataputlen) {
		BUG_ON(cm->length != putlen);
		list_add_tail(&cm->lh, &cr->msgs);
	} else {
		*split_conndata = cm;
		*sc_sendlen = dataputlen;
	}

	return putlen;
}

static __u32 cor_add_set_max_cmsg_dly(struct sk_buff *skb,
		struct cor_control_retrans *cr, struct cor_control_msg_out *cm,
		__u32 spaceleft, int *ackneeded)
{
	char *dst;

	BUG_ON(KP_MISC_SET_MAX_CMSG_DELAY_CMDLEN != 17);
	BUG_ON(cm->length != KP_MISC_SET_MAX_CMSG_DELAY_CMDLEN);

	if (unlikely(spaceleft < 17))
		return 0;

	dst = skb_put(skb, 17);
	BUG_ON(dst == 0);

	dst[0] = get_kp_code(KP_MISC, KP_MISC_SET_MAX_CMSG_DELAY);
	cor_put_u32(dst + 1, cm->msg.set_max_cmsg_delay.ack_fast_delay);
	cor_put_u32(dst + 5, cm->msg.set_max_cmsg_delay.ack_slow_delay);
	cor_put_u32(dst + 9,
			cm->msg.set_max_cmsg_delay.ackconn_lowlatency_delay);
	cor_put_u32(dst + 13, cm->msg.set_max_cmsg_delay.pong_delay);

	list_add_tail(&cm->lh, &cr->msgs);
	if (*ackneeded != ACK_NEEDED_FAST)
		*ackneeded = ACK_NEEDED_SLOW;

	return 17;
}

static __u32 cor_add_set_rcvmtu(struct sk_buff *skb,
		struct cor_control_retrans *cr, struct cor_control_msg_out *cm,
		__u32 spaceleft, int *ackneeded)
{
	char *dst;

	BUG_ON(KP_MISC_SET_RECEIVE_MTU_CMDLEN != 5);
	BUG_ON(cm->length != KP_MISC_SET_RECEIVE_MTU_CMDLEN);

	if (unlikely(spaceleft < 5))
		return 0;

	dst = skb_put(skb, 5);
	BUG_ON(dst == 0);

	dst[0] = get_kp_code(KP_MISC, KP_MISC_SET_RECEIVE_MTU);
	cor_put_u32(dst + 1, cm->msg.set_rcvmtu.rcvmtu);

	list_add_tail(&cm->lh, &cr->msgs);
	if (*ackneeded != ACK_NEEDED_FAST)
		*ackneeded = ACK_NEEDED_SLOW;

	return 5;
}

static __u32 cor_add_message(struct sk_buff *skb,
		struct cor_control_retrans *cr, struct cor_control_msg_out *cm,
		__u32 spaceleft, int nbstate, unsigned long cmsg_send_start_j,
		ktime_t cmsg_send_start_kt,
		struct cor_control_msg_out **split_conndata, __u32 *sc_sendlen,
		int *ackneeded)
{
	BUG_ON(split_conndata != 0 && *split_conndata != 0);
	BUG_ON(sc_sendlen != 0 && *sc_sendlen != 0);

	switch (cm->type) {
	case MSGTYPE_ACK:
		return cor_add_ack(skb, cr, cm, spaceleft);
	case MSGTYPE_ACK_CONN:
		return cor_add_ack_conn(skb, cr, cm, spaceleft,
				cmsg_send_start_j, ackneeded);
	case MSGTYPE_PONG:
		return cor_add_pong(skb, cr, cm, spaceleft, nbstate,
				cmsg_send_start_kt, ackneeded);
	case MSGTYPE_CONNECT:
		return cor_add_connect(skb, cr, cm, spaceleft, ackneeded);
	case MSGTYPE_CONNECT_SUCCESS:
		return cor_add_connect_success(skb, cr, cm, spaceleft,
				ackneeded);
	case MSGTYPE_RESET_CONN:
		return cor_add_reset_conn(skb, cr, cm, spaceleft, ackneeded);
	case MSGTYPE_CONNDATA:
		return cor_add_conndata(skb, cr, cm, spaceleft, split_conndata,
				sc_sendlen);
	case MSGTYPE_SET_MAX_CMSG_DELAY:
		return cor_add_set_max_cmsg_dly(skb, cr, cm, spaceleft,
				ackneeded);
	case MSGTYPE_SET_RCVMTU:
		return cor_add_set_rcvmtu(skb, cr, cm, spaceleft,
				ackneeded);
	default:
		BUG();
	}
	BUG();
	return 0;
}

static __u32 ___cor_send_messages(struct cor_neighbor *nb, struct sk_buff *skb,
		struct cor_control_retrans *cr, struct list_head *cmsgs,
		__u32 spaceleft, int nbstate, unsigned long cmsg_send_start_j,
		ktime_t cmsg_send_start_kt,
		struct cor_control_msg_out **split_conndata, __u32 *sc_sendlen,
		int *ackneeded)
{
	__u32 length = 0;

	while (!list_empty(cmsgs)) {
		__u32 rc;
		struct cor_control_msg_out *cm = container_of(cmsgs->next,
				struct cor_control_msg_out, lh);

		list_del(&cm->lh);

		rc = cor_add_message(skb, cr, cm, spaceleft - length, nbstate,
				cmsg_send_start_j, cmsg_send_start_kt,
				split_conndata, sc_sendlen, ackneeded);
		if (rc == 0) {
			BUG();
			list_add(&cm->lh, cmsgs);
			break;
		}

		BUG_ON(rc != cm->length && cm->type != MSGTYPE_CONNDATA);

		length += rc;
	}

	return length;
}

static __u32 ___cor_send_messages_smcd(struct cor_neighbor *nb,
		struct sk_buff *skb, struct cor_control_retrans *cr,
		__u32 spaceleft, int nbstate, unsigned long cmsg_send_start_j,
		ktime_t cmsg_send_start_kt, int *ackneeded)
{
	struct cor_control_msg_out *cm;
	__u32 rc;

	cm = cor_alloc_control_msg(nb, ACM_PRIORITY_MED);

	if (unlikely(cm == 0))
		return 0;

	cm->type = MSGTYPE_SET_MAX_CMSG_DELAY;
	cm->msg.set_max_cmsg_delay.ack_fast_delay =
			CMSG_MAXDELAY_ACK_FAST_MS * 1000;
	cm->msg.set_max_cmsg_delay.ack_slow_delay =
			CMSG_MAXDELAY_ACK_SLOW_MS * 1000;
	cm->msg.set_max_cmsg_delay.ackconn_lowlatency_delay =
			CMSG_MAXDELAY_ACKCONN_MS * 1000;
	cm->msg.set_max_cmsg_delay.pong_delay =
			CMSG_MAXDELAY_OTHER_MS * 1000;
	cm->length = KP_MISC_SET_MAX_CMSG_DELAY_CMDLEN;

	rc = cor_add_message(skb, cr, cm, spaceleft, nbstate, cmsg_send_start_j,
			cmsg_send_start_kt, 0, 0, ackneeded);

	nb->max_cmsg_delay_sent = 1;

	return rc;
}

static __u32 ___cor_send_messages_rcvmtu(struct cor_neighbor *nb,
		struct sk_buff *skb, struct cor_control_retrans *cr,
		__u32 spaceleft, int nbstate, unsigned long cmsg_send_start_j,
		ktime_t cmsg_send_start_kt, int *ackneeded)
{
	struct cor_control_msg_out *cm;
	__u32 rc;

	cm = cor_alloc_control_msg(nb, ACM_PRIORITY_MED);

	if (unlikely(cm == 0))
		return 0;

	cm->type = MSGTYPE_SET_RCVMTU;
	cm->msg.set_rcvmtu.rcvmtu = cor_rcv_mtu(nb);
	cm->length = KP_MISC_SET_RECEIVE_MTU_CMDLEN;

	rc = cor_add_message(skb, cr, cm, spaceleft, nbstate, cmsg_send_start_j,
			cmsg_send_start_kt, 0, 0, ackneeded);

	atomic_set(&nb->rcvmtu_sendneeded, 0);

	return rc;
}

#define CMSGQUEUE_PONG 1
#define CMSGQUEUE_ACK_FAST 2
#define CMSGQUEUE_ACK_SLOW 3
#define CMSGQUEUE_ACK_CONN_URGENT 4
#define CMSGQUEUE_ACK_CONN 5
#define CMSGQUEUE_CONNDATA_LOWLAT 6
#define CMSGQUEUE_CONNDATA_HIGHLAT 7
#define CMSGQUEUE_OTHER 8

static void cor_requeue_message(struct cor_control_msg_out *cm)
{
	if (cm->type == MSGTYPE_ACK_CONN) {
		struct cor_conn *cn_l = cm->msg.ack_conn.src_in;

		spin_lock_bh(&cn_l->rcv_lock);
		if (unlikely(cor_ackconn_prepare_requeue(cn_l, cm) == 0)) {
			cor_free_control_msg(cm);
		} else {
			spin_lock_bh(&cm->nb->cmsg_lock);

			if (unlikely(cm->msg.ack_conn.queue ==
					CMSGQUEUE_ACK_CONN_URGENT)) {
				list_add(&cm->lh, &cm->nb->cmsg_queue_ackconn_urgent);
			} else if (cm->msg.ack_conn.queue ==
					CMSGQUEUE_ACK_CONN) {
				list_add(&cm->lh, &cm->nb->cmsg_queue_ackconn);
			} else {
				BUG();
			}

			cm->nb->cmsg_otherlength += cm->length;

			list_add(&cm->msg.ack_conn.conn_acks,
					&cn_l->src.in.acks_pending);
			cor_try_merge_ackconns(cn_l, cm);

			spin_unlock_bh(&cm->nb->cmsg_lock);
		}
		spin_unlock_bh(&cn_l->rcv_lock);
		return;
	}

	cor_enqueue_control_msg(cm, ADDCMSG_SRC_READD);
}

static void cor_requeue_messages(struct list_head *lh)
{
	while (list_empty(lh) == 0) {
		struct cor_control_msg_out *cm = container_of(lh->prev,
				struct cor_control_msg_out, lh);
		list_del(&cm->lh);
		cor_requeue_message(cm);
	}
}

static int __cor_send_messages_send(struct cor_neighbor *nb,
		struct sk_buff *skb, char *packet_type, int ping,
		int initsession, struct cor_control_retrans *cr,
		struct list_head *cmsgs, __u32 spaceleft, int nbstate,
		unsigned long cmsg_send_start_j, ktime_t cmsg_send_start_kt,
		int *sent)
{
	int rc;
	int ackneeded = ACK_NEEDED_NO;
	__u32 length = 0;
	__u32 pinglen = 0;
	__u32 pingcookie = 0;
	unsigned long last_ping_time;
	struct cor_control_msg_out *split_conndata = 0;
	__u32 sc_sendlen = 0;

	if (ping != TIMETOSENDPING_NO) {
		__u32 rc;

		if (unlikely(initsession)) {
			rc = cor_add_init_session(skb, nb->sessionid,
					spaceleft - length);
			BUG_ON(rc <= 0);
			pinglen = rc;
			length += rc;
		}

		pingcookie = cor_add_ping_req(nb, &last_ping_time);
		rc = cor_add_ping(skb, pingcookie, spaceleft - length);
		BUG_ON(rc <= 0);
		pinglen += rc;
		length += rc;
	}

	if (likely(nbstate == NEIGHBOR_STATE_ACTIVE) &&
			unlikely(nb->max_cmsg_delay_sent == 0))
		length += ___cor_send_messages_smcd(nb, skb, cr,
				spaceleft - length, nbstate, cmsg_send_start_j,
				cmsg_send_start_kt, &ackneeded);

	if (unlikely(atomic_read(&nb->rcvmtu_sendneeded) != 0)) {
		length += ___cor_send_messages_rcvmtu(nb, skb, cr,
				spaceleft - length, nbstate, cmsg_send_start_j,
				cmsg_send_start_kt, &ackneeded);
	}

	length += ___cor_send_messages(nb, skb, cr, cmsgs, spaceleft - length,
			nbstate, cmsg_send_start_j, cmsg_send_start_kt,
			&split_conndata, &sc_sendlen, &ackneeded);

	BUG_ON(length > spaceleft);

	if (likely(ping != TIMETOSENDPING_FORCE) &&
			pinglen != 0 && unlikely(length == pinglen)) {
		cor_unadd_ping_req(nb, pingcookie, last_ping_time, 0);
		goto drop;
	}

	if (unlikely(length == 0)) {
drop:
		kfree_skb(skb);

		BUG_ON(list_empty(&cr->msgs) == 0);
		kref_put(&cr->ref, cor_free_control_retrans);

		nb->kpacket_seqno--;
		return QOS_RESUME_DONE;
	}

	//padding(skb, spaceleft - length);
	BUG_ON(spaceleft - length != 0 &&
			(split_conndata == 0 || spaceleft - length != 1));

	if (ackneeded == ACK_NEEDED_NO) {
		*packet_type = PACKET_TYPE_CMSG_NOACK;
	} else if (ackneeded == ACK_NEEDED_SLOW) {
		*packet_type = PACKET_TYPE_CMSG_ACKSLOW;
	} else if (ackneeded == ACK_NEEDED_FAST) {
		*packet_type = PACKET_TYPE_CMSG_ACKFAST;
	} else {
		BUG();
	}

	rc = cor_dev_queue_xmit(skb, nb->cd, QOS_CALLER_KPACKET);
	if (rc == NET_XMIT_SUCCESS)
		*sent = 1;

	if (rc == NET_XMIT_DROP) {
		if (ping != 0)
			cor_unadd_ping_req(nb, pingcookie, last_ping_time, 1);

		atomic_inc(&nb->cmsg_bulk_readds);
		if (split_conndata != 0)
			cor_requeue_message(split_conndata);

		cor_requeue_messages(&cr->msgs);

		kref_put(&cr->ref, cor_free_control_retrans);

		atomic_dec(&nb->cmsg_bulk_readds);

		spin_lock_bh(&nb->cmsg_lock);
		cor_schedule_controlmsg_timer(nb);
		spin_unlock_bh(&nb->cmsg_lock);
	} else {
		struct list_head *curr = cr->msgs.next;

		if (pingcookie != 0)
			cor_ping_sent(nb, pingcookie);

		while (curr != &cr->msgs) {
			struct cor_control_msg_out *cm = container_of(curr,
					struct cor_control_msg_out, lh);

			curr = curr->next;

			if (cm->type == MSGTYPE_ACK || unlikely(
					cm->type == MSGTYPE_PONG &&
					(nbstate != NEIGHBOR_STATE_ACTIVE))) {
				list_del(&cm->lh);
				cor_free_control_msg(cm);
			} else if (unlikely(cm->type == MSGTYPE_PONG &&
					atomic_inc_return(
					&nb->cmsg_pongs_retrans_cnt) >
					MAX_PONG_CMSGS_RETRANS_PER_NEIGH)) {
				atomic_dec(&nb->cmsg_pongs_retrans_cnt);
				list_del(&cm->lh);
				cor_free_control_msg(cm);
			} else if (cm->type == MSGTYPE_CONNDATA) {
				cor_schedule_retransmit_conn(
						cm->msg.conn_data.cr, 0, 0);
				kref_put(&cm->msg.conn_data.cr->ref,
						cor_free_connretrans);
				cm->msg.conn_data.cr = 0;
				kfree(cm->msg.conn_data.data_orig);
				list_del(&cm->lh);
				cor_free_control_msg(cm);
			}
		}

		if (split_conndata != 0) {
			BUG_ON(sc_sendlen == 0);
			BUG_ON(sc_sendlen >=
					split_conndata->msg.conn_data.datalen);

			split_conndata->msg.conn_data.seqno += sc_sendlen;
			split_conndata->msg.conn_data.data += sc_sendlen;
			split_conndata->msg.conn_data.datalen -= sc_sendlen;
			split_conndata->length = get_kp_conn_data_length(
					split_conndata->msg.conn_data.datalen);
			cor_enqueue_control_msg(split_conndata,
					ADDCMSG_SRC_SPLITCONNDATA);
		}


		if (list_empty(&cr->msgs)) {
			kref_put(&cr->ref, cor_free_control_retrans);
		} else {
			int fastack = (ackneeded == ACK_NEEDED_FAST);

			BUG_ON(ackneeded != ACK_NEEDED_FAST &&
					ackneeded != ACK_NEEDED_SLOW);
			cor_schedule_retransmit(cr, nb, fastack);
		}
	}

	return (rc == NET_XMIT_SUCCESS) ? QOS_RESUME_DONE : QOS_RESUME_CONG;
}

static int _cor_send_messages_send(struct cor_neighbor *nb, int ping,
		int initsession, struct list_head *cmsgs, int nbstate,
		__u32 length, __u32 seqno, unsigned long cmsg_send_start_j,
		ktime_t cmsg_send_start_kt, int *sent)
{
	struct sk_buff *skb;
	struct cor_control_retrans *cr;
	char *dst;
	int rc;

	BUG_ON(length > cor_mss_cmsg(nb));
	skb = cor_create_packet(nb, length + 5, GFP_ATOMIC);
	if (unlikely(skb == 0)) {
		printk(KERN_ERR "cor_send_messages(): cannot allocate skb (out of memory?)\n");

		cor_requeue_messages(cmsgs);
		return QOS_RESUME_CONG;
	}

	cr = kmem_cache_alloc(cor_controlretrans_slab, GFP_ATOMIC);
	if (unlikely(cr == 0)) {
		printk(KERN_ERR "cor_send_messages(): cannot allocate cor_control_retrans (out of memory?)\n");
		kfree_skb(skb);

		cor_requeue_messages(cmsgs);
		return QOS_RESUME_CONG;
	}

	memset(cr, 0, sizeof(struct cor_control_retrans));
	kref_init(&cr->ref);
	cr->nb = nb;
	cr->seqno = seqno;
	INIT_LIST_HEAD(&cr->msgs);


	dst = skb_put(skb, 5);
	BUG_ON(dst == 0);

	dst[0] = PACKET_TYPE_NONE;
	cor_put_u32(dst + 1, seqno);

	rc = __cor_send_messages_send(nb, skb, &dst[0], ping, initsession, cr,
			cmsgs, length, nbstate, cmsg_send_start_j,
			cmsg_send_start_kt, sent);

	BUG_ON(!list_empty(cmsgs));

	return rc;
}

static unsigned long cor_get_cmsg_timeout(struct cor_control_msg_out *cm,
		int queue)
{
	if (cm->type == MSGTYPE_ACK) {
		if (cm->msg.ack.fast != 0) {
			BUG_ON(queue != CMSGQUEUE_ACK_FAST);
			return cm->time_added + msecs_to_jiffies(
					CMSG_MAXDELAY_ACK_FAST_MS);
		} else {
			BUG_ON(queue != CMSGQUEUE_ACK_SLOW);
			return cm->time_added + msecs_to_jiffies(
					CMSG_MAXDELAY_ACK_SLOW_MS);
		}
	} else if (cm->type == MSGTYPE_ACK_CONN) {
		__u32 maxdelay_ms = 0;

		if (unlikely(queue == CMSGQUEUE_ACK_CONN_URGENT)) {
			maxdelay_ms = CMSG_MAXDELAY_ACKCONN_URGENT_MS;
		} else if (queue == CMSGQUEUE_ACK_CONN) {
			maxdelay_ms = CMSG_MAXDELAY_ACKCONN_MS;
		} else {
			BUG();
		}
		return cm->time_added + msecs_to_jiffies(maxdelay_ms);
	} else if (cm->type == MSGTYPE_CONNDATA) {
		if (cm->msg.conn_data.highlatency != 0) {
			BUG_ON(queue != CMSGQUEUE_CONNDATA_HIGHLAT);
			return cm->time_added +
					msecs_to_jiffies(
					CMSG_MAXDELAY_CONNDATA_MS);
		} else {
			BUG_ON(queue != CMSGQUEUE_CONNDATA_LOWLAT);
			return cm->time_added;
		}
	} else {
		BUG_ON(cm->type == MSGTYPE_PONG && queue != CMSGQUEUE_PONG);
		BUG_ON(cm->type != MSGTYPE_PONG && queue != CMSGQUEUE_OTHER);

		return cm->time_added +
				msecs_to_jiffies(CMSG_MAXDELAY_OTHER_MS);
	}
}

static void _cor_peek_message(struct cor_neighbor *nb_cmsglocked, int queue,
		struct cor_control_msg_out **currcm, unsigned long *currtimeout,
		__u32 **currlen)
{
	struct cor_control_msg_out *cm;
	unsigned long cmtimeout;

	struct list_head *queuelh;

	if (queue == CMSGQUEUE_PONG) {
		queuelh = &nb_cmsglocked->cmsg_queue_pong;
	} else if (queue == CMSGQUEUE_ACK_FAST) {
		queuelh = &nb_cmsglocked->cmsg_queue_ack_fast;
	} else if (queue == CMSGQUEUE_ACK_SLOW) {
		queuelh = &nb_cmsglocked->cmsg_queue_ack_slow;
	} else if (queue == CMSGQUEUE_ACK_CONN_URGENT) {
		queuelh = &nb_cmsglocked->cmsg_queue_ackconn_urgent;
	} else if (queue == CMSGQUEUE_ACK_CONN) {
		queuelh = &nb_cmsglocked->cmsg_queue_ackconn;
	} else if (queue == CMSGQUEUE_CONNDATA_LOWLAT) {
		queuelh = &nb_cmsglocked->cmsg_queue_conndata_lowlat;
	} else if (queue == CMSGQUEUE_CONNDATA_HIGHLAT) {
		queuelh = &nb_cmsglocked->cmsg_queue_conndata_highlat;
	} else if (queue == CMSGQUEUE_OTHER) {
		queuelh = &nb_cmsglocked->cmsg_queue_other;
	} else {
		BUG();
	}

	if (list_empty(queuelh))
		return;

	cm = container_of(queuelh->next, struct cor_control_msg_out, lh);
	cmtimeout = cor_get_cmsg_timeout(cm, queue);

	BUG_ON(cm->nb != nb_cmsglocked);

	if (*currcm == 0 || (time_before(cmtimeout, *currtimeout) &&
			time_before(jiffies, *currtimeout))) {
		*currcm = cm;
		*currtimeout = cmtimeout;

		if (queue == CMSGQUEUE_PONG) {
			*currlen = &nb_cmsglocked->cmsg_pongslength;
		} else {
			*currlen =  &nb_cmsglocked->cmsg_otherlength;
		}
	}
}

static void cor_peek_message(struct cor_neighbor *nb_cmsglocked, int nbstate,
		struct cor_control_msg_out **cm, unsigned long *cmtimeout,
		__u32 **len, int for_timeout)
{
	_cor_peek_message(nb_cmsglocked, CMSGQUEUE_PONG, cm, cmtimeout, len);
	if (likely(nbstate == NEIGHBOR_STATE_ACTIVE)) {
		_cor_peek_message(nb_cmsglocked, CMSGQUEUE_ACK_FAST, cm,
				cmtimeout, len);
		_cor_peek_message(nb_cmsglocked, CMSGQUEUE_ACK_SLOW, cm,
				cmtimeout, len);
		_cor_peek_message(nb_cmsglocked, CMSGQUEUE_ACK_CONN_URGENT, cm,
				cmtimeout, len);
		_cor_peek_message(nb_cmsglocked, CMSGQUEUE_ACK_CONN, cm,
				cmtimeout, len);
		if (!for_timeout || atomic_read(
				&nb_cmsglocked->cmsg_delay_conndata) == 0) {
			_cor_peek_message(nb_cmsglocked,
					CMSGQUEUE_CONNDATA_LOWLAT,
					cm, cmtimeout, len);
			_cor_peek_message(nb_cmsglocked,
					CMSGQUEUE_CONNDATA_HIGHLAT,
					cm, cmtimeout, len);
		}
		_cor_peek_message(nb_cmsglocked, CMSGQUEUE_OTHER, cm, cmtimeout,
				len);
	}
}

static unsigned long cor_get_cmsg_timer_timeout(
		struct cor_neighbor *nb_cmsglocked, int nbstate)
{
	unsigned long pingtimeout = cor_get_next_ping_time(nb_cmsglocked);

	struct cor_control_msg_out *cm = 0;
	unsigned long cmtimeout;
	__u32 *len;

	cor_peek_message(nb_cmsglocked, nbstate, &cm, &cmtimeout, &len, 1);

	if (cm != 0) {
		unsigned long jiffies_tmp = jiffies;

		if (time_before(cmtimeout, jiffies_tmp))
			return jiffies_tmp;
		if (time_before(cmtimeout, pingtimeout))
			return cmtimeout;
	}

	return pingtimeout;
}

static void _cor_dequeue_messages(struct cor_neighbor *nb_cmsglocked,
		int nbstate, __u32 targetmss, __u32 *length,
		struct list_head *cmsgs)
{
	while (1) {
		__u32 spaceleft = targetmss - *length;
		struct cor_control_msg_out *cm = 0;
		unsigned long cmtimeout;
		__u32 *len;

		cor_peek_message(nb_cmsglocked, nbstate, &cm, &cmtimeout, &len,
				0);

		if (unlikely(cm == 0))
			break;

		BUG_ON(len == 0);

		if (cm->length > spaceleft) {
			if (cm->type == MSGTYPE_CONNDATA) {
				BUG_ON(*length == 0 && spaceleft <
						get_kp_conn_data_length(1));

				if (spaceleft < get_kp_conn_data_length(1) ||
						*length > (targetmss / 4) * 3)
					break;
			} else {
				BUG_ON(*length == 0);
				break;
			}
		}

		list_del(&cm->lh);
		*len -= cm->length;

		if (cm->type == MSGTYPE_ACK_CONN)
			list_del(&cm->msg.ack_conn.conn_acks);
		if (unlikely(cm->type == MSGTYPE_PONG)) {
			BUG_ON(cm->nb->cmsg_pongscnt == 0);
			cm->nb->cmsg_pongscnt--;
		}

		if (unlikely(cm->type == MSGTYPE_RESET_CONN)) {
			BUG_ON(cm->msg.reset_conn.in_pending_conn_resets == 0);
			rb_erase(&cm->msg.reset_conn.rbn,
					&cm->nb->pending_conn_resets_rb);
			cm->msg.reset_conn.in_pending_conn_resets = 0;
				kref_put(&cm->ref, cor_kreffree_bug);
		}

		BUG_ON(*length + cm->length < *length);
		if (cm->length > targetmss - *length) {
			BUG_ON(*length >= targetmss);
			BUG_ON(cm->type != MSGTYPE_CONNDATA);
			*length = targetmss;
		} else {
			*length += cm->length;
		}

		list_add_tail(&cm->lh, cmsgs);
	}
}

static __u32 cor_get_total_messages_length(struct cor_neighbor *nb, int ping,
		int initsession, int nbstate, int *extralength)
{
	__u32 length = nb->cmsg_pongslength;

	if (likely(nbstate == NEIGHBOR_STATE_ACTIVE)) {
		length += nb->cmsg_otherlength;

		if (unlikely(nb->max_cmsg_delay_sent == 0)) {
			length += KP_MISC_SET_MAX_CMSG_DELAY_CMDLEN;
			*extralength += KP_MISC_SET_MAX_CMSG_DELAY_CMDLEN;
		}
	}

	if (unlikely(atomic_read(&nb->rcvmtu_sendneeded) != 0)) {
		length += KP_MISC_SET_RECEIVE_MTU_CMDLEN;
		*extralength += KP_MISC_SET_RECEIVE_MTU_CMDLEN;
	}

	if (ping == TIMETOSENDPING_FORCE ||
			(length > 0 && ping != TIMETOSENDPING_NO)) {
		length += KP_MISC_PING_CMDLEN;
		*extralength += KP_MISC_PING_CMDLEN;

		if (unlikely(initsession)) {
			length += KP_MISC_INIT_SESSION_CMDLEN;
			*extralength += KP_MISC_INIT_SESSION_CMDLEN;
		}
	}

	return length;
}

static int cor_dequeue_messages(struct cor_neighbor *nb_cmsglocked, int ping,
		int initsession, int nbstate, __u32 targetmss,
		__u32 *length, struct list_head *cmsgs)
{
	__u32 extralength = 0;
	__u32 totallength;

	int cmsgqueue_nonpong_empty = (
			list_empty(&nb_cmsglocked->cmsg_queue_ack_fast) &&
			list_empty(&nb_cmsglocked->cmsg_queue_ack_slow) &&
			list_empty(&nb_cmsglocked->cmsg_queue_ackconn_urgent) &&
			list_empty(&nb_cmsglocked->cmsg_queue_ackconn) &&
			list_empty(&nb_cmsglocked->cmsg_queue_conndata_lowlat) &&
			list_empty(&nb_cmsglocked->cmsg_queue_conndata_highlat) &&
			list_empty(&nb_cmsglocked->cmsg_queue_other));

	BUG_ON(list_empty(&nb_cmsglocked->cmsg_queue_pong) &&
			nb_cmsglocked->cmsg_pongslength != 0);
	BUG_ON(!list_empty(&nb_cmsglocked->cmsg_queue_pong) &&
			nb_cmsglocked->cmsg_pongslength == 0);
	BUG_ON(cmsgqueue_nonpong_empty &&
			nb_cmsglocked->cmsg_otherlength != 0);
	BUG_ON(!cmsgqueue_nonpong_empty &&
			nb_cmsglocked->cmsg_otherlength == 0);

	totallength = cor_get_total_messages_length(nb_cmsglocked, ping,
			initsession, nbstate, &extralength);

	if (totallength == 0)
		return 1;

	if (totallength < targetmss && ping != TIMETOSENDPING_FORCE &&
			time_after(cor_get_cmsg_timer_timeout(nb_cmsglocked,
			nbstate), jiffies))
		return 1;

	*length = extralength;

	_cor_dequeue_messages(nb_cmsglocked, nbstate, targetmss, length, cmsgs);

	BUG_ON(*length == 0);
	BUG_ON(*length > targetmss);

	return 0;
}

static struct cor_control_retrans *cor_get_next_timeouted_retrans(
		struct cor_neighbor *nb_retranslocked)
{
	if (list_empty(&nb_retranslocked->retrans_fast_list) == 0) {
		struct cor_control_retrans *cr = container_of(
				nb_retranslocked->retrans_fast_list.next,
				struct cor_control_retrans, timeout_list);
		BUG_ON(cr->nb != nb_retranslocked);

		if (time_before_eq(cr->timeout, jiffies)) {
			return cr;
		}
	}

	if (list_empty(&nb_retranslocked->retrans_slow_list) == 0) {
		struct cor_control_retrans *cr = container_of(
				nb_retranslocked->retrans_slow_list.next,
				struct cor_control_retrans, timeout_list);

		BUG_ON(cr->nb != nb_retranslocked);

		if (time_before_eq(cr->timeout, jiffies)) {
			return cr;
		}
	}

	return 0;
}

static void cor_add_timeouted_retrans(struct cor_neighbor *nb)
{
	spin_lock_bh(&nb->retrans_lock);

	while (1) {
		struct cor_control_retrans *cr =
				cor_get_next_timeouted_retrans(nb);

		if (cr == 0)
			break;

		list_del(&cr->timeout_list);
		rb_erase(&cr->rbn, &nb->kp_retransmits_rb);

		cor_requeue_control_retrans(cr);

		kref_put(&cr->ref, cor_kreffree_bug); /* list_del */
		kref_put(&cr->ref, cor_free_control_retrans); /* rb */
	}

	if (list_empty(&nb->retrans_fast_list) == 0 ||
			list_empty(&nb->retrans_slow_list) == 0) {
		if (mod_timer(&nb->retrans_timer,
				cor_get_retransmit_timeout(nb)) == 0) {
			cor_nb_kref_get(nb, "retransmit_timer");
		}
	}

	spin_unlock_bh(&nb->retrans_lock);
}

static void _cor_delete_all_cmsgs(struct list_head *cmsgs)
{
	while (!list_empty(cmsgs)) {
		struct cor_control_msg_out *cm = container_of(cmsgs->next,
				struct cor_control_msg_out, lh);

		list_del(&cm->lh);

		if (cm->type == MSGTYPE_CONNDATA) {
			cor_schedule_retransmit_conn(cm->msg.conn_data.cr, 0,
					0);
			kfree(cm->msg.conn_data.data_orig);
		}

		cor_free_control_msg(cm);
	}
}

static void cor_delete_all_cmsgs(struct cor_neighbor *nb)
{
	while (1) {
		struct list_head cmsgs;
		__u32 length = 0;

		INIT_LIST_HEAD(&cmsgs);

		spin_lock_bh(&nb->cmsg_lock);
		_cor_dequeue_messages(nb, NEIGHBOR_STATE_ACTIVE, 65536, &length,
				&cmsgs);
		spin_unlock_bh(&nb->cmsg_lock);

		if (list_empty(&cmsgs))
			break;

		_cor_delete_all_cmsgs(&cmsgs);
	}
}

static int cor_reset_timeouted_conn(struct cor_neighbor *nb,
		struct cor_conn *trgt_out)
{
	struct cor_conn_bidir *cnb = cor_get_conn_bidir(trgt_out);
	struct cor_conn *src_in = cor_get_conn_reversedir(trgt_out);

	int resetted = 0;

	spin_lock_bh(&cnb->cli.rcv_lock);
	spin_lock_bh(&cnb->srv.rcv_lock);

	BUG_ON(trgt_out->targettype != TARGET_OUT);
	BUG_ON(trgt_out->trgt.out.nb != nb);

	if (unlikely(trgt_out->isreset != 0))
		goto unlock;

	if (likely(trgt_out->trgt.out.in_nb_busy_list != 0)) {
		if (likely(time_before(jiffies,
				trgt_out->trgt.out.jiffies_last_act +
				CONN_BUSY_INACTIVITY_TIMEOUT_SEC * HZ))) {
			goto unlock;
		}
	} else {
		if (likely(time_before(jiffies,
				trgt_out->trgt.out.jiffies_last_act +
				CONN_ACTIVITY_UPDATEINTERVAL_SEC * HZ +
				CONN_INACTIVITY_TIMEOUT_SEC * HZ))) {
			goto unlock;
		}
	}

	resetted = (cor_send_reset_conn(nb, cor_get_connid_reverse(
			src_in->src.in.conn_id), 1) == 0);
	if (unlikely(resetted == 0))
		goto unlock;

	BUG_ON(trgt_out->isreset != 0);
	trgt_out->isreset = 1;

	cor_reset_conn_locked(cnb);

unlock:
	spin_unlock_bh(&cnb->srv.rcv_lock);
	spin_unlock_bh(&cnb->cli.rcv_lock);

	return resetted;
}

static void _cor_reset_timeouted_conns(struct cor_neighbor *nb,
		struct list_head *nb_snd_conn_list)
{
	int i;

	for (i = 0; i < 10000; i++) {
		unsigned long iflags;
		struct cor_conn *trgt_out;

		int resetted;

		spin_lock_irqsave(&nb->conn_list_lock, iflags);

		if (list_empty(nb_snd_conn_list)) {
			spin_unlock_irqrestore(&nb->conn_list_lock, iflags);
			break;
		}

		trgt_out = container_of(nb_snd_conn_list->next, struct cor_conn,
				trgt.out.nb_list);
		cor_conn_kref_get(trgt_out, "stack");

		spin_unlock_irqrestore(&nb->conn_list_lock, iflags);

		resetted = cor_reset_timeouted_conn(nb, trgt_out);

		cor_conn_kref_put(trgt_out, "stack");

		if (likely(resetted == 0))
			break;
	}
}

static void cor_reset_timeouted_conns(struct cor_neighbor *nb)
{
	_cor_reset_timeouted_conns(nb, &nb->snd_conn_busy_list);
	_cor_reset_timeouted_conns(nb, &nb->snd_conn_idle_list);
}


/**
 * may not be called by more than one thread at the same time, because
 * 1) readding cor_control_msg_out may reorder them
 * 2) multiple pings may be sent
 */
int cor_send_messages(struct cor_neighbor *nb, unsigned long cmsg_send_start_j,
		ktime_t cmsg_send_start_kt, int *sent)
{
	int rc = QOS_RESUME_DONE;
	int ping;
	int initsession;
	__u32 targetmss = cor_mss_cmsg(nb);

	int nbstate = cor_get_neigh_state(nb);

	if (likely(nbstate == NEIGHBOR_STATE_ACTIVE))
		cor_reset_timeouted_conns(nb);

	if (unlikely(nbstate == NEIGHBOR_STATE_KILLED)) {
		spin_lock_bh(&nb->retrans_lock);
		cor_empty_retrans_queue(nb);
		spin_unlock_bh(&nb->retrans_lock);

		cor_delete_all_cmsgs(nb);
		return QOS_RESUME_DONE;
	}

	ping = cor_time_to_send_ping(nb);

	spin_lock_bh(&nb->cmsg_lock);

	if (nb->add_retrans_needed != 0) {
		nb->add_retrans_needed = 0;
		spin_unlock_bh(&nb->cmsg_lock);
		cor_add_timeouted_retrans(nb);
		spin_lock_bh(&nb->cmsg_lock);
	}

	initsession = unlikely(atomic_read(&nb->sessionid_snd_needed) != 0);

	while (1) {
		struct list_head cmsgs;
		__u32 length = 0;
		__u32 seqno;

		INIT_LIST_HEAD(&cmsgs);

		if (cor_dequeue_messages(nb, ping, initsession, nbstate,
				targetmss, &length, &cmsgs) != 0) {
			cor_schedule_controlmsg_timer(nb);
			spin_unlock_bh(&nb->cmsg_lock);
			return QOS_RESUME_DONE;
		}

		nb->kpacket_seqno++;
		seqno = nb->kpacket_seqno;

		spin_unlock_bh(&nb->cmsg_lock);

		rc = _cor_send_messages_send(nb, ping, initsession, &cmsgs,
				nbstate, length, seqno, cmsg_send_start_j,
				cmsg_send_start_kt, sent);

		if (rc != QOS_RESUME_DONE)
			return rc;

		ping = 0;
		initsession = 0;

		spin_lock_bh(&nb->cmsg_lock);
	}
}

static unsigned long cor_calc_cmsg_send_start_j(
		unsigned long cmsg_timer_timeout)
{
	unsigned long jiffies_tmp = jiffies;

	if (unlikely(time_after(cmsg_timer_timeout, jiffies_tmp)))
		return jiffies_tmp;
	else
		return cmsg_timer_timeout;
}

static ktime_t cor_calc_cmsg_send_start_kt(unsigned long cmsg_timer_timeout)
{
	ktime_t now = ktime_get();
	unsigned long jiffies_tmp = jiffies;

	unsigned long jiffies_delayed;

	if (unlikely(time_after(cmsg_timer_timeout, jiffies_tmp))) {
		jiffies_delayed = 0;
	} else {
		jiffies_delayed = jiffies_tmp - cmsg_timer_timeout;
		if (unlikely(jiffies_delayed > HZ / 10)) {
			jiffies_delayed = HZ / 10;
		}
	}

	return ns_to_ktime(ktime_to_ns(now) -
			1000LL * jiffies_to_usecs(jiffies_delayed));
}


void cor_controlmsg_timerfunc(struct timer_list *cmsg_timer)
{
	struct cor_neighbor *nb = container_of(cmsg_timer,
			struct cor_neighbor, cmsg_timer);
	unsigned long cmsg_timer_timeout = (unsigned long)
			atomic64_read(&nb->cmsg_timer_timeout);
	unsigned long cmsg_send_start_j = cor_calc_cmsg_send_start_j(
			cmsg_timer_timeout);
	ktime_t cmsg_send_start_kt = cor_calc_cmsg_send_start_kt(
			cmsg_timer_timeout);
	cor_dev_queue_enqueue(nb->cd, &nb->rb_kp, cmsg_send_start_j,
			cmsg_send_start_kt, QOS_CALLER_KPACKET, 0);
	cor_nb_kref_put(nb, "controlmsg_timer");
}

static int cor_cmsg_full_packet(struct cor_neighbor *nb, int nbstate)
{
	__u32 extralength = 0;
	int ping = cor_time_to_send_ping(nb);
	int initsession = unlikely(atomic_read(&nb->sessionid_snd_needed) != 0);
	__u32 len = cor_get_total_messages_length(nb, ping, initsession,
			nbstate, &extralength);

	if (len == 0)
		return 0;
	if (len < cor_mss_cmsg(nb))
		return 0;

	return 1;
}

void cor_schedule_controlmsg_timer(struct cor_neighbor *nb_cmsglocked)
{
	unsigned long timeout;
	int nbstate = cor_get_neigh_state(nb_cmsglocked);

	if (unlikely(nbstate == NEIGHBOR_STATE_KILLED))
		goto now;

	if (unlikely(atomic_read(&nb_cmsglocked->rcvmtu_sendneeded) != 0))
		goto now;

	if (atomic_read(&nb_cmsglocked->cmsg_bulk_readds) != 0)
		return;

	if (cor_cmsg_full_packet(nb_cmsglocked, nbstate))
		goto now;

	if (nb_cmsglocked->add_retrans_needed != 0)
		goto now;

	timeout = cor_get_cmsg_timer_timeout(nb_cmsglocked, nbstate);

	if (0) {
now:
		cor_dev_queue_enqueue(nb_cmsglocked->cd, &nb_cmsglocked->rb_kp,
				jiffies, ktime_get(), QOS_CALLER_KPACKET, 0);
	} else if (time_before_eq(timeout, jiffies)) {
		unsigned long cmsg_send_start_j = cor_calc_cmsg_send_start_j(
				timeout);
		ktime_t cmsg_send_start_kt = cor_calc_cmsg_send_start_kt(
				timeout);

		cor_dev_queue_enqueue(nb_cmsglocked->cd, &nb_cmsglocked->rb_kp,
				cmsg_send_start_j, cmsg_send_start_kt,
				QOS_CALLER_KPACKET, 0);
	} else {
		atomic64_set(&nb_cmsglocked->cmsg_timer_timeout, timeout);
		barrier();
		if (mod_timer(&nb_cmsglocked->cmsg_timer, timeout) == 0) {
			cor_nb_kref_get(nb_cmsglocked, "controlmsg_timer");
		}
	}
}

static int cor_insert_pending_conn_resets(struct cor_control_msg_out *ins)
{
	struct cor_neighbor *nb = ins->nb;
	__u32 conn_id = ins->msg.reset_conn.conn_id;

	struct rb_root *root;
	struct rb_node **p;
	struct rb_node *parent = 0;

	BUG_ON(nb == 0);
	BUG_ON(ins->msg.reset_conn.in_pending_conn_resets != 0);

	root = &nb->pending_conn_resets_rb;
	p = &root->rb_node;

	while ((*p) != 0) {
		struct cor_control_msg_out *cm = container_of(*p,
				struct cor_control_msg_out,
				msg.reset_conn.rbn);
		__u32 cm_connid = cm->msg.reset_conn.conn_id;

		BUG_ON(cm->nb != ins->nb);
		BUG_ON(cm->type != MSGTYPE_RESET_CONN);

		parent = *p;
		if (conn_id == cm_connid) {
			return 1;
		} else if (conn_id < cm_connid) {
			p = &(*p)->rb_left;
		} else if (conn_id > cm_connid) {
			p = &(*p)->rb_right;
		}
	}

	kref_get(&ins->ref);
	rb_link_node(&ins->msg.reset_conn.rbn, parent, p);
	rb_insert_color(&ins->msg.reset_conn.rbn, root);
	ins->msg.reset_conn.in_pending_conn_resets = 1;

	return 0;
}

static void cor_free_oldest_pong(struct cor_neighbor *nb)
{
	struct cor_control_msg_out *cm = container_of(nb->cmsg_queue_pong.next,
			struct cor_control_msg_out, lh);

	BUG_ON(list_empty(&nb->cmsg_queue_pong));
	BUG_ON(unlikely(cm->type != MSGTYPE_PONG));

	list_del(&cm->lh);
	nb->cmsg_pongslength -= cm->length;
	BUG_ON(nb->cmsg_pongscnt == 0);
	cm->nb->cmsg_pongscnt--;
	cor_free_control_msg(cm);
}

static struct list_head *_cor_enqueue_control_msg_getqueue(
		struct cor_control_msg_out *cm)
{
	if (cm->type == MSGTYPE_ACK) {
		if (cm->msg.ack.fast != 0) {
			return &cm->nb->cmsg_queue_ack_fast;
		} else {
			return &cm->nb->cmsg_queue_ack_slow;
		}
	} else if (cm->type == MSGTYPE_ACK_CONN) {
		if (unlikely(cm->msg.ack_conn.queue == CMSGQUEUE_ACK_CONN_URGENT)) {
			return &cm->nb->cmsg_queue_ackconn_urgent;
		} else if (cm->msg.ack_conn.queue == CMSGQUEUE_ACK_CONN) {
			return &cm->nb->cmsg_queue_ackconn;
		} else {
			BUG();
		}
	} else if (cm->type == MSGTYPE_CONNDATA) {
		if (cm->msg.conn_data.highlatency != 0) {
			return &cm->nb->cmsg_queue_conndata_highlat;
		} else {
			return &cm->nb->cmsg_queue_conndata_lowlat;
		}
	} else {
		return &cm->nb->cmsg_queue_other;
	}
}

static int _cor_enqueue_control_msg(struct cor_control_msg_out *cm, int src)
{
	if (unlikely(cm->type == MSGTYPE_PONG)) {
		BUG_ON(src == ADDCMSG_SRC_SPLITCONNDATA);

		if (cm->nb->cmsg_pongscnt >= MAX_PONG_CMSGS_PER_NEIGH) {
			if (src != ADDCMSG_SRC_NEW) {
				BUG_ON(cm->nb->cmsg_pongscnt == 0);
				cor_free_control_msg(cm);
				return 1;
			} else {
				cor_free_oldest_pong(cm->nb);
			}
		}

		cm->nb->cmsg_pongscnt++;
		cm->nb->cmsg_pongslength += cm->length;

		if (src != ADDCMSG_SRC_NEW) {
			list_add(&cm->lh, &cm->nb->cmsg_queue_pong);
		} else {
			list_add_tail(&cm->lh, &cm->nb->cmsg_queue_pong);
		}

		return 0;
	} else if (unlikely(cm->type == MSGTYPE_RESET_CONN)) {
		if (cor_insert_pending_conn_resets(cm) != 0) {
			cm->type = 0;
			cor_free_control_msg(cm);
			return 1;
		}
	}

	cm->nb->cmsg_otherlength += cm->length;
	if (src == ADDCMSG_SRC_NEW) {
		list_add_tail(&cm->lh, _cor_enqueue_control_msg_getqueue(cm));
	} else {
		BUG_ON(src == ADDCMSG_SRC_SPLITCONNDATA &&
				cm->type != MSGTYPE_CONNDATA);
		BUG_ON(src == ADDCMSG_SRC_READD &&
				cm->type == MSGTYPE_ACK_CONN);

		list_add(&cm->lh, _cor_enqueue_control_msg_getqueue(cm));
	}

	return 0;
}

static void cor_enqueue_control_msg(struct cor_control_msg_out *cm, int src)
{
	struct cor_neighbor *nb;

	BUG_ON(cm == 0);
	nb = cm->nb;
	BUG_ON(nb == 0);


	if (src == ADDCMSG_SRC_NEW)
		cm->time_added = jiffies;

	spin_lock_bh(&nb->cmsg_lock);

	if (_cor_enqueue_control_msg(cm, src) != 0)
		goto out;

	if (src != ADDCMSG_SRC_READD && src != ADDCMSG_SRC_RETRANS)
		cor_schedule_controlmsg_timer(nb);

out:
	spin_unlock_bh(&nb->cmsg_lock);
}

void cor_send_rcvmtu(struct cor_neighbor *nb)
{
	atomic_set(&nb->rcvmtu_sendneeded, 1);

	spin_lock_bh(&nb->cmsg_lock);
	cor_schedule_controlmsg_timer(nb);
	spin_unlock_bh(&nb->cmsg_lock);
}

void cor_send_pong(struct cor_neighbor *nb, __u32 cookie, ktime_t ping_rcvtime)
{
	struct cor_control_msg_out *cm = _cor_alloc_control_msg(nb);

	if (unlikely(cm == 0))
		return;

	cm->nb = nb;
	cm->type = MSGTYPE_PONG;
	cm->msg.pong.cookie = cookie;
	cm->msg.pong.type = MSGTYPE_PONG_TIMEENQUEUED;
	cm->msg.pong.ping_rcvtime = ping_rcvtime;
	cm->msg.pong.time_enqueued = ktime_get();
	cm->length = 13;
	cor_enqueue_control_msg(cm, ADDCMSG_SRC_NEW);
}

void cor_send_ack(struct cor_neighbor *nb, __u32 seqno, __u8 fast)
{
	struct cor_control_msg_out *cm = cor_alloc_control_msg(nb,
			ACM_PRIORITY_HIGH);

	if (unlikely(cm == 0))
		return;

	cm->nb = nb;
	cm->type = MSGTYPE_ACK;
	cm->msg.ack.seqno = seqno;
	cm->msg.ack.fast = fast;
	cm->length = 5;
	cor_enqueue_control_msg(cm, ADDCMSG_SRC_NEW);
}

static __u8 get_queue_for_ackconn(struct cor_conn *src_in_lx)
{
	if (unlikely(cor_ackconn_urgent(src_in_lx))) {
		return CMSGQUEUE_ACK_CONN_URGENT;
	} else {
		return CMSGQUEUE_ACK_CONN;
	}
}

static void cor_set_ooolen_flags(struct cor_control_msg_out *cm)
{
	cm->msg.ack_conn.flags = (cm->msg.ack_conn.flags &
			(~KP_ACK_CONN_FLAGS_OOO));
	cm->msg.ack_conn.flags = (cm->msg.ack_conn.flags |
			cor_ooolen_to_flags(cm->msg.ack_conn.length));
}

/* cmsg_lock must be held */
static void cor_remove_pending_ackconn(struct cor_control_msg_out *cm)
{
	cm->nb->cmsg_otherlength -= cm->length;
	list_del(&cm->lh);

	list_del(&cm->msg.ack_conn.conn_acks);
	cor_conn_kref_put(cm->msg.ack_conn.src_in,
			"cor_control_msg_out ack_conn");
	cm->msg.ack_conn.src_in = 0;

	cm->type = 0;
	cor_free_control_msg(cm);
}

/* cmsg_lock must be held */
static void cor_recalc_scheduled_ackconn_size(struct cor_control_msg_out *cm)
{
	cm->nb->cmsg_otherlength -= cm->length;
	cm->length = 5 + cor_ack_conn_len(cm->msg.ack_conn.flags);
	cm->nb->cmsg_otherlength += cm->length;
}

/* cmsg_lock must be held */
static int _cor_try_merge_ackconn(struct cor_conn *src_in_l,
		struct cor_control_msg_out *fromcm,
		struct cor_control_msg_out *tocm, int from_newack)
{
	if (cor_ooolen(fromcm->msg.ack_conn.flags) != 0 &&
			cor_ooolen(tocm->msg.ack_conn.flags) != 0) {
		__u32 tocmseqno = tocm->msg.ack_conn.seqno_ooo;
		__u32 tocmlength = tocm->msg.ack_conn.length;
		__u32 fromcmseqno = fromcm->msg.ack_conn.seqno_ooo;
		__u32 fromcmlength = fromcm->msg.ack_conn.length;

		if (cor_seqno_eq(tocmseqno, fromcmseqno)) {
			if (fromcmlength > tocmlength)
				tocm->msg.ack_conn.length = fromcmlength;
		} else if (cor_seqno_after(fromcmseqno, tocmseqno) &&
				cor_seqno_before_eq(fromcmseqno, tocmseqno +
				tocmlength)) {
			__u32 len = fromcmseqno + fromcmlength - tocmseqno;
			BUG_ON(len > U32_MAX);
			tocm->msg.ack_conn.length = (__u32) len;
		} else if (cor_seqno_before(fromcmseqno, tocmseqno) &&
				cor_seqno_after_eq(fromcmseqno, tocmseqno)) {
			__u32 len = tocmseqno + tocmlength - fromcmseqno;
			tocm->msg.ack_conn.seqno_ooo = fromcmseqno;
			tocm->msg.ack_conn.length = len;
		} else {
			return 1;
		}
		cor_set_ooolen_flags(tocm);
	}

	if ((fromcm->msg.ack_conn.flags & KP_ACK_CONN_FLAGS_SEQNO) != 0 && (
			((tocm->msg.ack_conn.flags &
			KP_ACK_CONN_FLAGS_SEQNO) == 0) ||
			cor_seqno_after_eq(fromcm->msg.ack_conn.seqno,
			tocm->msg.ack_conn.seqno))) {

		tocm->msg.ack_conn.flags = (tocm->msg.ack_conn.flags |
					KP_ACK_CONN_FLAGS_SEQNO);
		tocm->msg.ack_conn.seqno = fromcm->msg.ack_conn.seqno;
		tocm->msg.ack_conn.flags = (tocm->msg.ack_conn.flags |
				(fromcm->msg.ack_conn.flags &
				KP_ACK_CONN_FLAGS_WINDOW));
	}

	if (cor_ooolen(fromcm->msg.ack_conn.flags) != 0) {
		tocm->msg.ack_conn.seqno_ooo = fromcm->msg.ack_conn.seqno_ooo;
		tocm->msg.ack_conn.length = fromcm->msg.ack_conn.length;
		cor_set_ooolen_flags(tocm);
	}

	if ((fromcm->msg.ack_conn.flags & KP_ACK_CONN_FLAGS_PRIORITY) != 0) {
		BUG_ON((tocm->msg.ack_conn.flags &
				KP_ACK_CONN_FLAGS_PRIORITY) != 0);
		tocm->msg.ack_conn.priority_seqno =
				fromcm->msg.ack_conn.priority_seqno;
		tocm->msg.ack_conn.priority = fromcm->msg.ack_conn.priority;
		tocm->msg.ack_conn.is_highlatency =
				fromcm->msg.ack_conn.is_highlatency;
	}

	cor_recalc_scheduled_ackconn_size(tocm);
	if (from_newack == 0)
		cor_remove_pending_ackconn(fromcm);

	return 0;
}

/* cmsg_lock must be held */
static void cor_try_merge_ackconns(struct cor_conn *src_in_l,
		struct cor_control_msg_out *cm)
{
	struct list_head *currlh = cm->msg.ack_conn.conn_acks.next;

	while (currlh != &src_in_l->src.in.acks_pending) {
		struct cor_control_msg_out *currcm = container_of(currlh,
				struct cor_control_msg_out,
				msg.ack_conn.conn_acks);
		currlh = currlh->next;
		cor_remove_connack_oooflag_ifold(src_in_l, currcm);
		_cor_try_merge_ackconn(src_in_l, currcm, cm, 0);
	}
}

static void cor_merge_or_enqueue_ackconn(struct cor_conn *src_in_l,
		struct cor_control_msg_out *cm, int src)
{
	struct list_head *currlh;

	BUG_ON(src_in_l->sourcetype != SOURCE_IN);

	spin_lock_bh(&cm->nb->cmsg_lock);

	currlh = src_in_l->src.in.acks_pending.next;
	while (currlh != &src_in_l->src.in.acks_pending) {
		struct cor_control_msg_out *currcm = container_of(currlh,
				struct cor_control_msg_out,
				msg.ack_conn.conn_acks);

		BUG_ON(currcm->nb != cm->nb);
		BUG_ON(currcm->type != MSGTYPE_ACK_CONN);
		BUG_ON(cm->msg.ack_conn.src_in != src_in_l);
		BUG_ON(currcm->msg.ack_conn.conn_id !=
				cm->msg.ack_conn.conn_id);

		if (_cor_try_merge_ackconn(src_in_l, cm, currcm, 1) == 0) {
			cor_try_merge_ackconns(src_in_l, currcm);
			cor_schedule_controlmsg_timer(currcm->nb);
			spin_unlock_bh(&currcm->nb->cmsg_lock);
			/**
			 * flags:
			 * when calling cor_free_control_msg here conn may
			 * already be locked and priority_send_allowed and
			 * priority_send_allowed should not be reset
			 */
			cm->msg.ack_conn.flags = 0;
			cor_free_control_msg(cm);
			return;
		}

		currlh = currlh->next;
	}

	list_add_tail(&cm->msg.ack_conn.conn_acks,
			&src_in_l->src.in.acks_pending);

	spin_unlock_bh(&cm->nb->cmsg_lock);

	cor_enqueue_control_msg(cm, src);
}

static int cor_try_update_ackconn_seqno(struct cor_conn *src_in_l)
{
	int rc = 1;

	spin_lock_bh(&src_in_l->src.in.nb->cmsg_lock);

	if (list_empty(&src_in_l->src.in.acks_pending) == 0) {
		struct cor_control_msg_out *cm = container_of(
				src_in_l->src.in.acks_pending.next,
				struct cor_control_msg_out,
				msg.ack_conn.conn_acks);
		BUG_ON(cm->nb != src_in_l->src.in.nb);
		BUG_ON(cm->type != MSGTYPE_ACK_CONN);
		BUG_ON(cm->msg.ack_conn.src_in != src_in_l);
		BUG_ON(cm->msg.ack_conn.conn_id != cor_get_connid_reverse(
				src_in_l->src.in.conn_id));

		cm->msg.ack_conn.flags = (cm->msg.ack_conn.flags |
				KP_ACK_CONN_FLAGS_SEQNO |
				KP_ACK_CONN_FLAGS_WINDOW);
		cm->msg.ack_conn.seqno = src_in_l->src.in.next_seqno;

		cor_remove_connack_oooflag_ifold(src_in_l, cm);
		cor_recalc_scheduled_ackconn_size(cm);

		cor_try_merge_ackconns(src_in_l, cm);

		rc = 0;
	}

	spin_unlock_bh(&src_in_l->src.in.nb->cmsg_lock);

	return rc;
}

void cor_send_ack_conn_ifneeded(struct cor_conn *src_in_l, __u32 seqno_ooo,
		__u32 ooo_length)
{
	struct cor_control_msg_out *cm;

	BUG_ON(src_in_l->sourcetype != SOURCE_IN);

	BUG_ON(ooo_length > 0 && cor_seqno_before_eq(seqno_ooo,
			src_in_l->src.in.next_seqno));

	cor_update_windowlimit(src_in_l);

	if (ooo_length != 0) {
		cm = cor_alloc_control_msg(src_in_l->src.in.nb,
				ACM_PRIORITY_LOW);
		if (cm != 0)
			goto add;
	}

	if (src_in_l->src.in.inorder_ack_needed != 0)
		goto ack_needed;

	if (src_in_l->src.in.window_seqnolimit - src_in_l->src.in.next_seqno <
			WINDOW_ENCODE_MIN)
		return;

	if (src_in_l->src.in.window_seqnolimit -
			src_in_l->src.in.next_seqno < WINDOW_ENCODE_MIN)
		return;

	if (src_in_l->src.in.window_seqnolimit_remote -
			src_in_l->src.in.next_seqno >= WINDOW_ENCODE_MIN &&
			(src_in_l->src.in.window_seqnolimit -
			src_in_l->src.in.next_seqno) * 7 <
			(src_in_l->src.in.window_seqnolimit_remote -
			src_in_l->src.in.next_seqno) * 8)
		return;

ack_needed:
	if (cor_try_update_ackconn_seqno(src_in_l) == 0)
		goto out;

	cm = cor_alloc_control_msg(src_in_l->src.in.nb, ACM_PRIORITY_MED);
	if (cm == 0) {
		printk(KERN_ERR "error allocating inorder ack\n");
		return;
	}

add:
	cm->type = MSGTYPE_ACK_CONN;
	cor_conn_kref_get(src_in_l, "cor_control_msg_out ack_conn");
	cm->msg.ack_conn.src_in = src_in_l;
	cm->msg.ack_conn.conn_id =
			cor_get_connid_reverse(src_in_l->src.in.conn_id);
	cm->msg.ack_conn.seqno = src_in_l->src.in.next_seqno;
	cm->msg.ack_conn.seqno_ooo = seqno_ooo;
	cm->msg.ack_conn.length = ooo_length;
	cm->msg.ack_conn.bufsize_changerate =
			_cor_bufsize_update_get_changerate(src_in_l);
	cm->msg.ack_conn.flags = KP_ACK_CONN_FLAGS_SEQNO |
			KP_ACK_CONN_FLAGS_WINDOW;
	cor_set_ooolen_flags(cm);
	cm->msg.ack_conn.queue = get_queue_for_ackconn(src_in_l);
	cm->length = 5 + cor_ack_conn_len(cm->msg.ack_conn.flags);

	cor_merge_or_enqueue_ackconn(src_in_l, cm, ADDCMSG_SRC_NEW);

out:
	src_in_l->src.in.inorder_ack_needed = 0;
	src_in_l->src.in.window_seqnolimit_remote =
			src_in_l->src.in.window_seqnolimit;
}

static int cor_try_add_priority(struct cor_conn *trgt_out_ll, __u16 priority)
{
	int rc = 1;
	struct cor_conn *src_in_ll = cor_get_conn_reversedir(trgt_out_ll);

	spin_lock_bh(&trgt_out_ll->trgt.out.nb->cmsg_lock);

	if (list_empty(&src_in_ll->src.in.acks_pending) == 0) {
		struct cor_control_msg_out *cm = container_of(
				src_in_ll->src.in.acks_pending.next,
				struct cor_control_msg_out,
				msg.ack_conn.conn_acks);
		BUG_ON(cm->nb != trgt_out_ll->trgt.out.nb);
		BUG_ON(cm->type != MSGTYPE_ACK_CONN);
		BUG_ON(cm->msg.ack_conn.src_in != src_in_ll);
		BUG_ON(cm->msg.ack_conn.conn_id !=
				trgt_out_ll->trgt.out.conn_id);

		BUG_ON((cm->msg.ack_conn.flags & KP_ACK_CONN_FLAGS_PRIORITY) !=
				0);
		cm->msg.ack_conn.flags = (cm->msg.ack_conn.flags |
				KP_ACK_CONN_FLAGS_PRIORITY);
		cm->msg.ack_conn.priority_seqno =
				trgt_out_ll->trgt.out.priority_seqno;
		cm->msg.ack_conn.priority = priority;
		cm->msg.ack_conn.is_highlatency = trgt_out_ll->is_highlatency;
		cor_recalc_scheduled_ackconn_size(cm);

		rc = 0;
	}

	spin_unlock_bh(&trgt_out_ll->trgt.out.nb->cmsg_lock);

	return rc;
}

void cor_send_priority(struct cor_conn *trgt_out_ll, __u16 priority)
{
	struct cor_conn *src_in_ll = cor_get_conn_reversedir(trgt_out_ll);
	struct cor_control_msg_out *cm;

	if (cor_try_add_priority(trgt_out_ll, priority) == 0)
		goto out;

	cm = cor_alloc_control_msg(trgt_out_ll->trgt.out.nb, ACM_PRIORITY_LOW);
	if (cm == 0)
		return;

	cm->type = MSGTYPE_ACK_CONN;
	cm->msg.ack_conn.flags = KP_ACK_CONN_FLAGS_PRIORITY;
	cor_conn_kref_get(src_in_ll, "cor_control_msg_out ack_conn");
	BUG_ON(trgt_out_ll->targettype != TARGET_OUT);
	cm->msg.ack_conn.src_in = src_in_ll;
	cm->msg.ack_conn.conn_id = trgt_out_ll->trgt.out.conn_id;
	cm->msg.ack_conn.bufsize_changerate =
			_cor_bufsize_update_get_changerate(src_in_ll);
	cm->msg.ack_conn.priority_seqno = trgt_out_ll->trgt.out.priority_seqno;
	cm->msg.ack_conn.priority = priority;
	cm->msg.ack_conn.is_highlatency = trgt_out_ll->is_highlatency;
	cm->msg.ack_conn.queue = get_queue_for_ackconn(src_in_ll);

	cm->length = 5 + cor_ack_conn_len(cm->msg.ack_conn.flags);
	cor_merge_or_enqueue_ackconn(src_in_ll, cm, ADDCMSG_SRC_NEW);

out:
	trgt_out_ll->trgt.out.priority_last = priority;
	trgt_out_ll->trgt.out.priority_seqno =
			(trgt_out_ll->trgt.out.priority_seqno + 1) & 15;
	trgt_out_ll->is_highlatency_send_needed = 0;
	trgt_out_ll->trgt.out.priority_send_allowed = 0;
}

void cor_free_ack_conns(struct cor_conn *src_in_lx)
{
	int changed = 0;

	spin_lock_bh(&src_in_lx->src.in.nb->cmsg_lock);
	while (list_empty(&src_in_lx->src.in.acks_pending) == 0) {
		struct list_head *currlh =
				src_in_lx->src.in.acks_pending.next;
		struct cor_control_msg_out *currcm = container_of(currlh,
				struct cor_control_msg_out,
				msg.ack_conn.conn_acks);

		cor_remove_pending_ackconn(currcm);
		changed = 1;
	}
	if (changed)
		cor_schedule_controlmsg_timer(src_in_lx->src.in.nb);
	spin_unlock_bh(&src_in_lx->src.in.nb->cmsg_lock);
}

void cor_send_connect_success(struct cor_control_msg_out *cm, __u32 conn_id,
		struct cor_conn *src_in)
{
	cm->type = MSGTYPE_CONNECT_SUCCESS;
	cm->msg.connect_success.conn_id = conn_id;
	cor_conn_kref_get(src_in, "cor_control_msg_out connect_success");
	cm->msg.connect_success.src_in = src_in;
	cm->length = 6;
	cor_enqueue_control_msg(cm, ADDCMSG_SRC_NEW);
}

void cor_send_connect_nb(struct cor_control_msg_out *cm, __u32 conn_id,
		__u32 seqno1, __u32 seqno2, struct cor_conn *src_in_ll)
{
	cm->type = MSGTYPE_CONNECT;
	cm->msg.connect.conn_id = conn_id;
	cm->msg.connect.seqno1 = seqno1;
	cm->msg.connect.seqno2 = seqno2;
	cor_conn_kref_get(src_in_ll, "cor_control_msg_out connect");
	BUG_ON(src_in_ll->sourcetype != SOURCE_IN);
	cm->msg.connect.src_in = src_in_ll;
	cm->length = 17;
	cor_enqueue_control_msg(cm, ADDCMSG_SRC_NEW);
}

void cor_send_conndata(struct cor_control_msg_out *cm, __u32 conn_id,
		__u32 seqno, char *data_orig, char *data, __u32 datalen,
		__u8 windowused, __u8 flush, __u8 highlatency,
		struct cor_conn_retrans *cr)
{
	cm->type = MSGTYPE_CONNDATA;
	cm->msg.conn_data.conn_id = conn_id;
	cm->msg.conn_data.seqno = seqno;
	cm->msg.conn_data.data_orig = data_orig;
	cm->msg.conn_data.data = data;
	cm->msg.conn_data.datalen = datalen;
	cm->msg.conn_data.windowused = windowused;
	cm->msg.conn_data.flush = flush;
	cm->msg.conn_data.highlatency = highlatency;
	cm->msg.conn_data.cr = cr;
	kref_get(&cr->ref);
	cm->length = get_kp_conn_data_length(datalen);
	cor_enqueue_control_msg(cm, ADDCMSG_SRC_NEW);
}

int cor_send_reset_conn(struct cor_neighbor *nb, __u32 conn_id, int lowprio)
{
	struct cor_control_msg_out *cm;

	if (unlikely(cor_get_neigh_state(nb) == NEIGHBOR_STATE_KILLED))
		return 0;

	cm = cor_alloc_control_msg(nb, lowprio ?
			ACM_PRIORITY_LOW : ACM_PRIORITY_MED);

	if (unlikely(cm == 0))
		return 1;

	cm->type = MSGTYPE_RESET_CONN;
	cm->msg.reset_conn.conn_id = conn_id;
	cm->length = 5;

	cor_enqueue_control_msg(cm, ADDCMSG_SRC_NEW);

	return 0;
}

int __init cor_kgen_init(void)
{
	cor_controlmsg_slab = kmem_cache_create("cor_controlmsg",
			sizeof(struct cor_control_msg_out), 8, 0, 0);
	if (unlikely(cor_controlmsg_slab == 0))
		return -ENOMEM;

	cor_controlretrans_slab = kmem_cache_create("cor_controlretransmsg",
			sizeof(struct cor_control_retrans), 8, 0, 0);
	if (unlikely(cor_controlretrans_slab == 0))
		return -ENOMEM;

	return 0;
}

void __exit cor_kgen_exit2(void)
{
	kmem_cache_destroy(cor_controlretrans_slab);
	cor_controlretrans_slab = 0;

	kmem_cache_destroy(cor_controlmsg_slab);
	cor_controlmsg_slab = 0;
}

MODULE_LICENSE("GPL");
