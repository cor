/**
 *	Connection oriented routing
 *	Copyright (C) 2007-2021 Michael Blizek
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License
 *	as published by the Free Software Foundation; either version 2
 *	of the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 */

#include <net/sock.h>
#include <linux/net.h>
#include <linux/uaccess.h>

#include "cor.h"

static int cor_rawsocket_release_trypasssocket(struct cor_sock *cs)
{
	struct cor_sock *passto;
	struct cor_conn *src_sock;
	struct cor_conn *trgt_sock;
	int rc = 0;

	mutex_lock(&cs->lock);

	BUG_ON(cs->type != CS_TYPE_CONN_RAW);
	passto = cs->data.conn_raw.pass_on_close;
	cs->data.conn_raw.pass_on_close = 0;

	src_sock = cs->data.conn_raw.src_sock;
	trgt_sock = cs->data.conn_raw.trgt_sock;

	mutex_unlock(&cs->lock);

	if (passto == 0)
		return 0;

	mutex_lock(&passto->lock);
	spin_lock_bh(&src_sock->rcv_lock);
	spin_lock_bh(&trgt_sock->rcv_lock);

	BUG_ON(src_sock->is_client == 0);
	BUG_ON(passto->type != CS_TYPE_CONN_MANAGED);

	if (unlikely(unlikely(passto->isreleased != 0) ||
			unlikely(passto->data.conn_managed.connect_state !=
			CS_CONNECTSTATE_CONNECTING)))
		goto out;

	BUG_ON(passto->data.conn_managed.src_sock != 0);
	BUG_ON(passto->data.conn_managed.trgt_sock != 0);

	if (unlikely(unlikely(src_sock->isreset != 0) ||
			unlikely(trgt_sock->isreset != 0))) {
		__cor_set_sock_connecterror(passto, ENETUNREACH);
		goto out;
	}

	BUG_ON(src_sock->sourcetype != SOURCE_SOCK);
	BUG_ON(src_sock->src.sock.ed->cs != cs);
	BUG_ON(trgt_sock->targettype != TARGET_SOCK);
	BUG_ON(trgt_sock->trgt.sock.cs != cs);

	passto->data.conn_managed.src_sock = src_sock;
	passto->data.conn_managed.trgt_sock = trgt_sock;
	src_sock->src.sock.ed->cs = passto;
	trgt_sock->trgt.sock.cs = passto;
	kref_get(&passto->ref);
	kref_get(&passto->ref);

	src_sock->src.sock.ed->priority = cs->priority;

	cor_set_conn_is_highlatency(src_sock, passto->is_highlatency, 1, 1);

	BUG_ON(passto->data.conn_managed.rcv_buf == 0);
	src_sock->src.sock.socktype = SOCKTYPE_MANAGED;
	trgt_sock->trgt.sock.socktype = SOCKTYPE_MANAGED;
	trgt_sock->trgt.sock.rcv_buf_state = RCV_BUF_STATE_INCOMPLETE;
	trgt_sock->trgt.sock.rcv_buf = passto->data.conn_managed.rcv_buf;
	trgt_sock->trgt.sock.rcvd = 0;

	BUG_ON(src_sock->src.sock.keepalive_intransit != 0);
	src_sock->src.sock.ed->jiffies_keepalive_lastact = jiffies -
			KEEPALIVE_INTERVAL_SECS * HZ + HZ;
	cor_keepalive_req_sched_timer(src_sock);

	cor_conn_refresh_priority(src_sock, 1);

	rc = 1;

out:
	spin_unlock_bh(&trgt_sock->rcv_lock);
	spin_unlock_bh(&src_sock->rcv_lock);
	mutex_unlock(&passto->lock);

	if (rc != 0) {
		mutex_lock(&cs->lock);
		cs->data.conn_raw.src_sock = 0;
		cs->data.conn_raw.trgt_sock = 0;
		mutex_unlock(&cs->lock);

		lock_sock(&cs->sk);
		cs->sk.sk_socket->state = SS_CONNECTED;
		release_sock(&cs->sk);

		mutex_lock(&passto->lock);
		BUG_ON(passto->type != CS_TYPE_CONN_MANAGED);
		passto->data.conn_managed.connect_state =
				CS_CONNECTSTATE_CONNECTED;
		if (likely(passto->isreleased == 0)) {
			atomic_set(&passto->ready_to_write, 1);
			barrier();
			passto->sk.sk_state_change(&passto->sk);
		}
		mutex_unlock(&passto->lock);


		/* pointers from struct cor_conn */
		kref_put(&cs->ref, cor_kreffree_bug);
		kref_put(&cs->ref, cor_kreffree_bug);
	}

	kref_put(&passto->ref, cor_free_sock);

	return rc;
}

int cor_rawsocket_release(struct socket *sock)
{
	struct cor_sock *cs = (struct cor_sock *) sock->sk;
	__u8 type;

	mutex_lock(&cs->lock);
	cs->isreleased = 1;
	type = cs->type;
	mutex_unlock(&cs->lock);

	if (type == CS_TYPE_UNCONNECTED) {
	} else if (type == CS_TYPE_CONN_RAW) {
		mutex_lock(&cs->lock);
		BUG_ON(cs->type != CS_TYPE_CONN_RAW);
		if (cs->data.conn_raw.rcvitem != 0) {
			BUG_ON(cs->data.conn_raw.trgt_sock == 0);

			cor_databuf_unpull_dpi(cs->data.conn_raw.trgt_sock, cs,
					cs->data.conn_raw.rcvitem,
					cs->data.conn_raw.rcvoffset);
			cs->data.conn_raw.rcvitem = 0;
		}
		mutex_unlock(&cs->lock);

		if (cor_rawsocket_release_trypasssocket(cs) != 0)
			goto out;

		mutex_lock(&cs->lock);
		BUG_ON(cs->type != CS_TYPE_CONN_RAW);
		if (cs->data.conn_raw.src_sock != 0 &&
				cs->data.conn_raw.trgt_sock != 0) {
			cor_reset_conn(cs->data.conn_raw.src_sock);
			cor_conn_kref_put_bug(cs->data.conn_raw.src_sock,
					"socket");
			cor_conn_kref_put(cs->data.conn_raw.trgt_sock,
					"socket");
			cs->data.conn_raw.src_sock = 0;
			cs->data.conn_raw.trgt_sock = 0;
		}
		mutex_unlock(&cs->lock);
	} else {
		BUG();
	}

out:
	kref_put(&cs->ref, cor_free_sock);

	return 0;
}

int cor_rawsocket_bind(struct socket *sock, struct sockaddr *saddr,
		int sockaddr_len)
{
	return -EROFS;
}

int cor_rawsocket_connect(struct socket *sock, struct sockaddr *saddr,
		int sockaddr_len, int flags)
{
	struct cor_sock *cs = (struct cor_sock *) sock->sk;

	struct cor_conn_bidir *cnb;
	struct cor_conn *src_sock;
	struct cor_conn *trgt_sock;

	mutex_lock(&cs->lock);
	if (cs->type != CS_TYPE_UNCONNECTED) {
		mutex_unlock(&cs->lock);
		return -EISCONN;
	}

	cnb = cor_alloc_conn(GFP_KERNEL, cs->is_highlatency);

	if (unlikely(cnb == 0)) {
		mutex_unlock(&cs->lock);
		return -ETIMEDOUT;
	}

	src_sock = &cnb->cli;
	trgt_sock = &cnb->srv;

	spin_lock_bh(&src_sock->rcv_lock);
	spin_lock_bh(&trgt_sock->rcv_lock);

	if (cor_conn_init_sock_source(src_sock) != 0) {
		spin_unlock_bh(&trgt_sock->rcv_lock);
		spin_unlock_bh(&src_sock->rcv_lock);
		mutex_unlock(&cs->lock);
		cor_reset_conn(src_sock);
		cor_conn_kref_put(src_sock, "alloc_conn");
		return -EISCONN;
	}
	cor_conn_init_sock_target(trgt_sock);

	memset(&cs->data, 0, sizeof(cs->data));
	cs->type = CS_TYPE_CONN_RAW;
	cs->data.conn_raw.src_sock = src_sock;
	cs->data.conn_raw.trgt_sock = trgt_sock;
	cor_conn_kref_get(src_sock, "socket");
	cor_conn_kref_get(trgt_sock, "socket");

	src_sock->src.sock.ed->cs = cs;
	trgt_sock->trgt.sock.cs = cs;
	kref_get(&cs->ref);
	kref_get(&cs->ref);

	src_sock->src.sock.socktype = SOCKTYPE_RAW;
	trgt_sock->trgt.sock.socktype = SOCKTYPE_RAW;
	src_sock->src.sock.ed->priority = cs->priority;

	cor_conn_refresh_priority(src_sock, 1);

	spin_unlock_bh(&trgt_sock->rcv_lock);
	spin_unlock_bh(&src_sock->rcv_lock);
	mutex_unlock(&cs->lock);

	lock_sock(&cs->sk);
	sock->state = SS_CONNECTED;
	release_sock(&cs->sk);

	cor_conn_kref_put(src_sock, "alloc_conn");

	return 0;
}

int cor_rawsocket_accept(struct socket *sock, struct socket *newsock, int flags,
		bool kern)
{
	return -EINVAL;
}

int cor_rawsocket_listen(struct socket *sock, int len)
{
	return -EOPNOTSUPP;
}

int cor_rawsocket_shutdown(struct socket *sock, int flags)
{
	return 0;
}

int cor_rawsocket_ioctl(struct socket *sock, unsigned int cmd,
		unsigned long arg)
{
	return -ENOIOCTLCMD;
}

static int cor_rawsocket_setsockopt_passonclose(struct socket *sock,
		char __user *optval, unsigned int optlen)
{
	struct cor_sock *cs = (struct cor_sock *) sock->sk;

	int rc = 0;

	__be64 cookie;
	int notread;
	struct cor_sock *passto;

	if (unlikely(optlen != 8))
		return -EINVAL;

	notread = copy_from_user((char *) &cookie, optval, 8);
	if (unlikely(notread != 0))
		return -EFAULT;

	passto = cor_get_sock_by_cookie(cookie);
	if (unlikely(passto == 0))
		return -EINVAL;

	mutex_lock(&cs->lock);
	if (unlikely(cs->type != CS_TYPE_CONN_RAW)) {
		rc = -EINVAL;
		goto out;
	}

	BUG_ON(passto->type != CS_TYPE_CONN_MANAGED);

	if (unlikely(cs->data.conn_raw.pass_on_close != 0))
		kref_put(&cs->data.conn_raw.pass_on_close->ref, cor_free_sock);

	cs->data.conn_raw.pass_on_close = passto;

out:
	mutex_unlock(&cs->lock);

	if (unlikely(rc != 0))
		kref_put(&passto->ref, cor_free_sock);

	return rc;
}

int cor_rawsocket_setsockopt(struct socket *sock, int level,
		int optname, char __user *optval, unsigned int optlen)
{
	if (unlikely(level != SOL_COR))
		return -ENOPROTOOPT;

	if (optname == COR_PASS_ON_CLOSE) {
		return cor_rawsocket_setsockopt_passonclose(sock, optval,
				optlen);
	} else if (optname == COR_TOS) {
		return cor_socket_setsockopt_tos(sock, optval, optlen);
	} else if (optname == COR_PRIORITY) {
		return cor_socket_setsockopt_priority(sock, optval, optlen);
	} else {
		return -ENOPROTOOPT;
	}
}

int cor_rawsocket_getsockopt(struct socket *sock, int level,
		int optname, char __user *optval, int __user *optlen)
{
	return -ENOPROTOOPT;
}

static unsigned int _cor_rawsocket_poll(struct cor_sock *cs, __u32 writelen,
		int frompoll)
{
	unsigned int mask = 0;

	struct cor_conn *trgt_sock;
	struct cor_conn *src_sock;

	mutex_lock(&cs->lock);

	if (frompoll == 0) {
		BUG_ON(cs->type != CS_TYPE_CONN_RAW);
	} else {
		BUG_ON(cs->type != CS_TYPE_UNCONNECTED &&
				cs->type != CS_TYPE_CONN_RAW);
		if (unlikely(cs->type != CS_TYPE_CONN_RAW)) {
			mutex_unlock(&cs->lock);
			return 0;
		}
	}

	trgt_sock = cs->data.conn_raw.trgt_sock;
	src_sock = cs->data.conn_raw.src_sock;

	if (unlikely(trgt_sock == 0 || src_sock == 0)) {
		mutex_unlock(&cs->lock);
		return U32_MAX;
	}

	spin_lock_bh(&trgt_sock->rcv_lock);
	if (unlikely(trgt_sock->isreset != 0 ||
			cor_is_trgt_sock(trgt_sock, cs) == 0)) {
		mask = U32_MAX;
	} else if (cs->data.conn_raw.rcvitem != 0 ||
			trgt_sock->data_buf.read_remaining != 0) {
		mask |= (POLLIN | POLLRDNORM);
	}
	spin_unlock_bh(&trgt_sock->rcv_lock);

	spin_lock_bh(&src_sock->rcv_lock);
	if (unlikely(src_sock->isreset != 0 ||
			cor_is_src_sock(src_sock, cs) == 0)) {
		mask = U32_MAX;
	} else if (cor_sock_sndbufavailable(src_sock, 1)) {
		mask |= (POLLOUT | POLLWRNORM);
	}
	spin_unlock_bh(&src_sock->rcv_lock);

	mutex_unlock(&cs->lock);

	return mask;
}

static int ___cor_rawsocket_sendmsg(char *buf, __u32 bufread,
		__u32 buflen, __u8 flush, struct cor_sock *cs_r_l)
{
	struct cor_conn *src_sock;

	int rc = 0;
	__u32 rc2;

	BUG_ON(cs_r_l->type != CS_TYPE_CONN_RAW);

	src_sock = cs_r_l->data.conn_raw.src_sock;
	if (unlikely(src_sock == 0))
		return -ENOTCONN;

	spin_lock_bh(&src_sock->rcv_lock);

	if (unlikely(unlikely(cor_is_src_sock(src_sock, cs_r_l) == 0) ||
			unlikely(src_sock->isreset != 0))) {
		spin_unlock_bh(&src_sock->rcv_lock);
		return -EPIPE;
	}

	if (cor_sock_sndbufavailable(src_sock, 0) == 0) {
		rc = -EAGAIN;
		atomic_set(&cs_r_l->ready_to_write, 0);
		src_sock->flush = 0;
		goto out;
	}

	BUG_ON(bufread > (1024 * 1024 * 1024));
	BUG_ON(buflen > (1024 * 1024 * 1024));

	rc2 = cor_receive_sock(src_sock, buf, bufread, flush);

	BUG_ON(rc2 > (1024 * 1024 * 1024));
	if (unlikely(rc2 == 0))
		rc = -ENOMEM;
	else
		rc = rc2;

	if (likely(rc > 0))
		cor_flush_buf(src_sock);

out:
	spin_unlock_bh(&src_sock->rcv_lock);

	return rc;
}

static int __cor_rawsocket_sendmsg(struct msghdr *msg, __u32 totallen,
		__u8 flush, struct cor_sock *cs_r_l)
{
	char *buf = 0;
	__u32 bufread = 0;
	__u32 buflen = cor_buf_optlen(totallen, 1);
	__u32 len = totallen;
	size_t st_rc;
	int rc;

	BUG_ON(totallen > (1024 * 1024 * 1024));
	BUG_ON(buflen > (1024 * 1024 * 1024));

	if (buflen < len) {
		len = buflen;
		flush = 0;
	}

	if (unlikely(len <= 0))
		return 0;

	buf = kmalloc(buflen, GFP_KERNEL);
	if (unlikely(buf == 0))
		return -ENOMEM;

	memset(buf, 0, buflen);

	st_rc = copy_from_iter(buf + bufread, len, &msg->msg_iter);

	if (unlikely(st_rc != len)) {
		kfree(buf);
		return -EFAULT;
	}

	rc = ___cor_rawsocket_sendmsg(buf, len, buflen, flush, cs_r_l);

	kfree(buf);

	return rc;
}

static int _cor_rawsocket_sendmsg(struct msghdr *msg, __u32 totallen,
		struct cor_sock *cs, __u8 flush)
{
	int copied;

	BUG_ON(totallen > (1024 * 1024 * 1024));

	mutex_lock(&cs->lock);

	BUG_ON(cs->type != CS_TYPE_UNCONNECTED && cs->type != CS_TYPE_CONN_RAW);
	if (unlikely(cs->type == CS_TYPE_UNCONNECTED)) {
		mutex_unlock(&cs->lock);
		return -ENOTCONN;
	} else if (unlikely(cs->type != CS_TYPE_CONN_RAW)) {
		mutex_unlock(&cs->lock);
		return -EBADF;
	}

	copied = __cor_rawsocket_sendmsg(msg, totallen, flush, cs);
	BUG_ON(copied > 0 && ((__u32) copied) > totallen);

	mutex_unlock(&cs->lock);

	return copied;
}

int cor_rawsocket_sendmsg(struct socket *sock, struct msghdr *msg,
		size_t total_len)
{
	__u8 flush = ((msg->msg_flags & MSG_MORE) == 0) ? 1 : 0;
	int blocking = (msg->msg_flags & MSG_DONTWAIT) == 0;

	int rc = 0;
	int copied = 0;

	struct cor_sock *cs = (struct cor_sock *) sock->sk;

	__u32 max = (1024 * 1024 * 1024);
	__u32 totallen;

	totallen = total_len;
	if (unlikely(totallen > max || total_len > max)) {
		totallen = max;
		flush = 0;
	}

	while (rc >= 0 && copied < totallen) {
		rc = _cor_rawsocket_sendmsg(msg, totallen, cs, flush);

		BUG_ON(rc > 0 && unlikely((rc > total_len || rc > totallen)));

		if (rc == -EAGAIN && blocking && copied == 0) {
			long waitret;

			waitret = wait_event_interruptible_timeout(
					*sk_sleep(&cs->sk),
					atomic_read(&cs->ready_to_write) != 0,
					cs->sk.sk_sndtimeo);

			if (unlikely(waitret < 0))
				rc = sock_intr_errno(cs->sk.sk_sndtimeo);
			else if (unlikely(waitret == 0))
				rc = -ETIMEDOUT;
			else
				continue;
		}

		if (rc > 0 || copied == 0)
			copied += rc;
		if (unlikely(rc == -EFAULT))
			copied = rc;

		BUG_ON(copied > 0 && ((__u32) copied > totallen));
	}

	return rc;
}

static int __cor_rawsocket_recvmsg(struct msghdr *msg, __u32 totallen,
		struct cor_sock *cs)
{
	struct cor_data_buf_item *dbi = cs->data.conn_raw.rcvitem;
	__u32 written = 0;

	__u32 len;
	size_t st_rc;

	BUG_ON(totallen > (1024 * 1024 * 1024));

	if (dbi == 0)
		return -EAGAIN;

	BUG_ON(dbi->datalen <= cs->data.conn_raw.rcvoffset);

	len = totallen;
	if (len > (dbi->datalen - cs->data.conn_raw.rcvoffset))
		len = dbi->datalen - cs->data.conn_raw.rcvoffset;

	if (unlikely(len <= 0))
		return -EAGAIN;

	st_rc = copy_to_iter(dbi->buf + cs->data.conn_raw.rcvoffset, len,
			&msg->msg_iter);

	if (unlikely(st_rc != len))
		return -EFAULT;

	written += len;
	cs->data.conn_raw.rcvoffset += len;
	if (dbi->datalen == cs->data.conn_raw.rcvoffset) {
		cor_databuf_item_free(cs->data.conn_raw.rcvitem);
		cs->data.conn_raw.rcvitem = 0;
		cs->data.conn_raw.rcvoffset = 0;
	}

	BUG_ON(written > totallen);

	return written;
}

static int _cor_rawsocket_recvmsg(struct msghdr *msg, __u32 totallen,
		struct cor_sock *cs_r)
{
	int copied = 0;
	int rc = 0;

	struct cor_conn *trgt_sock;

	mutex_lock(&cs_r->lock);

	BUG_ON(cs_r->type != CS_TYPE_CONN_RAW);

	trgt_sock = cs_r->data.conn_raw.trgt_sock;

	if (unlikely(cs_r->data.conn_raw.src_sock == 0 || trgt_sock == 0)) {
		mutex_unlock(&cs_r->lock);
		return -ENOTCONN;
	}

	cor_conn_kref_get(trgt_sock, "stack");

	while (rc >= 0 && copied < totallen) {
		if (cs_r->data.conn_raw.rcvitem != 0)
			goto recv;

		spin_lock_bh(&trgt_sock->rcv_lock);
		if (unlikely(unlikely(cor_is_trgt_sock(trgt_sock, cs_r) == 0) |
				unlikely(trgt_sock->isreset != 0))) {
			spin_unlock_bh(&trgt_sock->rcv_lock);
			cor_conn_kref_put(trgt_sock, "stack");
			mutex_unlock(&cs_r->lock);
			return -EPIPE;
		}

		cor_databuf_pull_dbi(cs_r, trgt_sock);
		if (cs_r->data.conn_raw.rcvitem == 0)
			atomic_set(&cs_r->ready_to_read, 0);

		cor_bufsize_read_to_sock(trgt_sock);

		spin_unlock_bh(&trgt_sock->rcv_lock);

recv:
		rc = __cor_rawsocket_recvmsg(msg, totallen - copied, cs_r);

		if (rc > 0 || copied == 0)
			copied += rc;
		if (unlikely(rc == -EFAULT))
			copied = rc;

		BUG_ON(copied > 0 && ((__u32) copied > totallen));
	}

	mutex_unlock(&cs_r->lock);

	if (likely(copied > 0))
		cor_wake_sender(trgt_sock);

	cor_conn_kref_put(trgt_sock, "stack");

	return copied;
}

int cor_rawsocket_recvmsg(struct socket *sock, struct msghdr *msg,
		size_t total_len, int flags)
{
	struct cor_sock *cs = (struct cor_sock *) sock->sk;

	int blocking = (flags & MSG_DONTWAIT) == 0;

	int rc = 0;
	__u32 max = (1024 * 1024 * 1024);
	__u32 totallen;

	totallen = total_len;
	if (unlikely(totallen > max || total_len > max))
		totallen = max;

	if (unlikely((flags & MSG_PEEK) != 0))
		return -EINVAL;

	mutex_lock(&cs->lock);
	BUG_ON(cs->type != CS_TYPE_UNCONNECTED && cs->type != CS_TYPE_CONN_RAW);
	if (unlikely(cs->type == CS_TYPE_UNCONNECTED)) {
		mutex_unlock(&cs->lock);
		return -ENOTCONN;
	} else if (unlikely(cs->type != CS_TYPE_CONN_RAW)) {
		mutex_unlock(&cs->lock);
		return -EBADF;
	}
	mutex_unlock(&cs->lock);

recv:
	rc = _cor_rawsocket_recvmsg(msg, totallen, cs);

	BUG_ON(rc > 0 && unlikely((rc > total_len || rc > totallen)));

	if (rc == -EAGAIN && blocking) {
		if (wait_event_interruptible(*sk_sleep(&cs->sk),
				atomic_read(&cs->ready_to_read) != 0) == 0)
			goto recv;
		rc = -ERESTARTSYS;
	}

	return rc;
}

static unsigned int cor_rawsocket_poll(struct file *file, struct socket *sock,
		poll_table *wait)
{
	struct cor_sock *cs = (struct cor_sock *) sock->sk;

	sock_poll_wait(file, sock, wait);
	return _cor_rawsocket_poll(cs, U32_MAX, 1);
}


const struct proto_ops cor_raw_proto_ops = {
		.family = PF_COR,
		.owner = THIS_MODULE,
		.release = cor_rawsocket_release,
		.bind = cor_rawsocket_bind,
		.connect = cor_rawsocket_connect,
		.accept = cor_rawsocket_accept,
		.listen = cor_rawsocket_listen,
		.shutdown = cor_rawsocket_shutdown,
		.ioctl = cor_rawsocket_ioctl,
		.setsockopt = cor_rawsocket_setsockopt,
		.getsockopt = cor_rawsocket_getsockopt,
 #ifdef CONFIG_COMPAT
		.combat_ioctl = cor_rawsocket_ioctl,
		.compat_setsockopt = cor_rawsocket_setsockopt,
		.compat_getsockopt = cor_rawsocket_getsockopt,
#endif
		.sendmsg = cor_rawsocket_sendmsg,
		.recvmsg = cor_rawsocket_recvmsg,
		.poll = cor_rawsocket_poll,
		.socketpair = cor_socket_socketpair,
		.getname = cor_socket_getname,
		.mmap = cor_socket_mmap,

	/* sendpage, splice_read, are optional */
};

int cor_create_raw_sock(struct net *net, struct socket *sock, int protocol,
		int kern)
{
	int rc = _cor_createsock(net, sock, protocol, kern, 1);

	if (rc != 0)
		return rc;

	sock->ops = &cor_raw_proto_ops;

	return 0;
}

MODULE_LICENSE("GPL");
