/**
 *	Connection oriented routing
 *	Copyright (C) 2007-2021 Michael Blizek
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License
 *	as published by the Free Software Foundation; either version 2
 *	of the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 */

#include "cor.h"


static __u8 cor_flush_sock_managed_hdr(struct cor_conn *trgt_sock_lx,
		__u8 *do_wake_sender)
{
	__u32 hdr_rcvd = trgt_sock_lx->trgt.sock.rcvd;

	if (hdr_rcvd < CONN_MNGD_HEADERLEN) {
		__u32 len = CONN_MNGD_HEADERLEN - hdr_rcvd;
		__u32 pulled;
		__u16 hdr;

		pulled = cor_databuf_trypull(trgt_sock_lx,
				trgt_sock_lx->trgt.sock.rcv_hdr + hdr_rcvd,
				len);

		if (pulled > 0 && trgt_sock_lx->sourcetype == SOURCE_SOCK)
			cor_update_src_sock_sndspeed(trgt_sock_lx, pulled);

		trgt_sock_lx->trgt.sock.rcvd += pulled;
		if (pulled > 0)
			*do_wake_sender = 1;

		if (pulled < len)
			return RCV_BUF_STATE_INCOMPLETE;

		BUG_ON(CONN_MNGD_HEADERLEN != 2);

		hdr = cor_parse_u16(&trgt_sock_lx->trgt.sock.rcv_hdr[0]);

		trgt_sock_lx->trgt.sock.rcv_hdr_flags = hdr;
		trgt_sock_lx->trgt.sock.rcv_data_len = 0;

		if (likely((hdr & CONN_MNGD_HASDATA) != 0)) {
			trgt_sock_lx->trgt.sock.rcv_data_len =
					(hdr & CONN_MNGD_DATALEN) + 1;
		} else {
			if (unlikely((hdr & CONN_MNGD_KEEPALIVE_REQ) != 0))
				trgt_sock_lx->trgt.sock.rcv_data_len += 4;

			if (unlikely((hdr & CONN_MNGD_KEEPALIVE_RESP) != 0))
				trgt_sock_lx->trgt.sock.rcv_data_len += 4;
		}
	}

	return RCV_BUF_STATE_OK;
}

static __u8 cor_flush_sock_managed_data(struct cor_conn *trgt_sock_lx,
		__u8 *do_wake_sender)
{
	__u32 data_rcvd = trgt_sock_lx->trgt.sock.rcvd - CONN_MNGD_HEADERLEN;

	if (data_rcvd < trgt_sock_lx->trgt.sock.rcv_data_len) {
		__u32 len = trgt_sock_lx->trgt.sock.rcv_data_len - data_rcvd;
		__u32 pulled;

		BUG_ON(trgt_sock_lx->trgt.sock.rcv_buf == 0);

		pulled = cor_databuf_trypull(trgt_sock_lx,
				trgt_sock_lx->trgt.sock.rcv_buf + data_rcvd,
				len);

		if (pulled > 0 && trgt_sock_lx->sourcetype == SOURCE_SOCK)
			cor_update_src_sock_sndspeed(trgt_sock_lx, pulled);

		trgt_sock_lx->trgt.sock.rcvd += pulled;
		if (pulled > 0)
			*do_wake_sender = 1;

		if (pulled < len)
			return RCV_BUF_STATE_INCOMPLETE;
	}

	return RCV_BUF_STATE_OK;
}

static __u8 cor_flush_sock_managed_chksum(struct cor_conn *trgt_sock_lx,
		__u8 *do_wake_sender)
{
	__u32 checksum_rcvd = trgt_sock_lx->trgt.sock.rcvd -
			CONN_MNGD_HEADERLEN -
			trgt_sock_lx->trgt.sock.rcv_data_len;
	if (checksum_rcvd < CONN_MNGD_CHECKSUMLEN) {
		__u32 len = CONN_MNGD_CHECKSUMLEN - checksum_rcvd;
		__u32 pulled;
		char checksum_calc[CONN_MNGD_CHECKSUMLEN];

		pulled = cor_databuf_trypull(trgt_sock_lx,
				trgt_sock_lx->trgt.sock.rcv_chksum +
				checksum_rcvd, len);

		if (pulled > 0 && trgt_sock_lx->sourcetype == SOURCE_SOCK)
			cor_update_src_sock_sndspeed(trgt_sock_lx, pulled);

		trgt_sock_lx->trgt.sock.rcvd += pulled;
		if (pulled > 0)
			*do_wake_sender = 1;

		if (pulled < len)
			return RCV_BUF_STATE_INCOMPLETE;

		cor_mngdsocket_chksum(trgt_sock_lx->trgt.sock.rcv_hdr,
				CONN_MNGD_HEADERLEN,
				trgt_sock_lx->trgt.sock.rcv_buf,
				trgt_sock_lx->trgt.sock.rcv_data_len,
				checksum_calc, CONN_MNGD_CHECKSUMLEN);

		if (unlikely(memcmp(trgt_sock_lx->trgt.sock.rcv_chksum,
				checksum_calc, CONN_MNGD_CHECKSUMLEN) != 0)) {
#ifdef COR_DEBUG
			__u16 hdr = cor_parse_u16(
					trgt_sock_lx->trgt.sock.rcv_hdr);
			__u32 chk_calc = cor_parse_u32(checksum_calc);
			__u32 chk = cor_parse_u32(
					trgt_sock_lx->trgt.sock.rcv_chksum);
			printk(KERN_WARNING "cor: user socket chksum mismatch hdr %u rcvlen %u chksum_calc %x chksum_rcvd %x\n",
					hdr,
					trgt_sock_lx->trgt.sock.rcv_data_len,
					chk_calc, chk);
#endif
			return RCV_BUF_STATE_RESET;
		}
	}

	return RCV_BUF_STATE_OK;
}


void cor_flush_sock_managed(struct cor_conn *trgt_sock_lx, int from_recvmsg,
		__u8 *do_wake_sender)
{
	struct cor_sock *cs = trgt_sock_lx->trgt.sock.cs;
	__u8 rc;

	BUG_ON(cs == 0);

	if (trgt_sock_lx->trgt.sock.rcv_buf_state != RCV_BUF_STATE_INCOMPLETE)
		return;

	rc = cor_flush_sock_managed_hdr(trgt_sock_lx, do_wake_sender);
	if (rc != RCV_BUF_STATE_OK)
		goto out;

	rc = cor_flush_sock_managed_data(trgt_sock_lx, do_wake_sender);
	if (rc != RCV_BUF_STATE_OK)
		goto out;

	rc = cor_flush_sock_managed_chksum(trgt_sock_lx, do_wake_sender);
out:
	BUG_ON(trgt_sock_lx->trgt.sock.rcvd >
			CONN_MNGD_HEADERLEN +
			trgt_sock_lx->trgt.sock.rcv_data_len +
			CONN_MNGD_CHECKSUMLEN);

	cor_databuf_ackread(trgt_sock_lx);
	cor_bufsize_read_to_sock(trgt_sock_lx);

	trgt_sock_lx->trgt.sock.rcv_buf_state = rc;

	if (from_recvmsg) {
		if (rc == RCV_BUF_STATE_INCOMPLETE) {
			atomic_set(&cs->ready_to_read, 0);
		}
	} else {
		/* This does not work because on the next call
		 * rcv_buf_state == RCV_BUF_STATE_OK and we do not get this far
		 */
		/*if (trgt_sock_lx->is_highlatency != 0 &&
				trgt_sock_lx->flush == 0 &&
				trgt_sock_lx->data_buf.read_remaining < 4096 &&
				trgt_sock_lx->sourcetype == SOURCE_SOCK &&
				cor_sock_sndbufavailable(trgt_sock_lx) != 0)
			return;

		if (trgt_sock_lx->is_highlatency != 0 &&
				trgt_sock_lx->flush == 0 &&
				trgt_sock_lx->data_buf.read_remaining < 4096 &&
				trgt_sock_lx->sourcetype == SOURCE_IN &&
				cor_srcin_buflimit_reached(trgt_sock_lx) == 0)
			return;*/

		if (likely(rc == RCV_BUF_STATE_OK &&
				(trgt_sock_lx->trgt.sock.rcv_hdr_flags &
				CONN_MNGD_HASDATA) != 0 &&
				trgt_sock_lx->trgt.sock.rcv_data_len > 0)) {
			cor_sk_data_ready(cs);
		} else {
			cor_mngdsocket_readfromconn_fromatomic(cs);
		}
	}
}

void cor_flush_sock(struct cor_conn *trgt_sock_lx)
{
	struct cor_sock *cs = trgt_sock_lx->trgt.sock.cs;

	if (unlikely(cs == 0))
		return;

	if (unlikely(trgt_sock_lx->data_buf.read_remaining == 0))
		return;

	if (trgt_sock_lx->trgt.sock.waiting_for_userspace == 0) {
		trgt_sock_lx->trgt.sock.waiting_for_userspace = 1;
		trgt_sock_lx->trgt.sock.waiting_for_userspace_since = jiffies;
	}

	if (trgt_sock_lx->trgt.sock.socktype == SOCKTYPE_RAW) {
		cor_sk_data_ready(cs);
	} else if (trgt_sock_lx->trgt.sock.socktype == SOCKTYPE_MANAGED) {
		__u8 do_wake_sender = 0;

		cor_flush_sock_managed(trgt_sock_lx, 0, &do_wake_sender);
	} else {
		BUG();
	}
}

MODULE_LICENSE("GPL");
