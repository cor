/**
 *	Connection oriented routing
 *	Copyright (C) 2007-2021 Michael Blizek
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License
 *	as published by the Free Software Foundation; either version 2
 *	of the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 */

#include "cor.h"

static atomic_t cor_packets_in_workqueue = ATOMIC_INIT(0);

static DEFINE_MUTEX(cor_announce_rcv_lock);
static LIST_HEAD(cor_nb_dd_list); /* protected by cor_announce_rcv_lock */
static __u32 cor_num_nb_dd;
static struct kmem_cache *cor_nb_dd_slab;

static int cor_parse_announce_version(struct cor_neighbor_discdata *nb_dd,
		char *cmddata, __u16 len)
{
	__u16 version;
	__u16 minversion;

	if (unlikely(len < 4))
		return 1;

	version = cor_parse_u16(cmddata);
	cmddata += 2;
	len -= 2;
	minversion = cor_parse_u16(cmddata);
	cmddata += 2;
	len -= 2;

	if (nb_dd->rcvd_version != 0 && (
			nb_dd->version != version ||
			nb_dd->minversion != minversion))
		return 1;

	if (minversion != 0)
		return 1;

	nb_dd->version = version;
	nb_dd->minversion = minversion;
	nb_dd->rcvd_version = 1;

	return 0;
}

static int cor_parse_announce_addr(struct cor_neighbor_discdata *nb_dd,
		char *cmddata, __u16 len)
{
	__be64 addr;

	if (len != 8)
		return 1;

	addr = cor_parse_be64(cmddata);
	cmddata += 8;
	len -= 8;

	if (unlikely(nb_dd->rcvd_addr != 0 && nb_dd->rcvd_addr != addr))
		return 1;
	if (unlikely(be64_to_cpu(addr) == 0))
		return 1;


	nb_dd->addr = addr;
	nb_dd->has_addr = 1;
	nb_dd->rcvd_addr = 1;

	return 0;
}

static int cor_parse_announce_noaddr(struct cor_neighbor_discdata *nb_dd,
		char *cmddata, __u16 len)
{
	if (unlikely(nb_dd->rcvd_addr != 0 && nb_dd->has_addr != 0))
		return 1;

	nb_dd->addr = 0;
	nb_dd->has_addr = 0;
	nb_dd->rcvd_addr = 1;

	return 0;
}

static int cor_parse_announce_cmd(struct cor_neighbor_discdata *nb_dd,
		__u16 cmd, char *cmddata, __u16 len)
{
	if (cmd == ANNCMD_VERSION)
		return cor_parse_announce_version(nb_dd, cmddata, len);
	else if (cmd == ANNCMD_ADDR)
		return cor_parse_announce_addr(nb_dd, cmddata, len);
	else if (cmd == ANNCMD_NOADDR)
		return cor_parse_announce_noaddr(nb_dd, cmddata, len);
	else
		return 1;
}

static int cor_parse_announce_cmds(struct cor_neighbor_discdata *nb_dd,
		char *msg, __u32 len)
{
	__u32 zeros = 0;

	while (zeros < len) {
		if (msg[len - zeros - 1] != 0)
			break;
		zeros++;
	}

	while (len >= 4 && len > zeros) {
		__u16 cmd;
		__u16 cmdlen;

		cmd = cor_parse_u16(msg);
		msg += 2;
		len -= 2;
		cmdlen = cor_parse_u16(msg);
		msg += 2;
		len -= 2;

		if (cmdlen > len)
			return 1;

		if (cor_parse_announce_cmd(nb_dd, cmd, msg, cmdlen) != 0)
			return 1;

		msg += cmdlen;
		len -= cmdlen;
	}

	if (len != 0 && len < zeros)
		return 1;

	return 0;
}

static void cor_neighbor_discdata_free(struct cor_neighbor_discdata *nb_dd)
{
	list_del(&nb_dd->lh);

	BUG_ON(nb_dd->dev == 0);
	dev_put(nb_dd->dev);

	nb_dd->addr = 0;

	kmem_cache_free(cor_nb_dd_slab, nb_dd);

	BUG_ON(cor_num_nb_dd == 0);
	cor_num_nb_dd--;
}

static struct cor_neighbor_discdata *cor_findoralloc_neighbor_discdata(
		struct net_device *dev, char *source_hw, __be32 sessionid)
{
	unsigned long jiffies_tmp = jiffies;
	struct list_head *currlh;

	__u32 neighs;
	struct cor_neighbor_discdata *nb_dd;

	currlh = cor_nb_dd_list.next;
	while (currlh != &cor_nb_dd_list) {
		struct cor_neighbor_discdata *curr = container_of(currlh,
				struct cor_neighbor_discdata, lh);

		currlh = currlh->next;

		if (time_after(jiffies_tmp, curr->jiffies_created +
				HZ * NEIGHBOR_DISCOVERY_TIMEOUT_SEC)) {
			cor_neighbor_discdata_free(curr);
			continue;
		}

		if (curr->sessionid == sessionid && curr->dev == dev &&
				memcmp(curr->mac, source_hw, MAX_ADDR_LEN) == 0)
			return curr;
	}

	neighs = atomic_read(&cor_num_neighs);
	if (neighs + cor_num_nb_dd < neighs ||
			neighs + cor_num_nb_dd >= MAX_NEIGHBORS)
		return 0;
	cor_num_nb_dd++;

	nb_dd = kmem_cache_alloc(cor_nb_dd_slab, GFP_KERNEL);
	if (unlikely(nb_dd == 0))
		return 0;

	memset(nb_dd, 0, sizeof(struct cor_neighbor_discdata));

	nb_dd->sessionid = sessionid;

	dev_hold(dev);
	nb_dd->dev = dev;

	memcpy(nb_dd->mac, source_hw, MAX_ADDR_LEN);

	list_add_tail(&nb_dd->lh, &cor_nb_dd_list);
	nb_dd->jiffies_created = jiffies_tmp;

	if (cor_is_clientmode())
		cor_announce_send_start(dev, source_hw, ANNOUNCE_TYPE_UNICAST);

	return nb_dd;
}

static void cor_parse_announce(struct net_device *dev, char *source_hw,
		char *msg, __u32 len)
{
	__be32 sessionid;
	struct cor_neighbor_discdata *nb_dd;


	if (unlikely(len < 4))
		return;

	sessionid = cor_parse_be32(msg);
	msg += 4;
	len -= 4;

	nb_dd = cor_findoralloc_neighbor_discdata(dev, source_hw, sessionid);
	if (unlikely(nb_dd == 0))
		return;

	if (cor_parse_announce_cmds(nb_dd, msg, len) == 0 &&
			nb_dd->rcvd_version != 0 && nb_dd->rcvd_addr != 0)
		cor_add_neighbor(nb_dd);

	cor_neighbor_discdata_free(nb_dd);
}

static void _cor_rcv_announce(struct work_struct *work)
{
	struct cor_skb_procstate *ps = container_of(work,
			struct cor_skb_procstate, funcstate.announce1.work);
	struct sk_buff *skb = cor_skb_from_pstate(ps);

	char source_hw[MAX_ADDR_LEN];

	struct cor_neighbor *nb;

	char *msg;
	__u16 len;

	if (cor_is_device_configurated(skb->dev) == 0)
		goto discard;

	memset(source_hw, 0, MAX_ADDR_LEN);
	if (skb->dev->header_ops != 0 &&
			skb->dev->header_ops->parse != 0)
		skb->dev->header_ops->parse(skb, source_hw);

	nb = _cor_get_neigh_by_mac(skb->dev, source_hw);
	if (nb != 0) {
		cor_nb_kref_put(nb, "stack");
		nb = 0;
		goto discard;
	}

	if (unlikely(skb->len > 65535 || skb->len < 0))
		goto discard;
	len = (__u16) skb->len;

	msg = cor_pull_skb(skb, len);
	if (msg == 0)
		goto discard;

	mutex_lock(&cor_announce_rcv_lock);
	cor_parse_announce(skb->dev, source_hw, msg, len);
	mutex_unlock(&cor_announce_rcv_lock);

discard:
	kfree_skb(skb);

	atomic_dec(&cor_packets_in_workqueue);
}

int cor_rcv_announce(struct sk_buff *skb)
{
	struct cor_skb_procstate *ps = cor_skb_pstate(skb);
	long queuelen;

	queuelen = atomic_inc_return(&cor_packets_in_workqueue);

	BUG_ON(queuelen <= 0);

	if (queuelen > MAX_PACKETS_IN_ANNOUNCE_RCVQUEUE) {
		atomic_dec(&cor_packets_in_workqueue);
		kfree_skb(skb);
		return NET_RX_SUCCESS;
	}

	INIT_WORK(&ps->funcstate.announce1.work, _cor_rcv_announce);
	schedule_work(&ps->funcstate.announce1.work);
	return NET_RX_SUCCESS;
}


int __init cor_neigh_ann_rcv_init(void)
{
	cor_nb_dd_slab = kmem_cache_create("cor_neighbor_discoverydata",
			sizeof(struct cor_neighbor_discdata), 8, 0, 0);
	if (unlikely(cor_nb_dd_slab == 0))
		return -ENOMEM;

	return 0;
}

void __exit cor_neigh_ann_rcv_exit2(void)
{
	kmem_cache_destroy(cor_nb_dd_slab);
	cor_nb_dd_slab = 0;
}

MODULE_LICENSE("GPL");
