/**
 *	Connection oriented routing
 *	Copyright (C) 2007-2021 Michael Blizek
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License
 *	as published by the Free Software Foundation; either version 2
 *	of the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 */

#include <linux/version.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/in.h>


#include "cor.h"

static struct kmem_cache *cor_rcvooo_buf_slab;

void cor_reset_ooo_queue(struct cor_conn *src_in_lx)
{
	BUG_ON(src_in_lx->sourcetype != SOURCE_IN);

	while (list_empty(&src_in_lx->src.in.reorder_queue) == 0) {
		struct cor_rcvooo *r = container_of(
				src_in_lx->src.in.reorder_queue.next,
				struct cor_rcvooo, lh);

		list_del(&r->lh);

		if (r->type == RCVOOO_BUF) {
			struct cor_rcvooo_buf *rb = container_of(r,
					struct cor_rcvooo_buf, r);
			src_in_lx->src.in.reorder_memused -= (rb->len +
					sizeof(struct cor_rcvooo_buf));
			kfree(rb->data);
			kmem_cache_free(cor_rcvooo_buf_slab, rb);
		} else if (r->type == RCVOOO_SKB) {
			struct cor_skb_procstate *ps = container_of(r,
					struct cor_skb_procstate,
					funcstate.rcv_ooo.r);
			struct sk_buff *skb = cor_skb_from_pstate(ps);

			src_in_lx->src.in.reorder_memused -=
					ps->funcstate.rcv_ooo.skb_memused;
			kfree_skb(skb);
		} else {
			BUG();
		}
	}

	src_in_lx->src.in.small_ooo_packets = 0;
	BUG_ON(src_in_lx->src.in.reorder_memused != 0);

	cor_account_bufspace(src_in_lx);
}

static int cor_drain_ooo_queue_buf(struct cor_conn *src_in_l,
		struct cor_rcvooo *r, __u8 flush)
{
	struct cor_rcvooo_buf *rb = container_of(r, struct cor_rcvooo_buf, r);

	__u32 data_offset = 0;
	__u32 rc;

	if (unlikely(cor_seqno_after(src_in_l->src.in.next_seqno,
			r->seqno))) {
		__u32 overlap = r->seqno - src_in_l->src.in.next_seqno;

		if (overlap >= rb->len)
			goto free;

		src_in_l->src.in.reorder_memused -= overlap;
		rb->len -= overlap;
		data_offset += overlap;
		r->seqno += overlap;
	}

	BUG_ON(cor_seqno_eq(src_in_l->src.in.next_seqno, r->seqno) == 0);
	rc = cor_receive_buf(src_in_l, rb->data + data_offset, rb->len,
			r->windowused, flush);

	BUG_ON(rc > rb->len);
	src_in_l->src.in.next_seqno += rc;

	if (unlikely(rc != rb->len)) {
		src_in_l->src.in.reorder_memused -= rc;
		rb->len -= rc;
		r->seqno += rc;

		data_offset += rc;
		memmove(rb->data, rb->data + data_offset, rb->len);

		if (rb->len + data_offset > SMALL_OOO_PACKET_MAXSIZE &&
				rb->len <= SMALL_OOO_PACKET_MAXSIZE) {
			src_in_l->src.in.small_ooo_packets++;
			BUG_ON(src_in_l->src.in.small_ooo_packets == 0);
		}

		return 1;
	} else {
free:
		src_in_l->src.in.reorder_memused -= (rb->len +
				sizeof(struct cor_rcvooo_buf));
		list_del(&r->lh);
		kfree(rb->data);
		if (rb->len <= SMALL_OOO_PACKET_MAXSIZE) {
			BUG_ON(src_in_l->src.in.small_ooo_packets == 0);
			src_in_l->src.in.small_ooo_packets--;
		}
		kmem_cache_free(cor_rcvooo_buf_slab, rb);

		return 0;
	}
}

static int cor_drain_ooo_queue_skb(struct cor_conn *src_in_l,
		struct cor_rcvooo *r, __u8 flush)
{
	struct cor_skb_procstate *ps = container_of(r, struct cor_skb_procstate,
			funcstate.rcv_ooo.r);
	struct sk_buff *skb = cor_skb_from_pstate(ps);

	__u8 rcv_as_buf = 0;

	if (unlikely(cor_seqno_after(src_in_l->src.in.next_seqno,
			r->seqno))) {
		__u32 overlap = r->seqno - src_in_l->src.in.next_seqno;

		if (overlap >= skb->len) {
			src_in_l->src.in.reorder_memused -=
					ps->funcstate.rcv_ooo.skb_memused;
			list_del(&r->lh);
			kfree_skb(skb);
			return 0;
		}

		skb->data += overlap;
		skb->len -= overlap;
		r->seqno += overlap;

		rcv_as_buf = 1;
	}

	BUG_ON(cor_seqno_eq(src_in_l->src.in.next_seqno, r->seqno) == 0);
	BUG_ON(skb->len <= 0);

	if (unlikely(rcv_as_buf != 0)) {
		__u32 rc = cor_receive_buf(src_in_l, skb->data, skb->len,
				r->windowused, flush);

		BUG_ON(rc > skb->len);

		src_in_l->src.in.next_seqno += rc;

		if (unlikely(rc != skb->len)) {
			skb->data += rc;
			skb->len -= rc;
			r->seqno += rc;

			return 1;
		} else {
			src_in_l->src.in.reorder_memused -=
					ps->funcstate.rcv_ooo.skb_memused;
			list_del(&r->lh);
			kfree_skb(skb);

			return 0;
		}
	} else {
		__u32 len = skb->len;
		__u32 rc;
		__u32 memused = ps->funcstate.rcv_ooo.skb_memused;

		list_del(&r->lh);
		rc = cor_receive_skb(src_in_l, skb, 0, flush);

		BUG_ON(rc > len);

		src_in_l->src.in.next_seqno += rc;

		if (unlikely(rc != len)) {
			BUG_ON(rc > skb->len);
			skb->data += rc;
			skb->len -= rc;
			r->seqno += rc;
			list_add(&r->lh, &src_in_l->src.in.reorder_queue);
			return 1;
		}

		src_in_l->src.in.reorder_memused -= memused;

		return 0;
	}
}

void cor_drain_ooo_queue(struct cor_conn *src_in_l)
{
	int drained = 0;

	BUG_ON(src_in_l->sourcetype != SOURCE_IN);

	while (list_empty(&src_in_l->src.in.reorder_queue) == 0) {
		struct cor_rcvooo *r = container_of(
				src_in_l->src.in.reorder_queue.next,
				struct cor_rcvooo, lh);
		__u8 flush = r->flush;
		int rc;

		if (cor_seqno_before(src_in_l->src.in.next_seqno, r->seqno))
			break;

		/* do not flush if there are more ooo packets in queue */
		if (src_in_l->src.in.reorder_queue.prev !=
				src_in_l->src.in.reorder_queue.next)
			flush = 0;

		if (r->type == RCVOOO_BUF)
			rc = cor_drain_ooo_queue_buf(src_in_l, r, flush);
		else if (r->type == RCVOOO_SKB)
			rc = cor_drain_ooo_queue_skb(src_in_l, r, flush);
		else
			BUG();

		if (unlikely(rc != 0))
			break;

		drained = 1;
	}

	BUG_ON(list_empty(&src_in_l->src.in.reorder_queue) != 0 &&
			src_in_l->src.in.reorder_memused != 0);
	BUG_ON(list_empty(&src_in_l->src.in.reorder_queue) == 0 &&
			src_in_l->src.in.reorder_memused == 0);

	if (drained)
		cor_account_bufspace(src_in_l);
}

static __u32 cor_rcvooo_len(struct cor_rcvooo *r)
{
	if (r->type == RCVOOO_BUF) {
		struct cor_rcvooo_buf *rb = container_of(r,
				struct cor_rcvooo_buf, r);
		return rb->len;
	} else if (r->type == RCVOOO_SKB) {
		struct sk_buff *skb = cor_skb_from_pstate(container_of(r,
				struct cor_skb_procstate, funcstate.rcv_ooo.r));
		return skb->len;
	} else {
		BUG();
	}
}

static struct cor_rcvooo_buf *_cor_conn_rcv_ooo_buf_checkmerge(
		struct cor_conn *src_in_l, struct list_head *lh_rcvooo)
{
	struct cor_rcvooo *r;
	struct cor_rcvooo_buf *rb;

	if (lh_rcvooo == &src_in_l->src.in.reorder_queue)
		return 0;

	r = container_of(lh_rcvooo, struct cor_rcvooo, lh);
	if (r->type != RCVOOO_BUF)
		return 0;

	rb = container_of(r, struct cor_rcvooo_buf, r);
	if (rb->len > 256)
		return 0;

	return rb;
}

static int _cor_conn_rcv_ooo_accountmem(struct cor_conn *src_in_l,
		__u32 new_bytes)
{
	if (new_bytes == 0)
		return 0;

	if (unlikely(src_in_l->src.in.reorder_memused + new_bytes <
			src_in_l->src.in.reorder_memused))
		return 1;

	src_in_l->src.in.reorder_memused += new_bytes;

	if (unlikely(cor_account_bufspace(src_in_l))) {
		src_in_l->src.in.reorder_memused -= new_bytes;
		cor_account_bufspace(src_in_l);
		return 1;
	}

	return 0;
}

static void _cor_conn_rcv_ooo_merge(struct cor_conn *src_in_l, char *data,
		__u32 len, __u32 seqno, __u8 windowused, __u8 flush,
		struct cor_rcvooo_buf *merge_prev,
		struct cor_rcvooo_buf *merge_next)
{
	char *tmpbuf;
	__u32 tmpbuf_len = 0;
	__u32 tmpbuf_offset = 0;

	struct cor_rcvooo_buf *rb;

	if (merge_prev != 0)
		tmpbuf_len += merge_prev->len;
	tmpbuf_len += len;
	if (merge_next != 0)
		tmpbuf_len += merge_next->len;

	tmpbuf = kmalloc(tmpbuf_len, GFP_ATOMIC);
	if (unlikely(tmpbuf == 0))
		return;
	if (merge_prev != 0 && merge_next != 0 && len <
			sizeof(struct cor_rcvooo_buf)) {
		src_in_l->src.in.reorder_memused += len -
				sizeof(struct cor_rcvooo_buf);
	} else {
		__u32 new_bytes = len;

		if (merge_prev != 0 && merge_next != 0)
			new_bytes -= sizeof(struct cor_rcvooo_buf);

		if (unlikely(_cor_conn_rcv_ooo_accountmem(src_in_l,
				new_bytes))) {
			kfree(tmpbuf);
			return;
		}
	}


	if (merge_prev != 0) {
		memcpy(tmpbuf + tmpbuf_offset, merge_prev->data,
				merge_prev->len);
		tmpbuf_offset += merge_prev->len;
		windowused = merge_prev->r.windowused;
	}
	memcpy(tmpbuf + tmpbuf_offset, data, len);
	tmpbuf_offset += len;
	if (merge_next != 0) {
		memcpy(tmpbuf + tmpbuf_offset, merge_next->data,
				merge_next->len);
		tmpbuf_offset += merge_next->len;
	}

	BUG_ON(tmpbuf_offset != tmpbuf_len);


	if (merge_prev != 0) {
		kfree(merge_prev->data);
		merge_prev->data = 0;
		if (merge_prev->len <= SMALL_OOO_PACKET_MAXSIZE) {
			BUG_ON(src_in_l->src.in.small_ooo_packets == 0);
			src_in_l->src.in.small_ooo_packets--;
		}
	}

	if (merge_next != 0) {
		kfree(merge_next->data);
		merge_next->data = 0;
		if (merge_next->len <= SMALL_OOO_PACKET_MAXSIZE) {
			BUG_ON(src_in_l->src.in.small_ooo_packets == 0);
			src_in_l->src.in.small_ooo_packets--;
		}

		flush = merge_next->r.flush;

		if (merge_prev != 0) {
			list_del(&merge_next->r.lh);
			kmem_cache_free(cor_rcvooo_buf_slab, merge_next);
			merge_next = 0;
		}
	}

	if (merge_prev != 0) {
		rb = merge_prev;
	} else {
		BUG_ON(merge_next == 0);
		rb = merge_next;
		rb->r.seqno = seqno;
	}

	rb->data = tmpbuf;
	rb->len = tmpbuf_len;
	rb->r.windowused = windowused;
	rb->r.flush = flush;

	if (tmpbuf_len <= SMALL_OOO_PACKET_MAXSIZE) {
		src_in_l->src.in.small_ooo_packets++;
		BUG_ON(src_in_l->src.in.small_ooo_packets == 0);
	}

	cor_send_ack_conn_ifneeded(src_in_l, seqno, len);
}

static void _cor_conn_rcv_ooo_nomerge(struct cor_conn *src_in_l, char *data,
		__u32 len, __u32 seqno, __u8 windowused, __u8 flush,
		struct list_head *next_rcvooo)
{
	struct cor_rcvooo_buf *rb;

	/* avoid oom if a neighbor sends very small packets */
	if (len <= SMALL_OOO_PACKET_MAXSIZE &&
			src_in_l->src.in.small_ooo_packets >=
			MAX_SMALL_OOO_PACKETS_PER_CONN)
		return;

	if (unlikely(_cor_conn_rcv_ooo_accountmem(src_in_l,
			len + sizeof(struct cor_rcvooo_buf))))
		return;

	rb = kmem_cache_alloc(cor_rcvooo_buf_slab, GFP_ATOMIC);
	if (unlikely(rb == 0)) {
		src_in_l->src.in.reorder_memused -=
				(len + sizeof(struct cor_rcvooo_buf));
		cor_account_bufspace(src_in_l);
		return;
	}
	memset(rb, 0, sizeof(struct cor_rcvooo_buf));

	rb->data = kmalloc(len, GFP_ATOMIC);
	if (unlikely(rb->data == 0)) {
		kmem_cache_free(cor_rcvooo_buf_slab, rb);

		src_in_l->src.in.reorder_memused -=
				(len + sizeof(struct cor_rcvooo_buf));
		cor_account_bufspace(src_in_l);
		return;
	}

	memcpy(rb->data, data, len);
	rb->len = len;
	rb->r.type = RCVOOO_BUF;
	rb->r.seqno = seqno;
	rb->r.windowused = windowused;
	if (flush)
		rb->r.flush = 1;
	else
		rb->r.flush = 0;
	list_add_tail(&rb->r.lh, next_rcvooo);

	if (len <= SMALL_OOO_PACKET_MAXSIZE) {
		src_in_l->src.in.small_ooo_packets++;
		BUG_ON(src_in_l->src.in.small_ooo_packets == 0);
	}

	cor_send_ack_conn_ifneeded(src_in_l, seqno, len);
}

static void _cor_conn_rcv_ooo_buf(struct cor_conn *src_in_l, char *data,
		__u32 len, __u32 seqno, __u8 windowused, __u8 flush,
		struct list_head *next_rcvooo)
{
	struct cor_rcvooo_buf *merge_prev;
	struct cor_rcvooo_buf *merge_next;

	if (len > 128)
		goto nomerge;

	merge_prev = _cor_conn_rcv_ooo_buf_checkmerge(src_in_l,
			next_rcvooo->prev);
	if (merge_prev != 0) {
		__u32 next_seqno = merge_prev->r.seqno + merge_prev->len;

		BUG_ON(cor_seqno_after(next_seqno, seqno));
		if (cor_seqno_eq(next_seqno, seqno) == 0)
			merge_prev = 0;
	}

	merge_next = _cor_conn_rcv_ooo_buf_checkmerge(src_in_l, next_rcvooo);
	if (merge_next != 0) {
		__u32 next_seqno = seqno + len;

		BUG_ON(cor_seqno_after(next_seqno, merge_next->r.seqno));
		if (cor_seqno_eq(next_seqno, merge_next->r.seqno) == 0)
			merge_next = 0;
	}

	if (merge_prev == 0 && merge_next == 0) {
nomerge:
		_cor_conn_rcv_ooo_nomerge(src_in_l, data, len, seqno,
				windowused, flush, next_rcvooo);
	} else {
		_cor_conn_rcv_ooo_merge(src_in_l, data, len, seqno,
				windowused, flush, merge_prev, merge_next);
	}
}

static void _cor_conn_rcv_ooo_skb(struct cor_conn *src_in_l,
		struct sk_buff *skb, __u32 seqno, __u8 windowused, __u8 flush,
		struct list_head *next_rcvooo)
{
	struct cor_rcvooo *newr;
	struct cor_skb_procstate *ps = cor_skb_pstate(skb);

	memset(&ps->funcstate, 0, sizeof(ps->funcstate));
	ps->funcstate.rcv_ooo.skb_memused = sizeof(struct sk_buff) +
			skb->len;

	if (unlikely(_cor_conn_rcv_ooo_accountmem(src_in_l,
			ps->funcstate.rcv_ooo.skb_memused))) {
		kfree_skb(skb);
		return;
	}

	newr = &ps->funcstate.rcv_ooo.r;
	newr->type = RCVOOO_SKB;
	newr->seqno = seqno;
	newr->windowused = windowused;
	newr->flush = flush;
	list_add_tail(&newr->lh, next_rcvooo);

	cor_send_ack_conn_ifneeded(src_in_l, seqno, skb->len);
}

static void __cor_conn_rcv_ooo(struct cor_conn *src_in_l, struct sk_buff *skb,
		char *data, __u32 len, __u32 seqno, __u8 windowused, __u8 flush,
		struct list_head *prev_rcvooo_lh)
{
	struct list_head *reorder_queue = &src_in_l->src.in.reorder_queue;
	struct list_head *next_rcvooo_lh = prev_rcvooo_lh->next;

	if (prev_rcvooo_lh != reorder_queue) {
		struct cor_rcvooo *prev_rcvooo = container_of(prev_rcvooo_lh,
				struct cor_rcvooo, lh);
		__u32 currlen = cor_rcvooo_len(prev_rcvooo);

		if (cor_seqno_after(prev_rcvooo->seqno + currlen, seqno)) {
			__u32 overlap = prev_rcvooo->seqno + currlen - seqno;

			if (unlikely(len <= overlap))
				goto drop;

			data += overlap;
			len -= overlap;
			seqno += overlap;
		}
	}

	if (next_rcvooo_lh != reorder_queue) {
		struct cor_rcvooo *next_rcvooo = container_of(next_rcvooo_lh,
				struct cor_rcvooo, lh);

		if (unlikely(cor_seqno_before_eq(next_rcvooo->seqno, seqno)))
			goto drop;

		if (unlikely(cor_seqno_before(next_rcvooo->seqno, seqno + len)))
			len = next_rcvooo->seqno - seqno;
	}

	if (unlikely(len == 0)) {
drop:
		if (skb != 0)
			kfree_skb(skb);
		return;
	}

	if (skb == 0 || len < 1024 ||
			skb->data != ((unsigned char *) data) ||
			skb->len != len) {
		_cor_conn_rcv_ooo_buf(src_in_l, data, len, seqno, windowused,
				flush, next_rcvooo_lh);

		if (skb != 0)
			kfree_skb(skb);
	} else {
		skb->data = data;
		skb->len = len;

		_cor_conn_rcv_ooo_skb(src_in_l, skb, seqno, windowused, flush,
				next_rcvooo_lh);
	}
}

static void _cor_conn_rcv_ooo(struct cor_conn *src_in_l, struct sk_buff *skb,
		char *data, __u32 len, __u32 seqno, __u8 windowused, __u8 flush)
{
	struct list_head *reorder_queue = &src_in_l->src.in.reorder_queue;
	struct list_head *currlh = reorder_queue->prev;

	BUG_ON(skb != 0 && skb->data != ((unsigned char *)data));
	BUG_ON(skb != 0 && skb->len != len);

	while (currlh != reorder_queue) {
		struct cor_rcvooo *currr = container_of(currlh,
				struct cor_rcvooo, lh);

		if (cor_seqno_before_eq(currr->seqno, seqno))
			break;

		currlh = currlh->prev;
	}

	__cor_conn_rcv_ooo(src_in_l, skb, data, len, seqno, windowused, flush,
			currlh);
}

static void _cor_conn_rcv(struct cor_neighbor *nb, struct cor_conn *src_in,
		__u32 conn_id, struct sk_buff *skb, char *data, __u32 len,
		__u32 seqno, __u8 windowused, __u8 flush)
{
	BUG_ON(nb == 0);

	spin_lock_bh(&src_in->rcv_lock);

	if (cor_is_conn_in(src_in, nb, conn_id) == 0)
		goto drop;

	if (unlikely(cor_seqno_before(seqno + len, src_in->src.in.next_seqno)))
		goto drop_ack;
	if (unlikely(unlikely(cor_seqno_after(seqno + len,
			src_in->src.in.window_seqnolimit)) &&
			cor_seqno_after(seqno + len,
			src_in->src.in.window_seqnolimit_remote)))
		goto drop;

	if (cor_seqno_after(seqno, src_in->src.in.next_seqno)) {
		_cor_conn_rcv_ooo(src_in, skb, data, len, seqno, windowused,
				flush);
	} else {
		__u32 rcvlen;

		if (cor_seqno_after(src_in->src.in.next_seqno, seqno)) {
			__u32 overlap = src_in->src.in.next_seqno - seqno;

			BUG_ON(overlap > len);

			data += overlap;
			len -= overlap;
			seqno += overlap;

			rcvlen = cor_receive_buf(src_in, data, len, windowused,
					flush);
			if (skb != 0)
				kfree_skb(skb);
		} else if (skb != 0) {
			__u32 skblen = skb->len;

			rcvlen = cor_receive_skb(src_in, skb, windowused,
					flush);
			if (unlikely(rcvlen < skblen))
				kfree_skb(skb);
		} else {
			rcvlen = cor_receive_buf(src_in, data, len, windowused,
					flush);
		}

		if (likely(rcvlen > 0)) {
			src_in->src.in.next_seqno += rcvlen;

			cor_drain_ooo_queue(src_in);
			src_in->src.in.inorder_ack_needed = 1;
			cor_flush_buf(src_in);
			cor_send_ack_conn_ifneeded(src_in, 0, 0);
		}
	}

	if (0) {
drop_ack:
		cor_send_ack_conn_ifneeded(src_in, 0, 0);
drop:
		if (skb != 0) {
			kfree_skb(skb);
		}
	}
	spin_unlock_bh(&src_in->rcv_lock);
}

void cor_conn_rcv(struct cor_neighbor *nb, struct sk_buff *skb, char *data,
		__u32 len, __u32 conn_id, __u32 seqno, __u8 windowused,
		__u8 flush)
{
	struct cor_conn *src_in;

	BUG_ON(nb == 0);

	if (skb != 0) {
		BUG_ON(data != 0);
		BUG_ON(len != 0);

		data = skb->data;
		len = skb->len;
	}

	src_in = cor_get_conn(nb, conn_id);

	if (unlikely(src_in == 0)) {
		/* printk(KERN_DEBUG "unknown conn_id when receiving: %d\n",
				conn_id); */

		if (skb != 0)
			kfree_skb(skb);
		cor_send_reset_conn(nb, cor_get_connid_reverse(conn_id), 0);
		return;
	}

	/* for testing */
	/* len = 1;
	if (skb != 0)
		skb->len = 1; */

	_cor_conn_rcv(nb, src_in, conn_id, skb, data, len, seqno, windowused,
			flush);
	cor_conn_kref_put(src_in, "stack");
}

int __init cor_rcv_init(void)
{
	BUG_ON(sizeof(struct cor_skb_procstate) > 48);

	cor_rcvooo_buf_slab = kmem_cache_create("cor_rcvooo_buf",
			sizeof(struct cor_rcvooo_buf), 8, 0, 0);
	if (unlikely(cor_rcvooo_buf_slab == 0))
		return -ENOMEM;

	return 0;
}

void __exit cor_rcv_exit2(void)
{
	kmem_cache_destroy(cor_rcvooo_buf_slab);
	cor_rcvooo_buf_slab = 0;
}

MODULE_LICENSE("GPL");
