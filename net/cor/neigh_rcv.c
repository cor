/**
 *	Connection oriented routing
 *	Copyright (C) 2007-2021 Michael Blizek
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License
 *	as published by the Free Software Foundation; either version 2
 *	of the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 */

#include <asm/byteorder.h>

#include "cor.h"

static void cor_parse_connect(struct cor_neighbor *nb, struct sk_buff *skb)
{
	struct cor_conn_bidir *cnb;
	struct cor_conn *src_in;
	struct cor_conn *trgt_out;
	__u32 rcv_conn_id = cor_pull_u32(skb);
	__u32 snd_conn_id = cor_get_connid_reverse(rcv_conn_id);
	__u32 rcv_seqno = cor_pull_u32(skb);
	__u32 snd_seqno = cor_pull_u32(skb);
	__u8 window = cor_pull_u8(skb);
	__u16 priority_raw = cor_pull_u16(skb);
	__u8 priority_seqno = (priority_raw >> 12);
	__u16 priority = (priority_raw & 4095);
	__u8 is_highlatency = cor_pull_u8(skb);
	struct cor_control_msg_out *cm = 0;

	/* do not send reset - doing so would only corrupt things further */
	if (unlikely((rcv_conn_id & (1 << 31)) == 0))
		return;

	BUG_ON((snd_conn_id & (1 << 31)) != 0);

	if (unlikely(snd_conn_id == 0))
		return;

	if (cor_is_clientmode())
		goto err;

	if (cor_new_incoming_conn_allowed(nb) == 0)
		goto err;

	cm = cor_alloc_control_msg(nb, ACM_PRIORITY_MED);
	if (unlikely(cm == 0))
		goto err;

	if (is_highlatency != 1)
		is_highlatency = 0;

	cnb = cor_alloc_conn(GFP_ATOMIC, is_highlatency);
	if (unlikely(cnb == 0))
		goto err;

	src_in = &cnb->cli;
	trgt_out = &cnb->srv;

	spin_lock_bh(&src_in->rcv_lock);
	spin_lock_bh(&trgt_out->rcv_lock);
	if (unlikely(cor_conn_init_out(trgt_out, nb, rcv_conn_id, 1) != 0)) {
		src_in->isreset = 2;
		trgt_out->isreset = 2;
		spin_unlock_bh(&trgt_out->rcv_lock);
		spin_unlock_bh(&src_in->rcv_lock);
		cor_conn_kref_put(src_in, "alloc_conn");
		src_in = 0;
		goto err;
	}

	src_in->src.in.established = 1;
	trgt_out->trgt.out.established = 1;

	src_in->src.in.next_seqno = rcv_seqno;
	src_in->src.in.window_seqnolimit = rcv_seqno;
	src_in->src.in.window_seqnolimit_remote = rcv_seqno;

	cor_update_windowlimit(src_in);

	trgt_out->trgt.out.conn_id = snd_conn_id;

	trgt_out->trgt.out.seqno_nextsend = snd_seqno;
	trgt_out->trgt.out.seqno_acked = snd_seqno;
	cor_reset_seqno(trgt_out, snd_seqno);

	trgt_out->trgt.out.seqno_windowlimit =
			trgt_out->trgt.out.seqno_nextsend +
			cor_dec_log_64_11(window);

	spin_unlock_bh(&trgt_out->rcv_lock);
	spin_unlock_bh(&src_in->rcv_lock);

	src_in->src.in.priority_seqno = priority_seqno;
	cor_set_conn_in_priority(nb, rcv_conn_id, src_in, priority_seqno,
			priority, is_highlatency);

	cor_send_connect_success(cm, snd_conn_id, src_in);

	cor_conn_kref_put(src_in, "alloc_conn");

	if (0) {
		struct cor_conn *src_in;
err:
		if (cm != 0)
			cor_free_control_msg(cm);

		src_in = cor_get_conn(nb, rcv_conn_id);
		if (src_in == 0) {
			cor_send_reset_conn(nb, snd_conn_id, 0);
		} else {
			cor_conn_kref_put(src_in, "stack");
		}
	}
}

static void cor_parse_conn_success(struct cor_neighbor *nb, struct sk_buff *skb)
{
	__u32 rcv_conn_id = cor_pull_u32(skb);
	__u8 window = cor_pull_u8(skb);

	struct cor_conn *src_in = cor_get_conn(nb, rcv_conn_id);
	struct cor_conn *trgt_out;

	if (unlikely(src_in == 0))
		goto err;

	if (unlikely(src_in->is_client))
		goto err;

	trgt_out = cor_get_conn_reversedir(src_in);


	spin_lock_bh(&trgt_out->rcv_lock);
	spin_lock_bh(&src_in->rcv_lock);

	if (unlikely(cor_is_conn_in(src_in, nb, rcv_conn_id) == 0))
		goto err_unlock;

	BUG_ON(trgt_out->targettype != TARGET_OUT);
	BUG_ON(trgt_out->trgt.out.nb != nb);

	if (unlikely(trgt_out->isreset != 0))
		goto err_unlock;

	cor_conn_set_last_act(trgt_out);

	src_in->src.in.established = 1;

	if (likely(trgt_out->trgt.out.established == 0)) {
		trgt_out->trgt.out.established = 1;
		trgt_out->trgt.out.priority_send_allowed = 1;
		cor_conn_refresh_priority(trgt_out, 1);

		trgt_out->trgt.out.seqno_windowlimit =
				trgt_out->trgt.out.seqno_nextsend +
				cor_dec_log_64_11(window);
	}

	spin_unlock_bh(&src_in->rcv_lock);

	cor_flush_buf(trgt_out);

	spin_unlock_bh(&trgt_out->rcv_lock);


	cor_wake_sender(trgt_out);

	if (0) {
err_unlock:
		spin_unlock_bh(&trgt_out->rcv_lock);
		spin_unlock_bh(&src_in->rcv_lock);
err:
		if (src_in == 0)
			cor_send_reset_conn(nb,
					cor_get_connid_reverse(rcv_conn_id), 0);
	}

	if (likely(src_in != 0))
		cor_conn_kref_put(src_in, "stack");
}

static void cor_parse_reset(struct cor_neighbor *nb, struct sk_buff *skb)
{
	__u32 conn_id = cor_pull_u32(skb);

	int send;

	struct cor_conn_bidir *cnb;

	struct cor_conn *src_in = cor_get_conn(nb, conn_id);

	if (src_in == 0)
		return;

	cnb = cor_get_conn_bidir(src_in);

	spin_lock_bh(&cnb->cli.rcv_lock);
	spin_lock_bh(&cnb->srv.rcv_lock);

	send = unlikely(cor_is_conn_in(src_in, nb, conn_id));
	if (send && cor_get_conn_reversedir(src_in)->isreset == 0)
		cor_get_conn_reversedir(src_in)->isreset = 1;

	spin_unlock_bh(&cnb->srv.rcv_lock);
	spin_unlock_bh(&cnb->cli.rcv_lock);

	if (send)
		cor_reset_conn(src_in);

	cor_conn_kref_put(src_in, "stack");
}

static int _cor_kernel_packet_misc(struct cor_neighbor *nb,
		struct sk_buff *skb, int *ping_rcvd,
		__u32 *pingcookie, __u8 code_min)
{
	if (code_min == KP_MISC_PADDING) {
	} else if (code_min == KP_MISC_INIT_SESSION) {
		/* ignore if sessionid_rcv_needed is false */
		__be32 sessionid = cor_pull_be32(skb);

		if (sessionid != nb->sessionid) {
			return 1;
		}
	} else if (code_min == KP_MISC_PING) {
		*ping_rcvd = 1;
		*pingcookie = cor_pull_u32(skb);
	} else if (code_min == KP_MISC_PONG) {
		__u32 cookie = cor_pull_u32(skb);
		__u32 respdelay_full = cor_pull_u32(skb);

		cor_pull_u32(skb); /* respdelay_netonly */
		cor_ping_resp(nb, cookie, respdelay_full);
	} else if (code_min == KP_MISC_ACK) {
		__u32 seqno = cor_pull_u32(skb);

		cor_kern_ack_rcvd(nb, seqno);
	} else if (code_min == KP_MISC_CONNECT) {
		cor_parse_connect(nb, skb);
	} else if (code_min == KP_MISC_CONNECT_SUCCESS) {
		cor_parse_conn_success(nb, skb);
	} else if (code_min == KP_MISC_RESET_CONN) {
		cor_parse_reset(nb, skb);
	} else if (code_min == KP_MISC_SET_MAX_CMSG_DELAY) {
		atomic_set(&nb->max_remote_ack_fast_delay_us,
				cor_pull_u32(skb));
		atomic_set(&nb->max_remote_ack_slow_delay_us,
				cor_pull_u32(skb));
		atomic_set(&nb->max_remote_ackconn_delay_us, cor_pull_u32(skb));
		atomic_set(&nb->max_remote_pong_delay_us, cor_pull_u32(skb));
	} else if (code_min == KP_MISC_SET_RECEIVE_MTU) {
		atomic_set(&nb->remote_rcvmtu, cor_pull_u32(skb));
	} else {
		BUG();
	}
	return 0;
}

static void cor_parse_ack_conn(struct cor_neighbor *nb, struct sk_buff *skb,
		__u8 code_min, __u64 *bytes_acked)
{
	__u32 conn_id = cor_pull_u32(skb);
	__u8 delay_remaining = 0;

	struct cor_conn *src_in = cor_get_conn(nb, conn_id);

	if ((code_min & KP_ACK_CONN_FLAGS_SEQNO) != 0 ||
			cor_ooolen(code_min) != 0)
		delay_remaining = cor_pull_u8(skb);

	if ((code_min & KP_ACK_CONN_FLAGS_SEQNO) != 0) {
		__u32 seqno = cor_pull_u32(skb);
		int setwindow = 0;
		__u8 window = 0;
		__u8 bufsize_changerate = 0;

		if ((code_min & KP_ACK_CONN_FLAGS_WINDOW) != 0) {
			setwindow = 1;
			window = cor_pull_u8(skb);
			bufsize_changerate = cor_pull_u8(skb);
		}

		if (likely(src_in != 0))
			cor_conn_ack_rcvd(nb, conn_id,
					cor_get_conn_reversedir(src_in),
					seqno, setwindow, window,
					bufsize_changerate, bytes_acked);
	}

	if (cor_ooolen(code_min) != 0) {
		__u32 seqno_ooo = cor_pull_u32(skb);
		__u32 ooo_len;

		if (cor_ooolen(code_min) == 1) {
			ooo_len = cor_pull_u8(skb);
		} else if (cor_ooolen(code_min) == 2) {
			ooo_len = cor_pull_u16(skb);
		} else if (cor_ooolen(code_min) == 4) {
			ooo_len = cor_pull_u32(skb);
		} else {
			BUG();
		}

		if (likely(src_in != 0))
			cor_conn_ack_ooo_rcvd(nb, conn_id,
					cor_get_conn_reversedir(src_in),
					seqno_ooo, ooo_len, bytes_acked);
	}

	if (code_min & KP_ACK_CONN_FLAGS_PRIORITY) {
		__u16 priority_raw = cor_pull_u16(skb);
		__u8 priority_seqno = (priority_raw >> 12);
		__u16 priority = (priority_raw & 4095);
		__u8 is_highlatency = cor_pull_u8(skb);

		if (likely(src_in != 0))
			cor_set_conn_in_priority(nb, conn_id, src_in,
					priority_seqno, priority,
					is_highlatency);
	}

	if (unlikely(src_in == 0)) {
		cor_send_reset_conn(nb, cor_get_connid_reverse(conn_id), 0);
		return;
	}

	cor_conn_kref_put(src_in, "stack");
}

static int cor_parse_conndata_length(struct sk_buff *skb, __u32 *ret)
{
	char *highptr = cor_pull_skb(skb, 1);
	__u8 high;

	if (highptr == 0)
		return 1;

	high = cor_parse_u8(highptr);

	if (high < 128) {
		*ret = cor_parse_u8(highptr);
	} else {
		char *lowptr = cor_pull_skb(skb, 1);

		if (lowptr == 0)
			return 1;
		*ret = 128 +
				((__u32) (high - 128)) * 256 +
				((__u32) cor_parse_u8(lowptr));
	}

	return 0;
}

static void cor_parse_conndata(struct cor_neighbor *nb, struct sk_buff *skb,
		__u8 code_min)
{
	__u8 flush = ((code_min & KP_CONN_DATA_FLAGS_FLUSH) != 0) ? 1 : 0;
	__u8 windowused = (code_min & KP_CONN_DATA_FLAGS_WINDOWUSED);
	__u32 conn_id = cor_pull_u32(skb);
	__u32 seqno = cor_pull_u32(skb);
	__u32 datalength = 0;
	char *data;

	if (unlikely(cor_parse_conndata_length(skb, &datalength) != 0))
		BUG();

	if (unlikely(datalength == 0))
		return;

	data = cor_pull_skb(skb, datalength);
	BUG_ON(data == 0);

	cor_conn_rcv(nb, 0, data, datalength, conn_id, seqno,
			windowused, flush);
}

static int __cor_kernel_packet(struct cor_neighbor *nb, struct sk_buff *skb,
		int *ping_rcvd, __u32 *pingcookie, __u64 *bytes_acked,
		__u8 code)
{
	__u8 code_maj = kp_maj(code);
	__u8 code_min = kp_min(code);

	if (code_maj == KP_MISC) {
		return _cor_kernel_packet_misc(nb, skb, ping_rcvd,
				pingcookie, code_min);
	} else if (code_maj == KP_ACK_CONN) {
		cor_parse_ack_conn(nb, skb, code_min, bytes_acked);
		return 0;
	} else if (code_maj == KP_CONN_DATA) {
		cor_parse_conndata(nb, skb, code_min);
		return 0;
	} else {
		BUG();
		return 1;
	}
}

static void _cor_kernel_packet(struct cor_neighbor *nb, struct sk_buff *skb,
		int ackneeded, ktime_t pkg_rcv_start)
{
	int ping_rcvd = 0;
	__u32 pingcookie = 0;
	__u64 bytes_acked = 0;

	__u32 seqno = cor_pull_u32(skb);

	if (unlikely(atomic_read(&nb->sessionid_rcv_needed) != 0)) {
		__u8 *codeptr = cor_pull_skb(skb, 1);
		__u8 code;
		__be32 sessionid;

		if (codeptr == 0)
			return;

		code = *codeptr;
		if (kp_maj(code) != KP_MISC ||
				kp_min(code) != KP_MISC_INIT_SESSION)
			return;

		sessionid = cor_pull_be32(skb);
		if (sessionid == nb->sessionid) {
			atomic_set(&nb->sessionid_rcv_needed, 0);
			cor_announce_send_stop(nb->dev, nb->mac,
					ANNOUNCE_TYPE_UNICAST);
		} else {
			return;
		}
	}

	while (1) {
		__u8 *codeptr = cor_pull_skb(skb, 1);
		__u8 code;

		if (codeptr == 0)
			break;

		code = *codeptr;

		if (__cor_kernel_packet(nb, skb, &ping_rcvd,
				&pingcookie, &bytes_acked, code) != 0)
			return;
	}

	if (bytes_acked > 0)
		cor_nbcongwin_data_acked(nb, bytes_acked);

	if (ackneeded == ACK_NEEDED_SLOW)
		cor_send_ack(nb, seqno, 0);
	else if (ackneeded == ACK_NEEDED_FAST)
		cor_send_ack(nb, seqno, 1);

	/* do this at the end to include packet processing time */
	if (ping_rcvd)
		cor_send_pong(nb, pingcookie, pkg_rcv_start);
}

static int _cor_kernel_packet_checklen_misc(struct sk_buff *skb, __u8 code_min)
{
	if (code_min == KP_MISC_PADDING) {
	} else if (code_min == KP_MISC_INIT_SESSION ||
			code_min == KP_MISC_PING) {
		if (cor_pull_skb(skb, 4) == 0)
			return 1;
	} else if (code_min == KP_MISC_PONG) {
		if (cor_pull_skb(skb, 12) == 0)
			return 1;
	} else if (code_min == KP_MISC_ACK) {
		if (cor_pull_skb(skb, 4) == 0)
			return 1;
	} else if (code_min == KP_MISC_CONNECT) {
		if (cor_pull_skb(skb, 16) == 0)
			return 1;
	} else if (code_min == KP_MISC_CONNECT_SUCCESS) {
		if (cor_pull_skb(skb, 5) == 0)
			return 1;
	} else if (code_min == KP_MISC_RESET_CONN) {
		if (cor_pull_skb(skb, 4) == 0)
			return 1;
	} else if (code_min == KP_MISC_SET_MAX_CMSG_DELAY) {
		if (cor_pull_skb(skb, 16) == 0)
			return 1;
	} else if (code_min == KP_MISC_SET_RECEIVE_MTU) {
		if (cor_pull_skb(skb, 4) == 0)
			return 1;
	} else {
		return 1;
	}
	return 0;
}


static int _cor_kernel_packet_checklen_ackconn(struct sk_buff *skb,
		__u8 code_min)
{
	if (cor_pull_skb(skb, 4 + cor_ack_conn_len(code_min)) == 0)
		return 1;
	return 0;
}

static int _cor_kernel_packet_checklen_conndata(struct sk_buff *skb,
		__u8 code_min)
{
	__u32 datalength;

	if (cor_pull_skb(skb, 8) == 0)
		return 1;

	if (unlikely(cor_parse_conndata_length(skb, &datalength) != 0))
		return 1;

	if (cor_pull_skb(skb, datalength) == 0)
		return 1;

	return 0;
}

static int _cor_kernel_packet_checklen(struct sk_buff *skb, __u8 code)
{
	__u8 code_maj = kp_maj(code);
	__u8 code_min = kp_min(code);

	if (code_maj == KP_MISC)
		return _cor_kernel_packet_checklen_misc(skb, code_min);
	else if (code_maj == KP_ACK_CONN)
		return _cor_kernel_packet_checklen_ackconn(skb, code_min);
	else if (code_maj == KP_CONN_DATA)
		return _cor_kernel_packet_checklen_conndata(skb, code_min);
	else
		return 1;
}

static int cor_kernel_packet_checklen(struct sk_buff *skb)
{
	if (cor_pull_skb(skb, 4) == 0) /* seqno */
		return 1;

	while (1) {
		__u8 *codeptr = cor_pull_skb(skb, 1);
		__u8 code;

		if (codeptr == 0)
			break;
		code = *codeptr;

		if (unlikely(_cor_kernel_packet_checklen(skb, code) != 0))
			return 1;
	}
	return 0;
}

void cor_kernel_packet(struct cor_neighbor *nb, struct sk_buff *skb,
		int ackneeded)
{
	unsigned char *data = skb->data;
	unsigned int len = skb->len;

	ktime_t pkg_rcv_start = ktime_get();

	if (unlikely(cor_kernel_packet_checklen(skb) != 0)) {
		/* printk(KERN_ERR "kpacket discard\n"); */
		goto discard;
	}

	skb->data = data;
	skb->len = len;

	_cor_kernel_packet(nb, skb, ackneeded, pkg_rcv_start);
discard:
	kfree_skb(skb);
}

MODULE_LICENSE("GPL");
