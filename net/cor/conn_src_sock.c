/**
 *	Connection oriented routing
 *	Copyright (C) 2007-2021 Michael Blizek
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License
 *	as published by the Free Software Foundation; either version 2
 *	of the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 */

#include "cor.h"

static DEFINE_SPINLOCK(cor_flushtoconn_oom_lock);
static LIST_HEAD(cor_flushtoconn_oom_list);
static struct delayed_work cor_flushtoconn_oom_work;


static void cor_keepalive_req_workfunc(struct work_struct *work);

static DEFINE_SPINLOCK(cor_keepalive_req_lock);
DECLARE_WORK(cor_keepalive_req_work, cor_keepalive_req_workfunc);
static LIST_HEAD(cor_keepalive_req_list);


static void _cor_update_src_sock_sndspeed(struct cor_snd_speed *snd_speed,
		__u32 timediff_jiffies)
{
	__u32 timediff_us = jiffies_to_usecs(timediff_jiffies);

	__u64 curr_sndspeed;

	if (timediff_us == 0)
		return;

	curr_sndspeed = div64_u64(snd_speed->bytes_sent * 1000000LL,
			timediff_us);

	snd_speed->speed = snd_speed->speed - snd_speed->speed / 8 +
			((__u32) min((__u64) U32_MAX, curr_sndspeed) / 8);

	BUG_ON(SOCK_SNDSPEED_MAX > U32_MAX);
	snd_speed->speed_limited = snd_speed->speed_limited -
			snd_speed->speed_limited / 8 + ((__u32)
			min((__u64) SOCK_SNDSPEED_MAX, curr_sndspeed) / 8);
}

void cor_update_src_sock_sndspeed(struct cor_conn *src_sock_l, __u32 bytes_sent)
{
	struct cor_snd_speed *snd_speed =
			&src_sock_l->src.sock.ed->snd_speed;
	__u32 update_interval_jiffies = HZ / 10;
	unsigned long jiffies_tmp = jiffies;

	if (src_sock_l->targettype == TARGET_OUT &&
			src_sock_l->trgt.out.nb != 0) {
		struct cor_neighbor *nb = src_sock_l->trgt.out.nb;

		__u32 lat = atomic_read(&nb->latency_retrans_us);
		__u32 stddev = atomic_read(&nb->latency_stddev_retrans_us);
		__u32 delay = atomic_read(&nb->max_remote_ackconn_delay_us);

		update_interval_jiffies = max(update_interval_jiffies,
				(__u32) msecs_to_jiffies(lat / 1000 +
				stddev / 500 + delay / 1000));
	}

	if (snd_speed->flushed != 0 && unlikely(time_after(jiffies_tmp,
			snd_speed->jiffies_last_refresh +
			update_interval_jiffies * 2))) {
		snd_speed->state = SNDSPEED_INIT;
		snd_speed->jiffies_last_refresh = jiffies_tmp;
	}

	if (snd_speed->state == SNDSPEED_INIT) {
		if (time_after(jiffies_tmp, snd_speed->jiffies_last_refresh +
				update_interval_jiffies)) {
			snd_speed->state = SNDSPEED_ACTIVE;
			snd_speed->jiffies_last_refresh = jiffies_tmp;
			snd_speed->bytes_sent = 0;
		}
	} else if (likely(snd_speed->state == SNDSPEED_ACTIVE)) {
		if (unlikely(snd_speed->bytes_sent + bytes_sent <
				snd_speed->bytes_sent))
			snd_speed->bytes_sent = U32_MAX;
		else
			snd_speed->bytes_sent += bytes_sent;

		if (time_after(jiffies_tmp, snd_speed->jiffies_last_refresh +
				update_interval_jiffies)) {
			_cor_update_src_sock_sndspeed(snd_speed, jiffies_tmp -
					snd_speed->jiffies_last_refresh);
			snd_speed->jiffies_last_refresh = jiffies_tmp;
			snd_speed->bytes_sent = 0;
		}
	} else {
		BUG();
	}

	snd_speed->flushed = src_sock_l->flush;
}

int cor_sock_sndbufavailable(struct cor_conn *src_sock_lx, int for_wakeup)
{
	__u32 limit = src_sock_lx->bufsize.bufsize >> BUFSIZE_SHIFT;

	/* printk(KERN_ERR "sndbuf %p %u %u\n", src_sock_lx,
			(src_sock_lx->bufsize.bufsize >> BUFSIZE_SHIFT),
			src_sock_lx->data_buf.read_remaining); */

	if (src_sock_lx->targettype == TARGET_OUT &&
			cor_seqno_after(
			src_sock_lx->trgt.out.seqno_windowlimit,
			src_sock_lx->trgt.out.seqno_nextsend +
			src_sock_lx->data_buf.read_remaining)) {
		__u32 windowleft = src_sock_lx->trgt.out.seqno_windowlimit -
				src_sock_lx->trgt.out.seqno_nextsend -
				src_sock_lx->data_buf.read_remaining;

		if (unlikely(windowleft > 1 << 31))
			windowleft = 0;

		limit = max(limit, min(windowleft,
				(__u32) WINDOW_MAX_PER_CONN_MIN_OUT_WINOK));
	}

	/**
	 * High cpu usage may cause high latency of the userspace sender.
	 * Increasing bufferspace to compensate may increase latency further.
	 */
	if (src_sock_lx->is_highlatency == 0) {
		limit = min(limit,
				src_sock_lx->src.sock.ed->snd_speed.speed / 20);
		limit = max(limit,
				src_sock_lx->src.sock.ed->snd_speed.speed / 512);
	} else {
		limit = max(limit,
				src_sock_lx->src.sock.ed->snd_speed.speed / 64);
	}

	/* reduce number of wakeups to reduce cpu usage */
	if (for_wakeup)
		limit -= limit / 4;

	/* ">=" ... return 1 if read_remaining == 0 */
	return limit >= src_sock_lx->data_buf.read_remaining ? 1 : 0;
}


static void cor_update_flushtoconn_oom_list(struct cor_conn *src_sock_l,
		int oom)
{
	BUG_ON(src_sock_l->sourcetype != SOURCE_SOCK);

	if (unlikely(oom != 0)) {
		if (src_sock_l->src.sock.in_flushtoconn_oom_list != 0)
			return;

		spin_lock_bh(&cor_flushtoconn_oom_lock);

		if (list_empty(&cor_flushtoconn_oom_list)) {
			schedule_delayed_work(&cor_flushtoconn_oom_work,
					msecs_to_jiffies(100));
		}

		list_add_tail(&src_sock_l->src.sock.flushtoconn_oom_lh,
				&cor_flushtoconn_oom_list);
		src_sock_l->src.sock.in_flushtoconn_oom_list = 1;

		spin_unlock_bh(&cor_flushtoconn_oom_lock);

		cor_conn_kref_get(src_sock_l, "flushtoconn_oom_list");
	} else {
		if (likely(src_sock_l->src.sock.in_flushtoconn_oom_list == 0))
			return;

		spin_lock_bh(&cor_flushtoconn_oom_lock);
		list_del(&src_sock_l->src.sock.flushtoconn_oom_lh);
		src_sock_l->src.sock.in_flushtoconn_oom_list = 0;

		if (list_empty(&cor_flushtoconn_oom_list))
			cancel_delayed_work(&cor_flushtoconn_oom_work);

		spin_unlock_bh(&cor_flushtoconn_oom_lock);

		cor_conn_kref_put_bug(src_sock_l, "flushtoconn_oom_list");

		if (likely(src_sock_l->src.sock.ed->cs != 0)) {
			cor_sk_write_space(src_sock_l->src.sock.ed->cs);
		}
	}
}

static int __cor_mngdsocket_flushtoconn(struct cor_conn *src_sock_l,
		char *snd_hdr, __u16 snd_hdr_len,
		char *snd_data, __u16 snd_data_len,
		char *snd_chksum, __u16 snd_chksum_len,
		__u8 flush)
{
	if (likely(src_sock_l->src.sock.ed->sent < snd_hdr_len)) {
		__u32 off = src_sock_l->src.sock.ed->sent;
		__u32 len = snd_hdr_len - off;

		__u32 rc = cor_receive_sock(src_sock_l, snd_hdr + off, len,
				0);

		src_sock_l->src.sock.ed->sent += rc;

		if (unlikely(rc < len))
			return RC_FTC_OOM;
	}

	if (likely(src_sock_l->src.sock.ed->sent - snd_hdr_len <
			snd_data_len)) {
		__u32 off = src_sock_l->src.sock.ed->sent - snd_hdr_len;
		__u32 len = snd_data_len - off;

		__u32 rc = cor_receive_sock(src_sock_l, snd_data + off, len,
				0);

		src_sock_l->src.sock.ed->sent += rc;

		if (unlikely(rc < len))
			return RC_FTC_OOM;
	}

	if (likely(src_sock_l->src.sock.ed->sent - snd_hdr_len - snd_data_len <
			snd_chksum_len)) {
		__u32 off = src_sock_l->src.sock.ed->sent - snd_hdr_len -
				snd_data_len;
		__u32 len = CONN_MNGD_CHECKSUMLEN - off;

		__u32 rc = cor_receive_sock(src_sock_l, snd_chksum + off, len,
				flush);

		src_sock_l->src.sock.ed->sent += rc;

		if (unlikely(rc < len))
			return RC_FTC_OOM;
	}

	BUG_ON(src_sock_l->src.sock.ed->sent > snd_hdr_len +
			((__u32) snd_data_len) + snd_chksum_len);

#ifdef COR_DEBUG
	if (1) {
		char checksum_calc[CONN_MNGD_CHECKSUMLEN];

		BUG_ON(CONN_MNGD_CHECKSUMLEN != snd_chksum_len);

		cor_mngdsocket_chksum(snd_hdr, snd_hdr_len,
				snd_data, snd_data_len,
				checksum_calc, snd_chksum_len);

		WARN_ON(memcmp(snd_chksum, checksum_calc,
				snd_chksum_len) != 0);
	}
#endif

	return RC_FTC_OK;
}

static void _cor_mngdsocket_flushtoconn_fill_ctrl(struct cor_conn *src_sock_l)
{
	__u16 hdr = 0;

	if (unlikely(src_sock_l->src.sock.send_eof_needed != 0) &&
			src_sock_l->src.sock.buf_data_filled == 0) {
		hdr |= CONN_MNGD_EOF;
		src_sock_l->src.sock.send_eof_needed = 0;
	}

	if (unlikely(src_sock_l->src.sock.send_rcvend_needed != 0)) {
		hdr |= CONN_MNGD_RCVEND;
		src_sock_l->src.sock.send_rcvend_needed = 0;
	}

	if (unlikely(src_sock_l->src.sock.send_keepalive_req_needed != 0)) {
		hdr |= CONN_MNGD_KEEPALIVE_REQ;
		src_sock_l->src.sock.send_keepalive_req_needed = 0;
	}

	if (unlikely(src_sock_l->src.sock.send_keepalive_resp_needed != 0)) {
		hdr |= CONN_MNGD_KEEPALIVE_RESP;
		src_sock_l->src.sock.send_keepalive_resp_needed = 0;
	}

	if (likely(hdr == 0))
		return;

	BUILD_BUG_ON(CONN_MNGD_HEADERLEN != 2);
	BUILD_BUG_ON(sizeof(src_sock_l->src.sock.ed->buf_ctrl.snd_hdr) !=
			CONN_MNGD_HEADERLEN);
	BUILD_BUG_ON(CONN_MNGD_MAX_CTRL_DATALEN != 8);
	BUILD_BUG_ON(sizeof(src_sock_l->src.sock.ed->buf_ctrl.snd_data) !=
			CONN_MNGD_MAX_CTRL_DATALEN);
	BUILD_BUG_ON(sizeof(src_sock_l->src.sock.ed->buf_ctrl.snd_chksum) !=
			CONN_MNGD_CHECKSUMLEN);

	cor_put_u16(&src_sock_l->src.sock.ed->buf_ctrl.snd_hdr[0], hdr);

	src_sock_l->src.sock.ed->buf_ctrl.snd_data_len = 0;

	if ((hdr & CONN_MNGD_KEEPALIVE_REQ) != 0) {
		BUG_ON(src_sock_l->src.sock.ed->buf_ctrl.snd_data_len + 4 >
				CONN_MNGD_MAX_CTRL_DATALEN);
		cor_put_be32(&src_sock_l->src.sock.ed->buf_ctrl.snd_data[0] +
				src_sock_l->src.sock.ed->buf_ctrl.snd_data_len,
				src_sock_l->src.sock.ed->keepalive_req_cookie);
		src_sock_l->src.sock.ed->buf_ctrl.snd_data_len += 4;
	}

	if ((hdr & CONN_MNGD_KEEPALIVE_RESP) != 0) {
		BUG_ON(src_sock_l->src.sock.ed->buf_ctrl.snd_data_len + 4 >
				CONN_MNGD_MAX_CTRL_DATALEN);
		cor_put_be32(&src_sock_l->src.sock.ed->buf_ctrl.snd_data[0] +
				src_sock_l->src.sock.ed->buf_ctrl.snd_data_len,
				src_sock_l->src.sock.ed->keepalive_resp_cookie);
		src_sock_l->src.sock.ed->buf_ctrl.snd_data_len += 4;
	}

	cor_mngdsocket_chksum(
			&src_sock_l->src.sock.ed->buf_ctrl.snd_hdr[0],
			CONN_MNGD_HEADERLEN,
			&src_sock_l->src.sock.ed->buf_ctrl.snd_data[0],
			src_sock_l->src.sock.ed->buf_ctrl.snd_data_len,
			&src_sock_l->src.sock.ed->buf_ctrl.snd_chksum[0],
			CONN_MNGD_CHECKSUMLEN);

	src_sock_l->src.sock.buf_ctrl_filled = 1;
}

static int _cor_mngdsocket_flushtoconn_ctrl(struct cor_conn *src_sock_l)
{
	int rc;

	__u16 snd_data_len = 0;
	__u8 flush = 0;

	BUG_ON(snd_data_len >
			65535 - CONN_MNGD_HEADERLEN - CONN_MNGD_CHECKSUMLEN);
	BUG_ON(snd_data_len >
			sizeof(src_sock_l->src.sock.ed->buf_ctrl.snd_data));

	if (src_sock_l->src.sock.flush != 0 &&
			src_sock_l->src.sock.buf_data_filled == 0 &&
			src_sock_l->src.sock.send_eof_needed == 0 &&
			src_sock_l->src.sock.send_rcvend_needed == 0) {
		flush = 1;
	}

	BUILD_BUG_ON(sizeof(src_sock_l->src.sock.ed->buf_ctrl.snd_hdr) !=
			CONN_MNGD_HEADERLEN);
	BUILD_BUG_ON(sizeof(src_sock_l->src.sock.ed->buf_ctrl.snd_chksum) !=
			CONN_MNGD_CHECKSUMLEN);

	rc = __cor_mngdsocket_flushtoconn(src_sock_l,
			&src_sock_l->src.sock.ed->buf_ctrl.snd_hdr[0],
			CONN_MNGD_HEADERLEN,
			&src_sock_l->src.sock.ed->buf_ctrl.snd_data[0],
			src_sock_l->src.sock.ed->buf_ctrl.snd_data_len,
			&src_sock_l->src.sock.ed->buf_ctrl.snd_chksum[0],
			CONN_MNGD_CHECKSUMLEN,
			flush);

	if (likely(rc == RC_FTC_OK)) {
		memset(&src_sock_l->src.sock.ed->buf_ctrl, 0,
				sizeof(src_sock_l->src.sock.ed->buf_ctrl));
		src_sock_l->src.sock.buf_ctrl_filled = 0;
		src_sock_l->src.sock.ed->sent = 0;
	}

	BUG_ON(src_sock_l->src.sock.ed->sent > CONN_MNGD_HEADERLEN +
			((__u32) snd_data_len) + CONN_MNGD_CHECKSUMLEN);

	cor_flush_buf(src_sock_l);

	return rc;
}

static int _cor_mngdsocket_flushtoconn_data(struct cor_conn *src_sock_l)
{
	int rc;

	__u8 flush = 0;

	BUG_ON(src_sock_l->src.sock.ed->buf_data.snd_data_len >
			65535 - CONN_MNGD_HEADERLEN - CONN_MNGD_CHECKSUMLEN);
	BUG_ON(src_sock_l->src.sock.ed->buf_data.snd_data_len >
			CONN_MNGD_MAX_SEGMENT_SIZE);

	if (src_sock_l->src.sock.flush != 0 &&
			likely(src_sock_l->src.sock.send_eof_needed == 0) &&
			likely(src_sock_l->src.sock.send_rcvend_needed == 0)) {
		flush = 1;
	}

	BUILD_BUG_ON(sizeof(src_sock_l->src.sock.ed->buf_data.snd_hdr) !=
			CONN_MNGD_HEADERLEN);
	BUILD_BUG_ON(sizeof(src_sock_l->src.sock.ed->buf_data.snd_chksum) !=
			CONN_MNGD_CHECKSUMLEN);

	rc = __cor_mngdsocket_flushtoconn(src_sock_l,
			&src_sock_l->src.sock.ed->buf_data.snd_hdr[0],
			CONN_MNGD_HEADERLEN,
			src_sock_l->src.sock.ed->buf_data.snd_data,
			src_sock_l->src.sock.ed->buf_data.snd_data_len,
			&src_sock_l->src.sock.ed->buf_data.snd_chksum[0],
			CONN_MNGD_CHECKSUMLEN,
			flush);

	if (likely(rc == RC_FTC_OK)) {
		memset(&src_sock_l->src.sock.ed->buf_data, 0,
				sizeof(src_sock_l->src.sock.ed->buf_ctrl));
		src_sock_l->src.sock.buf_data_filled = 0;
		src_sock_l->src.sock.ed->sent = 0;
	}

	BUG_ON(src_sock_l->src.sock.ed->sent > CONN_MNGD_HEADERLEN +
			((__u32) src_sock_l->src.sock.ed->buf_data.snd_data_len)
			+ CONN_MNGD_CHECKSUMLEN);

	cor_flush_buf(src_sock_l);

	return rc;
}

int _cor_mngdsocket_flushtoconn(struct cor_conn *src_sock_l)
{
	BUG_ON(src_sock_l->sourcetype != SOURCE_SOCK);

	if (unlikely(src_sock_l->isreset != 0))
		return RC_FTC_OK;

	if (unlikely(src_sock_l->src.sock.buf_ctrl_filled != 0)) {
		int rc = _cor_mngdsocket_flushtoconn_ctrl(src_sock_l);

		if (unlikely(rc != RC_FTC_OK))
			return rc;
	}

	if (src_sock_l->src.sock.buf_data_filled != 0) {
		int rc = _cor_mngdsocket_flushtoconn_data(src_sock_l);

		if (unlikely(rc != RC_FTC_OK))
			return rc;
	}

	BUG_ON(src_sock_l->src.sock.buf_ctrl_filled != 0);
	BUG_ON(src_sock_l->src.sock.buf_data_filled != 0);
	_cor_mngdsocket_flushtoconn_fill_ctrl(src_sock_l);
	if (unlikely(src_sock_l->src.sock.buf_ctrl_filled != 0)) {
		int rc = _cor_mngdsocket_flushtoconn_ctrl(src_sock_l);

		if (unlikely(rc != RC_FTC_OK))
			return rc;
	}

	return RC_FTC_OK;
}

static void cor_mngdsocket_flushtoconn_oomresume(struct work_struct *work)
{
	int rc = RC_FTC_OK;

	while (rc != RC_FTC_OOM) {
		struct cor_conn *src_sock_o;

		spin_lock_bh(&cor_flushtoconn_oom_lock);

		if (list_empty(&cor_flushtoconn_oom_list)) {
			spin_unlock_bh(&cor_flushtoconn_oom_lock);
			break;
		}

		src_sock_o = container_of(cor_flushtoconn_oom_list.next,
				struct cor_conn,
				src.sock.flushtoconn_oom_lh);

		BUG_ON(src_sock_o == 0);

		cor_conn_kref_get(src_sock_o, "stack");

		spin_unlock_bh(&cor_flushtoconn_oom_lock);

		spin_lock_bh(&src_sock_o->rcv_lock);
		BUG_ON(src_sock_o->sourcetype != SOURCE_SOCK);
		BUG_ON(src_sock_o->src.sock.in_flushtoconn_oom_list == 0);
		rc = _cor_mngdsocket_flushtoconn(src_sock_o);
		if (likely(rc != RC_FTC_OOM &&
				src_sock_o->src.sock.ed->cs != 0)) {
			cor_sk_write_space(src_sock_o->src.sock.ed->cs);
		}
		spin_unlock_bh(&src_sock_o->rcv_lock);

		cor_conn_kref_put(src_sock_o, "stack");
	}

	if (rc == RC_FTC_OOM) {
		schedule_delayed_work(&cor_flushtoconn_oom_work,
				msecs_to_jiffies(100));
	}
}

void cor_mngdsocket_flushtoconn_ctrl_send_keepalive_req(
		struct cor_conn *src_sock_l)
{
	int rc;

	BUG_ON(src_sock_l->src.sock.keepalive_intransit != 0);
	BUG_ON(src_sock_l->src.sock.send_keepalive_req_needed != 0);

	get_random_bytes((char *)
			&src_sock_l->src.sock.ed->keepalive_req_cookie,
			sizeof(src_sock_l->src.sock.ed->keepalive_req_cookie));
	src_sock_l->src.sock.send_keepalive_req_needed = 1;

	src_sock_l->src.sock.keepalive_intransit = 1;
	src_sock_l->src.sock.ed->jiffies_keepalive_lastact = jiffies;

	rc = _cor_mngdsocket_flushtoconn(src_sock_l);
	cor_update_flushtoconn_oom_list(src_sock_l, rc == RC_FTC_OOM);
}

int cor_mngdsocket_flushtoconn_ctrl(struct cor_sock *cs_m_l, __u8 send_eof,
		__u8 send_rcvend, __u8 send_keepalive_resp,
		__be32 keepalive_resp_cookie)
{
	int rc = RC_FTC_OK;

	struct cor_conn *src_sock;

	BUG_ON(cs_m_l->type != CS_TYPE_CONN_MANAGED);
	src_sock = cs_m_l->data.conn_managed.src_sock;

	if (unlikely(src_sock == 0))
		return RC_FTC_ERR;

	spin_lock_bh(&src_sock->rcv_lock);

	if (unlikely(cor_is_src_sock(src_sock, cs_m_l) == 0 ||
			src_sock->isreset != 0)) {
		cs_m_l->data.conn_managed.is_reset = 1;
		cor_sk_data_ready(cs_m_l);
		rc = RC_FTC_ERR;
		cor_flush_buf(src_sock);
		goto out_err;
	}

	if (send_eof != 0) {
		src_sock->src.sock.send_eof_needed = 1;
		src_sock->src.sock.flush = 1;
	}
	if (send_rcvend != 0)
		src_sock->src.sock.send_rcvend_needed = 1;

	if (send_keepalive_resp != 0 &&
			src_sock->src.sock.send_keepalive_resp_needed == 0) {
		src_sock->src.sock.ed->keepalive_resp_cookie =
				keepalive_resp_cookie;
		src_sock->src.sock.send_keepalive_resp_needed = 1;
	}

	rc = _cor_mngdsocket_flushtoconn(src_sock);

out_err:
	cor_update_flushtoconn_oom_list(src_sock, rc == RC_FTC_OOM);

	spin_unlock_bh(&src_sock->rcv_lock);

	return rc;
}

int cor_mngdsocket_flushtoconn_data(struct cor_sock *cs_m_l)
{
	int rc = RC_FTC_OK;

	struct cor_conn *src_sock = cs_m_l->data.conn_managed.src_sock;

	if (unlikely(src_sock == 0))
		return RC_FTC_ERR;

	spin_lock_bh(&src_sock->rcv_lock);

	if (unlikely(cor_is_src_sock(src_sock, cs_m_l) == 0 ||
			src_sock->isreset != 0)) {
		cs_m_l->data.conn_managed.is_reset = 1;
		cor_sk_data_ready(cs_m_l);
		rc = RC_FTC_ERR;
		cor_flush_buf(src_sock);
		goto out;
	}

	cs_m_l->is_highlatency = src_sock->is_highlatency;
	src_sock->src.sock.flush = cs_m_l->data.conn_managed.flush;

	if (unlikely(cs_m_l->data.conn_managed.send_in_progress != 0)) {
		if (src_sock->src.sock.buf_data_filled == 0)
			cs_m_l->data.conn_managed.send_in_progress = 0;
		else
			rc = RC_FTC_OOM;
		goto out;
	}

	BUG_ON(src_sock->src.sock.buf_data_filled != 0);
	BUG_ON(cs_m_l->data.conn_managed.snd_data_len == 0);
	BUG_ON(cs_m_l->data.conn_managed.snd_data_len >
			CONN_MNGD_MAX_SEGMENT_SIZE);

	BUILD_BUG_ON(CONN_MNGD_HEADERLEN != 2);
	BUILD_BUG_ON(sizeof(src_sock->src.sock.ed->buf_data.snd_hdr) !=
			CONN_MNGD_HEADERLEN);
	BUILD_BUG_ON(sizeof(src_sock->src.sock.ed->buf_data.snd_chksum) !=
			CONN_MNGD_CHECKSUMLEN);


	cor_put_u16(&src_sock->src.sock.ed->buf_data.snd_hdr[0],
			CONN_MNGD_HASDATA |
			(cs_m_l->data.conn_managed.snd_data_len - 1));

	src_sock->src.sock.ed->buf_data.snd_data =
			cs_m_l->data.conn_managed.snd_buf;
	src_sock->src.sock.ed->buf_data.snd_data_len =
			cs_m_l->data.conn_managed.snd_data_len;

	cor_mngdsocket_chksum(
			&src_sock->src.sock.ed->buf_data.snd_hdr[0],
			CONN_MNGD_HEADERLEN,
			&src_sock->src.sock.ed->buf_data.snd_data[0],
			src_sock->src.sock.ed->buf_data.snd_data_len,
			&src_sock->src.sock.ed->buf_data.snd_chksum[0],
			CONN_MNGD_CHECKSUMLEN);
	src_sock->src.sock.buf_data_filled = 1;

	rc = _cor_mngdsocket_flushtoconn(src_sock);

	if (unlikely(rc != RC_FTC_OK)) {
		cs_m_l->data.conn_managed.send_in_progress = 1;
	} else {
		BUG_ON(src_sock->src.sock.buf_data_filled != 0);
		cs_m_l->data.conn_managed.snd_data_len = 0;
	}

out:
	cor_update_flushtoconn_oom_list(src_sock, rc == RC_FTC_OOM);

	spin_unlock_bh(&src_sock->rcv_lock);

	return rc;
}

static void cor_keepalive_req_workfunc(struct work_struct *work)
{
	while (1) {
		unsigned long iflags;
		struct cor_conn_src_sock_extradata *ed;
		struct cor_conn *src_sock_o;

		spin_lock_irqsave(&cor_keepalive_req_lock, iflags);

		if (list_empty(&cor_keepalive_req_list)) {
			spin_unlock_irqrestore(&cor_keepalive_req_lock, iflags);
			break;
		}

		ed = container_of(cor_keepalive_req_list.next,
				struct cor_conn_src_sock_extradata,
				keepalive_lh);
		src_sock_o = ed->src_sock;
		BUG_ON(src_sock_o->src.sock.ed != ed);

		list_del(&src_sock_o->src.sock.ed->keepalive_lh);
		src_sock_o->src.sock.ed->in_keepalive_list = 0;

		spin_unlock_irqrestore(&cor_keepalive_req_lock, iflags);


		spin_lock_bh(&src_sock_o->rcv_lock);

		BUG_ON(src_sock_o->sourcetype != SOURCE_SOCK);

		if (likely(src_sock_o->isreset != 0)) {
			spin_unlock_bh(&src_sock_o->rcv_lock);
		} else if (src_sock_o->src.sock.keepalive_intransit == 0) {
			cor_mngdsocket_flushtoconn_ctrl_send_keepalive_req(
					src_sock_o);
			cor_keepalive_req_sched_timer(src_sock_o);
			spin_unlock_bh(&src_sock_o->rcv_lock);
		} else {
			spin_unlock_bh(&src_sock_o->rcv_lock);
			cor_reset_conn(src_sock_o);
		}

		cor_conn_kref_put(src_sock_o, "keepalive_snd_list");
	}
}

void cor_keepalive_req_timerfunc(struct timer_list *retrans_conn_timer)
{
	struct cor_conn_src_sock_extradata *ed = container_of(
				retrans_conn_timer,
				struct cor_conn_src_sock_extradata,
				keepalive_timer);
	struct cor_conn *src_sock_o = ed->src_sock;

	spin_lock_bh(&src_sock_o->rcv_lock);

	BUG_ON(src_sock_o->sourcetype != SOURCE_SOCK);
	BUG_ON(src_sock_o->src.sock.ed != ed);

	if (likely(src_sock_o->isreset == 0) &&
			src_sock_o->src.sock.ed->in_keepalive_list == 0) {
		unsigned long iflags;
		int schedule_work_needed;

		spin_lock_irqsave(&cor_keepalive_req_lock, iflags);

		schedule_work_needed = list_empty(&cor_keepalive_req_list);

		list_add_tail(&src_sock_o->src.sock.ed->keepalive_lh,
				&cor_keepalive_req_list);
		src_sock_o->src.sock.ed->in_keepalive_list = 1;
		cor_conn_kref_get(src_sock_o, "keepalive_snd_list");

		if (schedule_work_needed)
			schedule_work(&cor_keepalive_req_work);

		spin_unlock_irqrestore(&cor_keepalive_req_lock, iflags);
	}

	spin_unlock_bh(&src_sock_o->rcv_lock);

	cor_conn_kref_put(src_sock_o, "keepalive_snd_timer");
}

void cor_keepalive_req_sched_timer(struct cor_conn *src_sock_lx)
{
	unsigned long timeout;

	BUG_ON(src_sock_lx->sourcetype != SOURCE_SOCK);

	BUG_ON(src_sock_lx->src.sock.socktype != SOCKTYPE_MANAGED);

	if (src_sock_lx->src.sock.keepalive_intransit == 0) {
		timeout = src_sock_lx->src.sock.ed->jiffies_keepalive_lastact +
				KEEPALIVE_INTERVAL_SECS * HZ;
	} else {
		timeout = src_sock_lx->src.sock.ed->jiffies_keepalive_lastact +
				KEEPALIVE_TIMEOUT_SECS * HZ;
	}

	if (mod_timer(&src_sock_lx->src.sock.ed->keepalive_timer, timeout) == 0)
		cor_conn_kref_get(src_sock_lx, "keepalive_snd_timer");
}

void cor_keepalive_resp_rcvd(struct cor_sock *cs_m_l, __be32 cookie)
{
	int reset_needed = 0;
	struct cor_conn *src_sock;

	BUG_ON(cs_m_l->type != CS_TYPE_CONN_MANAGED);
	src_sock = cs_m_l->data.conn_managed.src_sock;

	if (unlikely(src_sock == 0))
		return;

	spin_lock_bh(&src_sock->rcv_lock);

	if (unlikely(cor_is_src_sock(src_sock, cs_m_l) == 0 ||
			src_sock->isreset != 0)) {
		cs_m_l->data.conn_managed.is_reset = 1;
		cor_sk_data_ready(cs_m_l);
		cor_flush_buf(src_sock);
		goto out_err;
	}

	if (cookie != src_sock->src.sock.ed->keepalive_req_cookie) {
		reset_needed = 1;
	} else {
		src_sock->src.sock.keepalive_intransit = 0;
		src_sock->src.sock.ed->jiffies_keepalive_lastact = jiffies;
		cor_keepalive_req_sched_timer(src_sock);
	}

	spin_unlock_bh(&src_sock->rcv_lock);

out_err:
	if (reset_needed)
		cor_reset_conn(src_sock);
}

int __init cor_conn_src_sock_init1(void)
{
	INIT_DELAYED_WORK(&cor_flushtoconn_oom_work,
			cor_mngdsocket_flushtoconn_oomresume);

	return 0;
}

void __exit cor_conn_src_sock_exit1(void)
{
	flush_delayed_work(&cor_flushtoconn_oom_work);
}

MODULE_LICENSE("GPL");
