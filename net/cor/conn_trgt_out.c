/**
 *	Connection oriented routing
 *	Copyright (C) 2007-2021 Michael Blizek
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License
 *	as published by the Free Software Foundation; either version 2
 *	of the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 */

#include <linux/gfp.h>
#include <linux/jiffies.h>
#include <linux/slab.h>

#include "cor.h"

static struct kmem_cache *cor_connretrans_slab;


void cor_free_connretrans(struct kref *ref)
{
	struct cor_conn_retrans *cr = container_of(ref, struct cor_conn_retrans,
			ref);
	struct cor_conn *cn = cr->trgt_out_o;

	BUG_ON(cr->state != CONN_RETRANS_ACKED);

	kmem_cache_free(cor_connretrans_slab, cr);
	cor_conn_kref_put(cn, "conn_retrans");
}

static struct cor_conn_retrans *cor_peek_next_conn_retrans(
		struct cor_neighbor *nb_retransconnlocked)
{
	struct cor_conn_retrans *cr1 = 0;
	struct cor_conn_retrans *cr2 = 0;

	if (list_empty(&nb_retransconnlocked->retrans_conn_lowlatency_list) == 0)
		cr1 = container_of(
				nb_retransconnlocked->retrans_conn_lowlatency_list.next,
				struct cor_conn_retrans, timeout_list);

	if (list_empty(&nb_retransconnlocked->retrans_conn_highlatency_list) == 0)
		cr2 = container_of(
				nb_retransconnlocked->retrans_conn_highlatency_list.next,
				struct cor_conn_retrans, timeout_list);

	if (cr1 == 0)
		return cr2;
	if (cr2 == 0)
		return cr1;

	if (time_before_eq(cr1->timeout, jiffies))
		return cr1;

	if (time_before_eq(cr1->timeout, cr2->timeout))
		return cr1;

	return cr2;
}

void cor_reschedule_conn_retrans_timer(
		struct cor_neighbor *nb_retransconnlocked)
{
	struct cor_conn_retrans *cr =
			cor_peek_next_conn_retrans(nb_retransconnlocked);

	if (cr == 0)
		return;

	if (time_before_eq(cr->timeout, jiffies)) {
		cor_dev_queue_enqueue(nb_retransconnlocked->cd,
				&nb_retransconnlocked->rb_cr, 0,
				ns_to_ktime(0), QOS_CALLER_CONN_RETRANS, 0);
	}  else {
		if (mod_timer(&nb_retransconnlocked->retrans_conn_timer,
				cr->timeout) == 0) {
			cor_nb_kref_get(nb_retransconnlocked,
					"retrans_conn_timer");
		}
	}
}

/**
 * caller must also call kref_get/put, see cor_reschedule_conn_retrans_timer
 */
static void cor_cancel_conn_retrans(struct cor_neighbor *nb_retransconnlocked,
		struct cor_conn *trgt_out_lx, struct cor_conn_retrans *cr,
		__u64 *bytes_acked)
{
	if (unlikely(cr->state == CONN_RETRANS_ACKED))
		return;

	if (cr->state == CONN_RETRANS_SCHEDULED) {
		list_del(&cr->timeout_list);
		kref_put(&cr->ref, cor_kreffree_bug);
	} else if (cr->state == CONN_RETRANS_LOWWINDOW) {
		BUG_ON(trgt_out_lx->trgt.out.retrans_lowwindow == 0);
		if (likely(trgt_out_lx->trgt.out.retrans_lowwindow != 65535))
			trgt_out_lx->trgt.out.retrans_lowwindow--;
	}

	if (cr->state != CONN_RETRANS_INITIAL)
		*bytes_acked += cr->length;

	list_del(&cr->conn_list);
	cr->state = CONN_RETRANS_ACKED;

	kref_put(&cr->ref, cor_free_connretrans); /* conn_list */
}

/**
 * nb->retrans_conn_lock must be held when calling this
 * (see cor_schedule_retransmit_conn())
 */
static void cor_cancel_acked_conn_retrans(struct cor_conn *trgt_out_l,
		__u64 *bytes_acked)
{
	__u32 seqno_acked = trgt_out_l->trgt.out.seqno_acked;

	while (list_empty(&trgt_out_l->trgt.out.retrans_list) == 0) {
		struct cor_conn_retrans *cr = container_of(
				trgt_out_l->trgt.out.retrans_list.next,
				struct cor_conn_retrans, conn_list);

		if (cor_seqno_after(cr->seqno + cr->length, seqno_acked)) {
			if (cor_seqno_before(cr->seqno, seqno_acked)) {
				*bytes_acked += seqno_acked - cr->seqno;
				cr->length -= seqno_acked - cr->seqno;
				cr->seqno = seqno_acked;
			}
			break;
		}

		cor_cancel_conn_retrans(trgt_out_l->trgt.out.nb, trgt_out_l, cr,
				bytes_acked);
	}

	cor_reschedule_conn_retrans_timer(trgt_out_l->trgt.out.nb);
}

void cor_cancel_all_conn_retrans(struct cor_conn *trgt_out_lx)
{
	struct cor_neighbor *nb = trgt_out_lx->trgt.out.nb;
	__u64 bytes_acked = 0;

	spin_lock_bh(&nb->retrans_conn_lock);

	while (list_empty(&trgt_out_lx->trgt.out.retrans_list) == 0) {
		struct cor_conn_retrans *cr = container_of(
				trgt_out_lx->trgt.out.retrans_list.next,
				struct cor_conn_retrans, conn_list);
		BUG_ON(cr->trgt_out_o != trgt_out_lx);

		cor_cancel_conn_retrans(nb, trgt_out_lx, cr, &bytes_acked);
	}

	cor_reschedule_conn_retrans_timer(nb);

	spin_unlock_bh(&nb->retrans_conn_lock);

	if (bytes_acked > 0)
		cor_nbcongwin_data_acked(nb, bytes_acked);
}

static void _cor_cancel_all_conn_retrans_nb(struct cor_neighbor *nb,
		struct list_head *retrans_list, __u64 *bytes_acked)
{
	while (1) {
		struct cor_conn_retrans *cr;

		spin_lock_bh(&nb->retrans_conn_lock);

		if (list_empty(retrans_list)) {
			spin_unlock_bh(&nb->retrans_conn_lock);
			break;
		}

		cr = container_of(retrans_list->next, struct cor_conn_retrans,
				timeout_list);

		kref_get(&cr->ref);

		spin_unlock_bh(&nb->retrans_conn_lock);


		spin_lock_bh(&cr->trgt_out_o->rcv_lock);
		spin_lock_bh(&nb->retrans_conn_lock);

		if (likely(cr == container_of(retrans_list->next,
				struct cor_conn_retrans, timeout_list)))
			cor_cancel_conn_retrans(nb, cr->trgt_out_o, cr,
					bytes_acked);

		spin_unlock_bh(&nb->retrans_conn_lock);
		spin_unlock_bh(&cr->trgt_out_o->rcv_lock);

		kref_put(&cr->ref, cor_free_connretrans);
	}
}

static void cor_cancel_all_conn_retrans_nb(struct cor_neighbor *nb)
{
	__u64 bytes_acked = 0;

	_cor_cancel_all_conn_retrans_nb(nb,
			&nb->retrans_conn_lowlatency_list, &bytes_acked);
	_cor_cancel_all_conn_retrans_nb(nb,
			&nb->retrans_conn_highlatency_list, &bytes_acked);

	if (bytes_acked > 0)
		cor_nbcongwin_data_acked(nb, bytes_acked);
}

static struct cor_conn_retrans *cor_prepare_conn_retrans(
		struct cor_conn *trgt_out_l, __u32 seqno, __u32 len,
		__u8 windowused, struct cor_conn_retrans *cr_splitted,
		int retransconnlocked)
{
	struct cor_neighbor *nb = trgt_out_l->trgt.out.nb;

	struct cor_conn_retrans *cr = kmem_cache_alloc(cor_connretrans_slab,
			GFP_ATOMIC);

	if (unlikely(cr == 0))
		return 0;

	BUG_ON(trgt_out_l->isreset != 0);

	memset(cr, 0, sizeof(struct cor_conn_retrans));
	cr->trgt_out_o = trgt_out_l;
	cor_conn_kref_get(trgt_out_l, "conn_retrans");
	cr->seqno = seqno;
	cr->length = len;
	cr->windowused = windowused;
	kref_init(&cr->ref);

	if (retransconnlocked == 0)
		spin_lock_bh(&nb->retrans_conn_lock);

	if (cr_splitted != 0)
		list_add(&cr->conn_list, &cr_splitted->conn_list);
	else
		list_add_tail(&cr->conn_list,
				&cr->trgt_out_o->trgt.out.retrans_list);
	kref_get(&cr->ref); /* conn_list */

	if (retransconnlocked == 0)
		spin_unlock_bh(&nb->retrans_conn_lock);

	return cr;
}

#define RC_SENDRETRANS_OK 0
#define RC_SENDRETRANS_OOM 1
#define RC_SENDRETRANS_QUEUEFULL 2
#define RC_SENDRETRANS_QUEUEFULLDROPPED 3

static int __cor_send_retrans(struct cor_neighbor *nb,
		struct cor_conn *trgt_out_l, struct cor_conn_retrans *cr,
		__u64 *bytes_sent)
{
	__u8 flush = 0;

	BUG_ON(cr->length == 0);

	if (trgt_out_l->flush != 0 && cor_seqno_eq(cr->seqno + cr->length,
			trgt_out_l->trgt.out.seqno_nextsend) &&
			trgt_out_l->data_buf.read_remaining == 0)
		flush = 1;

	if (cor_send_conndata_as_skb(nb, cr->length)) {
		struct sk_buff *skb;
		char *dst;
		int rc;

		skb = cor_create_packet_conndata(nb, cr->length, GFP_ATOMIC,
				trgt_out_l->trgt.out.conn_id, cr->seqno,
				cr->windowused, flush);
		if (unlikely(skb == 0))
			return RC_SENDRETRANS_OOM;

		dst = skb_put(skb, cr->length);

		cor_databuf_pullold(trgt_out_l, cr->seqno, dst, cr->length);

		rc = cor_dev_queue_xmit(skb, nb->cd,
				QOS_CALLER_CONN_RETRANS);
		if (rc == NET_XMIT_DROP)
			return RC_SENDRETRANS_QUEUEFULLDROPPED;
		cor_schedule_retransmit_conn(cr, 1, 0);
		if (rc != NET_XMIT_SUCCESS)
			return RC_SENDRETRANS_QUEUEFULL;

	} else {
		struct cor_control_msg_out *cm;
		char *buf;

		buf = kmalloc(cr->length, GFP_ATOMIC);
		if (unlikely(buf == 0))
			return RC_SENDRETRANS_OOM;

		cm = cor_alloc_control_msg(nb, ACM_PRIORITY_LOW);
		if (unlikely(cm == 0)) {
			kfree(buf);
			return RC_SENDRETRANS_OOM;
		}

		cor_databuf_pullold(trgt_out_l, cr->seqno, buf, cr->length);

		cor_send_conndata(cm, trgt_out_l->trgt.out.conn_id, cr->seqno,
				buf, buf, cr->length, cr->windowused,
				flush, trgt_out_l->is_highlatency, cr);
	}

	*bytes_sent += cr->length;

	return RC_SENDRETRANS_OK;
}

static int _cor_send_retrans_splitcr_ifneeded(
		struct cor_neighbor *nb_retransconnlocked,
		struct cor_conn *trgt_out_l, struct cor_conn_retrans *cr)
{
	__u32 targetmss = cor_mss_conndata(nb_retransconnlocked,
			trgt_out_l->is_highlatency != 0);
	__u32 windowlimit = trgt_out_l->trgt.out.seqno_windowlimit - cr->seqno;
	__u32 maxsize = targetmss;

	if (windowlimit < maxsize)
		maxsize = windowlimit;

	if (unlikely(cr->length > maxsize)) {
		struct cor_conn_retrans *cr2 = cor_prepare_conn_retrans(
				trgt_out_l, cr->seqno + maxsize,
				cr->length - maxsize, cr->windowused, cr, 1);

		if (unlikely(cr2 == 0))
			return RC_SENDRETRANS_OOM;

		cr2->timeout = cr->timeout;

		if (trgt_out_l->is_highlatency)
			list_add(&cr2->timeout_list,
					&nb_retransconnlocked->retrans_conn_highlatency_list);
		else
			list_add(&cr2->timeout_list,
					&nb_retransconnlocked->retrans_conn_lowlatency_list);
		/* cr2: kref from alloc goes to kref for timeout_list */
		cr2->state = CONN_RETRANS_SCHEDULED;

		cr->length = maxsize;
	}

	return RC_SENDRETRANS_OK;
}

static int _cor_send_retrans(struct cor_neighbor *nb,
		struct cor_conn_retrans *cr, __u64 *bytes_sent)
{
	struct cor_conn *trgt_out_o = cr->trgt_out_o;
	int rc;

	spin_lock_bh(&trgt_out_o->rcv_lock);

	BUG_ON(trgt_out_o->targettype != TARGET_OUT);
	BUG_ON(trgt_out_o->trgt.out.nb != nb);

	spin_lock_bh(&nb->retrans_conn_lock);
	if (unlikely(cr->state == CONN_RETRANS_ACKED)) {
		spin_unlock_bh(&nb->retrans_conn_lock);
		spin_unlock_bh(&trgt_out_o->rcv_lock);
		return 0;
	}

	BUG_ON(trgt_out_o->isreset != 0);

	BUG_ON(cor_seqno_before(cr->seqno, trgt_out_o->trgt.out.seqno_acked));

	if (cor_seqno_after_eq(cr->seqno,
			trgt_out_o->trgt.out.seqno_windowlimit)) {
		BUG_ON(cr->state != CONN_RETRANS_SENDING);
		cr->state = CONN_RETRANS_LOWWINDOW;
		if (likely(trgt_out_o->trgt.out.retrans_lowwindow != 65535))
			trgt_out_o->trgt.out.retrans_lowwindow++;

		spin_unlock_bh(&nb->retrans_conn_lock);
		spin_unlock_bh(&trgt_out_o->rcv_lock);
		return 0;
	}

	rc = _cor_send_retrans_splitcr_ifneeded(nb, trgt_out_o, cr);

	spin_unlock_bh(&nb->retrans_conn_lock);

	cor_conn_kref_get(trgt_out_o, "stack");

	if (rc == RC_SENDRETRANS_OK)
		rc = __cor_send_retrans(nb, trgt_out_o, cr, bytes_sent);

	if (rc == RC_SENDRETRANS_OOM || rc == RC_SENDRETRANS_QUEUEFULLDROPPED) {
		spin_lock_bh(&nb->retrans_conn_lock);
		if (unlikely(cr->state == CONN_RETRANS_ACKED)) {
		} else if (likely(cr->state == CONN_RETRANS_SENDING)) {
			if (rc == RC_SENDRETRANS_OOM)
				cr->timeout = jiffies + 1;
			if (trgt_out_o->is_highlatency)
				list_add(&cr->timeout_list,
						&nb->retrans_conn_highlatency_list);
			else
				list_add(&cr->timeout_list,
						&nb->retrans_conn_lowlatency_list);
			kref_get(&cr->ref);
			cr->state = CONN_RETRANS_SCHEDULED;
		} else {
			BUG();
		}
		spin_unlock_bh(&nb->retrans_conn_lock);
	}

	spin_unlock_bh(&trgt_out_o->rcv_lock);

	cor_conn_kref_put(trgt_out_o, "stack");

	return (rc == RC_SENDRETRANS_OOM ||
			rc == RC_SENDRETRANS_QUEUEFULL ||
			rc == RC_SENDRETRANS_QUEUEFULLDROPPED);
}

int cor_send_retrans(struct cor_neighbor *nb, int *sent)
{
	int queuefull = 0;
	int nbstate = cor_get_neigh_state(nb);
	__u64 bytes_sent = 0;

	if (unlikely(nbstate == NEIGHBOR_STATE_STALLED)) {
		return QOS_RESUME_DONE;
	} else if (unlikely(nbstate == NEIGHBOR_STATE_KILLED)) {
		/**
		 * cor_cancel_all_conn_retrans_nb should not be needed, because
		 * cor_reset_all_conns calls cor_cancel_all_conn_retrans
		 */
		cor_cancel_all_conn_retrans_nb(nb);
		return QOS_RESUME_DONE;
	}

	while (1) {
		struct cor_conn_retrans *cr = 0;

		spin_lock_bh(&nb->retrans_conn_lock);

		cr = cor_peek_next_conn_retrans(nb);
		if (cr == 0) {
			spin_unlock_bh(&nb->retrans_conn_lock);
			break;
		}

		BUG_ON(cr->state != CONN_RETRANS_SCHEDULED);

		if (time_after(cr->timeout, jiffies)) {
			cor_reschedule_conn_retrans_timer(nb);
			spin_unlock_bh(&nb->retrans_conn_lock);
			break;
		}

		list_del(&cr->timeout_list);
		cr->state = CONN_RETRANS_SENDING;

		spin_unlock_bh(&nb->retrans_conn_lock);

		queuefull = _cor_send_retrans(nb, cr, &bytes_sent);
		kref_put(&cr->ref, cor_free_connretrans); /* timeout_list */
		if (queuefull)
			break;

		*sent = 1;
	}

	if (bytes_sent > 0)
		cor_nbcongwin_data_retransmitted(nb, bytes_sent);

	return queuefull ? QOS_RESUME_CONG : QOS_RESUME_DONE;
}

void cor_retransmit_conn_timerfunc(struct timer_list *retrans_conn_timer)
{
	struct cor_neighbor *nb = container_of(retrans_conn_timer,
			struct cor_neighbor, retrans_conn_timer);
	cor_dev_queue_enqueue(nb->cd, &nb->rb_cr, 0, ns_to_ktime(0),
			QOS_CALLER_CONN_RETRANS, 0);
	cor_nb_kref_put(nb, "retransmit_conn_timer");
}

static void cor_conn_ack_ooo_rcvd_splitcr(struct cor_conn *trgt_out_l,
		struct cor_conn_retrans *cr, __u32 seqno_ooo, __u32 length,
		__u64 *bytes_acked)
{
	struct cor_conn_retrans *cr2;
	__u32 seqno_cr2start;
	__u32 oldcrlenght = cr->length;

	if (cr->state != CONN_RETRANS_SCHEDULED &&
			cr->state != CONN_RETRANS_LOWWINDOW)
		return;

	seqno_cr2start = seqno_ooo + length;
	cr2 = cor_prepare_conn_retrans(trgt_out_l, seqno_cr2start,
			cr->seqno + cr->length - seqno_cr2start,
			cr->windowused, cr, 1);

	if (unlikely(cr2 == 0))
		return;

	BUG_ON(cr2->length > cr->length);

	cr2->timeout = cr->timeout;
	cr2->state = cr->state;

	if (cr->state != CONN_RETRANS_SCHEDULED) {
		list_add(&cr2->timeout_list, &cr->timeout_list);
		kref_get(&cr2->ref);
	}

	BUG_ON(seqno_ooo - cr->seqno > cr->length);

	cr->length -= seqno_ooo - cr->seqno;
	BUG_ON(cr->length + length + cr2->length != oldcrlenght);
	kref_put(&cr2->ref, cor_kreffree_bug); /* alloc */

	*bytes_acked += length;
}

void cor_conn_ack_ooo_rcvd(struct cor_neighbor *nb, __u32 conn_id,
		struct cor_conn *trgt_out, __u32 seqno_ooo, __u32 length,
		__u64 *bytes_acked)
{
	struct list_head *curr;

	if (unlikely(length == 0))
		return;

	spin_lock_bh(&trgt_out->rcv_lock);

	if (unlikely(trgt_out->targettype != TARGET_OUT))
		goto out;
	if (unlikely(trgt_out->trgt.out.nb != nb))
		goto out;
	if (unlikely(trgt_out->trgt.out.conn_id != conn_id))
		goto out;

	cor_nb_kref_get(nb, "stack");
	spin_lock_bh(&nb->retrans_conn_lock);

	curr = trgt_out->trgt.out.retrans_list.next;
	while (curr != &trgt_out->trgt.out.retrans_list) {
		struct cor_conn_retrans *cr = container_of(curr,
				struct cor_conn_retrans, conn_list);

		int ack_covers_start = cor_seqno_after_eq(cr->seqno, seqno_ooo);
		int ack_covers_end = cor_seqno_before_eq(cr->seqno + cr->length,
				seqno_ooo + length);

		curr = curr->next;

		if (cor_seqno_before(cr->seqno + cr->length, seqno_ooo))
			continue;

		if (cor_seqno_after(cr->seqno, seqno_ooo + length))
			break;

		if (likely(ack_covers_start && ack_covers_end)) {
			cor_cancel_conn_retrans(nb, trgt_out, cr, bytes_acked);
			cor_reschedule_conn_retrans_timer(nb);
		} else if (ack_covers_start) {
			__u32 diff = seqno_ooo + length - cr->seqno -
					cr->length;

			BUG_ON(diff >= cr->length);
			cr->seqno += diff;
			cr->length -= diff;
			*bytes_acked += diff;
		} else if (ack_covers_end) {
			__u32 diff = seqno_ooo + length - cr->seqno;
			BUG_ON(diff >= length);
			cr->length -= diff;
			*bytes_acked += diff;
		} else {
			cor_conn_ack_ooo_rcvd_splitcr(trgt_out, cr, seqno_ooo,
					length, bytes_acked);
			break;
		}
	}

	if (unlikely(list_empty(&trgt_out->trgt.out.retrans_list) == 0)) {
		trgt_out->trgt.out.seqno_acked =
				trgt_out->trgt.out.seqno_nextsend;
	} else {
		struct cor_conn_retrans *cr = container_of(
				trgt_out->trgt.out.retrans_list.next,
				struct cor_conn_retrans, conn_list);
		if (cor_seqno_after(cr->seqno,
				trgt_out->trgt.out.seqno_acked))
			trgt_out->trgt.out.seqno_acked = cr->seqno;
	}

	spin_unlock_bh(&nb->retrans_conn_lock);
	cor_nb_kref_put(nb, "stack");

out:
	spin_unlock_bh(&trgt_out->rcv_lock);
}

static void _cor_conn_ack_rcvd_nosendwin(struct cor_conn *trgt_out_l)
{
	if (trgt_out_l->sourcetype != SOURCE_IN ||
			trgt_out_l->is_highlatency != 0)
		return;

	if (trgt_out_l->bufsize.state == BUFSIZE_INCR ||
			trgt_out_l->bufsize.state == BUFSIZE_INCR_FAST)
		trgt_out_l->bufsize.state = BUFSIZE_NOACTION;

	if (trgt_out_l->bufsize.state == BUFSIZE_NOACTION)
		trgt_out_l->bufsize.act.noact.bytesleft = max(
				trgt_out_l->bufsize.act.noact.bytesleft,
				(__u32) BUF_OUT_WIN_NOK_NOINCR);

	trgt_out_l->bufsize.ignore_rcv_lowbuf = max(
			trgt_out_l->bufsize.ignore_rcv_lowbuf,
			(__u32) BUF_OUT_WIN_NOK_NOINCR);
}

/**
 * nb->retrans_conn_lock must be held when calling this
 * (see cor_schedule_retransmit_conn())
 */
static void cor_reschedule_lowwindow_retrans(struct cor_conn *trgt_out_l)
{
	struct list_head *lh = trgt_out_l->trgt.out.retrans_list.next;
	int cnt = 0;

	while (trgt_out_l->trgt.out.retrans_lowwindow > 0 && cnt < 100) {
		struct cor_conn_retrans *cr;

		if (unlikely(lh == &trgt_out_l->trgt.out.retrans_list)) {
			BUG_ON(trgt_out_l->trgt.out.retrans_lowwindow != 65535);
			trgt_out_l->trgt.out.retrans_lowwindow = 0;
			break;
		}

		cr = container_of(lh, struct cor_conn_retrans, conn_list);

		if (cor_seqno_after_eq(cr->seqno,
				trgt_out_l->trgt.out.seqno_windowlimit)) {
			break;
		}

		if (cr->state == CONN_RETRANS_LOWWINDOW)
			cor_schedule_retransmit_conn(cr, 1, 1);

		lh = lh->next;
		cnt++;
	}
}

void cor_conn_ack_rcvd(struct cor_neighbor *nb, __u32 conn_id,
		struct cor_conn *trgt_out, __u32 seqno, int setwindow,
		__u8 window, __u8 bufsize_changerate, __u64 *bytes_acked)
{
	int seqno_advanced = 0;
	int window_enlarged = 0;

	spin_lock_bh(&trgt_out->rcv_lock);

	/* printk(KERN_ERR "ack rcvd %x %u (+%u) %u %u %u \n", conn_id, seqno,
			seqno - trgt_out->trgt.out.seqno_acked,
			cor_dec_window(window),
			trgt_out->bufsize.bufsize >> BUFSIZE_SHIFT,
			trgt_out->data_buf.read_remaining); */

	if (unlikely(trgt_out->isreset != 0))
		goto out;
	if (unlikely(trgt_out->targettype != TARGET_OUT))
		goto out;
	if (unlikely(trgt_out->trgt.out.nb != nb))
		goto out;
	if (unlikely(cor_get_connid_reverse(trgt_out->trgt.out.conn_id) !=
			conn_id))
		goto out;

	if (unlikely(cor_seqno_after(seqno,
			trgt_out->trgt.out.seqno_nextsend) ||
			cor_seqno_before(seqno,
			trgt_out->trgt.out.seqno_acked)))
		goto out;

	if (setwindow) {
		__u32 windowdec = cor_dec_log_64_11(window);

		if (likely(cor_seqno_after(seqno,
				trgt_out->trgt.out.seqno_acked)) ||
				cor_seqno_after(seqno + windowdec,
				trgt_out->trgt.out.seqno_windowlimit)) {
			trgt_out->trgt.out.seqno_windowlimit = seqno +
					windowdec;
			window_enlarged = 1;
		}
		trgt_out->trgt.out.remote_bufsize_changerate =
				bufsize_changerate;
	}

	if (cor_seqno_after(seqno, trgt_out->trgt.out.seqno_acked))
		seqno_advanced = 1;

	if (seqno_advanced == 0 && window_enlarged == 0)
		goto out;

	cor_nb_kref_get(nb, "stack");
	spin_lock_bh(&nb->retrans_conn_lock);

	if (seqno_advanced) {
		trgt_out->trgt.out.seqno_acked = seqno;
		cor_cancel_acked_conn_retrans(trgt_out, bytes_acked);
	}

	if (window_enlarged)
		cor_reschedule_lowwindow_retrans(trgt_out);

	spin_unlock_bh(&nb->retrans_conn_lock);
	cor_nb_kref_put(nb, "stack");

	if (seqno_advanced)
		cor_databuf_ack(trgt_out, trgt_out->trgt.out.seqno_acked);

	if (cor_seqno_eq(trgt_out->trgt.out.seqno_acked,
			trgt_out->trgt.out.seqno_nextsend))
		_cor_conn_ack_rcvd_nosendwin(trgt_out);

out:
	if (seqno_advanced || window_enlarged)
		cor_flush_buf(trgt_out);

	spin_unlock_bh(&trgt_out->rcv_lock);

	cor_wake_sender(trgt_out);
}

static void cor_try_combine_conn_retrans_prev(
		struct cor_neighbor *nb_retransconnlocked,
		struct cor_conn *trgt_out_lx, struct cor_conn_retrans *cr)
{
	struct cor_conn_retrans *cr_prev;
	__u64 bytes_dummyacked = 0;

	BUG_ON(cr->state != CONN_RETRANS_SCHEDULED);

	if (cr->conn_list.prev == &trgt_out_lx->trgt.out.retrans_list)
		return;

	cr_prev = container_of(cr->conn_list.prev, struct cor_conn_retrans,
			conn_list);

	if (cr_prev->state != CONN_RETRANS_SCHEDULED)
		return;
	if (cr_prev->timeout != cr->timeout)
		return;
	if (!cor_seqno_eq(cr_prev->seqno + cr_prev->length, cr->seqno))
		return;

	cr->seqno -= cr_prev->length;
	cr->length += cr_prev->length;

	cor_cancel_conn_retrans(nb_retransconnlocked, trgt_out_lx, cr_prev,
			&bytes_dummyacked);
}

static void cor_try_combine_conn_retrans_next(
		struct cor_neighbor *nb_retranslocked,
		struct cor_conn *trgt_out_lx, struct cor_conn_retrans *cr)
{
	struct cor_conn_retrans *cr_next;
	__u64 bytes_dummyacked = 0;

	BUG_ON(cr->state != CONN_RETRANS_SCHEDULED);

	if (cr->conn_list.next == &trgt_out_lx->trgt.out.retrans_list)
		return;

	cr_next = container_of(cr->conn_list.next, struct cor_conn_retrans,
			conn_list);

	if (cr_next->state != CONN_RETRANS_SCHEDULED)
		return;
	if (cr_next->timeout != cr->timeout)
		return;
	if (!cor_seqno_eq(cr->seqno + cr->length, cr_next->seqno))
		return;

	cr->length += cr_next->length;

	cor_cancel_conn_retrans(nb_retranslocked, trgt_out_lx, cr_next,
			&bytes_dummyacked);
}

void cor_schedule_retransmit_conn(struct cor_conn_retrans *cr, int connlocked,
		int nbretransconn_locked)
{
	struct cor_conn *trgt_out_o = cr->trgt_out_o;
	struct cor_neighbor *nb;
	int first;

	if (connlocked == 0)
		spin_lock_bh(&trgt_out_o->rcv_lock);

	BUG_ON(trgt_out_o->targettype != TARGET_OUT);
	nb = trgt_out_o->trgt.out.nb;

	cr->timeout = cor_calc_timeout(
			atomic_read(&nb->latency_retrans_us),
			atomic_read(&nb->latency_stddev_retrans_us),
			atomic_read(&nb->max_remote_ackconn_delay_us));

	if (trgt_out_o->is_highlatency)
		cr->timeout += 1 + msecs_to_jiffies(
				atomic_read(&nb->latency_retrans_us)/1000 +
				CMSG_MAXDELAY_ACK_FAST_MS);

	if (nbretransconn_locked == 0)
		spin_lock_bh(&nb->retrans_conn_lock);

	cor_nb_kref_get(nb, "stack");

	BUG_ON(cr->state == CONN_RETRANS_SCHEDULED);

	if (unlikely(cr->state == CONN_RETRANS_ACKED)) {
		goto out;
	} else if (unlikely(cr->state == CONN_RETRANS_LOWWINDOW)) {
		BUG_ON(trgt_out_o->trgt.out.retrans_lowwindow == 0);
		if (likely(trgt_out_o->trgt.out.retrans_lowwindow != 65535))
			trgt_out_o->trgt.out.retrans_lowwindow--;
	}

	if (trgt_out_o->is_highlatency) {
		first = unlikely(list_empty(
				&nb->retrans_conn_highlatency_list));
		list_add_tail(&cr->timeout_list,
				&nb->retrans_conn_highlatency_list);
	} else {
		first = unlikely(list_empty(
				&nb->retrans_conn_lowlatency_list));
		list_add_tail(&cr->timeout_list,
				&nb->retrans_conn_lowlatency_list);
	}
	kref_get(&cr->ref);
	cr->state = CONN_RETRANS_SCHEDULED;

	if (unlikely(first)) {
		cor_reschedule_conn_retrans_timer(nb);
	} else {
		cor_try_combine_conn_retrans_prev(nb, trgt_out_o, cr);
		cor_try_combine_conn_retrans_next(nb, trgt_out_o, cr);
	}

out:
	if (nbretransconn_locked == 0)
		spin_unlock_bh(&nb->retrans_conn_lock);

	cor_nb_kref_put(nb, "stack");

	if (connlocked == 0)
		spin_unlock_bh(&trgt_out_o->rcv_lock);
}

static int __cor_flush_out_skb(struct cor_conn *trgt_out_lx, __u32 len)
{
	struct cor_neighbor *nb = trgt_out_lx->trgt.out.nb;

	__u32 seqno;
	struct cor_conn_retrans *cr;
	struct sk_buff *skb;
	char *dst;
	__u8 flush;
	int rc;

	if (trgt_out_lx->flush != 0 &&
			trgt_out_lx->data_buf.read_remaining == len)
		flush = 1;

	seqno = trgt_out_lx->trgt.out.seqno_nextsend;
	skb = cor_create_packet_conndata(trgt_out_lx->trgt.out.nb, len,
			GFP_ATOMIC, trgt_out_lx->trgt.out.conn_id, seqno,
			trgt_out_lx->trgt.out.lastsend_windowused, flush);
	if (unlikely(skb == 0))
		return RC_FLUSH_CONN_OUT_OOM;

	cr = cor_prepare_conn_retrans(trgt_out_lx, seqno, len,
			trgt_out_lx->trgt.out.lastsend_windowused, 0, 0);
	if (unlikely(cr == 0)) {
		kfree_skb(skb);
		return RC_FLUSH_CONN_OUT_OOM;
	}

	dst = skb_put(skb, len);

	cor_databuf_pull(trgt_out_lx, dst, len);

	rc = cor_dev_queue_xmit(skb, nb->cd, QOS_CALLER_NEIGHBOR);
	if (rc == NET_XMIT_DROP) {
		cor_databuf_unpull(trgt_out_lx, len);
		spin_lock_bh(&nb->retrans_conn_lock);
		cor_cancel_conn_retrans(nb, trgt_out_lx, cr, 0);
		spin_unlock_bh(&nb->retrans_conn_lock);
		kref_put(&cr->ref, cor_free_connretrans); /* alloc */
		return RC_FLUSH_CONN_OUT_CONG;
	}

	trgt_out_lx->trgt.out.seqno_nextsend += len;
	cor_nbcongwin_data_sent(nb, len);
	cor_schedule_retransmit_conn(cr, 1, 0);
	if (trgt_out_lx->sourcetype == SOURCE_SOCK)
		cor_update_src_sock_sndspeed(trgt_out_lx, len);

	kref_put(&cr->ref, cor_free_connretrans); /* alloc */

	return (rc == NET_XMIT_SUCCESS) ?
			RC_FLUSH_CONN_OUT_OK : RC_FLUSH_CONN_OUT_SENT_CONG;
}

static int __cor_flush_out_conndata(struct cor_conn *trgt_out_lx, __u32 len)
{
	__u32 seqno;
	struct cor_control_msg_out *cm;
	struct cor_conn_retrans *cr;
	char *buf;
	__u8 flush = 0;

	if (trgt_out_lx->flush != 0 &&
			trgt_out_lx->data_buf.read_remaining == len)
		flush = 1;

	buf = kmalloc(len, GFP_ATOMIC);

	if (unlikely(buf == 0))
		return RC_FLUSH_CONN_OUT_OOM;

	cm = cor_alloc_control_msg(trgt_out_lx->trgt.out.nb, ACM_PRIORITY_LOW);
	if (unlikely(cm == 0)) {
		kfree(buf);
		return RC_FLUSH_CONN_OUT_OOM;
	}

	seqno = trgt_out_lx->trgt.out.seqno_nextsend;

	cr = cor_prepare_conn_retrans(trgt_out_lx, seqno, len,
			trgt_out_lx->trgt.out.lastsend_windowused, 0, 0);
	if (unlikely(cr == 0)) {
		kfree(buf);
		cor_free_control_msg(cm);
		return RC_FLUSH_CONN_OUT_OOM;
	}

	cor_databuf_pull(trgt_out_lx, buf, len);
	trgt_out_lx->trgt.out.seqno_nextsend += len;
	cor_nbcongwin_data_sent(trgt_out_lx->trgt.out.nb, len);
	if (trgt_out_lx->sourcetype == SOURCE_SOCK)
		cor_update_src_sock_sndspeed(trgt_out_lx, len);

	cor_send_conndata(cm, trgt_out_lx->trgt.out.conn_id, seqno, buf, buf,
			len, trgt_out_lx->trgt.out.lastsend_windowused, flush,
			trgt_out_lx->is_highlatency, cr);

	kref_put(&cr->ref, cor_free_connretrans); /* alloc */

	return RC_FLUSH_CONN_OUT_OK;
}

static void cor_set_last_windowused(struct cor_conn *trgt_out_lx)
{
	__u64 total_window;
	__u64 bytes_ackpending;

	if (trgt_out_lx->bufsize.ignore_rcv_lowbuf > 0) {
		trgt_out_lx->trgt.out.lastsend_windowused = 31;
		return;
	}


	BUG_ON(cor_seqno_before(trgt_out_lx->trgt.out.seqno_windowlimit,
			trgt_out_lx->trgt.out.seqno_acked));
	BUG_ON(cor_seqno_before(trgt_out_lx->trgt.out.seqno_nextsend,
			trgt_out_lx->trgt.out.seqno_acked));

	total_window = (__u64) (
			trgt_out_lx->trgt.out.seqno_windowlimit -
			trgt_out_lx->trgt.out.seqno_acked);
	bytes_ackpending = (__u64) (
			trgt_out_lx->trgt.out.seqno_nextsend -
			trgt_out_lx->trgt.out.seqno_acked);

	BUG_ON(bytes_ackpending > total_window);

	trgt_out_lx->trgt.out.lastsend_windowused = div64_u64(
			bytes_ackpending * 31 + total_window - 1, total_window);
}

static int __cor_flush_out(struct cor_neighbor *nb,
		struct cor_conn *trgt_out_lx, __u32 len, __u32 *sent,
		__u32 *maxsend_left)
{
	int rc;

	if (likely(cor_send_conndata_as_skb(nb, len)))
		rc = __cor_flush_out_skb(trgt_out_lx, len);
	else
		rc = __cor_flush_out_conndata(trgt_out_lx, len);

	if (rc == RC_FLUSH_CONN_OUT_OK ||
			rc == RC_FLUSH_CONN_OUT_SENT_CONG) {
		*maxsend_left -= len;
		*sent += len;
		cor_set_last_windowused(trgt_out_lx);
	}

	if (rc == RC_FLUSH_CONN_OUT_SENT_CONG)
		return RC_FLUSH_CONN_OUT_CONG;
	return rc;
}

int cor_srcin_buflimit_reached(struct cor_conn *src_in_lx)
{
	__u32 window_left;

	if (unlikely(cor_seqno_before(src_in_lx->src.in.window_seqnolimit,
			src_in_lx->src.in.next_seqno)))
		return 1;

	window_left = src_in_lx->src.in.window_seqnolimit -
			src_in_lx->src.in.next_seqno;

	if (window_left < WINDOW_ENCODE_MIN)
		return 1;

	if (window_left / 2 < src_in_lx->data_buf.read_remaining)
		return 1;

	return 0;
}

static __u32 cor_maxsend_left_to_len(__u32 maxsend_left)
{
	__u32 i;

	if (maxsend_left < 128)
		return maxsend_left;

	for (i = 128; i < 4096;) {
		if (i * 2 > maxsend_left)
			return i;
		i = i * 2;
	}

	return maxsend_left - maxsend_left % 4096;
}

static void _cor_flush_out_ignore_lowbuf(struct cor_conn *trgt_out_lx)
{
	if (trgt_out_lx->sourcetype == SOURCE_IN &&
			trgt_out_lx->is_highlatency == 0)
		trgt_out_lx->bufsize.ignore_rcv_lowbuf = max(
				trgt_out_lx->bufsize.ignore_rcv_lowbuf,
				trgt_out_lx->bufsize.bufsize >> BUFSIZE_SHIFT);
}

static __u32 cor_get_windowlimit(struct cor_conn *trgt_out_lx)
{
	if (unlikely(cor_seqno_before(trgt_out_lx->trgt.out.seqno_windowlimit,
			trgt_out_lx->trgt.out.seqno_nextsend)))
		return 0;

	return trgt_out_lx->trgt.out.seqno_windowlimit -
			trgt_out_lx->trgt.out.seqno_nextsend;
}

static int cor_delay_send(struct cor_conn *trgt_out_lx, __u32 len)
{
	__u32 data_inflight = trgt_out_lx->trgt.out.seqno_nextsend -
			trgt_out_lx->trgt.out.seqno_acked;
	int buflimit_reached;

	if (trgt_out_lx->sourcetype == SOURCE_IN) {
		buflimit_reached = cor_srcin_buflimit_reached(trgt_out_lx);
	} else if (trgt_out_lx->sourcetype == SOURCE_SOCK) {
		buflimit_reached = (cor_sock_sndbufavailable(
				trgt_out_lx, 1) == 0);
	} else if (trgt_out_lx->sourcetype == SOURCE_UNCONNECTED) {
		buflimit_reached = (cor_conn_src_unconn_write_allowed(
				trgt_out_lx) == 0);
	} else {
		WARN_ONCE(1, "cor_delay_send: invalid sourcetype");
		buflimit_reached = 1;
	}

	if ((trgt_out_lx->flush != 0 || buflimit_reached) &&
			data_inflight == 0)
		return 0;

	if (trgt_out_lx->flush == 0)
		return 1;

	if (trgt_out_lx->is_highlatency != 0)
		return 1;

	if (data_inflight > 0)
		return 1;

	return 0;
}

int _cor_flush_out(struct cor_conn *trgt_out_lx, __u32 maxsend, __u32 *sent,
		int from_qos, int maxsend_forcedelay)
{
	struct cor_neighbor *nb = trgt_out_lx->trgt.out.nb;
	__u32 maxsend_left = maxsend;
	__u32 mss;

	BUG_ON(trgt_out_lx->targettype != TARGET_OUT);

	if (unlikely(trgt_out_lx->trgt.out.established == 0) ||
			unlikely(trgt_out_lx->isreset != 0))
		return RC_FLUSH_CONN_OUT_OK;

	BUG_ON(trgt_out_lx->trgt.out.conn_id == 0);

	if (unlikely(trgt_out_lx->data_buf.read_remaining == 0))
		return RC_FLUSH_CONN_OUT_OK;

	if (from_qos == 0 && cor_qos_fastsend_allowed_conn(trgt_out_lx) == 0)
		return RC_FLUSH_CONN_OUT_CONG;

	cor_get_conn_idletime(trgt_out_lx);

	if (unlikely(cor_get_neigh_state(nb) != NEIGHBOR_STATE_ACTIVE))
		return RC_FLUSH_CONN_OUT_NBNOTACTIVE;

	/* printk(KERN_ERR "flush %p %u %u\n", trgt_out_lx,
			cor_get_windowlimit(trgt_out_lx),
			trgt_out_lx->data_buf.read_remaining); */

	mss = cor_mss_conndata(nb, trgt_out_lx->is_highlatency != 0);

	while (trgt_out_lx->data_buf.read_remaining >= mss &&
			maxsend_left >= mss) {
		__u32 windowlimit = cor_get_windowlimit(trgt_out_lx);
		int rc;

		if (cor_nbcongwin_send_allowed(nb) == 0)
			return RC_FLUSH_CONN_OUT_CONG;

		if (mss > windowlimit) {
			trgt_out_lx->trgt.out.lastsend_windowused = 31;
			_cor_flush_out_ignore_lowbuf(trgt_out_lx);
			break;
		}

		rc = __cor_flush_out(nb, trgt_out_lx, mss, sent, &maxsend_left);
		if (rc != RC_FLUSH_CONN_OUT_OK)
			return rc;
	}

	if (trgt_out_lx->data_buf.read_remaining > 0) {
		__u32 len = trgt_out_lx->data_buf.read_remaining;
		__u32 windowlimit = cor_get_windowlimit(trgt_out_lx);
		int rc;

		if (len > maxsend_left) {
			if (maxsend_left >= 65536 || (
					maxsend_left == maxsend &&
					maxsend_left >= 128 &&
					trgt_out_lx->is_highlatency == 0 &&
					!maxsend_forcedelay)) {
				len = cor_maxsend_left_to_len(maxsend_left);
			} else {
				return RC_FLUSH_CONN_OUT_MAXSENT;
			}
		}

		if (cor_delay_send(trgt_out_lx, len))
			return RC_FLUSH_CONN_OUT_OK;

		if (cor_nbcongwin_send_allowed(nb) == 0)
			return RC_FLUSH_CONN_OUT_CONG;

		if (len > windowlimit) {
			trgt_out_lx->trgt.out.lastsend_windowused = 31;
			_cor_flush_out_ignore_lowbuf(trgt_out_lx);

			if (windowlimit == 0 || cor_seqno_eq(
					trgt_out_lx->trgt.out.seqno_nextsend,
					trgt_out_lx->trgt.out.seqno_acked) == 0)
				return RC_FLUSH_CONN_OUT_OK;

			len = windowlimit;
		}

		rc = __cor_flush_out(nb, trgt_out_lx, len,sent, &maxsend_left);
		if (rc != RC_FLUSH_CONN_OUT_OK)
			return rc;
	}

	return RC_FLUSH_CONN_OUT_OK;
}

unsigned long cor_get_conn_idletime(struct cor_conn *trgt_out_lx)
{
	unsigned long jiffies_shifted = jiffies << JIFFIES_LAST_IDLE_SHIFT;
	__u32 burst_maxidle_secs;
	__u32 burst_maxidle_hz_shifted;
	unsigned long idletime_hz_shifted;

	if (trgt_out_lx->is_highlatency != 0)
		burst_maxidle_secs = BURSTPRIO_MAXIDLETIME_HIGHLATENCY_SECS;
	else
		burst_maxidle_secs = BURSTPRIO_MAXIDLETIME_LOWLATENCY_SECS;

	burst_maxidle_hz_shifted = (burst_maxidle_secs * HZ) <<
				JIFFIES_LAST_IDLE_SHIFT;

	idletime_hz_shifted = jiffies_shifted -
			trgt_out_lx->trgt.out.jiffies_idle_since;

	if (unlikely(idletime_hz_shifted > burst_maxidle_hz_shifted)) {
		idletime_hz_shifted = burst_maxidle_hz_shifted;
		trgt_out_lx->trgt.out.jiffies_idle_since = jiffies_shifted -
				burst_maxidle_hz_shifted;
	}

	return idletime_hz_shifted;
}

int __init cor_snd_init(void)
{
	cor_connretrans_slab = kmem_cache_create("cor_connretrans",
			sizeof(struct cor_conn_retrans), 8, 0, 0);
	if (unlikely(cor_connretrans_slab == 0))
		return -ENOMEM;

	return 0;
}

void __exit cor_snd_exit2(void)
{
	kmem_cache_destroy(cor_connretrans_slab);
	cor_connretrans_slab = 0;
}

MODULE_LICENSE("GPL");
