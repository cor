/**
 *	Connection oriented routing
 *	Copyright (C) 2007-2021 Michael Blizek
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License
 *	as published by the Free Software Foundation; either version 2
 *	of the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 */

#include <net/sock.h>
#include <linux/net.h>
#include <linux/uaccess.h>

#include "cor.h"

struct proto cor_proto = {
	.name = "cor",
	.obj_size = sizeof(struct cor_sock),
	.owner = THIS_MODULE,
};

void cor_free_sock(struct kref *ref)
{
	struct cor_sock *cs = container_of(ref, struct cor_sock, ref);

	if (cs->type == CS_TYPE_CONN_MANAGED) {
		if (cs->data.conn_managed.rcv_buf != 0) {
			kfree(cs->data.conn_managed.rcv_buf);
			cs->data.conn_managed.rcv_buf = 0;
		}

		if (cs->data.conn_managed.snd_buf != 0) {
			kfree(cs->data.conn_managed.snd_buf);
			cs->data.conn_managed.snd_buf = 0;
		}
	}

	sock_put((struct sock *) cs);
}

int cor_socket_setsockopt_tos(struct socket *sock,
		char __user *optval, unsigned int optlen)
{
	struct cor_sock *cs = (struct cor_sock *) sock->sk;

	__u32 tos;
	int notread;
	__u8 is_highlatency;
	int rc = 0;

	if (unlikely(optlen != 4))
		return -EINVAL;

	notread = copy_from_user((char *) &tos, optval, 4);
	if (unlikely(notread != 0))
		return -EFAULT;

	if (unlikely(tos != COR_TOS_DEFAULT && tos != COR_TOS_LOW_LATENCY &&
			tos != COR_TOS_HIGH_LATENCY))
		return -EINVAL;

	is_highlatency = (tos == COR_TOS_HIGH_LATENCY);

	mutex_lock(&cs->lock);
	if (unlikely(cs->is_client == 0)) {
		rc = -EINVAL;
	} else if (cs->type != CS_TYPE_UNCONNECTED) {
		cs->is_highlatency = is_highlatency;
	} else if (cs->type != CS_TYPE_CONN_RAW) {
		cs->is_highlatency = is_highlatency;

		if (cs->data.conn_raw.src_sock != 0)
			cor_set_conn_is_highlatency(cs->data.conn_raw.src_sock,
					is_highlatency, 0, 1);
	} else if (cs->type != CS_TYPE_CONN_MANAGED) {
		cs->is_highlatency = (tos == COR_TOS_HIGH_LATENCY);

		if (cs->data.conn_managed.src_sock != 0) {
			cor_set_conn_is_highlatency(
					cs->data.conn_managed.src_sock,
					is_highlatency, 0, 1);
		}
	}
	mutex_unlock(&cs->lock);

	return rc;
}

static void _cor_socket_setsockopt_priority(struct cor_conn *src_sock_o,
		__u32 priority)
{
	spin_lock_bh(&src_sock_o->rcv_lock);
	BUG_ON(src_sock_o->sourcetype != SOURCE_SOCK);
	src_sock_o->src.sock.ed->priority = priority;
	spin_unlock_bh(&src_sock_o->rcv_lock);
	cor_conn_refresh_priority(src_sock_o, 0);
}

int cor_socket_setsockopt_priority(struct socket *sock,
		char __user *optval, unsigned int optlen)
{
	struct cor_sock *cs = (struct cor_sock *) sock->sk;

	__u32 priority;
	int notread;
	int rc = 0;

	if (unlikely(optlen != 4))
		return -EINVAL;

	notread = copy_from_user((char *) &priority, optval, 4);
	if (unlikely(notread != 0))
		return -EFAULT;

	if (unlikely(priority < 0 || priority > 1000000))
		return -EINVAL;

	mutex_lock(&cs->lock);

	if (cs->type == CS_TYPE_CONN_RAW && cs->data.conn_raw.src_sock != 0) {
		BUG_ON(cs->is_client != cs->data.conn_raw.src_sock->is_client);
	} else if (cs->type == CS_TYPE_CONN_MANAGED &&
			cs->data.conn_managed.src_sock != 0) {
		BUG_ON(cs->is_client !=
				cs->data.conn_managed.src_sock->is_client);
	}

	if (unlikely(cs->is_client == 0)) {
		rc = -EINVAL;
		goto out;
	}

	cs->priority = div_u64(((__u64) priority) * cor_priority_max(),
			1000000);
	BUG_ON(cs->priority > cor_priority_max());

	if (cs->type == CS_TYPE_CONN_RAW && cs->data.conn_raw.src_sock != 0) {
		_cor_socket_setsockopt_priority(cs->data.conn_raw.src_sock,
					cs->priority);
	} else if (cs->type == CS_TYPE_CONN_MANAGED &&
			cs->data.conn_managed.src_sock != 0) {
		_cor_socket_setsockopt_priority(cs->data.conn_managed.src_sock,
				cs->priority);
	}

out:
	mutex_unlock(&cs->lock);

	return rc;
}

int cor_socket_socketpair(struct socket *sock1, struct socket *sock2)
{
	return -EOPNOTSUPP;
}

int cor_socket_getname(struct socket *sock, struct sockaddr *addr, int peer)
{
	return 0;
}

int cor_socket_mmap(struct file *file, struct socket *sock,
		struct vm_area_struct *vma)
{
	return -ENODEV;
}

int _cor_createsock(struct net *net, struct socket *sock, int protocol,
		int kern, __u8 is_client)
{
	struct cor_sock *cs = (struct cor_sock *) sk_alloc(net, PF_COR,
			GFP_KERNEL, &cor_proto, kern);
	if (unlikely(cs == 0))
		return -ENOMEM;

	BUILD_BUG_ON(offsetof(struct cor_sock, sk) != 0);

	sock_init_data(sock, &cs->sk);
	cs->sk.sk_protocol = protocol;

	memset(((char *)cs) + sizeof(struct sock), 0, sizeof(struct cor_sock) -
			sizeof(struct sock));

	cs->type = CS_TYPE_UNCONNECTED;
	mutex_init(&cs->lock);
	kref_init(&cs->ref);

	cs->is_client = is_client;

	INIT_WORK(&cs->readfromconn_work, cor_mngdsocket_readfromconn_wq);
	atomic_set(&cs->readfromconn_work_scheduled, 0);

	atomic_set(&cs->ready_to_read, 0);
	atomic_set(&cs->ready_to_write, 0);
	atomic_set(&cs->ready_to_accept, 0);

	cs->priority = cor_priority_max();

	sock->state = SS_UNCONNECTED;
	sock->sk = &cs->sk;

	return 0;
}

int cor_createsock(struct net *net, struct socket *sock, int protocol, int kern)
{
	if (unlikely(net != &init_net))
		return -EACCES;

	if (sock->type == SOCK_RAW) {
		if (unlikely(kern == 0 && capable(CAP_NET_ADMIN) == 0))
			return -EACCES;

		if (likely(protocol == PROTO_COR_RAW)) {
			return cor_create_raw_sock(net, sock, protocol, kern);
		} else if (protocol == PROTO_COR_RDEAMON) {
			return cor_create_rdaemon_sock(net, sock, protocol,
					kern);
		} else {
			return -EPROTONOSUPPORT;
		}
	} else if (sock->type == SOCK_STREAM) {
		if (unlikely(protocol != 0))
			return -EPROTONOSUPPORT;
		return cor_create_managed_sock(net, sock, protocol, kern);
	} else {
		return -EPROTONOSUPPORT;
	}
}

static struct net_proto_family cor_net_proto_family __read_mostly = {
	.family = PF_COR,
	.create = cor_createsock,
	.owner = THIS_MODULE
};

int __init cor_sock_init2(void)
{
	int rc;

	rc = proto_register(&cor_proto, 1);
	if (rc != 0)
		return rc;

	rc = sock_register(&cor_net_proto_family);
	if (rc != 0)
		return rc;

	return 0;
}

void __exit cor_sock_exit1(void)
{
	proto_unregister(&cor_proto);
	sock_unregister(PF_COR);
}

MODULE_LICENSE("GPL");
