/**
 *	Connection oriented routing
 *	Copyright (C) 2007-2021 Michael Blizek
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License
 *	as published by the Free Software Foundation; either version 2
 *	of the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 */

#include <linux/delay.h>
#include "cor.h"


static DEFINE_SPINLOCK(cor_neighbor_list_lock);
static LIST_HEAD(cor_nb_list);
static struct kmem_cache *cor_nb_slab;
atomic_t cor_num_neighs;

static DEFINE_SPINLOCK(cor_connid_gen);


void cor_neighbor_free(struct kref *ref)
{
	struct cor_neighbor *nb = container_of(ref, struct cor_neighbor, ref);

	WARN_ONCE(list_empty(&nb->cmsg_queue_pong) == 0,
			"cor_neighbor_free(): nb->cmsg_queue_pong is not empty");
	WARN_ONCE(list_empty(&nb->cmsg_queue_ack_fast) == 0,
			"cor_neighbor_free(): nb->cmsg_queue_ack_fast is not empty");
	WARN_ONCE(list_empty(&nb->cmsg_queue_ack_slow) == 0,
			"cor_neighbor_free(): nb->cmsg_queue_ack_slow is not empty");
	WARN_ONCE(list_empty(&nb->cmsg_queue_ackconn_urgent) == 0,
			"cor_neighbor_free(): nb->cmsg_queue_ackconn_urgent is not empty");
	WARN_ONCE(list_empty(&nb->cmsg_queue_ackconn) == 0,
			"cor_neighbor_free(): nb->cmsg_queue_ackconn is not empty");
	WARN_ONCE(list_empty(&nb->cmsg_queue_conndata_lowlat) == 0,
			"cor_neighbor_free(): nb->cmsg_queue_conndata_lowlat is not empty");
	WARN_ONCE(list_empty(&nb->cmsg_queue_conndata_highlat) == 0,
			"cor_neighbor_free(): nb->cmsg_queue_conndata_highlat is not empty");
	WARN_ONCE(list_empty(&nb->cmsg_queue_other) == 0,
			"cor_neighbor_free(): nb->cmsg_queue_other is not empty");
	WARN_ONCE(nb->pending_conn_resets_rb.rb_node != 0,
			"cor_neighbor_free(): nb->pending_conn_resets_rb is not empty");
	WARN_ONCE(nb->rb_kp.in_queue != RB_INQUEUE_FALSE,
			"cor_neighbor_free(): nb->rb_kp.in_queue is not RB_INQUEUE_FALSE");
	WARN_ONCE(nb->rb_cr.in_queue != RB_INQUEUE_FALSE,
			"cor_neighbor_free(): nb->rb_cr.in_queue is not RB_INQUEUE_FALSE");
	WARN_ONCE(nb->rb.in_queue != RB_INQUEUE_FALSE,
			"cor_neighbor_free(): nb->rb.in_queue is not RB_INQUEUE_FALSE");
	WARN_ONCE(list_empty(&nb->conns_waiting.lh) == 0,
			"cor_neighbor_free(): nb->conns_waiting.lh is not empty");
	WARN_ONCE(list_empty(&nb->conns_waiting.lh_nextpass) == 0,
			"cor_neighbor_free(): nb->conns_waiting.lh_nextpass is not empty");
	WARN_ONCE(nb->str_timer_pending != 0,
			"cor_neighbor_free(): nb->str_timer_pending is not 0");
	WARN_ONCE(nb->connid_rb.rb_node != 0,
			"cor _neighbor_free(): nb->connid_rb is not empty");
	WARN_ONCE(nb->connid_reuse_rb.rb_node != 0,
			"cor_neighbor_free(): nb->connid_reuse_rb is not empty");
	WARN_ONCE(list_empty(&nb->connid_reuse_list) == 0,
			"cor_neighbor_free(): nb->connid_reuse_list is not empty");
	WARN_ONCE(nb->kp_retransmits_rb.rb_node != 0,
			"cor_neighbor_free(): nb->kp_retransmits_rb is not empty");
	WARN_ONCE(list_empty(&nb->snd_conn_idle_list) == 0,
			"cor_neighbor_free(): nb->snd_conn_idle_list is not empty");
	WARN_ONCE(list_empty(&nb->snd_conn_busy_list) == 0,
			"cor_neighbor_free(): nb->snd_conn_busy_list is not empty");
	WARN_ONCE(list_empty(&nb->retrans_fast_list) == 0,
			"cor_neighbor_free(): nb->retrans_fast_list is not empty");
	WARN_ONCE(list_empty(&nb->retrans_slow_list) == 0,
			"cor_neighbor_free(): nb->retrans_slow_list is not empty");
	WARN_ONCE(list_empty(&nb->retrans_conn_lowlatency_list) == 0,
			"cor_neighbor_free(): nb->retrans_conn_lowlatency_list is not empty");
	WARN_ONCE(list_empty(&nb->retrans_conn_highlatency_list) == 0,
			"cor_neighbor_free(): nb->retrans_conn_highlatency_list is not empty");

	/* printk(KERN_ERR "neighbor free\n"); */
	BUG_ON(nb->nb_list.next != LIST_POISON1);
	BUG_ON(nb->nb_list.prev != LIST_POISON2);
	if (nb->dev != 0)
		dev_put(nb->dev);
	nb->dev = 0;
	if (nb->cd != 0)
		kref_put(&nb->cd->ref, cor_dev_free);
	nb->cd = 0;
	kmem_cache_free(cor_nb_slab, nb);
	atomic_dec(&cor_num_neighs);
}

static void cor_stall_timer(struct work_struct *work);

static void _cor_reset_neighbor(struct work_struct *work);

static struct cor_neighbor *cor_alloc_neighbor(gfp_t allocflags)
{
	struct cor_neighbor *nb;
	__u32 seqno;

	if (atomic_inc_return(&cor_num_neighs) >= MAX_NEIGHBORS) {
		atomic_dec(&cor_num_neighs);
		return 0;
	}

	nb = kmem_cache_alloc(cor_nb_slab, allocflags);
	if (unlikely(nb == 0))
		return 0;

	memset(nb, 0, sizeof(struct cor_neighbor));

	kref_init(&nb->ref);
	atomic_set(&nb->sessionid_rcv_needed, 1);
	atomic_set(&nb->sessionid_snd_needed, 1);
	timer_setup(&nb->cmsg_timer, cor_controlmsg_timerfunc, 0);
	spin_lock_init(&nb->cmsg_lock);
	INIT_LIST_HEAD(&nb->cmsg_queue_pong);
	INIT_LIST_HEAD(&nb->cmsg_queue_ack_fast);
	INIT_LIST_HEAD(&nb->cmsg_queue_ack_slow);
	INIT_LIST_HEAD(&nb->cmsg_queue_ackconn_urgent);
	INIT_LIST_HEAD(&nb->cmsg_queue_ackconn);
	INIT_LIST_HEAD(&nb->cmsg_queue_conndata_lowlat);
	INIT_LIST_HEAD(&nb->cmsg_queue_conndata_highlat);
	INIT_LIST_HEAD(&nb->cmsg_queue_other);
	atomic_set(&nb->cmsg_pongs_retrans_cnt, 0);
	atomic_set(&nb->cmsg_othercnt, 0);
	atomic_set(&nb->cmsg_bulk_readds, 0);
	atomic_set(&nb->cmsg_delay_conndata, 0);
	atomic_set(&nb->rcvmtu_sendneeded, 1);
	nb->last_ping_time = jiffies;
	atomic_set(&nb->latency_retrans_us, PING_GUESSLATENCY_MS * 1000);
	atomic_set(&nb->latency_advertised_us, PING_GUESSLATENCY_MS * 1000);
	atomic_set(&nb->max_remote_ack_fast_delay_us, 1000000);
	atomic_set(&nb->max_remote_ack_slow_delay_us, 1000000);
	atomic_set(&nb->max_remote_ackconn_delay_us, 1000000);
	atomic_set(&nb->max_remote_pong_delay_us, 1000000);
	atomic_set(&nb->remote_rcvmtu, 128);
	spin_lock_init(&nb->conns_waiting.lock);
	INIT_LIST_HEAD(&nb->conns_waiting.lh);
	INIT_LIST_HEAD(&nb->conns_waiting.lh_nextpass);
	spin_lock_init(&nb->nbcongwin.lock);
	hrtimer_init(&nb->nbcongwin.acked_timer, CLOCK_MONOTONIC,
			HRTIMER_MODE_REL);
	nb->nbcongwin.acked_timer.function = cor_nbcongwin_hrtimerfunc;
	cor_nb_kref_get(nb, "congwin_timer");
	atomic64_set(&nb->nbcongwin.data_intransit, 0);
	atomic64_set(&nb->nbcongwin.cwin, 0);
	spin_lock_init(&nb->state_lock);
	nb->state = NEIGHBOR_STATE_INITIAL;
	nb->state_time.initial_state_since = jiffies;
	INIT_DELAYED_WORK(&nb->stalltimeout_timer, cor_stall_timer);
	spin_lock_init(&nb->connid_lock);
	spin_lock_init(&nb->connid_reuse_lock);
	INIT_LIST_HEAD(&nb->connid_reuse_list);
	get_random_bytes((char *) &seqno, sizeof(seqno));
	nb->kpacket_seqno = seqno;
	atomic64_set(&nb->priority_sum, 0);
	spin_lock_init(&nb->conn_list_lock);
	INIT_LIST_HEAD(&nb->snd_conn_idle_list);
	INIT_LIST_HEAD(&nb->snd_conn_busy_list);
	spin_lock_init(&nb->retrans_lock);
	timer_setup(&nb->retrans_timer, cor_retransmit_timerfunc, 0);
	INIT_LIST_HEAD(&nb->retrans_fast_list);
	INIT_LIST_HEAD(&nb->retrans_slow_list);
	spin_lock_init(&nb->retrans_conn_lock);
	timer_setup(&nb->retrans_conn_timer, cor_retransmit_conn_timerfunc, 0);
	INIT_LIST_HEAD(&nb->retrans_conn_lowlatency_list);
	INIT_LIST_HEAD(&nb->retrans_conn_highlatency_list);
	INIT_WORK(&nb->reset_neigh_work, _cor_reset_neighbor);

	return nb;
}

int cor_is_from_nb(struct sk_buff *skb, struct cor_neighbor *nb)
{
	int rc;

	char source_hw[MAX_ADDR_LEN];

	memset(source_hw, 0, MAX_ADDR_LEN);
	if (skb->dev->header_ops != 0 &&
			skb->dev->header_ops->parse != 0)
		skb->dev->header_ops->parse(skb, source_hw);

	rc = (skb->dev == nb->dev && memcmp(nb->mac, source_hw,
			MAX_ADDR_LEN) == 0);
	return rc;
}

struct cor_neighbor *_cor_get_neigh_by_mac(struct net_device *dev,
		char *source_hw)
{
	struct list_head *currlh;
	struct cor_neighbor *ret = 0;

	spin_lock_bh(&cor_neighbor_list_lock);

	currlh = cor_nb_list.next;
	while (currlh != &cor_nb_list) {
		struct cor_neighbor *curr = container_of(currlh,
				struct cor_neighbor, nb_list);

		BUG_ON(curr->in_nb_list == 0);

		if (curr->dev == dev && memcmp(curr->mac, source_hw,
				MAX_ADDR_LEN) == 0) {
			ret = curr;
			cor_nb_kref_get(ret, "stack");
			break;
		}

		currlh = currlh->next;
	}

	spin_unlock_bh(&cor_neighbor_list_lock);

	if (ret != 0 && unlikely(cor_get_neigh_state(ret) ==
			NEIGHBOR_STATE_KILLED)) {
		cor_nb_kref_put(ret, "stack");
		return 0;
	} else {
		return ret;
	}
}

struct cor_neighbor *cor_get_neigh_by_mac(struct sk_buff *skb)
{
	char source_hw[MAX_ADDR_LEN];

	memset(source_hw, 0, MAX_ADDR_LEN);
	if (skb->dev->header_ops != 0 &&
			skb->dev->header_ops->parse != 0)
		skb->dev->header_ops->parse(skb, source_hw);

	return _cor_get_neigh_by_mac(skb->dev, source_hw);
}

struct cor_neighbor *cor_find_neigh(__be64 addr)
{
	struct list_head *currlh;
	struct cor_neighbor *ret = 0;

	spin_lock_bh(&cor_neighbor_list_lock);

	currlh = cor_nb_list.next;
	while (currlh != &cor_nb_list) {
		struct cor_neighbor *curr = container_of(currlh,
				struct cor_neighbor, nb_list);

		BUG_ON(curr->in_nb_list == 0);

		BUG_ON((curr->has_addr == 0) && (curr->addr != 0));
		if (curr->has_addr != 0 && curr->addr == addr) {
			ret = curr;
			cor_nb_kref_get(ret, "stack");

			goto out;
		}

		currlh = currlh->next;
	}

out:
	spin_unlock_bh(&cor_neighbor_list_lock);

	if (ret != 0 && unlikely(cor_get_neigh_state(ret) ==
			NEIGHBOR_STATE_KILLED)) {
		cor_nb_kref_put(ret, "stack");
		return 0;
	} else {
		return ret;
	}
}

void cor_resend_rcvmtu(struct net_device *dev)
{
	struct list_head *currlh;

	spin_lock_bh(&cor_neighbor_list_lock);

	currlh = cor_nb_list.next;
	while (currlh != &cor_nb_list) {
		struct cor_neighbor *nb = container_of(currlh,
				struct cor_neighbor, nb_list);

		unsigned long iflags;

		if (nb->dev != dev)
			goto cont;

		cor_send_rcvmtu(nb);

		spin_lock_irqsave(&nb->state_lock, iflags);

		if (nb->rcvmtu_allowed_countdown != 0)
			nb->rcvmtu_delayed_send_needed = 1;
		nb->rcvmtu_allowed_countdown = 3;

		spin_unlock_irqrestore(&nb->state_lock, iflags);

cont:
		currlh = currlh->next;
	}

	spin_unlock_bh(&cor_neighbor_list_lock);
}

__u32 cor_generate_neigh_list(char *buf, __u32 buflen)
{
	struct list_head *currlh;

	__u32 cnt = 0;

	__u32 buf_offset = 4;

	int rc;

	/*
	 * The variable length header rowcount need to be generated after the
	 * data. This is done by reserving the maximum space they could take. If
	 * they end up being smaller, the data is moved so that there is no gap.
	 */

	BUG_ON(buf == 0);
	BUG_ON(buflen < buf_offset);

	/* num_fields */
	rc = cor_encode_len(buf + buf_offset, buflen - buf_offset, 2);
	BUG_ON(rc <= 0);
	buf_offset += rc;

	/* addr field */
	BUG_ON(buflen < buf_offset + 2);
	cor_put_u16(buf + buf_offset, LIST_NEIGH_FIELD_ADDR);
	buf_offset += 2;

	rc = cor_encode_len(buf + buf_offset, buflen - buf_offset, 8);
	BUG_ON(rc <= 0);
	buf_offset += rc;

	/* latency field */
	BUG_ON(buflen < buf_offset + 2);
	cor_put_u16(buf + buf_offset, LIST_NEIGH_FIELD_LATENCY);
	buf_offset += 2;

	rc = cor_encode_len(buf + buf_offset, buflen - buf_offset, 1);
	BUG_ON(rc <= 0);
	buf_offset += rc;

	spin_lock_bh(&cor_neighbor_list_lock);

	currlh = cor_nb_list.next;
	while (currlh != &cor_nb_list) {
		struct cor_neighbor *curr = container_of(currlh,
				struct cor_neighbor, nb_list);
		int state;

		BUG_ON(curr->in_nb_list == 0);

		state = cor_get_neigh_state(curr);
		if (state != NEIGHBOR_STATE_ACTIVE)
			goto cont;

		BUG_ON((curr->has_addr == 0) && (curr->addr != 0));
		if (curr->has_addr == 0)
			goto cont;


		if (unlikely(buflen < buf_offset + 8 + 1))
			break;

		cor_put_be64(buf + buf_offset, curr->addr);
		buf_offset += 8;

		buf[buf_offset] = cor_enc_log_64_11(atomic_read(
				&curr->latency_advertised_us));
		buf_offset += 1;

		BUG_ON(buf_offset > buflen);

		cnt++;

cont:
		currlh = currlh->next;
	}

	spin_unlock_bh(&cor_neighbor_list_lock);

	rc = cor_encode_len(buf, 4, cnt);
	BUG_ON(rc <= 0);
	BUG_ON(rc > 4);

	if (likely(rc < 4))
		memmove(buf + ((__u32) rc), buf + 4, buf_offset);

	return buf_offset - 4 + ((__u32) rc);
}

static void cor_reset_all_conns(struct cor_neighbor *nb)
{
	while (1) {
		unsigned long iflags;
		struct cor_conn *trgt_out;
		struct cor_conn *src_in;
		struct cor_conn_bidir *cnb;
		int rc;

		spin_lock_irqsave(&nb->conn_list_lock, iflags);

		if (!list_empty(&nb->snd_conn_busy_list)) {
			trgt_out = container_of(nb->snd_conn_busy_list.next,
					struct cor_conn, trgt.out.nb_list);
		} else if (!list_empty(&nb->snd_conn_idle_list)) {
			trgt_out = container_of(nb->snd_conn_idle_list.next,
					struct cor_conn, trgt.out.nb_list);
		} else {
			spin_unlock_irqrestore(&nb->conn_list_lock, iflags);
			break;
		}

		cor_conn_kref_get(trgt_out, "stack");

		spin_unlock_irqrestore(&nb->conn_list_lock, iflags);

		src_in = cor_get_conn_reversedir(trgt_out);
		cnb = cor_get_conn_bidir(trgt_out);

		spin_lock_bh(&cnb->cli.rcv_lock);
		spin_lock_bh(&cnb->srv.rcv_lock);

		if (unlikely(unlikely(src_in->sourcetype != SOURCE_IN) ||
				unlikely(src_in->src.in.nb != nb))) {
			rc = 1;
			goto unlock;
		}

		rc = cor_send_reset_conn(nb, trgt_out->trgt.out.conn_id, 1);

		if (unlikely(rc != 0))
			goto unlock;

		if (trgt_out->isreset == 0)
			trgt_out->isreset = 1;

unlock:
		spin_unlock_bh(&cnb->srv.rcv_lock);
		spin_unlock_bh(&cnb->cli.rcv_lock);

		if (rc == 0) {
			cor_reset_conn(src_in);
			cor_conn_kref_put(src_in, "stack");
		} else {
			cor_conn_kref_put(src_in, "stack");
			cor_nb_kref_get(nb, "stalltimeout_timer");
			schedule_delayed_work(&nb->stalltimeout_timer, HZ);
			break;
		}
	}
}

static void cor_delete_connid_reuse_items(struct cor_neighbor *nb);

static void __cor_reset_neighbor(struct cor_neighbor *nb)
{
	unsigned long iflags;

	cor_reset_all_conns(nb);
	cor_delete_connid_reuse_items(nb);

	spin_lock_bh(&cor_neighbor_list_lock);
	if (nb->in_nb_list != 0) {
		list_del(&nb->nb_list);
		nb->in_nb_list = 0;
		cor_nb_kref_put_bug(nb, "neigh_list");
	}
	spin_unlock_bh(&cor_neighbor_list_lock);

	spin_lock_irqsave(&nb->nbcongwin.lock, iflags);
	hrtimer_cancel(&nb->nbcongwin.acked_timer);
	spin_unlock_irqrestore(&nb->nbcongwin.lock, iflags);
	cor_nb_kref_put(nb, "congwin_timer");
}

static void _cor_reset_neighbor(struct work_struct *work)
{
	struct cor_neighbor *nb = container_of(work, struct cor_neighbor,
			reset_neigh_work);
	__cor_reset_neighbor(nb);
	cor_nb_kref_put(nb, "reset_neigh_work");
}

static void cor_reset_neighbor(struct cor_neighbor *nb, int use_workqueue)
{
	unsigned long iflags;

	spin_lock_irqsave(&nb->state_lock, iflags);
	/* if (nb->state != NEIGHBOR_STATE_KILLED) {
		printk(KERN_ERR "cor_reset_neighbor\n");
		show_stack(0, 0);
	} */
	nb->state = NEIGHBOR_STATE_KILLED;
	spin_unlock_irqrestore(&nb->state_lock, iflags);


	if (use_workqueue) {
		cor_nb_kref_get(nb, "reset_neigh_work");
		schedule_work(&nb->reset_neigh_work);
	} else {
		cor_nb_kref_get(nb, "stack");
		__cor_reset_neighbor(nb);
		cor_nb_kref_put(nb, "stack");
	}
}

void cor_reset_neighbors(struct net_device *dev)
{
	struct list_head *currlh;

restart:
	spin_lock_bh(&cor_neighbor_list_lock);

	currlh = cor_nb_list.next;
	while (currlh != &cor_nb_list) {
		unsigned long iflags;
		struct cor_neighbor *currnb = container_of(currlh,
				struct cor_neighbor, nb_list);
		__u8 state;

		BUG_ON(currnb->in_nb_list == 0);

		if (dev != 0 && currnb->dev != dev)
			goto cont;

		spin_lock_irqsave(&currnb->state_lock, iflags);
		state = currnb->state;
		spin_unlock_irqrestore(&currnb->state_lock, iflags);

		if (state != NEIGHBOR_STATE_KILLED) {
			spin_unlock_bh(&cor_neighbor_list_lock);
			cor_reset_neighbor(currnb, 0);
			goto restart;
		}

cont:
		currlh = currlh->next;
	}

	spin_unlock_bh(&cor_neighbor_list_lock);
}

static void cor_stall_timer(struct work_struct *work)
{
	struct cor_neighbor *nb = container_of(to_delayed_work(work),
			struct cor_neighbor, stalltimeout_timer);

	int stall_time_ms;
	__u8 nbstate;

	unsigned long iflags;

	spin_lock_irqsave(&nb->state_lock, iflags);
	nbstate = nb->state;
	spin_unlock_irqrestore(&nb->state_lock, iflags);

	if (nbstate == NEIGHBOR_STATE_STALLED) {
		stall_time_ms = jiffies_to_msecs(jiffies -
			nb->state_time.last_roundtrip);

		if (stall_time_ms < NB_KILL_TIME_MS) {
			schedule_delayed_work(&nb->stalltimeout_timer,
					msecs_to_jiffies(NB_KILL_TIME_MS -
					stall_time_ms));
			return;
		}

		cor_reset_neighbor(nb, 1);
	}

	nb->str_timer_pending = 0;
	cor_nb_kref_put(nb, "stalltimeout_timer");
}

int cor_get_neigh_state(struct cor_neighbor *nb)
{
	int ret;
	unsigned long iflags;
	int stall_time_ms;

	BUG_ON(nb == 0);

	spin_lock_irqsave(&nb->state_lock, iflags);

	stall_time_ms = jiffies_to_msecs(jiffies -
			nb->state_time.last_roundtrip);

	WARN_ONCE(likely(nb->state == NEIGHBOR_STATE_ACTIVE) && unlikely(
			jiffies_to_msecs(jiffies - nb->last_ping_time) >
			PING_FORCETIME_ACTIVEIDLE_MS * 4) &&
			nb->ping_intransit == 0,
			"We have stopped sending pings to a neighbor!?");

	if (likely(nb->state == NEIGHBOR_STATE_ACTIVE) &&
			unlikely(stall_time_ms > NB_STALL_TIME_MS) && (
			nb->ping_intransit >= NB_STALL_MINPINGS ||
			nb->ping_intransit >= PING_COOKIES_PER_NEIGH)) {
		nb->state = NEIGHBOR_STATE_STALLED;
		nb->ping_success = 0;
		if (nb->str_timer_pending == 0) {
			nb->str_timer_pending = 1;
			cor_nb_kref_get(nb, "stalltimeout_timer");

			schedule_delayed_work(&nb->stalltimeout_timer,
					msecs_to_jiffies(NB_KILL_TIME_MS -
					stall_time_ms));
		}

		/* printk(KERN_ERR "changed to stalled\n"); */
		BUG_ON(nb->ping_intransit > PING_COOKIES_PER_NEIGH);
	} else if (unlikely(nb->state == NEIGHBOR_STATE_INITIAL) &&
			time_after(jiffies, nb->state_time.initial_state_since +
			INITIAL_TIME_LIMIT_SEC * HZ)) {
		spin_unlock_irqrestore(&nb->state_lock, iflags);
		cor_reset_neighbor(nb, 1);
		return NEIGHBOR_STATE_KILLED;
	}

	ret = nb->state;

	spin_unlock_irqrestore(&nb->state_lock, iflags);

	return ret;
}

static struct cor_ping_cookie *cor_find_cookie(struct cor_neighbor *nb,
		__u32 cookie)
{
	int i;

	for (i = 0; i < PING_COOKIES_PER_NEIGH; i++) {
		if (nb->cookies[i].cookie == cookie)
			return &nb->cookies[i];
	}
	return 0;
}

static void cor_reset_cookie(struct cor_neighbor *nb, struct cor_ping_cookie *c)
{
	if (c->cookie == 0)
		return;

	if (nb->cookie_unsent != c->cookie)
		nb->ping_intransit--;

	c->cookie = 0;
}

static __u32 sqrt(__u64 x)
{
	int i;
	__u64 y = 65536;

	if (unlikely(x <= 1))
		return 0;

	for (i = 0; i < 20; i++) {
		y = y / 2 + div64_u64(x / 2, y);
		if (unlikely(y == 0))
			y = 1;
	}

	if (unlikely(y > U32_MAX))
		y = U32_MAX;

	return (__u32) y;
}

static __u32 cor_calc_newlatency(struct cor_neighbor *nb_statelocked,
		__u32 oldlatency_us, __s64 newlatency_ns)
{
	__s64 oldlatency = oldlatency_us * 1000LL;
	__s64 newlatency;

	if (unlikely(unlikely(nb_statelocked->state == NEIGHBOR_STATE_INITIAL) &&
			nb_statelocked->ping_success < 16))
		newlatency = div64_s64(
				oldlatency * nb_statelocked->ping_success +
				newlatency_ns,
				nb_statelocked->ping_success + 1);
	else
		newlatency = (oldlatency * 15 + newlatency_ns) / 16;

	newlatency = div_s64(newlatency + 500, 1000);

	if (unlikely(newlatency < 0))
		newlatency = 0;
	if (unlikely(newlatency > U32_MAX))
		newlatency = U32_MAX;

	return (__u32) newlatency;
}

static void cor_update_nb_latency(struct cor_neighbor *nb_statelocked,
		struct cor_ping_cookie *c, __u32 respdelay)
{
	ktime_t now  = ktime_get();

	__s64 pinglatency_retrans_ns = ktime_to_ns(now) -
			ktime_to_ns(c->time_sent) - respdelay * 1000LL;
	__s64 pinglatency_advertised_ns = ktime_to_ns(now) -
			ktime_to_ns(c->time_created) - respdelay * 1000LL;

	__u32 oldlatency_retrans_us =
			atomic_read(&nb_statelocked->latency_retrans_us);

	__u32 newlatency_retrans_us = cor_calc_newlatency(nb_statelocked,
			oldlatency_retrans_us, pinglatency_retrans_ns);

	atomic_set(&nb_statelocked->latency_retrans_us, newlatency_retrans_us);

	if (unlikely(unlikely(nb_statelocked->state == NEIGHBOR_STATE_INITIAL) &&
			nb_statelocked->ping_success < 16)) {
		nb_statelocked->latency_variance_retrans_us =
				((__u64) newlatency_retrans_us) *
				newlatency_retrans_us;
		atomic_set(&nb_statelocked->latency_stddev_retrans_us, sqrt(
				nb_statelocked->latency_variance_retrans_us));
	} else if (pinglatency_retrans_ns > oldlatency_retrans_us *
			((__s64) 1000)) {
		__s64 newdiff = div_s64(pinglatency_retrans_ns -
				oldlatency_retrans_us * ((__s64) 1000), 1000);
		__u32 newdiff32 = (__u32) (unlikely(newdiff >= U32_MAX) ?
				U32_MAX : newdiff);
		__u64 newvar = ((__u64) newdiff32) * newdiff32;

		__u64 oldval = nb_statelocked->latency_variance_retrans_us;

		if (unlikely(unlikely(newvar > (1LL << 55)) || unlikely(
				oldval > (1LL << 55)))) {
			nb_statelocked->latency_variance_retrans_us =
					(oldval / 16) * 15 + newvar / 16;
		} else {
			nb_statelocked->latency_variance_retrans_us =
					(oldval * 15 + newvar) / 16;
		}

		atomic_set(&nb_statelocked->latency_stddev_retrans_us, sqrt(
				nb_statelocked->latency_variance_retrans_us));
	}

	atomic_set(&nb_statelocked->latency_advertised_us,
			cor_calc_newlatency(nb_statelocked,
			atomic_read(&nb_statelocked->latency_advertised_us),
			pinglatency_advertised_ns));

	nb_statelocked->last_roundtrip_end = now;
}

static void cor_connid_used_pingsuccess(struct cor_neighbor *nb);

void cor_ping_resp(struct cor_neighbor *nb, __u32 cookie, __u32 respdelay)
{
	unsigned long iflags;

	struct cor_ping_cookie *c;
	int i;
	int stalledresume = 0;

	int call_connidreuse = 0;
	int call_send_rcvmtu = 0;

	if (unlikely(cookie == 0))
		return;

	spin_lock_irqsave(&nb->state_lock, iflags);

	c = cor_find_cookie(nb, cookie);

	if (unlikely(c == 0))
		goto out;

	atomic_set(&nb->sessionid_snd_needed, 0);

	call_connidreuse = ktime_before_eq(nb->last_roundtrip_end,
			c->time_created);

	cor_update_nb_latency(nb, c, respdelay);

	nb->ping_success++;

	cor_reset_cookie(nb, c);

	for (i = 0; i < PING_COOKIES_PER_NEIGH; i++) {
		if (nb->cookies[i].cookie != 0 && ktime_before(
				nb->cookies[i].time_created, c->time_created)) {
			nb->cookies[i].pongs++;
			if (nb->cookies[i].pongs >= PING_PONGLIMIT) {
				cor_reset_cookie(nb, &nb->cookies[i]);
			}
		}
	}

	if (unlikely(nb->state == NEIGHBOR_STATE_INITIAL ||
			nb->state == NEIGHBOR_STATE_STALLED)) {
		call_connidreuse = 0;

		if ((nb->state == NEIGHBOR_STATE_INITIAL &&
				nb->ping_success >= PING_SUCCESS_CNT_INIT) || (
				nb->state == NEIGHBOR_STATE_STALLED &&
				nb->ping_success >= PING_SUCCESS_CNT_STALLED)) {
			stalledresume = (nb->state == NEIGHBOR_STATE_STALLED);
			nb->state = NEIGHBOR_STATE_ACTIVE;
			/* printk(KERN_ERR "changed to active\n"); */
		}
	}

	if (likely(nb->state == NEIGHBOR_STATE_ACTIVE) ||
			nb->state == NEIGHBOR_STATE_STALLED)
		nb->state_time.last_roundtrip = c->jiffies_sent;

	if (c == &nb->cookies[0] &&
			unlikely(nb->rcvmtu_allowed_countdown != 0)) {
		nb->rcvmtu_allowed_countdown--;

		if (unlikely(nb->rcvmtu_allowed_countdown == 0 &&
				nb->rcvmtu_delayed_send_needed != 0)) {
			nb->rcvmtu_allowed_countdown = 3;
			nb->rcvmtu_delayed_send_needed = 0;
			call_send_rcvmtu = 1;
		}
	}

out:
	spin_unlock_irqrestore(&nb->state_lock, iflags);

	if (call_connidreuse)
		cor_connid_used_pingsuccess(nb);

	if (unlikely(call_send_rcvmtu))
		cor_send_rcvmtu(nb);

	if (unlikely(stalledresume)) {
		spin_lock_bh(&nb->retrans_conn_lock);
		cor_reschedule_conn_retrans_timer(nb);
		spin_unlock_bh(&nb->retrans_conn_lock);

		cor_dev_queue_enqueue(nb->cd, &nb->rb, 0, ns_to_ktime(0),
				QOS_CALLER_NEIGHBOR, 1);
	}
}

__u32 cor_add_ping_req(struct cor_neighbor *nb, unsigned long *last_ping_time)
{
	unsigned long iflags;
	struct cor_ping_cookie *c;
	__u32 i;

	__u32 cookie;

	ktime_t now = ktime_get();

	spin_lock_irqsave(&nb->state_lock, iflags);

	if (nb->cookie_unsent != 0) {
		c = cor_find_cookie(nb, nb->cookie_unsent);
		if (c != 0)
			goto unsent;
		c = 0;
		nb->cookie_unsent = 0;
	}

	c = cor_find_cookie(nb, 0);
	if (c != 0)
		goto found;

	get_random_bytes((char *) &i, sizeof(i));
	i = (i % PING_COOKIES_PER_NEIGH);
	c = &nb->cookies[i];
	cor_reset_cookie(nb, c);

found:
	nb->lastcookie++;
	if (unlikely(nb->lastcookie == 0))
		nb->lastcookie++;
	c->cookie = nb->lastcookie;
	c->time_created = now;

unsent:
	c->pongs = 0;
	c->time_sent = now;
	c->jiffies_sent = jiffies;
	cookie = c->cookie;

	nb->ping_intransit++;

	*last_ping_time = nb->last_ping_time;
	nb->last_ping_time = c->jiffies_sent;

	spin_unlock_irqrestore(&nb->state_lock, iflags);

	BUG_ON(cookie == 0);

	return cookie;
}

void cor_ping_sent(struct cor_neighbor *nb, __u32 cookie)
{
	unsigned long iflags;

	BUG_ON(cookie == 0);

	spin_lock_irqsave(&nb->state_lock, iflags);

	if (nb->cookie_unsent == cookie)
		nb->cookie_unsent = 0;

	spin_unlock_irqrestore(&nb->state_lock, iflags);
}

void cor_unadd_ping_req(struct cor_neighbor *nb, __u32 cookie,
		unsigned long last_ping_time, int congested)
{
	unsigned long iflags;

	struct cor_ping_cookie *c;

	BUG_ON(cookie == 0);

	spin_lock_irqsave(&nb->state_lock, iflags);

	if (congested) {
		BUG_ON(nb->cookie_unsent != 0 && nb->cookie_unsent != cookie);
		nb->cookie_unsent = cookie;
	}

	c = cor_find_cookie(nb, cookie);
	if (likely(c != 0)) {
		if (congested == 0)
			c->cookie = 0;
		nb->ping_intransit--;
	}

	nb->last_ping_time = last_ping_time;

	spin_unlock_irqrestore(&nb->state_lock, iflags);
}

static int cor_get_ping_forcetime_ms(struct cor_neighbor *nb)
{
	unsigned long iflags;
	int fast;
	int idle;

	if (unlikely(cor_get_neigh_state(nb) != NEIGHBOR_STATE_ACTIVE))
		return PING_FORCETIME_MS;

	spin_lock_irqsave(&nb->state_lock, iflags);
	fast = ((nb->ping_success < PING_ACTIVE_FASTINITIAL_COUNT) ||
			(nb->ping_intransit > 0));
	if (unlikely(nb->rcvmtu_delayed_send_needed != 0)) {
		BUG_ON(nb->rcvmtu_allowed_countdown == 0);
		fast = 1;
	}
	spin_unlock_irqrestore(&nb->state_lock, iflags);

	if (fast)
		return PING_FORCETIME_ACTIVE_FAST_MS;

	spin_lock_irqsave(&nb->conn_list_lock, iflags);
	idle = list_empty(&nb->snd_conn_idle_list) &&
			list_empty(&nb->snd_conn_busy_list);
	spin_unlock_irqrestore(&nb->conn_list_lock, iflags);

	if (idle)
		return PING_FORCETIME_ACTIVEIDLE_MS;
	else
		return PING_FORCETIME_ACTIVE_MS;
}

static __u32 cor_get_ping_mindelay_ms(struct cor_neighbor *nb_statelocked)
{
	__u32 latency_us = ((__u32) atomic_read(
			&nb_statelocked->latency_advertised_us));
	__u32 max_remote_pong_delay_us = ((__u32) atomic_read(
			&nb_statelocked->max_remote_pong_delay_us));
	__u32 mindelay_ms;

	if (latency_us < PING_GUESSLATENCY_MS * 1000)
		latency_us = PING_GUESSLATENCY_MS * 1000;

	if (unlikely(nb_statelocked->state != NEIGHBOR_STATE_ACTIVE))
		mindelay_ms = latency_us / 1000;
	else
		mindelay_ms = ((latency_us / 2 +
				max_remote_pong_delay_us / 2) / 500);

	if (likely(nb_statelocked->ping_intransit < PING_COOKIES_THROTTLESTART))
		return mindelay_ms;

	mindelay_ms = mindelay_ms * (1 + 9 * (nb_statelocked->ping_intransit *
			nb_statelocked->ping_intransit /
			(PING_COOKIES_PER_NEIGH * PING_COOKIES_PER_NEIGH)));

	return mindelay_ms;
}

/**
 * Check whether we want to send a ping now:
 * 0... Do not send ping.
 * 1... Send ping now, but only if it can be merged with other messages. This
 *      can happen way before the time requested by cor_get_next_ping_time().
 * 2... Send ping now, even if a packet has to be created just for the ping
 *      alone.
 */
int cor_time_to_send_ping(struct cor_neighbor *nb)
{
	unsigned long iflags;
	int rc = TIMETOSENDPING_YES;

	__u32 ms_since_last_ping;

	__u32 forcetime = cor_get_ping_forcetime_ms(nb);
	__u32 mindelay;

	spin_lock_irqsave(&nb->state_lock, iflags);

	ms_since_last_ping = jiffies_to_msecs(jiffies - nb->last_ping_time);

	mindelay = cor_get_ping_mindelay_ms(nb);

	if (forcetime < (mindelay * 3))
		forcetime = mindelay * 3;
	else if (forcetime > (mindelay * 3))
		mindelay = forcetime / 3;

	if (ms_since_last_ping < mindelay ||
			ms_since_last_ping < (forcetime / 4))
		rc = TIMETOSENDPING_NO;
	else if (ms_since_last_ping >= forcetime)
		rc = TIMETOSENDPING_FORCE;

	spin_unlock_irqrestore(&nb->state_lock, iflags);

	return rc;
}

unsigned long cor_get_next_ping_time(struct cor_neighbor *nb)
{
	unsigned long iflags;

	__u32 forcetime = cor_get_ping_forcetime_ms(nb);
	__u32 mindelay;

	spin_lock_irqsave(&nb->state_lock, iflags);
	mindelay = cor_get_ping_mindelay_ms(nb);
	spin_unlock_irqrestore(&nb->state_lock, iflags);

	if (forcetime < (mindelay * 3))
		forcetime = mindelay * 3;

	return nb->last_ping_time + msecs_to_jiffies(forcetime);
}

void cor_add_neighbor(struct cor_neighbor_discdata *nb_dd)
{
	struct cor_neighbor *nb;
	struct list_head *currlh;

	nb = cor_alloc_neighbor(GFP_KERNEL);
	if (unlikely(nb == 0))
		return;

	nb->cd = cor_dev_get(nb_dd->dev);
	if (nb->cd == 0) {
		kmem_cache_free(cor_nb_slab, nb);
		atomic_dec(&cor_num_neighs);
		return;
	}

	dev_hold(nb_dd->dev);
	nb->dev = nb_dd->dev;

	memcpy(nb->mac, nb_dd->mac, MAX_ADDR_LEN);

	nb->has_addr = nb_dd->has_addr;
	nb->addr = nb_dd->addr;

	nb_dd->nb_allocated = 1;

	spin_lock_bh(&cor_neighbor_list_lock);

	BUG_ON((nb->has_addr == 0) && (nb->addr != 0));

	if (cor_is_clientmode() && nb->has_addr == 0)
		goto already_present;

	currlh = cor_nb_list.next;
	while (currlh != &cor_nb_list) {
		struct cor_neighbor *curr = container_of(currlh,
				struct cor_neighbor, nb_list);

		BUG_ON((curr->has_addr == 0) && (curr->addr != 0));

		if (curr->dev == nb->dev &&
				memcmp(curr->mac, nb->mac, MAX_ADDR_LEN) == 0)
			goto already_present;

		if (curr->has_addr != 0 && curr->addr == nb->addr)
			goto already_present;

		currlh = currlh->next;
	}

	/* printk(KERN_ERR "add_neigh\n"); */

	spin_lock_bh(&cor_local_addr_lock);
	nb->sessionid = cor_local_addr_sessionid ^ nb_dd->sessionid;
	spin_unlock_bh(&cor_local_addr_lock);

	spin_lock_bh(&nb->cmsg_lock);
	nb->last_ping_time = jiffies;
	cor_schedule_controlmsg_timer(nb);
	spin_unlock_bh(&nb->cmsg_lock);

	list_add_tail(&nb->nb_list, &cor_nb_list);
	nb->in_nb_list = 1;
	cor_nb_kref_get(nb, "neigh_list");
	cor_nb_kref_put_bug(nb, "alloc");

	if (0) {
already_present:
		kmem_cache_free(cor_nb_slab, nb);
		atomic_dec(&cor_num_neighs);
	}

	spin_unlock_bh(&cor_neighbor_list_lock);
}

struct cor_conn *cor_get_conn(struct cor_neighbor *nb, __u32 conn_id)
{
	unsigned long iflags;

	struct rb_node *n = 0;
	struct cor_conn *ret = 0;

	spin_lock_irqsave(&nb->connid_lock, iflags);

	n = nb->connid_rb.rb_node;

	while (likely(n != 0) && ret == 0) {
		struct cor_conn *src_in_o = container_of(n, struct cor_conn,
				src.in.rbn);

		BUG_ON(src_in_o->sourcetype != SOURCE_IN);

		if (conn_id < src_in_o->src.in.conn_id)
			n = n->rb_left;
		else if (conn_id > src_in_o->src.in.conn_id)
			n = n->rb_right;
		else
			ret = src_in_o;
	}

	if (ret != 0)
		cor_conn_kref_get(ret, "stack");

	spin_unlock_irqrestore(&nb->connid_lock, iflags);

	return ret;
}

int cor_insert_connid(struct cor_neighbor *nb, struct cor_conn *src_in_ll)
{
	int rc = 0;

	unsigned long iflags;

	__u32 conn_id = src_in_ll->src.in.conn_id;

	struct rb_root *root;
	struct rb_node **p;
	struct rb_node *parent = 0;

	BUG_ON(src_in_ll->sourcetype != SOURCE_IN);

	spin_lock_irqsave(&nb->connid_lock, iflags);

	root = &nb->connid_rb;
	p = &root->rb_node;

	while ((*p) != 0) {
		struct cor_conn *src_in_o = container_of(*p, struct cor_conn,
				src.in.rbn);

		BUG_ON(src_in_o->sourcetype != SOURCE_IN);

		parent = *p;
		if (unlikely(conn_id == src_in_o->src.in.conn_id)) {
			goto duplicate;
		} else if (conn_id < src_in_o->src.in.conn_id) {
			p = &(*p)->rb_left;
		} else if (conn_id > src_in_o->src.in.conn_id) {
			p = &(*p)->rb_right;
		}
	}

	cor_conn_kref_get(src_in_ll, "connid table");
	rb_link_node(&src_in_ll->src.in.rbn, parent, p);
	rb_insert_color(&src_in_ll->src.in.rbn, root);

	if (0) {
duplicate:
		rc = 1;
	}

	spin_unlock_irqrestore(&nb->connid_lock, iflags);

	return rc;
}

static struct cor_connid_reuse_item *cor_get_connid_reuseitem(
		struct cor_neighbor *nb, __u32 conn_id)
{
	unsigned long iflags;

	struct rb_node *n = 0;
	struct cor_connid_reuse_item *ret = 0;

	spin_lock_irqsave(&nb->connid_reuse_lock, iflags);

	n = nb->connid_reuse_rb.rb_node;

	while (likely(n != 0) && ret == 0) {
		struct cor_connid_reuse_item *cir = container_of(n,
				struct cor_connid_reuse_item, rbn);

		BUG_ON(cir->conn_id == 0);

		if (conn_id < cir->conn_id)
			n = n->rb_left;
		else if (conn_id > cir->conn_id)
			n = n->rb_right;
		else
			ret = cir;
	}

	if (ret != 0)
		kref_get(&ret->ref);

	spin_unlock_irqrestore(&nb->connid_reuse_lock, iflags);

	return ret;
}

/* nb->connid_reuse_lock must be held by the caller */
static void _cor_insert_connid_reuse_insertrb(struct cor_neighbor *nb,
		struct cor_connid_reuse_item *ins)
{
	struct rb_root *root;
	struct rb_node **p;
	struct rb_node *parent = 0;

	BUG_ON(ins->conn_id == 0);

	root = &nb->connid_reuse_rb;
	p = &root->rb_node;

	while ((*p) != 0) {
		struct cor_connid_reuse_item *curr = container_of(*p,
				struct cor_connid_reuse_item, rbn);

		BUG_ON(curr->conn_id == 0);

		parent = *p;
		if (unlikely(ins->conn_id == curr->conn_id)) {
			BUG();
		} else if (ins->conn_id < curr->conn_id) {
			p = &(*p)->rb_left;
		} else if (ins->conn_id > curr->conn_id) {
			p = &(*p)->rb_right;
		}
	}

	kref_get(&ins->ref);
	rb_link_node(&ins->rbn, parent, p);
	rb_insert_color(&ins->rbn, root);
}

void cor_insert_connid_reuse(struct cor_neighbor *nb, __u32 conn_id)
{
	unsigned long iflags;

	struct cor_connid_reuse_item *cir = kmem_cache_alloc(
			cor_connid_reuse_slab, GFP_ATOMIC);

	if (unlikely(cir == 0)) {
		BUILD_BUG_ON(CONNID_REUSE_RTTS > 255);

		spin_lock_irqsave(&nb->connid_reuse_lock, iflags);
		nb->connid_reuse_oom_countdown = CONNID_REUSE_RTTS;
		spin_unlock_irqrestore(&nb->connid_reuse_lock, iflags);

		return;
	}

	memset(cir, 0, sizeof(struct cor_connid_reuse_item));

	kref_init(&cir->ref);
	cir->conn_id = conn_id;

	spin_lock_irqsave(&nb->connid_reuse_lock, iflags);

	cir->pingcnt = nb->connid_reuse_pingcnt;

	_cor_insert_connid_reuse_insertrb(nb, cir);
	list_add_tail(&cir->lh, &nb->connid_reuse_list);

	spin_unlock_irqrestore(&nb->connid_reuse_lock, iflags);
}

static void cor_free_connid_reuse(struct kref *ref)
{
	struct cor_connid_reuse_item *cir = container_of(ref,
			struct cor_connid_reuse_item, ref);

	kmem_cache_free(cor_connid_reuse_slab, cir);
}

static void cor_delete_connid_reuse_items(struct cor_neighbor *nb)
{
	unsigned long iflags;
	struct cor_connid_reuse_item *cri;

	spin_lock_irqsave(&nb->connid_reuse_lock, iflags);

	while (list_empty(&nb->connid_reuse_list) == 0) {
		cri = container_of(nb->connid_reuse_list.next,
				struct cor_connid_reuse_item, lh);

		rb_erase(&cri->rbn, &nb->connid_reuse_rb);
		kref_put(&cri->ref, cor_kreffree_bug);

		list_del(&cri->lh);
		kref_put(&cri->ref, cor_free_connid_reuse);
	}

	spin_unlock_irqrestore(&nb->connid_reuse_lock, iflags);
}

static void cor_connid_used_pingsuccess(struct cor_neighbor *nb)
{
	unsigned long iflags;
	struct cor_connid_reuse_item *cri;

	spin_lock_irqsave(&nb->connid_reuse_lock, iflags);

	nb->connid_reuse_pingcnt++;
	while (list_empty(&nb->connid_reuse_list) == 0) {
		cri = container_of(nb->connid_reuse_list.next,
				struct cor_connid_reuse_item, lh);
		if ((cri->pingcnt + CONNID_REUSE_RTTS -
				nb->connid_reuse_pingcnt) < 32768)
			break;

		rb_erase(&cri->rbn, &nb->connid_reuse_rb);
		kref_put(&cri->ref, cor_kreffree_bug);

		list_del(&cri->lh);
		kref_put(&cri->ref, cor_free_connid_reuse);
	}

	if (unlikely(nb->connid_reuse_oom_countdown != 0))
		nb->connid_reuse_oom_countdown--;


	spin_unlock_irqrestore(&nb->connid_reuse_lock, iflags);
}

static int cor_connid_used(struct cor_neighbor *nb, __u32 conn_id)
{
	struct cor_conn *cn;
	struct cor_connid_reuse_item *cir;

	cn = cor_get_conn(nb, conn_id);
	if (unlikely(cn != 0)) {
		cor_conn_kref_put(cn, "stack");
		return 1;
	}

	cir = cor_get_connid_reuseitem(nb, conn_id);
	if (unlikely(cir != 0)) {
		kref_put(&cir->ref, cor_free_connid_reuse);
		return 1;
	}

	return 0;
}

int cor_connid_alloc(struct cor_neighbor *nb, struct cor_conn *src_in_ll)
{
	unsigned long iflags;
	struct cor_conn *trgt_out_ll = cor_get_conn_reversedir(src_in_ll);
	__u32 conn_id;
	int i;

	BUG_ON(src_in_ll->sourcetype != SOURCE_IN);
	BUG_ON(trgt_out_ll->targettype != TARGET_OUT);

	spin_lock_irqsave(&cor_connid_gen, iflags);
	for (i = 0; i < 16; i++) {
		conn_id = 0;
		get_random_bytes((char *) &conn_id, sizeof(conn_id));
		conn_id = (conn_id & ~(1 << 31));

		if (unlikely(conn_id == 0))
			continue;

		if (unlikely(cor_connid_used(nb, conn_id)))
			continue;

		goto found;
	}
	spin_unlock_irqrestore(&cor_connid_gen, iflags);

	return 1;

found:
	spin_lock_irqsave(&nb->connid_reuse_lock, iflags);
	if (unlikely(nb->connid_reuse_oom_countdown != 0)) {
		spin_unlock_irqrestore(&nb->connid_reuse_lock, iflags);
		return 1;
	}
	spin_unlock_irqrestore(&nb->connid_reuse_lock, iflags);


	src_in_ll->src.in.conn_id = conn_id;
	trgt_out_ll->trgt.out.conn_id = cor_get_connid_reverse(conn_id);
	if (unlikely(cor_insert_connid(nb, src_in_ll) != 0)) {
		BUG();
	}
	spin_unlock_irqrestore(&cor_connid_gen, iflags);
	return 0;
}

int __init cor_neighbor_init(void)
{
	cor_nb_slab = kmem_cache_create("cor_neighbor",
			sizeof(struct cor_neighbor), 8, 0, 0);
	if (unlikely(cor_nb_slab == 0))
		return -ENOMEM;

	atomic_set(&cor_num_neighs, 0);

	return 0;
}

void __exit cor_neighbor_exit2(void)
{
	BUG_ON(atomic_read(&cor_num_neighs) != 0);

	kmem_cache_destroy(cor_nb_slab);
	cor_nb_slab = 0;
}

MODULE_LICENSE("GPL");
