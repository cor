/**
 *	Connection oriented routing
 *	Copyright (C) 2007-2021 Michael Blizek
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License
 *	as published by the Free Software Foundation; either version 2
 *	of the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 */

#include <linux/mutex.h>

#include "cor.h"

struct kmem_cache *cor_data_buf_item_slab;

atomic64_t cor_bufused_sum;

/* __u64 get_bufspace_used(void)
{
	return atomic64_read(&cor_bufused_sum);
} */

void cor_databuf_init(struct cor_conn *cn_init)
{
	memset(&cn_init->data_buf, 0, sizeof(cn_init->data_buf));
	INIT_LIST_HEAD(&cn_init->data_buf.items);
}

void cor_bufsize_init(struct cor_conn *cn_l, __u32 bufsize)
{
	__u32 bufsize_shifted;

	memset(&cn_l->bufsize, 0, sizeof(cn_l->bufsize));

	if (unlikely((bufsize >> (32 - BUFSIZE_SHIFT)) != 0))
		bufsize_shifted = U32_MAX;
	else
		bufsize_shifted = bufsize << 5;

	cn_l->bufsize.bufsize = bufsize_shifted;
	cn_l->bufsize.state = BUFSIZE_NOACTION;
	cn_l->bufsize.act.noact.bytesleft = bufsize * 4;
}

int cor_account_bufspace(struct cor_conn *cn_lx)
{
	__u64 space_needed = 0;
	__u32 space_req;
	__u64 bufused_sum_int;

	if (likely(cn_lx->isreset == 0)) {
		space_needed += cn_lx->data_buf.datasize;
		space_needed += cn_lx->data_buf.overhead;
		if (cn_lx->sourcetype == SOURCE_IN) {
			space_needed += cn_lx->src.in.reorder_memused;
		}
	}

	if (cn_lx->bufspace_accounted == space_needed) {
		if (space_needed >= BUFUSAGE_PER_CONN_MAX) {
			return 1;
		} else if (atomic64_read(&cor_bufused_sum) >=
				BUFUSAGE_GLOBAL_MAX) {
			return 1;
		} else {
			return 0;
		}
	}

	if (unlikely(space_needed >= U32_MAX))
		space_req = U32_MAX;
	else
		space_req = space_needed;

	if (cn_lx->bufspace_accounted == space_req)
		return 1;

	bufused_sum_int = cor_update_atomic_sum(&cor_bufused_sum,
			cn_lx->bufspace_accounted, space_req);

	cn_lx->bufspace_accounted = space_req;

	if (cn_lx->targettype == TARGET_OUT && unlikely(
			(cn_lx->trgt.out.in_nb_busy_list == 0) !=
			(cn_lx->data_buf.datasize == 0)))
		cor_conn_set_last_act(cn_lx);

	if (space_needed != space_req)
		return 1;
	else if (bufused_sum_int >= BUFUSAGE_GLOBAL_MAX)
		return 1;
	else if (space_needed >= BUFUSAGE_PER_CONN_MAX)
		return 1;
	else
		return 0;
}

int cor_conn_src_unconn_write_allowed(struct cor_conn *src_unconn_lx)
{
	BUG_ON(src_unconn_lx->sourcetype != SOURCE_UNCONNECTED);

	if (src_unconn_lx->data_buf.datasize == 0)
		return 1;
	else if (src_unconn_lx->data_buf.datasize < BUFFERLIMIT_SRC_UNCONN &&
			cor_account_bufspace(src_unconn_lx) == 0)
		return 1;
	else
		return 0;
}

void cor_update_windowlimit(struct cor_conn *src_in_lx)
{
	__u32 bufsize;

	BUG_ON(src_in_lx->sourcetype != SOURCE_IN);

	bufsize = src_in_lx->bufsize.bufsize >> BUFSIZE_SHIFT;

	if (src_in_lx->targettype != TARGET_OUT) {
		if (bufsize < WINDOW_MAX_PER_CONN_MIN ||
				cor_account_bufspace(src_in_lx)) {
			bufsize = WINDOW_MAX_PER_CONN_MIN;
		}
	} else if (cor_seqno_before_eq(src_in_lx->trgt.out.seqno_windowlimit,
			src_in_lx->trgt.out.seqno_nextsend)) {
		if (cor_account_bufspace(src_in_lx)) {
			bufsize = min(bufsize, (__u32) WINDOW_MAX_PER_CONN_MIN);
		}
	} else {
		__u32 windowleft = src_in_lx->trgt.out.seqno_windowlimit -
				src_in_lx->trgt.out.seqno_nextsend;

		bufsize = max(bufsize, min(windowleft,
				(__u32) WINDOW_MAX_PER_CONN_MIN_OUT_WINOK));

		if (bufsize < WINDOW_MAX_PER_CONN_MIN ||
				cor_account_bufspace(src_in_lx)) {
			bufsize = WINDOW_MAX_PER_CONN_MIN;
		}
	}

	if (bufsize > WINDOW_MAX_PER_CONN_MAX)
		bufsize = WINDOW_MAX_PER_CONN_MAX;

	if (unlikely(src_in_lx->data_buf.read_remaining > bufsize))
		bufsize = 0;
	else
		bufsize -= src_in_lx->data_buf.read_remaining;

	if (unlikely(src_in_lx->targettype == TARGET_DISCARD))
		bufsize = 0;

	src_in_lx->src.in.window_seqnolimit =
			src_in_lx->src.in.next_seqno + bufsize;
}

static int cor_bufsize_high_latency_sender(struct cor_conn *cn_lx)
{
	struct cor_neighbor *nb;

	__u64 latency_us;

	if (cn_lx->sourcetype != SOURCE_IN)
		return 0;

	nb = cn_lx->src.in.nb;
	if (unlikely(nb == 0))
		return 0;

	latency_us = atomic_read(&nb->latency_retrans_us);
	latency_us += atomic_read(&nb->latency_stddev_retrans_us);
	latency_us += CMSG_MAXDELAY_ACKCONN_MS * 1000;

	return latency_us > 100000 ? 1 : 0;
}

__u8 _cor_bufsize_update_get_changerate(struct cor_conn *cn_lx)
{
	int high_latency_sender = cor_bufsize_high_latency_sender(cn_lx);
	int high_latency_conn = (cn_lx->is_highlatency != 0);

	__u32 changerate;

	if (cn_lx->bufsize.state == BUFSIZE_NOACTION) {
		changerate = 128;
	} else if (cn_lx->bufsize.state == BUFSIZE_DECR ||
			cn_lx->bufsize.state == BUFSIZE_DECR_FAST) {
		__u8 speed = 4;

		if (cn_lx->bufsize.state == BUFSIZE_DECR_FAST)
			speed *= 2;
		if (high_latency_sender)
			speed *= 2;
		if (high_latency_conn)
			speed /= 2;

		changerate = 128 - speed;
	} else if (cn_lx->bufsize.state == BUFSIZE_INCR ||
			cn_lx->bufsize.state == BUFSIZE_INCR_FAST) {
		__u8 speed = 4;

		if (high_latency_sender)
			speed *= 2;

		if (unlikely(cor_bufsize_initial_phase(cn_lx)))
			speed *= 4;
		else if (cn_lx->bufsize.state == BUFSIZE_INCR_FAST)
			speed *= 2;

		changerate = 128 + speed;
	} else {
		BUG();
	}

	/* printk(KERN_ERR "changerate1 %u\n", changerate); */

	if (cn_lx->targettype == TARGET_OUT) {
		__u16 remote_changerate = ((__u16)
				cn_lx->trgt.out.remote_bufsize_changerate) + 64;
		/* printk(KERN_ERR "changerate2 %u\n", remote_changerate); */
		changerate = (changerate * remote_changerate) / 128;
		/* printk(KERN_ERR "changerate3 %u\n", changerate); */
	}

	if (unlikely(changerate < 64))
		return 0;
	else if (unlikely(changerate - 64 >= 256))
		return 255;
	else
		return (__u8) (changerate - 64);
}

static void _cor_bufsize_update(struct cor_conn *cn_lx, __u32 rcvd,
		int high_latency_sender, int high_latency_conn)
{
	/* speed of increase/decrease changes with BUFSIZE_SHIFT */
	BUILD_BUG_ON(BUFSIZE_SHIFT != 3);

	/**
	 * If you change the speed here, change it in
	 * _cor_bufsize_update_get_changerate too
	 */

	if (cn_lx->bufsize.state == BUFSIZE_NOACTION) {
		if (likely(cn_lx->bufsize.act.noact.bytesleft >= rcvd)) {
			cn_lx->bufsize.act.noact.bytesleft -= rcvd;
			return;
		}

		rcvd -= cn_lx->bufsize.act.noact.bytesleft;
		cn_lx->bufsize.state = BUFSIZE_DECR;
		cn_lx->bufsize.act.decr.size_start = cn_lx->bufsize.bufsize;
	}

	if (cn_lx->bufsize.state == BUFSIZE_DECR ||
			cn_lx->bufsize.state == BUFSIZE_DECR_FAST) {
		__u8 speed = 1;
		__u32 change;

		if (cn_lx->bufsize.state == BUFSIZE_DECR_FAST)
			speed *= 2;

		if (high_latency_sender)
			speed *= 2;

		change = (rcvd * speed) / 4;
		if (high_latency_conn)
			change /= 2;

		if (cn_lx->bufsize.bufsize < change)
			cn_lx->bufsize.bufsize = 0;
		else
			cn_lx->bufsize.bufsize -= change;

		if (cn_lx->bufsize.act.decr.size_start / 4 >
				cn_lx->bufsize.bufsize)
			cn_lx->bufsize.state = BUFSIZE_DECR_FAST;
	} else if (cn_lx->bufsize.state == BUFSIZE_INCR ||
			cn_lx->bufsize.state == BUFSIZE_INCR_FAST) {
		__u8 speed = 1;
		__u32 change;

		if (high_latency_sender)
			speed *= 2;

		if (unlikely(cor_bufsize_initial_phase(cn_lx)))
			speed *= 4;
		else if (cn_lx->bufsize.state == BUFSIZE_INCR_FAST)
			speed *= 2;

		change = (rcvd * speed) / 4;
		if (cn_lx->bufsize.bufsize + change < cn_lx->bufsize.bufsize)
			cn_lx->bufsize.bufsize = U32_MAX;
		else
			cn_lx->bufsize.bufsize += change;

		if (cn_lx->bufsize.bufsize >=
				cn_lx->bufsize.act.incr.size_end) {
			cn_lx->bufsize.bufsize =
					cn_lx->bufsize.act.incr.size_end;
			cn_lx->bufsize.state = BUFSIZE_NOACTION;
			BUILD_BUG_ON(BUFSIZE_SHIFT < 3);
			if (high_latency_conn) {
				cn_lx->bufsize.act.noact.bytesleft =
						(cn_lx->bufsize.bufsize >>
						(BUFSIZE_SHIFT - 3));
			} else {
				cn_lx->bufsize.act.noact.bytesleft =
						(cn_lx->bufsize.bufsize >>
						(BUFSIZE_SHIFT - 2));
			}
		}
	} else {
		BUG();
	}

	if (unlikely(rcvd >= (1 << 24)) ||
			cn_lx->bufsize.bytes_rcvd + rcvd >= (1 << 24))
		cn_lx->bufsize.bytes_rcvd = (1 << 24) - 1;
	else
		cn_lx->bufsize.bytes_rcvd += rcvd;
}

static __u32 cor_get_read_remaining_min(__u32 bufsize_bytes,
		int high_latency_sender, int high_latency_conn)
{
	int bufspace_low = (atomic64_read(&cor_bufused_sum) >=
			3 * (BUFUSAGE_GLOBAL_MAX / 4));

	if (high_latency_conn) {
		if (high_latency_sender) {
			if (bufspace_low) {
				return bufsize_bytes / 6 + 1;
			} else {
				return bufsize_bytes / 3 + 1;
			}
		} else {
			if (bufspace_low) {
				return bufsize_bytes / 8 + 1;
			} else {
				return bufsize_bytes / 4 + 1;
			}
		}
	} else {
		if (high_latency_sender) {
			if (bufspace_low) {
				return bufsize_bytes / 6 + 1;
			} else {
				return bufsize_bytes / 4 + 1;
			}
		} else {
			if (bufspace_low) {
				return bufsize_bytes / 12 + 1;
			} else {
				return bufsize_bytes / 8 + 1;
			}
		}
	}
}

static void cor_bufsize_update(struct cor_conn *cn_lx, __u32 rcvd,
		__u8 windowused, __u8 rcv_flushrcvd)
{
	int high_latency_sender = cor_bufsize_high_latency_sender(cn_lx);
	int high_latency_conn = (cn_lx->is_highlatency != 0);
	__u32 bufsize_bytes = (cn_lx->bufsize.bufsize >> BUFSIZE_SHIFT);
	__u32 read_remaining_min = cor_get_read_remaining_min(bufsize_bytes,
			high_latency_sender, high_latency_conn);
	__u32 read_remaining_min_nofastdecr = read_remaining_min * 2;
	__u32 read_remaining_min_nodecr = (read_remaining_min +
			read_remaining_min_nofastdecr) / 2;
	__u32 read_remaining;

	BUG_ON(cn_lx->data_buf.read_remaining < rcvd);
	BUG_ON(windowused > 31);

	/* if (cn_lx->is_highlatency == 0)
		printk(KERN_ERR "bufsize %p %u %u %u %u %u %u\n",
				cn_lx, bufsize_bytes,
				cn_lx->data_buf.read_remaining, rcvd,
				windowused, rcv_flushrcvd,
				cn_lx->bufsize.ignore_rcv_lowbuf); */

	if (cn_lx->bufsize.ignore_rcv_lowbuf > 0) {
		if (rcvd > cn_lx->bufsize.ignore_rcv_lowbuf)
			cn_lx->bufsize.ignore_rcv_lowbuf = 0;
		else
			cn_lx->bufsize.ignore_rcv_lowbuf -= rcvd;

		read_remaining = bufsize_bytes;
	} else {
		read_remaining = max(cn_lx->data_buf.read_remaining - rcvd,
				(bufsize_bytes * (31 - windowused)) / 31);
	}

	if (rcv_flushrcvd != 0) {
		__u32 bytesleft = (cn_lx->bufsize.bufsize >> BUFSIZE_SHIFT);

		if (cn_lx->bufsize.state == BUFSIZE_DECR ||
				cn_lx->bufsize.state == BUFSIZE_DECR_FAST) {
			cn_lx->bufsize.state = BUFSIZE_NOACTION;
			cn_lx->bufsize.act.noact.bytesleft = bytesleft;
		} else if (cn_lx->bufsize.state == BUFSIZE_NOACTION &&
				cn_lx->bufsize.act.noact.bytesleft <
				bytesleft) {
			cn_lx->bufsize.act.noact.bytesleft = bytesleft;
		}
	}

	if (read_remaining < read_remaining_min) {
		__u32 buf_increase_bytes = read_remaining_min - read_remaining;
		__u32 buf_increase;
		__u32 bufsize_end;

		if (high_latency_sender) {
			if (buf_increase_bytes < rcvd / 16)
				buf_increase_bytes = rcvd / 16;
		} else {
			if (buf_increase_bytes < rcvd / 32)
				buf_increase_bytes = rcvd / 32;
		}

		buf_increase = (buf_increase_bytes << BUFSIZE_SHIFT);
		if (unlikely((buf_increase >> BUFSIZE_SHIFT) !=
				buf_increase_bytes))
			buf_increase = U32_MAX;

		bufsize_end = cn_lx->bufsize.bufsize + buf_increase;
		if (unlikely(bufsize_end < cn_lx->bufsize.bufsize))
			bufsize_end = U32_MAX;

		if (cn_lx->bufsize.state != BUFSIZE_INCR &&
				cn_lx->bufsize.state != BUFSIZE_INCR_FAST) {
			cn_lx->bufsize.state = BUFSIZE_INCR;
			cn_lx->bufsize.act.incr.size_start =
					cn_lx->bufsize.bufsize;
			cn_lx->bufsize.act.incr.size_end = 0;
		}

		if (bufsize_end > cn_lx->bufsize.act.incr.size_end)
			cn_lx->bufsize.act.incr.size_end = bufsize_end;
		if (bufsize_end / 4 > cn_lx->bufsize.act.incr.size_start)
			cn_lx->bufsize.state = BUFSIZE_INCR_FAST;
	} else if (read_remaining < read_remaining_min_nodecr) {
		if (cn_lx->bufsize.state == BUFSIZE_NOACTION ||
				cn_lx->bufsize.state == BUFSIZE_DECR ||
				cn_lx->bufsize.state == BUFSIZE_DECR_FAST) {
			__u32 bytesleft = 0;
			__u8 rtt_mul = 2;

			if (cn_lx->bufsize.state == BUFSIZE_NOACTION)
				bytesleft = cn_lx->bufsize.act.noact.bytesleft;
			if (high_latency_conn)
				rtt_mul *= 2;
			if (likely((cn_lx->bufsize.bufsize >> BUFSIZE_SHIFT) *
					rtt_mul > bytesleft))
				bytesleft = (cn_lx->bufsize.bufsize >>
						BUFSIZE_SHIFT) * rtt_mul;

			cn_lx->bufsize.state = BUFSIZE_NOACTION;
			cn_lx->bufsize.act.noact.bytesleft = bytesleft;
		}
	} else if (read_remaining < read_remaining_min_nofastdecr) {
		if (cn_lx->bufsize.state == BUFSIZE_DECR ||
				cn_lx->bufsize.state == BUFSIZE_DECR_FAST) {
			cn_lx->bufsize.state = BUFSIZE_DECR;
			cn_lx->bufsize.act.decr.size_start =
					cn_lx->bufsize.bufsize;
		}
	}

	if (cn_lx->targettype == TARGET_OUT) {
		__u16 rem_changerate = ((__u16)
				cn_lx->trgt.out.remote_bufsize_changerate) + 64;
		__u16 crate_nofastdecr;
		__u16 crate_nodecr;
		__u16 crate_nofastincr;
		__u16 crate_noincr;

		if (high_latency_conn) {
			crate_nofastdecr = 128 - 128 / 8;
			crate_nodecr = 128 - 128 / 6;
			crate_nofastincr = 128 + 128 / 4;
			crate_noincr = 128 + 128 / 3;
		} else {
			crate_nofastdecr = 128 - 128 / 16;
			crate_nodecr = 128 - 128 / 12;
			crate_nofastincr = 128 + 128 / 8;
			crate_noincr = 128 + 128 / 6;
		}

		if ((rem_changerate < crate_nodecr ||
				rem_changerate > crate_noincr) &&
				cn_lx->bufsize.state == BUFSIZE_NOACTION) {
			cn_lx->bufsize.act.noact.bytesleft = max(
					cn_lx->bufsize.act.noact.bytesleft,
					cn_lx->bufsize.bufsize >>
					BUFSIZE_SHIFT);
		}

		if (rem_changerate < crate_nodecr && (
				cn_lx->bufsize.state == BUFSIZE_DECR ||
				cn_lx->bufsize.state == BUFSIZE_DECR_FAST)) {
			cn_lx->bufsize.state = BUFSIZE_NOACTION;
			cn_lx->bufsize.act.noact.bytesleft =
					cn_lx->bufsize.bufsize >> BUFSIZE_SHIFT;
		}
		if (rem_changerate < crate_nofastdecr &&
				cn_lx->bufsize.state == BUFSIZE_DECR_FAST) {
			cn_lx->bufsize.state = BUFSIZE_DECR;
			cn_lx->bufsize.act.decr.size_start =
					cn_lx->bufsize.bufsize;
		}

		if (rem_changerate > crate_noincr && (
				cn_lx->bufsize.state == BUFSIZE_INCR ||
				cn_lx->bufsize.state == BUFSIZE_INCR_FAST)) {
			cn_lx->bufsize.state = BUFSIZE_NOACTION;
			cn_lx->bufsize.act.noact.bytesleft =
					cn_lx->bufsize.bufsize >> BUFSIZE_SHIFT;
		}
		if (rem_changerate > crate_nofastincr &&
				cn_lx->bufsize.state == BUFSIZE_INCR_FAST) {
			cn_lx->bufsize.state = BUFSIZE_INCR;
			cn_lx->bufsize.act.incr.size_start =
					cn_lx->bufsize.bufsize;
		}
	}

	_cor_bufsize_update(cn_lx, rcvd, high_latency_sender,
			high_latency_conn);

#ifdef FIXED_CONN_BUFSIZE
	cn_lx->bufsize.bufsize = FIXED_CONN_BUFSIZE << BUFSIZE_SHIFT;
	cn_lx->bufsize.state = BUFSIZE_NOACTION;
	cn_lx->bufsize.act.noact.bytesleft =
			cn_lx->bufsize.bufsize >> BUFSIZE_SHIFT;
#endif
}

void cor_bufsize_read_to_sock(struct cor_conn *trgt_sock_lx)
{
	unsigned long jiffies_tmp = jiffies;
	__u32 latency_limit = (trgt_sock_lx->is_highlatency != 0 ?
			HZ / 10 : HZ / 40);

	/**
	 * High cpu usage may cause high latency of the userspace receiver.
	 * Increasing bufferspace to compensate may increase latency further.
	 */
	if (trgt_sock_lx->trgt.sock.waiting_for_userspace != 0 && time_before(
			trgt_sock_lx->trgt.sock.waiting_for_userspace_since,
			jiffies - latency_limit)) {
		trgt_sock_lx->bufsize.ignore_rcv_lowbuf =
				trgt_sock_lx->bufsize.bufsize >> BUFSIZE_SHIFT;
	}

	if (trgt_sock_lx->data_buf.read_remaining == 0) {
		trgt_sock_lx->trgt.sock.waiting_for_userspace = 0;
	} else {
		trgt_sock_lx->trgt.sock.waiting_for_userspace = 1;
		trgt_sock_lx->trgt.sock.waiting_for_userspace_since =
				jiffies_tmp;
	}
}

static inline void cor_databuf_item_unlink(struct cor_conn *cn_lx,
		struct cor_data_buf_item *item)
{
	BUG_ON(item == cn_lx->data_buf.nextread);
	list_del(&item->buf_list);
	if (item->type == DATABUF_BUF) {
		cn_lx->data_buf.overhead -= sizeof(struct cor_data_buf_item) +
				item->buflen - item->datalen;
	} else if (item->type == DATABUF_SKB) {
		cn_lx->data_buf.overhead -= sizeof(struct sk_buff);
	} else {
		BUG();
	}
}

void cor_databuf_ackdiscard(struct cor_conn *cn_lx)
{
	__u32 freed = 0;

	cn_lx->data_buf.next_read_offset = 0;
	cn_lx->data_buf.nextread = 0;

	while (!list_empty(&cn_lx->data_buf.items)) {
		struct cor_data_buf_item *item = container_of(
				cn_lx->data_buf.items.next,
				struct cor_data_buf_item, buf_list);
		freed += item->datalen;

		cor_databuf_item_unlink(cn_lx, item);
		cor_databuf_item_free(item);
	}

	cn_lx->data_buf.datasize -= freed;
	cn_lx->data_buf.first_offset += freed;

	BUG_ON(cn_lx->data_buf.datasize != 0);
	BUG_ON(cn_lx->data_buf.overhead != 0);

	cn_lx->data_buf.read_remaining = 0;
}

void cor_reset_seqno(struct cor_conn *cn_l, __u32 initseqno)
{
	cn_l->data_buf.first_offset = initseqno -
			cn_l->data_buf.datasize +
			cn_l->data_buf.read_remaining;
}

static void cor_databuf_nextreadchunk(struct cor_conn *cn_lx)
{
	BUG_ON(cn_lx->data_buf.nextread == 0);
	BUG_ON(cn_lx->data_buf.next_read_offset !=
			cn_lx->data_buf.nextread->datalen);

	if (&cn_lx->data_buf.nextread->buf_list == cn_lx->data_buf.items.prev) {
		BUG_ON(cn_lx->data_buf.read_remaining != 0);
		cn_lx->data_buf.nextread = 0;

	} else {
		BUG_ON(cn_lx->data_buf.read_remaining == 0);
		cn_lx->data_buf.nextread = container_of(
				cn_lx->data_buf.nextread->buf_list.next,
				struct cor_data_buf_item, buf_list);
	}

	cn_lx->data_buf.next_read_offset = 0;
}

void cor_databuf_pull(struct cor_conn *cn_lx, char *dst, __u32 len)
{
	BUG_ON(cn_lx->data_buf.read_remaining < len);

	while (len > 0) {
		int cpy = len;

		char *srcbufcpystart = 0;
		int srcbufcpylen = 0;

		BUG_ON(cn_lx->data_buf.nextread == 0);
		BUG_ON(cn_lx->data_buf.next_read_offset >=
				cn_lx->data_buf.nextread->datalen);

		srcbufcpystart = cn_lx->data_buf.nextread->buf +
				cn_lx->data_buf.next_read_offset;
		srcbufcpylen = cn_lx->data_buf.nextread->datalen -
				cn_lx->data_buf.next_read_offset;

		if (cpy > srcbufcpylen)
			cpy = srcbufcpylen;

		memcpy(dst, srcbufcpystart, cpy);

		dst += cpy;
		len -= cpy;

		cn_lx->data_buf.read_remaining -= cpy;
		cn_lx->data_buf.next_read_offset += cpy;

		if (cpy == srcbufcpylen)
			cor_databuf_nextreadchunk(cn_lx);
	}
}

void cor_databuf_unpull_dpi(struct cor_conn *trgt_sock, struct cor_sock *cs,
		struct cor_data_buf_item *item, __u16 next_read_offset)
{
	BUG_ON(next_read_offset > item->datalen);

	if (next_read_offset >= item->datalen)
		goto free;

	spin_lock_bh(&trgt_sock->rcv_lock);

	if (unlikely(cor_is_trgt_sock(trgt_sock, cs) == 0)) {
		spin_unlock_bh(&trgt_sock->rcv_lock);
		goto free;
	}

	BUG_ON(trgt_sock->data_buf.nextread != 0 &&
			&trgt_sock->data_buf.nextread->buf_list !=
			trgt_sock->data_buf.items.next);
	BUG_ON(trgt_sock->data_buf.next_read_offset != 0);

	trgt_sock->data_buf.first_offset -= item->datalen;
	trgt_sock->data_buf.datasize += item->datalen;
	trgt_sock->data_buf.read_remaining += item->datalen - next_read_offset;

	if (item->type == DATABUF_BUF) {
		trgt_sock->data_buf.overhead +=
				sizeof(struct cor_data_buf_item) +
				item->buflen - item->datalen;
	} else if (item->type == DATABUF_SKB) {
		trgt_sock->data_buf.overhead += sizeof(struct sk_buff);
	} else {
		BUG();
	}

	list_add(&item->buf_list, &trgt_sock->data_buf.items);

	trgt_sock->data_buf.nextread = item;
	trgt_sock->data_buf.next_read_offset = next_read_offset;

	cor_account_bufspace(trgt_sock);

	spin_unlock_bh(&trgt_sock->rcv_lock);

	if (0) {
free:
		cor_databuf_item_free(item);
	}
}

void cor_databuf_pull_dbi(struct cor_sock *cs_rl, struct cor_conn *trgt_sock_l)
{
	struct cor_data_buf_item *dbi = 0;
	BUG_ON(cs_rl->type != CS_TYPE_CONN_RAW);
	BUG_ON(cs_rl->data.conn_raw.rcvitem != 0);

	if (trgt_sock_l->data_buf.read_remaining == 0)
		return;

	BUG_ON(trgt_sock_l->data_buf.nextread == 0);
	BUG_ON(trgt_sock_l->data_buf.next_read_offset >=
				trgt_sock_l->data_buf.nextread->datalen);
	dbi = trgt_sock_l->data_buf.nextread;

	BUG_ON(&dbi->buf_list != trgt_sock_l->data_buf.items.next);

	cs_rl->data.conn_raw.rcvitem = dbi;
	cs_rl->data.conn_raw.rcvoffset = trgt_sock_l->data_buf.next_read_offset;

	trgt_sock_l->data_buf.first_offset += dbi->datalen;
	trgt_sock_l->data_buf.datasize -= dbi->datalen;
	trgt_sock_l->data_buf.read_remaining -= dbi->datalen;

	cor_account_bufspace(trgt_sock_l);

	trgt_sock_l->data_buf.next_read_offset = dbi->datalen;
	cor_databuf_nextreadchunk(trgt_sock_l);

	cor_databuf_item_unlink(trgt_sock_l, dbi);
}

void cor_databuf_unpull(struct cor_conn *trgt_out_l, __u32 bytes)
{
	trgt_out_l->data_buf.read_remaining += bytes;

	BUG_ON(list_empty(&trgt_out_l->data_buf.items) != 0);

	if (trgt_out_l->data_buf.nextread == 0) {
		BUG_ON(trgt_out_l->data_buf.next_read_offset != 0);

		trgt_out_l->data_buf.nextread = container_of(
				trgt_out_l->data_buf.items.prev,
				struct cor_data_buf_item, buf_list);
	}

	while (bytes > trgt_out_l->data_buf.next_read_offset) {
		bytes -= trgt_out_l->data_buf.next_read_offset;
		trgt_out_l->data_buf.nextread = container_of(
				trgt_out_l->data_buf.nextread->buf_list.prev,
				struct cor_data_buf_item, buf_list);
		BUG_ON(&trgt_out_l->data_buf.nextread->buf_list ==
				&trgt_out_l->data_buf.items);
		trgt_out_l->data_buf.next_read_offset =
				trgt_out_l->data_buf.nextread->datalen;
	}

	trgt_out_l->data_buf.next_read_offset -= bytes;
}

void cor_databuf_pullold(struct cor_conn *trgt_out_l, __u32 startpos, char *dst,
		int len)
{
	__u32 pos = trgt_out_l->data_buf.first_offset;
	struct cor_data_buf_item *dbi = container_of(
			trgt_out_l->data_buf.items.next,
			struct cor_data_buf_item, buf_list);

	while (1) {
		BUG_ON(&dbi->buf_list == &trgt_out_l->data_buf.items);

		if (cor_seqno_after(pos + dbi->datalen, startpos))
			break;

		pos += dbi->datalen;
		dbi = container_of(dbi->buf_list.next, struct cor_data_buf_item,
				buf_list);
	}

	while (len > 0) {
		int cpy = len;

		char *srcbufcpystart = 0;
		int srcbufcpylen = 0;

		__u32 offset = startpos - pos;

		BUG_ON(&dbi->buf_list == &trgt_out_l->data_buf.items);

		BUG_ON(cor_seqno_before(startpos, pos));
		BUG_ON(offset > dbi->datalen);

		srcbufcpystart = dbi->buf + offset;
		srcbufcpylen = dbi->datalen - offset;

		if (cpy > srcbufcpylen)
			cpy = srcbufcpylen;

		memcpy(dst, srcbufcpystart, cpy);

		dst += cpy;
		len -= cpy;
		startpos += cpy;

		pos += dbi->datalen;
		dbi = container_of(dbi->buf_list.next, struct cor_data_buf_item,
				buf_list);
	}
}

/* ack up to *not* including pos */
void cor_databuf_ack(struct cor_conn *trgt_out_l, __u32 pos)
{
	__u32 acked = 0;

	while (!list_empty(&trgt_out_l->data_buf.items)) {
		struct cor_data_buf_item *firstitem = container_of(
				trgt_out_l->data_buf.items.next,
				struct cor_data_buf_item, buf_list);

		if (firstitem == trgt_out_l->data_buf.nextread)
			break;

		if (cor_seqno_after_eq(trgt_out_l->data_buf.first_offset +
				firstitem->datalen, pos))
			break;

		trgt_out_l->data_buf.first_offset += firstitem->datalen;
		acked += firstitem->datalen;

		cor_databuf_item_unlink(trgt_out_l, firstitem);
		cor_databuf_item_free(firstitem);
	}

	trgt_out_l->data_buf.datasize -= acked;

	BUG_ON(trgt_out_l->data_buf.datasize == 0 &&
			trgt_out_l->data_buf.overhead != 0);

	if (unlikely(trgt_out_l->trgt.out.nblist_busy_remaining <= acked)) {
		trgt_out_l->trgt.out.nblist_busy_remaining = 0;
		cor_conn_set_last_act(trgt_out_l);
	} else {
		trgt_out_l->trgt.out.nblist_busy_remaining -= acked;
	}

	if (acked != 0)
		cor_account_bufspace(trgt_out_l);
}

void cor_databuf_ackread(struct cor_conn *cn_lx)
{
	__u32 acked = 0;

	while (!list_empty(&cn_lx->data_buf.items)) {
		struct cor_data_buf_item *firstitem = container_of(
				cn_lx->data_buf.items.next,
				struct cor_data_buf_item, buf_list);

		if (firstitem == cn_lx->data_buf.nextread)
			break;

		acked += firstitem->datalen;

		cor_databuf_item_unlink(cn_lx, firstitem);
		cor_databuf_item_free(firstitem);
	}

	cn_lx->data_buf.datasize -= acked;
	cn_lx->data_buf.first_offset += acked;

	BUG_ON(cn_lx->data_buf.datasize == 0 && cn_lx->data_buf.overhead != 0);

	if (cn_lx->targettype == TARGET_OUT) {
		if (unlikely(cn_lx->trgt.out.nblist_busy_remaining <=
				acked)) {
			cn_lx->trgt.out.nblist_busy_remaining = 0;
			cor_conn_set_last_act(cn_lx);
		} else {
			cn_lx->trgt.out.nblist_busy_remaining -= acked;
		}
	}

	if (acked != 0)
		cor_account_bufspace(cn_lx);
}

__u32 _cor_receive_buf(struct cor_conn *cn_lx, char *buf, __u32 datalen,
		int from_sock, __u8 windowused, __u8 flush)
{
	struct cor_data_buf_item *item = 0;

	__u32 totalcpy = 0;

	if (list_empty(&cn_lx->data_buf.items) == 0) {
		struct list_head *last = cn_lx->data_buf.items.prev;

		item = container_of(last, struct cor_data_buf_item, buf_list);
	}

	while (datalen > 0) {
		__u32 cpy = datalen;

		BUG_ON(cn_lx->data_buf.datasize + datalen > (1 << 30));
		BUG_ON(cn_lx->data_buf.overhead > (1 << 30));

		if (item == 0 || item->type != DATABUF_BUF ||
				item->buflen <= item->datalen) {
			item = kmem_cache_alloc(cor_data_buf_item_slab,
					GFP_ATOMIC);
			if (unlikely(item == 0))
				break;

			memset(item, 0, sizeof(struct cor_data_buf_item));
			item->type = DATABUF_BUF;

			item->buflen = cor_buf_optlen(datalen, from_sock);
			item->buf = kmalloc(item->buflen, GFP_ATOMIC);

			if (unlikely(item->buf == 0)) {
				kmem_cache_free(cor_data_buf_item_slab, item);
				break;
			}
			item->datalen = 0;

			list_add_tail(&item->buf_list, &cn_lx->data_buf.items);

			cn_lx->data_buf.overhead += item->buflen +
					sizeof(struct cor_data_buf_item);
		}

		BUG_ON(item->type != DATABUF_BUF);
		BUG_ON(item->buflen <= item->datalen);

		if (cn_lx->data_buf.nextread == 0) {
			cn_lx->data_buf.nextread = item;
			cn_lx->data_buf.next_read_offset = item->datalen;
		}

		if (item->buflen - item->datalen < cpy)
			cpy = (item->buflen - item->datalen);

		memcpy(item->buf + item->datalen, buf, cpy);
		item->datalen += cpy;

		BUG_ON(cpy > datalen);
		buf += cpy;
		datalen -= cpy;
		totalcpy += cpy;

		cn_lx->data_buf.read_remaining += cpy;
		cn_lx->data_buf.datasize += cpy;
		cn_lx->data_buf.overhead -= cpy;
		BUG_ON(cn_lx->data_buf.datasize != 0 &&
				cn_lx->data_buf.overhead == 0);
	}

	if (datalen != 0)
		flush = 0;
	cn_lx->flush = flush;

	cor_account_bufspace(cn_lx);
	cor_bufsize_update(cn_lx, totalcpy, windowused, flush);

	return totalcpy;
}

__u32 cor_receive_skb(struct cor_conn *src_in_l, struct sk_buff *skb,
		__u8 windowused, __u8 flush)
{
	struct cor_skb_procstate *ps = cor_skb_pstate(skb);
	struct cor_data_buf_item *item = &ps->funcstate.rcv.dbi;

	__u32 bufferleft = 0;

	BUG_ON(skb->len <= 0);

	if (unlikely(unlikely(src_in_l->data_buf.datasize + skb->len >
			(1 << 30)) || unlikely(src_in_l->data_buf.overhead >
			(1 << 30))))
		return 0;

	if (list_empty(&src_in_l->data_buf.items) == 0) {
		struct list_head *last = src_in_l->data_buf.items.prev;
		struct cor_data_buf_item *item = container_of(last,
				struct cor_data_buf_item, buf_list);
		bufferleft = item->buflen - item->datalen;
	}

	if (skb->len < (sizeof(struct sk_buff) + bufferleft)) {
		__u32 rc = cor_receive_buf(src_in_l, skb->data, skb->len,
				windowused, flush);
		if (likely(rc == skb->len))
			kfree_skb(skb);
		return rc;
	}

	memset(item, 0, sizeof(struct cor_data_buf_item));

	item->type = DATABUF_SKB;
	item->buf = skb->data;
	item->datalen = skb->len;
	item->buflen = item->datalen;
	list_add_tail(&item->buf_list, &src_in_l->data_buf.items);
	if (src_in_l->data_buf.nextread == 0)
		src_in_l->data_buf.nextread = item;

	src_in_l->data_buf.read_remaining += item->datalen;
	src_in_l->data_buf.datasize += item->datalen;
	src_in_l->data_buf.overhead += sizeof(struct sk_buff);

	cor_account_bufspace(src_in_l);
	cor_bufsize_update(src_in_l, skb->len, windowused, flush);

	src_in_l->flush = flush;

	return skb->len;
}

void cor_wake_sender(struct cor_conn *cn)
{
	spin_lock_bh(&cn->rcv_lock);

	if (unlikely(cn->isreset)) {
		spin_unlock_bh(&cn->rcv_lock);
		return;
	}

	switch (cn->sourcetype) {
	case SOURCE_UNCONNECTED:
		spin_unlock_bh(&cn->rcv_lock);
		spin_lock_bh(&cor_get_conn_reversedir(cn)->rcv_lock);
		if (likely(cor_get_conn_reversedir(cn)->isreset == 0 &&
				cor_get_conn_reversedir(cn)->targettype ==
				TARGET_UNCONNECTED))
			cor_proc_cpacket(cor_get_conn_reversedir(cn));
		spin_unlock_bh(&cor_get_conn_reversedir(cn)->rcv_lock);
		break;
	case SOURCE_SOCK:
		if (_cor_mngdsocket_flushtoconn(cn) == RC_FTC_OK &&
				cn->src.sock.ed->cs != 0 &&
				cor_sock_sndbufavailable(cn, 1))
			cor_sk_write_space(cn->src.sock.ed->cs);
		spin_unlock_bh(&cn->rcv_lock);
		break;
	case SOURCE_IN:
		cor_drain_ooo_queue(cn);
		if (likely(cn->src.in.established != 0))
			cor_send_ack_conn_ifneeded(cn, 0, 0);
		spin_unlock_bh(&cn->rcv_lock);
		break;
	default:
		BUG();
	}
}

int __init cor_forward_init(void)
{
	cor_data_buf_item_slab = kmem_cache_create("cor_data_buf_item",
			sizeof(struct cor_data_buf_item), 8, 0, 0);
	if (unlikely(cor_data_buf_item_slab == 0))
		return -ENOMEM;

	atomic64_set(&cor_bufused_sum, 0);

	return 0;
}

void __exit cor_forward_exit2(void)
{
	BUG_ON(atomic64_read(&cor_bufused_sum) != 0);

	kmem_cache_destroy(cor_data_buf_item_slab);
	cor_data_buf_item_slab = 0;
}

MODULE_LICENSE("GPL");
