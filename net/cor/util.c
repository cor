/**
 *	Connection oriented routing
 *	Copyright (C) 2007-2021 Michael Blizek
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License
 *	as published by the Free Software Foundation; either version 2
 *	of the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 */

#include "cor.h"


static const __u32 cor_log_64_11_table[] = {0,
		64, 68, 73, 77, 82, 88, 93, 99, 106, 113, 120,
		128, 136, 145, 155, 165, 175, 187, 199, 212, 226, 240,
		256, 273, 290, 309, 329, 351, 374, 398, 424, 451, 481,
		512, 545, 581, 619, 659, 702, 747, 796, 848, 903, 961,
		1024, 1091, 1162, 1237, 1318, 1403, 1495, 1592, 1695, 1805,
				1923,
		2048, 2181, 2323, 2474, 2635, 2806, 2989, 3183, 3390, 3611,
				3846,
		4096, 4362, 4646, 4948, 5270, 5613, 5978, 6367, 6781, 7222,
				7692,
		8192, 8725, 9292, 9897, 10540, 11226, 11956, 12734, 13562,
				14444, 15383,
		16384, 17450, 18585, 19793, 21081, 22452, 23912, 25467, 27124,
				28888, 30767,
		32768, 34899, 37169, 39587, 42161, 44904, 47824, 50935, 54248,
				57776, 61534,
		65536, 69799, 74338, 79173, 84323, 89807, 95648, 101870, 108495,
				115552, 123068,
		131072, 139597, 148677, 158347, 168646, 179615, 191297, 203739,
				216991, 231104, 246135,
		262144, 279194, 297353, 316693, 337291, 359229, 382594, 407478,
				433981, 462208, 492270,
		524288, 558388, 594706, 633387, 674583, 718459, 765188, 814957,
				867962, 924415, 984540,
		1048576, 1116777, 1189413, 1266774, 1349166, 1436917, 1530376,
				1629913, 1735924, 1848831, 1969081,
		2097152, 2233553, 2378826, 2533547, 2698332, 2873834, 3060752,
				3259826, 3471849, 3697662, 3938162,
		4194304, 4467106, 4757652, 5067094, 5396664, 5747669, 6121503,
				6519652, 6943698, 7395323, 7876323,
		8388608, 8934212, 9515303, 10134189, 10793327, 11495337,
				12243006, 13039305, 13887396, 14790647,
				15752647,
		16777216, 17868424, 19030606, 20268378, 21586655, 22990674,
				24486013, 26078610, 27774791, 29581294,
				31505293,
		33554432, 35736849, 38061212, 40536755, 43173310, 45981349,
				48972026, 52157220, 55549582, 59162588,
				63010587,
		67108864, 71473698, 76122425, 81073510, 86346620, 91962698,
				97944052, 104314440, 111099165, 118325175,
				126021174,
		134217728, 142947395, 152244850, 162147020, 172693239,
				183925396, 195888104, 208628880, 222198329,
				236650351, 252042347,
		268435456, 285894791, 304489699, 324294041, 345386479,
				367850791, 391776208, 417257759, 444396658,
				473300701, 504084694,
		536870912, 571789581};

__u8 __attribute__((const)) cor_enc_log_64_11(__u32 value)
{
	int i;

	BUILD_BUG_ON(cor_log_64_11_table[255] != 571789581);
	for (i = 1; i < 256; i++) {
		if (cor_log_64_11_table[i] > value)
			break;
	}

	return (__u8)(i - 1); /* round down */
}

__u32 __attribute__((const)) cor_dec_log_64_11(__u8 value)
{
	BUILD_BUG_ON(cor_log_64_11_table[255] != 571789581);
	return cor_log_64_11_table[value];
}

static void __init cor_check_log_64_11_table(void)
{
	int i;

	BUG_ON(cor_log_64_11_table[0] != 0);
	for (i = 1; i < 256; i++) {
		BUG_ON(cor_log_64_11_table[i] <= cor_log_64_11_table[i - 1]);
	}
}


void cor_swap_list_items(struct list_head *lh1, struct list_head *lh2)
{
	struct list_head *tmp;

	tmp = (lh1->next == lh1 ? lh2 : lh1->next);
	lh1->next = (lh2->next == lh2 ? lh1 : lh2->next);
	lh2->next = tmp;

	tmp = (lh1->prev == lh1 ? lh2 : lh2->prev);
	lh1->prev = (lh2->prev == lh2 ? lh1 : lh2->prev);
	lh2->prev = tmp;

	lh1->next->prev = lh1;
	lh1->prev->next = lh1;

	lh2->next->prev = lh2;
	lh2->prev->next = lh2;
}

__u64 cor_update_atomic_sum(atomic64_t *atomic_sum, __u32 oldvalue,
		__u32 newvalue)
{
	__u64 sum_old = atomic64_read(atomic_sum);
	__u64 sum;

	while (1) {
		__u64 cmpxchg_ret;

		sum = sum_old;

		BUG_ON(sum < oldvalue);
		sum -= oldvalue;

		BUG_ON(sum + newvalue < sum);
		sum += newvalue;

		cmpxchg_ret = atomic64_cmpxchg(atomic_sum, sum_old, sum);

		if (likely(cmpxchg_ret == sum_old))
			break;

		sum_old = cmpxchg_ret;
	}

	return sum;
}

void cor_kreffree_bug(struct kref *ref)
{
	BUG();
}

int __init cor_util_init(void)
{
	cor_check_log_64_11_table();

	return 0;
}

MODULE_LICENSE("GPL");
