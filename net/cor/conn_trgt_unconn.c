/**
 *	Connection oriented routing
 *	Copyright (C) 2007-2021 Michael Blizek
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License
 *	as published by the Free Software Foundation; either version 2
 *	of the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 */

#include "cor.h"

int cor_encode_len(char *buf, int buflen, __u32 len)
{
	BUG_ON(buf == 0);
	BUG_ON(buflen < 4);

	if (len < 128) {
		buf[0] = (__u8) len;
		return 1;
	} else if (len < 16512) {
		__u16 len_be = cpu_to_be16(len - 128);
		char *len_p = (char *) &len_be;

		buf[0] = len_p[0] + 128;
		buf[1] = len_p[1];
		return 2;
	} else if (len < 1073758336) {
		__u32 len_be = cpu_to_be32(len - 16512);
		char *len_p = (char *) &len_be;

		buf[0] = len_p[0] + 192;
		buf[1] = len_p[1];
		buf[2] = len_p[2];
		buf[3] = len_p[3];
		return 4;
	} else {
		return -1;
	}
}

int cor_decode_len(char *buf, int buflen, __u32 *len)
{
	__u8 b0 = (__u8) buf[0];

	*len = 0;

	if (buflen >= 1 && b0 < 128) {
		*len = (__u8) buf[0];
		return 1;
	} else if (buflen >= 2 && b0 >= 128 && b0 < 192) {
		((char *) len)[0] = buf[0] - 128;
		((char *) len)[1] = buf[1];

		*len = be16_to_cpu(*len) + 128;
		return 2;
	} else if (buflen >= 4 && b0 >= 192) {
		((char *) len)[0] = buf[0] - 192;
		((char *) len)[1] = buf[1];
		((char *) len)[2] = buf[2];
		((char *) len)[3] = buf[3];

		*len = be32_to_cpu(*len) + 16512;
		return 4;
	} else {
		return 0;
	}
}

static void cor_receive_cpacketresp(struct cor_conn *src_unconn_ll, char *buf,
		__u32 len, int *reset_needed)
{
	__u32 rc;

	if (unlikely((*reset_needed) != 0))
		return;
	rc = cor_receive_buf(src_unconn_ll, buf, len, 0, 0);
	if (unlikely(rc < len))
		(*reset_needed) = 1;
}


static void cor_send_resp_ok(struct cor_conn *trgt_unconn_ll, int *reset_needed)
{
	__u8 respcode = CDR_EXECOK;

	cor_receive_cpacketresp(cor_get_conn_reversedir(trgt_unconn_ll),
			(char *) &respcode, 1, reset_needed);
}

static void cor_send_resp_failed(struct cor_conn *trgt_unconn_ll,
		__u16 reasoncode, int *reset_needed)
{
	char msg[3];

	msg[0] = CDR_EXECFAILED;

	cor_put_u16(&msg[1], reasoncode);

	cor_receive_cpacketresp(cor_get_conn_reversedir(trgt_unconn_ll),
			msg, 3, reset_needed);
}

static void cor_send_resp_bin(struct cor_conn *trgt_unconn_ll, char *buf,
		__u32 len, int *reset_needed)
{
	char hdr[5];
	int len_len;

	if (len == 0) {
		hdr[0] = CDR_EXECOK_BINDATA_NORESP;
		len_len = 0;
	} else {
		hdr[0] = CDR_EXECOK_BINDATA;
		len_len = cor_encode_len(hdr + 1, 4, len);
		BUG_ON(len_len <= 0);
	}

	cor_receive_cpacketresp(cor_get_conn_reversedir(trgt_unconn_ll),
			hdr, len_len + 1, reset_needed);
	cor_receive_cpacketresp(cor_get_conn_reversedir(trgt_unconn_ll),
			buf, len, reset_needed);
}

static __u32 cor_list_l4_protocols(char *buf, __u32 buflen)
{
	if (unlikely(buflen < 2)) {
		BUG();
		return 0;
	}

	cor_put_u16(buf, L4PROTO_STREAM);
	return 2;
}

static int cor_proc_bindata(struct cor_conn *trgt_unconn_ll, __u16 cmd,
		int *reset_needed)
{
	__u32 len = 0;
	char *buf;

	buf = kmalloc(CD_RESP_BIN_MAXSIZE, GFP_ATOMIC);
	if (unlikely(buf == 0)) {
		cor_send_resp_failed(trgt_unconn_ll,
				CDR_EXECFAILED_TEMPORARILY_OUT_OF_RESOURCES,
				reset_needed);
		return 1;
	}

	if (cmd == CD_LIST_NEIGH) {
		len = cor_generate_neigh_list(buf, CD_RESP_BIN_MAXSIZE);
	} else if (cmd == CD_LIST_SERVICES) {
		len = cor_list_services(buf, CD_RESP_BIN_MAXSIZE);
	} else if (cmd == CD_LIST_L4PROTOCOLS) {
		len = cor_list_l4_protocols(buf, CD_RESP_BIN_MAXSIZE);
	} else {
		BUG();
	}
	cor_send_resp_bin(trgt_unconn_ll, buf, len, reset_needed);
	kfree(buf);
	return 0;
}

static int cor_proc_connect_port(struct cor_conn *trgt_unconn_ll,
		int *reset_needed)
{
	__u16 proto;
	__be32 port;
	int rc;

	if (unlikely(trgt_unconn_ll->trgt.unconnected.paramlen < 6)) {
		/* command too short */
		cor_send_resp_failed(trgt_unconn_ll,
				CDR_EXECFAILED_INVALID_COMMAND, reset_needed);
		return 1;
	}

	proto = cor_parse_u16(trgt_unconn_ll->trgt.unconnected.cmdparams);
	port = cor_parse_be32(trgt_unconn_ll->trgt.unconnected.cmdparams + 2);

	if (proto != L4PROTO_STREAM) {
		cor_send_resp_failed(trgt_unconn_ll,
				CDR_EXECFAILED_UNKNOWN_L4PROTOCOL,
				reset_needed);
		return 1;
	}

	rc = cor_connect_port(trgt_unconn_ll, port);

	if (rc == CONNECT_PORT_OK) {
		cor_send_resp_ok(trgt_unconn_ll, reset_needed);
		return 0;
	} else if (rc == CONNECT_PORT_PORTCLOSED) {
		cor_send_resp_failed(trgt_unconn_ll,
				CDR_EXECFAILED_PORTCLOSED, reset_needed);
	} else if (rc == CONNECT_PORT_TEMPORARILY_OUT_OF_RESOURCES) {
		cor_send_resp_failed(trgt_unconn_ll,
				CDR_EXECFAILED_TEMPORARILY_OUT_OF_RESOURCES,
				reset_needed);
	} else {
		BUG();
	}
	return 1;
}

static int cor_proc_connect_nb(struct cor_conn *trgt_unconn_ll,
		int *reset_needed)
{
	__be64 addr;

	int rc;

	if (unlikely(trgt_unconn_ll->trgt.unconnected.paramlen != 8)) {
		cor_send_resp_failed(trgt_unconn_ll,
				CDR_EXECFAILED_COMMAND_PARSE_ERROR,
				reset_needed);
		return 1;
	}

	addr = cor_parse_be64(trgt_unconn_ll->trgt.unconnected.cmdparams);

	rc = cor_connect_neigh(trgt_unconn_ll, addr);

	if (rc == 0) {
		cor_send_resp_ok(trgt_unconn_ll, reset_needed);
		return 0;
	} else if (rc == 3) {
		cor_send_resp_failed(trgt_unconn_ll,
				CDR_EXECFAILED_NB_DOESNTEXIST, reset_needed);
	} else if (rc == 4) {
		cor_send_resp_failed(trgt_unconn_ll,
				CDR_EXECFAILED_TEMPORARILY_OUT_OF_RESOURCES,
				reset_needed);
	} else {
		BUG();
	}
	return 1;
}

static void cor_proc_cmd(struct cor_conn *trgt_unconn_ll, int *reset_needed)
{
	__u16 cmd = trgt_unconn_ll->trgt.unconnected.cmd;
	char *params = trgt_unconn_ll->trgt.unconnected.cmdparams;
	int discard_on_error = (cmd & CD_CONTINUE_ON_ERROR_FLAG) == 0;
	int rc;

	cmd = cmd & (~CD_CONTINUE_ON_ERROR_FLAG);
	cmd = cmd & (~CD_NOPARAM_FLAG);

	BUG_ON(trgt_unconn_ll->trgt.unconnected.cmdparams == 0);

	if (cmd == CD_CONNECT_NB) {
		rc = cor_proc_connect_nb(trgt_unconn_ll, reset_needed);
	} else if (cmd == CD_CONNECT_PORT) {
		rc = cor_proc_connect_port(trgt_unconn_ll, reset_needed);
	} else if (cmd == CD_LIST_NEIGH || cmd == CD_LIST_SERVICES ||
			cmd == CD_LIST_L4PROTOCOLS) {
		rc = cor_proc_bindata(trgt_unconn_ll, cmd, reset_needed);
	} else {
		cor_send_resp_failed(trgt_unconn_ll,
				CDR_EXECFAILED_INVALID_COMMAND, reset_needed);
		rc = 1;
	}

	kfree(params);

	if (trgt_unconn_ll->targettype == TARGET_UNCONNECTED) {
		trgt_unconn_ll->trgt.unconnected.cmdparams = 0;
		trgt_unconn_ll->trgt.unconnected.cmdread = 0;
		trgt_unconn_ll->trgt.unconnected.paramlen_read = 0;
		trgt_unconn_ll->trgt.unconnected.cmd = 0;
		trgt_unconn_ll->trgt.unconnected.paramlen = 0;

		if (rc != 0 && discard_on_error) {
			trgt_unconn_ll->targettype = TARGET_DISCARD;
		}
	}
}

static void cor_read_cmd(struct cor_conn *trgt_unconn_ll)
{
	int pull;

	pull = min(trgt_unconn_ll->trgt.unconnected.paramlen -
			trgt_unconn_ll->trgt.unconnected.cmdread,
			trgt_unconn_ll->data_buf.read_remaining);

	BUG_ON(pull < 0);
	BUG_ON(trgt_unconn_ll->trgt.unconnected.cmdparams == 0);

	if (pull == 0)
		return;

	cor_databuf_pull(trgt_unconn_ll,
			trgt_unconn_ll->trgt.unconnected.cmdparams +
			trgt_unconn_ll->trgt.unconnected.cmdread, pull);
	cor_databuf_ackread(trgt_unconn_ll);

	trgt_unconn_ll->trgt.unconnected.cmdread += pull;
}

static void cor_read_discard(struct cor_conn *trgt_unconn_ll)
{
	BUG_ON(trgt_unconn_ll->trgt.unconnected.paramlen_read == 0);

	while (trgt_unconn_ll->trgt.unconnected.paramlen <
			trgt_unconn_ll->trgt.unconnected.cmdread &&
			trgt_unconn_ll->data_buf.read_remaining > 0) {
		char buf[1];

		cor_databuf_pull(trgt_unconn_ll, buf, 1);
		cor_databuf_ackread(trgt_unconn_ll);

		trgt_unconn_ll->trgt.unconnected.cmdread++;
	}
}

#define RC_READ_HDR_FINISHED 0
#define RC_READ_HDR_INCOMPLETE 1
static int cor_read_hdr(struct cor_conn *trgt_unconn_ll)
{
	BUG_ON(trgt_unconn_ll->trgt.unconnected.cmdparams != 0);

	if (trgt_unconn_ll->trgt.unconnected.paramlen_read != 0)
		return RC_READ_HDR_FINISHED;


	while (trgt_unconn_ll->trgt.unconnected.cmdread < 2) {
		char buf[1];

		if (trgt_unconn_ll->data_buf.read_remaining == 0)
			return RC_READ_HDR_INCOMPLETE;

		cor_databuf_pull(trgt_unconn_ll, buf, 1);
		trgt_unconn_ll->trgt.unconnected.cmd <<= 8;
		trgt_unconn_ll->trgt.unconnected.cmd += buf[0];
		trgt_unconn_ll->trgt.unconnected.cmdread += 1;
	}

	if ((trgt_unconn_ll->trgt.unconnected.cmd & CD_NOPARAM_FLAG) != 0) {
		trgt_unconn_ll->trgt.unconnected.paramlen = 0;
		trgt_unconn_ll->trgt.unconnected.paramlen_read = 1;
		trgt_unconn_ll->trgt.unconnected.cmdread = 0;
		return RC_READ_HDR_FINISHED;
	}

	while (1) {
		int rc;

		if (trgt_unconn_ll->data_buf.read_remaining == 0)
			return RC_READ_HDR_INCOMPLETE;

		cor_databuf_pull(trgt_unconn_ll,
				trgt_unconn_ll->trgt.unconnected.paramlen_buf +
				trgt_unconn_ll->trgt.unconnected.cmdread - 2,
				1);
		cor_databuf_ackread(trgt_unconn_ll);
		trgt_unconn_ll->trgt.unconnected.cmdread += 1;

		BUG_ON(trgt_unconn_ll->trgt.unconnected.cmdread >= 6);

		rc = cor_decode_len(
				trgt_unconn_ll->trgt.unconnected.paramlen_buf,
				trgt_unconn_ll->trgt.unconnected.cmdread - 2,
				&trgt_unconn_ll->trgt.unconnected.paramlen);

		if (rc > 0) {
			trgt_unconn_ll->trgt.unconnected.paramlen_read = 1;
			trgt_unconn_ll->trgt.unconnected.cmdread = 0;
			return RC_READ_HDR_FINISHED;
		}
	}
}

#define RC_PC_FINISHED 0
#define RC_PC_OUT_OF_BUFSPACE 1
#define RC_PC_INCOMPLETEREAD 2
static int _cor_proc_cpacket(struct cor_conn *trgt_unconn_ll, int *reset_needed)
{
	int rc;

	if (cor_conn_src_unconn_write_allowed(cor_get_conn_reversedir(
			trgt_unconn_ll)) == 0)
		return RC_PC_OUT_OF_BUFSPACE;

	if (trgt_unconn_ll->trgt.unconnected.paramlen_read != 0)
		goto hdrdone;

	rc = cor_read_hdr(trgt_unconn_ll);
	if (rc == RC_READ_HDR_INCOMPLETE) {
		return RC_PC_INCOMPLETEREAD;
	} else if (rc == RC_READ_HDR_FINISHED) {
		trgt_unconn_ll->trgt.unconnected.paramlen_read = 1;

		if (trgt_unconn_ll->trgt.unconnected.paramlen >
				MAX_CONN_CMD_LEN) {
			/* command too long */
			cor_send_resp_failed(trgt_unconn_ll,
					CDR_EXECFAILED_INVALID_COMMAND,
					reset_needed);
		}
	} else {
		BUG();
	}

hdrdone:
	if (trgt_unconn_ll->trgt.unconnected.paramlen > MAX_CONN_CMD_LEN) {
		cor_read_discard(trgt_unconn_ll);
		return RC_PC_INCOMPLETEREAD;
	}

	if (trgt_unconn_ll->trgt.unconnected.cmdparams == 0) {
		trgt_unconn_ll->trgt.unconnected.cmdparams = kmalloc(
				trgt_unconn_ll->trgt.unconnected.paramlen,
				GFP_ATOMIC);
		if (trgt_unconn_ll->trgt.unconnected.cmdparams == 0) {
			*reset_needed = 1;
			return RC_PC_INCOMPLETEREAD;
		}
	}

	cor_read_cmd(trgt_unconn_ll);

	BUG_ON(trgt_unconn_ll->trgt.unconnected.cmdread >
			trgt_unconn_ll->trgt.unconnected.paramlen);

	if (trgt_unconn_ll->trgt.unconnected.cmdread <
			trgt_unconn_ll->trgt.unconnected.paramlen)
		return RC_PC_INCOMPLETEREAD;

	cor_proc_cmd(trgt_unconn_ll, reset_needed);

	return RC_PC_FINISHED;
}

void cor_proc_cpacket(struct cor_conn *trgt_unconn_l)
{
	struct cor_conn *src_in = cor_get_conn_reversedir(trgt_unconn_l);
	int reset_needed = 0;

	if (unlikely(trgt_unconn_l->isreset != 0))
		return;

	BUG_ON(trgt_unconn_l->targettype != TARGET_UNCONNECTED);

	BUG_ON(trgt_unconn_l->is_client == 0);
	spin_lock_bh(&src_in->rcv_lock);

	while (1) {
		int rc = _cor_proc_cpacket(trgt_unconn_l, &reset_needed);

		BUG_ON(rc != RC_PC_FINISHED &&
				rc != RC_PC_OUT_OF_BUFSPACE &&
				rc != RC_PC_INCOMPLETEREAD);

		if (trgt_unconn_l->targettype == TARGET_UNCONNECTED) {
			if (trgt_unconn_l->flush != 0 &&
					trgt_unconn_l->data_buf.read_remaining == 0)
				src_in->flush = 1;
			else
				src_in->flush = 0;

			cor_flush_buf(cor_get_conn_reversedir(trgt_unconn_l));

			if (rc == RC_PC_OUT_OF_BUFSPACE ||
					rc == RC_PC_INCOMPLETEREAD)
				break;
		} else {
			BUG_ON(rc != RC_PC_FINISHED);

			if (trgt_unconn_l->targettype == TARGET_SOCK || (
					trgt_unconn_l->flush != 0 &&
					trgt_unconn_l->data_buf.read_remaining == 0))
				src_in->flush = 1;
			else
				src_in->flush = 0;

			cor_flush_buf(src_in);
			cor_flush_buf(trgt_unconn_l);
			break;
		}

		if (reset_needed != 0)
			cor_reset_conn_locked(cor_get_conn_bidir(
					trgt_unconn_l));
	}

	spin_unlock_bh(&src_in->rcv_lock);
}

MODULE_LICENSE("GPL");
