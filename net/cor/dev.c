/**
 *	Connection oriented routing
 *	Copyright (C) 2007-2023 Michael Blizek
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License
 *	as published by the Free Software Foundation; either version 2
 *	of the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 */

#include "cor.h"

static struct notifier_block cor_netdev_notify;
__u8 cor_netdev_notify_registered;

__u8 cor_pack_registered;

static DEFINE_SPINLOCK(cor_devs_lock);
static LIST_HEAD(cor_devs);
static LIST_HEAD(cor_devs_waitexit);

static void cor_dev_queue_waitexit(struct work_struct *work);
DECLARE_WORK(cor_dev_waitexit_work, cor_dev_queue_waitexit);


#ifdef DEBUG_QOS_SLOWSEND
static DEFINE_SPINLOCK(slowsend_lock);
static unsigned long cor_last_send;


int _cor_dev_queue_xmit(struct sk_buff *skb, int caller)
{
	int allowsend = 0;
	unsigned long jiffies_tmp;

	spin_lock_bh(&slowsend_lock);
	jiffies_tmp = jiffies;
	if (cor_last_send != jiffies_tmp) {
		if (cor_last_send + 1 == jiffies_tmp)
			cor_last_send = jiffies_tmp;
		else
			cor_last_send = jiffies_tmp - 1;
		allowsend = 1;
	}
	spin_unlock_bh(&slowsend_lock);

	/* printk(KERN_ERR "cor_dev_queue_xmit %d, %d\n", caller, allowsend); */
	if (allowsend) {
		return dev_queue_xmit(skb);
	} else {
		kfree_skb(skb);
		return NET_XMIT_DROP;
	}
}
#endif

/*__u64 get_bufspace_used(void);

static void print_conn_bufstats(struct cor_neighbor *nb)
{
	/ * not threadsafe, but this is only for debugging... * /
	__u64 totalsize = 0;
	__u64 read_remaining = 0;
	__u32 numconns = 0;
	struct list_head *lh;
	unsigned long iflags;

	spin_lock_irqsave(&nb->conns_waiting.lock, iflags);

	lh = nb->conns_waiting.lh.next;
	while (lh != &nb->conns_waiting.lh) {
		struct cor_conn *cn = container_of(lh, struct cor_conn,
				trgt.out.rb.lh);
		totalsize += cn->data_buf.datasize;
		read_remaining += cn->data_buf.read_remaining;
		lh = lh->next;
	}

	lh = nb->conns_waiting.lh_nextpass.next;
	while (lh != &nb->conns_waiting.lh_nextpass) {
		struct cor_conn *cn = container_of(lh, struct cor_conn,
				target.out.rb.lh);
		totalsize += cn->data_buf.datasize;
		read_remaining += cn->data_buf.read_remaining;
		lh = lh->next;
	}

	numconns = nb->conns_waiting.cnt;

	spin_unlock_irqrestore(&nb->conns_waiting.lock, iflags);

	printk(KERN_ERR "conn %llu %llu %u\n", totalsize, read_remaining,
			numconns);
} */

struct sk_buff *cor_create_packet(struct cor_neighbor *nb, int size,
		gfp_t alloc_flags)
{
	struct sk_buff *ret;

	ret = alloc_skb(size + LL_RESERVED_SPACE(nb->dev) +
			nb->dev->needed_tailroom, alloc_flags);
	if (unlikely(ret == 0))
		return 0;

	ret->protocol = htons(ETH_P_COR);
	ret->dev = nb->dev;

	skb_reserve(ret, LL_RESERVED_SPACE(nb->dev));
	if (unlikely(dev_hard_header(ret, nb->dev, ETH_P_COR, nb->mac,
			nb->dev->dev_addr, ret->len) < 0))
		return 0;
	skb_reset_network_header(ret);

	return ret;
}

struct sk_buff *cor_create_packet_conndata(struct cor_neighbor *nb, int size,
		gfp_t alloc_flags, __u32 conn_id, __u32 seqno,
		__u8 windowused, __u8 flush)
{
	struct sk_buff *ret;
	char *dest;

	ret = cor_create_packet(nb, size + 9, alloc_flags);
	if (unlikely(ret == 0))
		return 0;

	dest = skb_put(ret, 9);
	BUG_ON(dest == 0);

	BUG_ON((windowused & (~PACKET_TYPE_CONNDATA_FLAGS_WINDOWUSED)) != 0);

	dest[0] = PACKET_TYPE_CONNDATA |
			(flush == 0 ? 0 : PACKET_TYPE_CONNDATA_FLAGS_FLUSH) |
			windowused;
	dest += 1;

	cor_put_u32(dest, conn_id);
	dest += 4;
	cor_put_u32(dest, seqno);
	dest += 4;

	return ret;
}


static void cor_rcv_conndata(struct sk_buff *skb, __u8 windowused, __u8 flush)
{
	struct cor_neighbor *nb = cor_get_neigh_by_mac(skb);

	__u32 conn_id;
	__u32 seqno;

	char *connid_p;
	char *seqno_p;

	/* __u8 rand; */

	if (unlikely(nb == 0))
		goto drop;

	connid_p = cor_pull_skb(skb, 4);
	if (unlikely(connid_p == 0))
		goto drop;

	seqno_p = cor_pull_skb(skb, 4);
	if (unlikely(seqno_p == 0))
		goto drop;

	conn_id = cor_parse_u32(connid_p);
	seqno = cor_parse_u32(seqno_p);

	/* get_random_bytes(&rand, 1);
	if (rand < 64)
		goto drop; */

	if (unlikely(skb->len <= 0))
		goto drop;

	cor_conn_rcv(nb, skb, 0, 0, conn_id, seqno, windowused, flush);

	if (0) {
drop:
		kfree_skb(skb);
	}

	if (nb != 0) {
		cor_nb_kref_put(nb, "stack");
	}
}

static void cor_rcv_cmsg(struct sk_buff *skb, int ackneeded)
{
	struct cor_neighbor *nb = cor_get_neigh_by_mac(skb);

	if (unlikely(nb == 0)) {
		kfree_skb(skb);
	} else {
		cor_kernel_packet(nb, skb, ackneeded);
		cor_nb_kref_put(nb, "stack");
	}
}

static int cor_rcv(struct sk_buff *skb, struct net_device *dev,
		struct packet_type *pt, struct net_device *orig_dev)
{
	__u8 packet_type;
	char *packet_type_p;

	if (skb->pkt_type == PACKET_OTHERHOST ||
			unlikely(skb->pkt_type == PACKET_LOOPBACK))
		goto drop;

	packet_type_p = cor_pull_skb(skb, 1);

	if (unlikely(packet_type_p == 0))
		goto drop;

	packet_type = *packet_type_p;

	if (unlikely(packet_type == PACKET_TYPE_ANNOUNCE)) {
		cor_rcv_announce(skb);
		return NET_RX_SUCCESS;
	} else if (packet_type == PACKET_TYPE_CMSG_NOACK) {
		cor_rcv_cmsg(skb, ACK_NEEDED_NO);
		return NET_RX_SUCCESS;
	} else if (packet_type == PACKET_TYPE_CMSG_ACKSLOW) {
		cor_rcv_cmsg(skb, ACK_NEEDED_SLOW);
		return NET_RX_SUCCESS;
	} else if (packet_type == PACKET_TYPE_CMSG_ACKFAST) {
		cor_rcv_cmsg(skb, ACK_NEEDED_FAST);
		return NET_RX_SUCCESS;
	} else if (likely((packet_type & (~PACKET_TYPE_CONNDATA_FLAGS)) ==
			PACKET_TYPE_CONNDATA)) {
		__u8 flush = 0;
		__u8 windowused;

		if ((packet_type & PACKET_TYPE_CONNDATA_FLAGS_FLUSH) != 0)
			flush = 1;
		windowused = (packet_type &
				PACKET_TYPE_CONNDATA_FLAGS_WINDOWUSED);
		cor_rcv_conndata(skb, windowused, flush);
		return NET_RX_SUCCESS;
	} else {
		kfree_skb(skb);
		return NET_RX_SUCCESS;
	}

drop:
	kfree_skb(skb);
	return NET_RX_DROP;
}

void cor_dev_free(struct kref *ref)
{
	struct cor_dev *cd = container_of(ref, struct cor_dev, ref);

	BUG_ON(cd->dev == 0);
	dev_put(cd->dev);
	cd->dev = 0;

	kfree(cd);
}

static struct cor_dev *_cor_dev_get(struct net_device *dev)
{
	struct list_head *curr = cor_devs.next;

	while (curr != (&cor_devs)) {
		struct cor_dev *cd = container_of(curr, struct cor_dev,
				dev_list);
		BUG_ON(cd->dev == 0);
		if (cd->dev == dev) {
			kref_get(&cd->ref);
			return cd;
		}
		curr = curr->next;
	}
	return 0;
}

struct cor_dev *cor_dev_get(struct net_device *dev)
{
	struct cor_dev *ret = 0;

	spin_lock_bh(&cor_devs_lock);
	ret = _cor_dev_get(dev);
	spin_unlock_bh(&cor_devs_lock);

	return ret;
}

static void cor_dev_queue_waitexit(struct work_struct *work)
{
	spin_lock_bh(&cor_devs_lock);
	while (!list_empty(&cor_devs_waitexit)) {
		struct cor_dev *cd = container_of(cor_devs_waitexit.next,
				struct cor_dev, dev_list);
		list_del(&cd->dev_list);

		spin_unlock_bh(&cor_devs_lock);

		kthread_stop(cd->send_queue.qos_resume_thread);
		put_task_struct(cd->send_queue.qos_resume_thread);
		kref_put(&cd->ref, cor_dev_free);

		spin_lock_bh(&cor_devs_lock);
	}
	spin_unlock_bh(&cor_devs_lock);
}

void cor_dev_destroy(struct net_device *dev)
{
	int rc = 1;

	while (1) {
		struct cor_dev *cd;

		spin_lock_bh(&cor_devs_lock);

		if (dev == 0) {
			if (list_empty(&cor_devs)) {
				cd = 0;
			} else {
				cd = container_of(cor_devs.next, struct cor_dev,
						dev_list);
			}
		} else {
			cd = _cor_dev_get(dev);
		}

		if (cd == 0) {
			spin_unlock_bh(&cor_devs_lock);
			break;
		}

		list_del(&cd->dev_list);
		kref_put(&cd->ref, cor_kreffree_bug);

		spin_unlock_bh(&cor_devs_lock);

		rc = 0;

		cor_dev_queue_destroy(cd);

		spin_lock_bh(&cor_devs_lock);
		list_add(&cd->dev_list, &cor_devs_waitexit);
		kref_get(&cd->ref);
		spin_unlock_bh(&cor_devs_lock);

		schedule_work(&cor_dev_waitexit_work);

		kref_put(&cd->ref, cor_dev_free);
	}
}

static int cor_dev_create(struct net_device *dev)
{
	struct cor_dev *cd = kmalloc(sizeof(struct cor_dev), GFP_KERNEL);

	BUG_ON(dev == 0);

	if (cd == 0) {
		printk(KERN_ERR "cor: unable to allocate memory for cor_dev, not enabling device\n");
		return 1;
	}

	memset(cd, 0, sizeof(struct cor_dev));

	kref_init(&cd->ref);

	cd->dev = dev;
	dev_hold(dev);

	if (cor_dev_queue_init(cd) != 0) {
		dev_put(cd->dev);
		cd->dev = 0;

		kfree(cd);

		return 1;
	}

	spin_lock_bh(&cor_devs_lock);
	list_add(&cd->dev_list, &cor_devs);
	spin_unlock_bh(&cor_devs_lock);

	return 0;
}

int cor_netdev_notify_func(struct notifier_block *not, unsigned long event,
		void *ptr)
{
	struct net_device *dev = netdev_notifier_info_to_dev(ptr);
	int rc;

	BUG_ON(dev == 0);

	switch (event) {
	case NETDEV_UP:
		if (dev->flags & IFF_LOOPBACK)
			break;

		rc = cor_dev_create(dev);
		if (rc == 1)
			return 1;
		if (cor_is_clientmode() == 0)
			cor_announce_send_start(dev, dev->broadcast,
					ANNOUNCE_TYPE_BROADCAST);
		break;
	case NETDEV_DOWN:
		printk(KERN_ERR "down 1\n");
		udelay(100);
		printk(KERN_ERR "down 2\n");
		udelay(100);
		cor_announce_send_stop(dev, 0, ANNOUNCE_TYPE_BROADCAST);
		printk(KERN_ERR "down 3\n");
		udelay(100);
		cor_reset_neighbors(dev);
		printk(KERN_ERR "down 4\n");
		udelay(100);
		cor_dev_destroy(dev);
		printk(KERN_ERR "down 5\n");
		udelay(100);
		break;
	case NETDEV_CHANGEMTU:
		cor_resend_rcvmtu(dev);
		break;
	case NETDEV_REBOOT:
	case NETDEV_CHANGE:
	case NETDEV_REGISTER:
	case NETDEV_UNREGISTER:
	case NETDEV_CHANGEADDR:
	case NETDEV_GOING_DOWN:
	case NETDEV_CHANGENAME:
	case NETDEV_FEAT_CHANGE:
	case NETDEV_BONDING_FAILOVER:
		break;
	default:
		return 1;
	}

	return 0;
}

static struct packet_type cor_ptype = {
	.type = htons(ETH_P_COR),
	.dev = 0,
	.func = cor_rcv
};

void cor_dev_down(void)
{
	if (cor_pack_registered != 0) {
		cor_pack_registered = 0;
		dev_remove_pack(&cor_ptype);
	}

	if (cor_netdev_notify_registered != 0) {
		if (unregister_netdevice_notifier(&cor_netdev_notify) != 0) {
			printk(KERN_WARNING "warning: cor_dev_down: unregister_netdevice_notifier failed\n");
			BUG();
		}
		cor_netdev_notify_registered = 0;
	}
}

int cor_dev_up(void)
{
	BUG_ON(cor_netdev_notify_registered != 0);
	if (register_netdevice_notifier(&cor_netdev_notify) != 0)
		return 1;
	cor_netdev_notify_registered = 1;

	BUG_ON(cor_pack_registered != 0);
	dev_add_pack(&cor_ptype);
	cor_pack_registered = 1;

	return 0;
}

int __init cor_dev_init(void)
{
	memset(&cor_netdev_notify, 0, sizeof(cor_netdev_notify));
	cor_netdev_notify.notifier_call = cor_netdev_notify_func;

	return 0;
}

void __exit cor_dev_exit1(void)
{
	flush_work(&cor_dev_waitexit_work);
}

MODULE_LICENSE("GPL");
