/**
 *	Connection oriented routing
 *	Copyright (C) 2007-2023 Michael Blizek
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License
 *	as published by the Free Software Foundation; either version 2
 *	of the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 */

#include "cor.h"


static void _cor_neigh_waitingsconns_resume_accountbusytime(
		struct cor_conn *trgt_out_l, __u32 priority, __u32 burstprio,
		unsigned long jiffies_nb_lastduration)
{
	unsigned long jiffies_tmp = jiffies;
	__u64 jiffies_nb_lastduration_shifted = (jiffies_nb_lastduration <<
			JIFFIES_LAST_IDLE_SHIFT);
	__u64 burstfactor;

	if (trgt_out_l->is_highlatency != 0) {
		burstfactor = 2048;
	} else {
		BUG_ON(burstprio < priority);
		burstfactor = div_u64(1024LL * (__u64) burstprio, priority) * 2 -
				1024;
		BUG_ON(burstfactor < 1024);
	}

	trgt_out_l->trgt.out.jiffies_idle_since +=
			(jiffies_nb_lastduration_shifted * burstfactor) / 1024;

	if (time_before(jiffies_tmp << JIFFIES_LAST_IDLE_SHIFT,
			trgt_out_l->trgt.out.jiffies_idle_since))
		trgt_out_l->trgt.out.jiffies_idle_since =
				jiffies_tmp << JIFFIES_LAST_IDLE_SHIFT;
}

static int _cor_neigh_waitingsconns_resume_nbnotactive(struct cor_neighbor *nb)
{
	struct cor_dev *cd = nb->cd;
	int krefput_cd = 0;

	spin_lock(&cd->send_queue.qlock);

	if (unlikely(cor_get_neigh_state(nb) == NEIGHBOR_STATE_ACTIVE)) {
		spin_unlock(&cd->send_queue.qlock);
		return 1;
	}

	if (likely(nb->rb.in_queue == RB_INQUEUE_TRUE)) {
		list_del(&nb->rb.lh);
		cor_nb_kref_put_bug(nb, "qos_queue_nb");
		nb->rb.in_queue = RB_INQUEUE_NBNOTACTIVE;
		BUG_ON(cd->send_queue.numconns < nb->conns_waiting.cnt);
		cd->send_queue.numconns -= nb->conns_waiting.cnt;
		BUG_ON(nb->conns_waiting.priority_sum >
				cd->send_queue.priority_sum);
		cd->send_queue.priority_sum -= nb->conns_waiting.priority_sum;

		if (list_empty(&cd->send_queue.neighbors_waiting) &&
				list_empty(&cd->send_queue.neighbors_waiting_nextpass)) {
			BUG_ON(cd->send_queue.numconns != 0);
			BUG_ON(cd->send_queue.priority_sum != 0);
		}

		krefput_cd = 1;

		cor_dev_queue_set_congstatus(cd);
	}

	spin_unlock(&cd->send_queue.qlock);

	if (krefput_cd != 0)
		kref_put(&cd->ref, cor_dev_free);

	return 0;
}

static __u32 _cor_neigh_waitingsconns_resume_burstprio(
		struct cor_conn *trgt_out_l, __u32 priority)
{
	unsigned long idletime_hz_shifted = cor_get_conn_idletime(trgt_out_l);
	__u32 idletime_msecs = jiffies_to_msecs(idletime_hz_shifted >>
			JIFFIES_LAST_IDLE_SHIFT);
	__u64 newprio;

	if (trgt_out_l->is_highlatency != 0) {
		/**
		 * negative burst for high latency conns:
		 * 50% if idle
		 * 125% if busy
		 */

		__u32 burstfactor;

		BUG_ON(idletime_msecs >
				BURSTPRIO_MAXIDLETIME_HIGHLATENCY_SECS * 1000);
		BUG_ON(BURSTPRIO_MAXIDLETIME_HIGHLATENCY_SECS * 1000LL >
				U32_MAX / 1024);

		burstfactor = 768 - (768 * idletime_msecs) /
				(BURSTPRIO_MAXIDLETIME_HIGHLATENCY_SECS * 1000);

		newprio =  (((__u64) priority) * (512 + burstfactor)) /
					1024;
	} else {
		__u32 burstfactor;

		BUG_ON(idletime_msecs >
				BURSTPRIO_MAXIDLETIME_LOWLATENCY_SECS * 1000);
		BUG_ON(BURSTPRIO_MAXIDLETIME_LOWLATENCY_SECS * 1000LL >
				U32_MAX / 1024);

		burstfactor = (1024 * idletime_msecs) /
				(BURSTPRIO_MAXIDLETIME_LOWLATENCY_SECS * 1000);

		newprio =  (((__u64) priority) * (1024 + burstfactor * 2)) /
					1024;
	}

	BUG_ON(newprio > U32_MAX);
	return (__u32) newprio;
}

static __u32 _cor_neigh_waitingsconns_resume_maxsend(
		struct cor_conn *trgt_out_l, __u32 newpriority,
		int *maxsend_forcedelay)
{
	unsigned long iflags;

	struct cor_neighbor *nb = trgt_out_l->trgt.out.nb;
	struct cor_dev *cd = nb->cd;
	__u64 priority_sum;
	__u32 numconns;
	__u64 bytes_per_round;
	__u64 ret;

	spin_lock_irqsave(&nb->conns_waiting.lock, iflags);
	spin_lock(&cd->send_queue.qlock);

	if (unlikely(unlikely(trgt_out_l->trgt.out.rb.in_queue !=
			RB_INQUEUE_TRUE) ||
			unlikely(nb->rb.in_queue != RB_INQUEUE_TRUE))) {
		spin_unlock(&cd->send_queue.qlock);
		spin_unlock_irqrestore(&nb->conns_waiting.lock, iflags);

		return 1024;
	}

	BUG_ON(nb->conns_waiting.priority_sum <
			trgt_out_l->trgt.out.rb_priority);
	BUG_ON(cd->send_queue.priority_sum < trgt_out_l->trgt.out.rb_priority);
	nb->conns_waiting.priority_sum -= trgt_out_l->trgt.out.rb_priority;
	cd->send_queue.priority_sum -= trgt_out_l->trgt.out.rb_priority;

	#warning todo remove priority_sum
	BUG_ON(nb->conns_waiting.priority_sum + newpriority <
			nb->conns_waiting.priority_sum);
	BUG_ON(cd->send_queue.priority_sum + newpriority < cd->send_queue.priority_sum);
	nb->conns_waiting.priority_sum += newpriority;
	cd->send_queue.priority_sum += newpriority;
	trgt_out_l->trgt.out.rb_priority = newpriority;

	priority_sum = cd->send_queue.priority_sum;
	numconns = cd->send_queue.numconns;

	spin_unlock(&cd->send_queue.qlock);
	spin_unlock_irqrestore(&nb->conns_waiting.lock, iflags);

	if (numconns <= 4) {
		*maxsend_forcedelay = 1;
		bytes_per_round = 2048;
	} else {
		*maxsend_forcedelay = 0;
		bytes_per_round = 1024;
	}

	ret = div_u64(bytes_per_round * ((__u64) newpriority) *
			((__u64) numconns), priority_sum);
	if (unlikely(ret > U32_MAX))
		return U32_MAX;
	return (__u32) ret;
}

static int _cor_neigh_waitingsconns_resume_nextpass(
		struct cor_neighbor *nb_waitingconnslocked)
{
	BUG_ON(list_empty(&nb_waitingconnslocked->conns_waiting.lh) == 0);

	if (list_empty(&nb_waitingconnslocked->conns_waiting.lh_nextpass)) {
		BUG_ON(nb_waitingconnslocked->conns_waiting.cnt != 0);
		return 1;
	}

	BUG_ON(nb_waitingconnslocked->conns_waiting.cnt == 0);

	cor_swap_list_items(&nb_waitingconnslocked->conns_waiting.lh,
			&nb_waitingconnslocked->conns_waiting.lh_nextpass);

	return 0;
}

int cor_neigh_waitingsconns_resume(struct cor_dev *cd, struct cor_neighbor *nb,
		unsigned long jiffies_nb_lastduration, int *progress)
{
	unsigned long iflags;

	while (1) {
		__u32 priority;
		__u32 burstprio;
		__u32 maxsend;
		int maxsend_forcedelay = 0;

		int rc2;
		__u32 sent2 = 0;

		struct cor_conn *cn = 0;

		spin_lock_irqsave(&nb->conns_waiting.lock, iflags);
		if (list_empty(&nb->conns_waiting.lh) != 0) {
			int done = _cor_neigh_waitingsconns_resume_nextpass(nb);

			spin_unlock_irqrestore(&nb->conns_waiting.lock, iflags);
			return done ? QOS_RESUME_DONE : QOS_RESUME_NEXTNEIGHBOR;
		}
		BUG_ON(nb->conns_waiting.cnt == 0);

		cn = container_of(nb->conns_waiting.lh.next, struct cor_conn,
				trgt.out.rb.lh);
		BUG_ON(cn->targettype != TARGET_OUT);
		BUG_ON(cn->trgt.out.rb.lh.prev != &nb->conns_waiting.lh);
		BUG_ON((cn->trgt.out.rb.lh.next == &nb->conns_waiting.lh) &&
				(nb->conns_waiting.lh.prev !=
				&cn->trgt.out.rb.lh));
		list_del(&cn->trgt.out.rb.lh);
		list_add_tail(&cn->trgt.out.rb.lh,
				&nb->conns_waiting.lh_nextpass);
		cor_conn_kref_get(cn, "stack");
		spin_unlock_irqrestore(&nb->conns_waiting.lock, iflags);


		priority = cor_conn_refresh_priority(cn, 0);

		spin_lock_bh(&cn->rcv_lock);

		if (unlikely(cn->targettype != TARGET_OUT)) {
			spin_unlock_bh(&cn->rcv_lock);
			continue;
		}

		burstprio = _cor_neigh_waitingsconns_resume_burstprio(cn,
				priority);

		maxsend = _cor_neigh_waitingsconns_resume_maxsend(cn, burstprio,
				&maxsend_forcedelay);
		if (cn->trgt.out.maxsend_extra >= maxsend)
			maxsend_forcedelay = 0;
		maxsend += cn->trgt.out.maxsend_extra;
		if (unlikely(maxsend > U32_MAX))
			maxsend = U32_MAX;
		if (unlikely(maxsend >= 65536))
			maxsend_forcedelay = 0;

retry:
		rc2 = _cor_flush_out(cn, maxsend, &sent2, 1,
				maxsend_forcedelay);

		if (rc2 == RC_FLUSH_CONN_OUT_OK) {
			cn->trgt.out.maxsend_extra = 0;
			cor_neigh_waitingconns_remove_conn(cn);
		} else if (unlikely(rc2 == RC_FLUSH_CONN_OUT_NBNOTACTIVE)) {
			if (_cor_neigh_waitingsconns_resume_nbnotactive(nb) != 0)
				goto retry;
		} else if (sent2 == 0 && (rc2 == RC_FLUSH_CONN_OUT_CONG ||
				unlikely(rc2 == RC_FLUSH_CONN_OUT_OOM))) {
			spin_lock_irqsave(&nb->conns_waiting.lock, iflags);
			if (likely(cn->trgt.out.rb.in_queue !=
					RB_INQUEUE_FALSE)) {
				list_del(&cn->trgt.out.rb.lh);
				list_add(&cn->trgt.out.rb.lh,
						&nb->conns_waiting.lh);
			}
			spin_unlock_irqrestore(&nb->conns_waiting.lock, iflags);
		} else if (rc2 == RC_FLUSH_CONN_OUT_CONG  ||
				unlikely(rc2 == RC_FLUSH_CONN_OUT_OOM)) {
			cn->trgt.out.maxsend_extra = 0;
		} else if (likely(rc2 == RC_FLUSH_CONN_OUT_MAXSENT)) {
			if (unlikely(maxsend - sent2 > 65535))
				cn->trgt.out.maxsend_extra = 65535;
			else
				cn->trgt.out.maxsend_extra = maxsend - sent2;
		}

		if (sent2 != 0)
			_cor_neigh_waitingsconns_resume_accountbusytime(cn,
					priority, burstprio,
					jiffies_nb_lastduration);

		spin_unlock_bh(&cn->rcv_lock);

		if (sent2 != 0) {
			*progress = 1;
			cor_wake_sender(cn);
		}

		cor_conn_kref_put(cn, "stack");

		if (rc2 == RC_FLUSH_CONN_OUT_CONG  ||
				unlikely(rc2 == RC_FLUSH_CONN_OUT_OOM)) {
			return QOS_RESUME_CONG;
		} else if (unlikely(rc2 == RC_FLUSH_CONN_OUT_NBNOTACTIVE)) {
			return QOS_RESUME_NEXTNEIGHBOR;
		}
	}
}

void cor_neigh_waitingconns_remove_conn(struct cor_conn *trgt_out_lx)
{
	unsigned long iflags;
	struct cor_neighbor *nb = trgt_out_lx->trgt.out.nb;
	struct cor_dev *cd = nb->cd;
	int sched_cmsg = 0;
	int krefput_nb = 0;

	BUG_ON(trgt_out_lx->targettype != TARGET_OUT);
	BUG_ON(cd == 0);

	spin_lock_irqsave(&nb->conns_waiting.lock, iflags);
	if (trgt_out_lx->trgt.out.rb.in_queue == RB_INQUEUE_FALSE) {
		spin_unlock_irqrestore(&nb->conns_waiting.lock, iflags);
		return;
	}
	spin_lock(&cd->send_queue.qlock);

	trgt_out_lx->trgt.out.rb.in_queue = RB_INQUEUE_FALSE;
	list_del(&trgt_out_lx->trgt.out.rb.lh);
	BUG_ON(nb->conns_waiting.cnt == 0);
	nb->conns_waiting.cnt--;
	if (nb->rb.in_queue == RB_INQUEUE_TRUE) {
		BUG_ON(cd->send_queue.numconns == 0);
		cd->send_queue.numconns--;
	}

	BUG_ON(nb->conns_waiting.priority_sum <
			trgt_out_lx->trgt.out.rb_priority);
	BUG_ON(cd->send_queue.priority_sum < trgt_out_lx->trgt.out.rb_priority);
	nb->conns_waiting.priority_sum -=
			trgt_out_lx->trgt.out.rb_priority;
	cd->send_queue.priority_sum -= trgt_out_lx->trgt.out.rb_priority;
	trgt_out_lx->trgt.out.rb_priority = 0;

	if (list_empty(&nb->conns_waiting.lh) &&
			list_empty(&nb->conns_waiting.lh_nextpass)) {
		BUG_ON(nb->conns_waiting.priority_sum != 0);
		BUG_ON(nb->conns_waiting.cnt != 0);
	} else {
		BUG_ON(nb->conns_waiting.cnt == 0);
	}

	if (list_empty(&nb->conns_waiting.lh) &&
			list_empty(&nb->conns_waiting.lh_nextpass) &&
			nb->rb.in_queue == RB_INQUEUE_TRUE) {
		BUG_ON(nb->conns_waiting.priority_sum != 0);

		nb->rb.in_queue = RB_INQUEUE_FALSE;
		list_del(&nb->rb.lh);
		if (atomic_read(&nb->cmsg_delay_conndata) != 0) {
			atomic_set(&nb->cmsg_delay_conndata, 0);
			sched_cmsg = 1;
		}
		krefput_nb = 1;

		if (list_empty(&cd->send_queue.neighbors_waiting) &&
				list_empty(&cd->send_queue.neighbors_waiting_nextpass)) {
			BUG_ON(cd->send_queue.numconns != 0);
			BUG_ON(cd->send_queue.priority_sum != 0);
		}

		cor_dev_queue_set_congstatus(cd);
	}

	spin_unlock(&cd->send_queue.qlock);
	spin_unlock_irqrestore(&nb->conns_waiting.lock, iflags);

	if (sched_cmsg) {
		spin_lock_bh(&nb->cmsg_lock);
		cor_schedule_controlmsg_timer(nb);
		spin_unlock_bh(&nb->cmsg_lock);
	}

	cor_conn_kref_put_bug(trgt_out_lx, "qos_queue");

	if (krefput_nb)
		cor_nb_kref_put(nb, "qos_queue_nb");
}

void cor_neigh_waitingconns_enqueue_conn(struct cor_conn *trgt_out_lx)
{
	unsigned long iflags;
	struct cor_neighbor *nb = trgt_out_lx->trgt.out.nb;
	struct cor_dev *cd;

	BUG_ON(trgt_out_lx->data_buf.read_remaining == 0);

	spin_lock_irqsave(&nb->conns_waiting.lock, iflags);

	if (trgt_out_lx->trgt.out.rb.in_queue != RB_INQUEUE_FALSE)
		goto out;

	BUG_ON(trgt_out_lx->trgt.out.rb_priority != 0);

	trgt_out_lx->trgt.out.rb.in_queue = RB_INQUEUE_TRUE;
	list_add_tail(&trgt_out_lx->trgt.out.rb.lh,
			&nb->conns_waiting.lh);
	cor_conn_kref_get(trgt_out_lx, "qos_queue");
	nb->conns_waiting.cnt++;

	cd = trgt_out_lx->trgt.out.nb->cd;
	spin_lock(&cd->send_queue.qlock);
	if (nb->rb.in_queue == RB_INQUEUE_TRUE) {
		cd->send_queue.numconns++;
	} else {
		_cor_dev_queue_enqueue(cd, &nb->rb, 0, ns_to_ktime(0),
				QOS_CALLER_NEIGHBOR, 0, 0);
	}
	spin_unlock(&cd->send_queue.qlock);

out:
	spin_unlock_irqrestore(&nb->conns_waiting.lock, iflags);
}
